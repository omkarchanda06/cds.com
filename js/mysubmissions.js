	
	var allOptions 		= [
							'Submitted to Vendor',
							'Submitted to Client',
							'Rejected (Reason: Comm Skills)',
							'Rejected (Reason: Position on Hold/Closed/Filled)',
							'Rejected (Reason: Technical Skills)',
							'Rejected (Reason: Rate Concern)',
							'Rejected (Reason: Location)',
							'Interview',
							'Rejected After Interview',
							'Placement',
							'Backout'
							];
	var aftersubmittoclient	= [
							'Submitted to Client',
							'Interview'
							];

	var afterVendor 	= [
							'Submitted to Vendor',
							'Submitted to Client',
							'Rejected (Reason: Comm Skills)',
							'Rejected (Reason: Position on Hold/Closed/Filled)',
							'Rejected (Reason: Technical Skills)',
							'Rejected (Reason: Rate Concern)',
							'Rejected (Reason: Location)'
							];

	var afterClient 	= [
							'Submitted to Client',
							'Interview',
							'Rejected'
							];

	var afterInterview	= [
							'Interview',
							'Rejected',
							'Rejected After Interview',
							'Placement',
							'Backout'
							];

	var afterPlacement  = [
							'Placement',
							'Backout'
							];


	function getOptionsHtml(statusText){
		var optionsArray 	= [];
		var html 			= '<select name="status-options" id="status-options">';
		if(statusText == 'Submitted to Vendor') {
			optionsArray = afterVendor;
		}

		else if(statusText == 'Submitted to Client') {
			optionsArray = aftersubmittoclient;
		}

		else if(statusText == 'Interview') {
			optionsArray = afterInterview;
		}

		else if(statusText == 'Placement') {
			optionsArray = afterPlacement;
		}

		else {
			optionsArray.push(statusText);
		}


		$.each(optionsArray, function(index, item) {
			html 	+= '<option value="'+item+'">'+item+'</option>';
		});

		html 		+= '</select>';
		html 		+= '<span class="submit-option">SUBMIT</span>';
		html 		+= '<span class="close-options">CLOSE</span>';

		return html;
	}																											


	$(document).on('click', '.edit-status', function(){
		// alert("working");
		var id 			= $(this).data('id');
		var statusText 	= $(this).attr('title');

		$('.status-option-section').html('');

		var html 		= $.trim($('#status-option-section-'+id).html());
		//var optionsHtml = $('#status-options').html();
		var optionsHtml = getOptionsHtml(statusText);

		if(html == '') {
			$('#status-option-section-'+id).html(optionsHtml);
			$('#status-option-section-'+id+' option[value="'+statusText+'"]').prop("selected", true);
			$('#status-option-section-'+id).find('#status-options').focus();
		}

		$('.edit-status').show();
		$(this).hide();
	});

	$(document).on('click', '.close-options', function(){
		$('.status-option-section').html('');		
		$('.edit-status').show();
	});

	$(document).on('click', '.submit-option', function(){

		var statusID 	= $(this).closest('td').find('.edit-status').data('id');
		var statusText 	= $(this).closest('td').find('#status-options').find(':selected').val();
		
		$.ajax({
            type 	:"POST",
            url 	: base_url+'/totalsubmissions/update_submission_status',
            data	: {id : statusID, status : statusText}, 
            success: function(data){
            	console.info(data);
            	if(data['status'] == 'success' && data['interview_modal'] === true) {
        			$('.status-option-section').html('');		
					$('.edit-status').show();
					$('.interview_modal').attr("id", "submission-id-"+statusID);
					$('.submit-interview-btn').attr("id", statusID);
					$('.interview_modal').click();
				} else if(data['status'] == 'success'){
					$('.status-option-section').html('');		
					$('.edit-status').show();
				} else {
					alert('Something went worng');
				}
            }
      	});
		$(this).closest('td').find('.edit-status').attr('title', statusText).text(statusText.substring(0, 6));
	});


	/**Interview modal popup */

	(function($){

		// Defining our jQuery plugin
	
		$.fn.interview_modal_box = function(prop){
	
			// Default parameters
	
			var options = $.extend({
				height : "auto",
				width : "360",
				title:"JQuery Modal Box",
				description: "Example of how to create a modal box.",
				top: "5%",
				left: "36%",
			},prop);
					
			return this.click(function(e){
				add_block_page();
				add_popup_box();
				add_styles();
				
				$('.interview_modal_box').fadeIn();
			});
			
			 function add_styles(){			
				$('.interview_modal_box').css({ 
					'position':'absolute', 
					'left':options.left,
					// 'top':options.top,
					'top':'50px',
					'display':'none',
					'height': options.height + 'px',
					'width': options.width + 'px',
					'border':'1px solid #fff',
					'box-shadow': '0px 2px 7px #292929',
					'-moz-box-shadow': '0px 2px 7px #292929',
					'-webkit-box-shadow': '0px 2px 7px #292929',
					'border-radius':'10px',
					'-moz-border-radius':'10px',
					'-webkit-border-radius':'10px',
					'background': '#f2f2f2', 
					'z-index':'50',
				});
				$('.interview_modal_close').css({
					'position':'relative',
					'top':'-3px',
					'left':'27px',
					'float':'right',
					'display':'block',
					'height':'50px',
					'width':'50px',
					//'background': 'url(images/close.png) no-repeat',
				});
							/*Block page overlay*/
				var pageHeight = $(document).height();
				var pageWidth = $(window).width();
	
				$('.interview_block_page').css({
					'position':'absolute',
					'top':'0',
					'left':'0',
					'background-color':'rgba(0,0,0,0.6)',
					'height':pageHeight,
					'width':pageWidth,
					'z-index':'10'
				});
				$('.interview_inner_modal_box').css({
					'background-color':'#fff',
					'height':(options.height - 50) + 'px',
					'width':(options.width - 50) + 'px',
					'padding':'10px',
					'margin':'15px',
					'border-radius':'10px',
					'-moz-border-radius':'10px',
					'-webkit-border-radius':'10px'
				});
			}
			
			 function add_block_page(){
				var block_page = $('<div class="interview_block_page"></div>');
							
				$(block_page).appendTo('body');
			}
					 
			 function add_popup_box(){							  
				 $('.interview_modal_close').click(function(){
					// $(this).parent().fadeOut().remove();
					// $('.paulund_block_page').fadeOut().remove();				 
					$(this).parent().fadeOut();
					$('.interview_block_page').fadeOut();				 
				 });
			}
	
			return this;
		};
		
	})(jQuery);

	$(document).ready(function(){
		$('.interview_modal').interview_modal_box();
		$('.interview_modal_box').hide();

	});

	$('.submit-interview-btn').click(function(){
		var submission_id = $(this).attr('id');
		var interview_date = new Date($('#interview-date').val());
      	var day = interview_date.getDate();
      	var month = interview_date.getMonth() + 1;
      	var year = interview_date.getFullYear();
		interview_date = [year, month, day].join('-');
		var interview_time = $("#interview-time").val();
		var interview_mode = $("#mode-of-interview").val();
		var time_zone = $("#timezone-of-interview").val();

		//console.log("submission id: "+submission_id+" date: "+interview_date+" time: "+interview_time+" mode: "+interview_mode+"time zone"+time_zone);

		var dataString = 'submission_id='+ submission_id
				   + '&interview_date='+ interview_date
				   + '&interview_time='+ interview_time
                   + '&interview_mode='+ interview_mode
                   + '&time_zone='+ time_zone;

		$.ajax({
            type:"POST",
            url: window.location.origin+'/cds.com/index.php/totalsubmissions/get_interview_details',
            data: dataString,
            // dataType: "text",
            success: function(data) {
				var message = data['message'];
				if(data['status'] == 'success'){
					$( "#interview-error-message" ).text(message);
					setTimeout(function() {
						location.reload();
					}, 3000);
				}else{
					$( "#interview-error-message" ).text(message);
				}
			}
		});
	});
// Comments section

$('.comment-section')
    .mouseenter(function() {
        var id 	= $(this).data('id');
		$('.comment-view-section').hide();
  		
		var existingHtml = $('#comment-view-section-'+id).html();
		var html 		 = $('.test-section').html();
		
		if(existingHtml.length > 10) {
			$('#comment-view-section-'+id).show();
		} else {
			$('#comment-view-section-'+id).show();	
		$.ajax({
	            type 	:"POST",
	            url 	: base_url+'/totalsubmissions/getsubmissioncomments',
	            data	: {id : id}, 
	            success : function(data){
	            	console.info(data);
	            	if(data['status'] == 'success') {
	        			$('#comment-view-section-'+id).html(data['html']);
	            	return true;
	            	} else {
	            		alert('Something went worng');
	            	}
	            }
      		});
		}
    }).mouseleave(function(){ 
    	$('.comment-view-section').hide(); 
    });



$(document).on('keyup', '.status-option-section',function(e){
     if (e.keyCode == 27) { 
   		$('.status-option-section').html('');		
		$('.edit-status').show();     
    }
});
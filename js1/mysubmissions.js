	
	var allOptions 		= [
							'Submitted to Vendor',
							'Submitted to Client',
							'Rejected (Reason: Comm Skills)',
							'Rejected (Reason: Position on Hold/Closed/Filled)',
							'Rejected (Reason: Technical Skills)',
							'Rejected (Reason: Rate Concern)',
							'Rejected (Reason: Location)',
							'Interview',
							'Rejected After Interview',
							'Placement',
							'Backout'
							];

	var afterVendor 	= [
							'Submitted to Vendor',
							'Submitted to Client',
							'Rejected (Reason: Comm Skills)',
							'Rejected (Reason: Position on Hold/Closed/Filled)',
							'Rejected (Reason: Technical Skills)',
							'Rejected (Reason: Rate Concern)',
							'Rejected (Reason: Location)'
							];

	var afterClient 	= [
							'Submitted to Client',
							'Interview',
							'Rejected'
							];

	var afterInterview	= [
							'Interview',
							'Rejected',
							'Rejected After Interview'
							];

	var afterPlacement  = [
							'Placement',
							'Backout'
							];


	function getOptionsHtml(statusText){

		var optionsArray 	= [];
		var html 			= '<select name="status-options" id="status-options">';
		if(statusText == 'Submitted to Vendor') {
			optionsArray = afterVendor;
		}

		// else if(statusText == 'Submitted to Client') {
		// 	optionsArray = afterClient;
		// }

		else if(statusText == 'Interview') {
			optionsArray = afterInterview;
		}

		else if(statusText == 'Placement') {
			optionsArray = afterPlacement;
		}

		else {
			optionsArray.push(statusText);
		}


		$.each(optionsArray, function(index, item) {
			html 	+= '<option value="'+item+'">'+item+'</option>';
		});

		html 		+= '</select>';
		html 		+= '<span class="submit-option">SUBMIT</span>';
		html 		+= '<span class="close-options">CLOSE</span>';

		return html;
	}																											


	$(document).on('click', '.edit-status', function(){

		var id 			= $(this).data('id');
		var statusText 	= $(this).attr('title');

		$('.status-option-section').html('');

		var html 		= $.trim($('#status-option-section-'+id).html());
		//var optionsHtml = $('#status-options').html();
		var optionsHtml = getOptionsHtml(statusText);

		if(html == '') {
		$('#status-option-section-'+id).html(optionsHtml);
		$('#status-option-section-'+id+' option[value="'+statusText+'"]').prop("selected", true);
		$('#status-option-section-'+id).find('#status-options').focus();
		}

		$('.edit-status').show();
		$(this).hide();
	});

	$(document).on('click', '.close-options', function(){
		$('.status-option-section').html('');		
		$('.edit-status').show();
	});

	$(document).on('click', '.submit-option', function(){

		var statusID 	= $(this).closest('td').find('.edit-status').data('id');
		var statusText 	= $(this).closest('td').find('#status-options').find(':selected').val();
		
		$.ajax({
            type 	:"POST",
            url 	: base_url+'/totalsubmissions/update_submission_status',
            data	: {id : statusID, status : statusText}, 
            success: function(data){
            	console.info(data);
            	if(data['status'] == 'success') {
        		$('.status-option-section').html('');		
				$('.edit-status').show();
				} else {
				alert('Something went worng');
				}
            }
      	});
		$(this).closest('td').find('.edit-status').attr('title', statusText).text(statusText.substring(0, 6));
	});




// Comments section

$('.comment-section')
    .mouseenter(function() {
        var id 	= $(this).data('id');
		$('.comment-view-section').hide();
  		
		var existingHtml = $('#comment-view-section-'+id).html();
		var html 		 = $('.test-section').html();

		if(existingHtml.length > 10) {
		$('#comment-view-section-'+id).show();
		} else {
		$('#comment-view-section-'+id).show();	
		$.ajax({
	            type 	:"POST",
	            url 	: base_url+'/totalsubmissions/getsubmissioncomments',
	            data	: {id : id}, 
	            success : function(data){
	            	console.info(data);
	            	if(data['status'] == 'success') {
	        		$('#comment-view-section-'+id).html(data['html']);
	            	return true;
	            	} else {
	            	alert('Something went worng');
	            	}
	            }
      		});
		}
    }).mouseleave(function(){ 
    	$('.comment-view-section').hide(); 
    });



$(document).on('keyup', '.status-option-section',function(e){
     if (e.keyCode == 27) { 
   		$('.status-option-section').html('');		
		$('.edit-status').show();     
    }
});
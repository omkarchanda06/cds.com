

(function(document) {
  'use strict';

  var LightTableFilter = (function(Arr) {

    var _input;

    function _onInputEvent(e) {
      _input = e.target;
      var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
      Arr.forEach.call(tables, function(table) {
        Arr.forEach.call(table.tBodies, function(tbody) {
          Arr.forEach.call(tbody.rows, _filter);
        });
      });
    }

    function _filter(row) {
      var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
      row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
    }

    return {
      init: function() {
        var inputs = document.getElementsByClassName('light-table-filter');
        Arr.forEach.call(inputs, function(input) {
          input.oninput = _onInputEvent;
        });
      }
    };
  })(Array.prototype);

  $('.reset-search-bar').click(function(){
     LightTableFilter.init();
  });

  document.addEventListener('readystatechange', function() {
    if (document.readyState === 'complete') {
      LightTableFilter.init();
    }
  });

})(document);


$('.light-table-filter').keyup(function(){
   var value = $('.light-table-filter').val();
   if(value.length > 0) {
   	$('.reset-search-bar').show();
   } else {
   	$('.reset-search-bar').hide();
   }
});

$('.reset-search-bar').click(function(){
   $('.light-table-filter').val('');
   $('.reset-search-bar').hide();
});



(function($){

	// Defining our jQuery plugin

	$.fn.paulund_modal_box = function(prop){

		// Default parameters

		var options = $.extend({
			height : "300",
			width : "360",
			title:"JQuery Modal Box",
			description: "Example of how to create a modal box.",
			top: "5%",
			left: "36%",
		},prop);
				
		return this.click(function(e){
			add_block_page();
			add_popup_box();
			add_styles();
			
			$('.paulund_modal_box').fadeIn();
		});
		
		 function add_styles(){			
			$('.paulund_modal_box').css({ 
				'position':'absolute', 
				'left':options.left,
				// 'top':options.top,
				'top':'50px',
				'display':'none',
				'height': options.height + 'px',
				'width': options.width + 'px',
				'border':'1px solid #fff',
				'box-shadow': '0px 2px 7px #292929',
				'-moz-box-shadow': '0px 2px 7px #292929',
				'-webkit-box-shadow': '0px 2px 7px #292929',
				'border-radius':'10px',
				'-moz-border-radius':'10px',
				'-webkit-border-radius':'10px',
				'background': '#f2f2f2', 
				'z-index':'50',
			});
			$('.paulund_modal_close').css({
				'position':'relative',
				'top':'-3px',
				'left':'27px',
				'float':'right',
				'display':'block',
				'height':'50px',
				'width':'50px',
				//'background': 'url(img/close.png) no-repeat',
			});
                        /*Block page overlay*/
			var pageHeight = $(document).height();
			var pageWidth = $(window).width();

			$('.paulund_block_page').css({
				'position':'absolute',
				'top':'0',
				'left':'0',
				'background-color':'rgba(0,0,0,0.6)',
				'height':pageHeight,
				'width':pageWidth,
				'z-index':'10'
			});
			$('.paulund_inner_modal_box').css({
				'background-color':'#fff',
				'height':(options.height - 50) + 'px',
				'width':(options.width - 50) + 'px',
				'padding':'10px',
				'margin':'15px',
				'border-radius':'10px',
				'-moz-border-radius':'10px',
				'-webkit-border-radius':'10px'
			});
		}
		
		 function add_block_page(){
			var block_page = $('<div class="paulund_block_page"></div>');
						
			$(block_page).appendTo('body');
		}
		 		
		 function add_popup_box(){
			 //var pop_up = $('<div class="paulund_modal_box"><a href="#" class="paulund_modal_close"></a><div class="paulund_inner_modal_box"><h2>' + options.title + '</h2><p>' + options.description + '</p></div></div>');

			 // var pop_up = $('.paulund_modal_box');
			 // if($('#target-id').val() == '') {
			 // $(pop_up).appendTo('.paulund_block_page');
			 // }
			 			 
			 $('.paulund_modal_close').click(function(){
				// $(this).parent().fadeOut().remove();
				// $('.paulund_block_page').fadeOut().remove();				 
				$(this).parent().fadeOut();
				$('.paulund_block_page').fadeOut();				 
			 });
		}

		return this;
	};
	
})(jQuery);


$(document).ready(function(){
	$('.paulund_modal').paulund_modal_box();
});

$('.paulund_modal').click(function(){
    var id = $(this).attr('id');
    var getrecruiter = $('.target-recruiter-'+id).find('td')[2].innerHTML;
    var getmanager = $('.target-recruiter-'+id).find('td')[3].innerHTML;
    var gettarget = $('.target-recruiter-'+id).find('td')[4].innerHTML;
    var getstatus = $('.target-recruiter-'+id).find('td')[5].innerHTML;

    $.ajax({
            type:"POST",
            url: window.location.origin+'/cds/index.php/cdsdashboard/get_all_managers',
            success: function(data){

               console.log(data['managers']);
			   var 	managerHTML = '<select id="rec-manager">';
               data['managers'].forEach(function(item) {
               	    if(item.username == getmanager) {
                    managerHTML += '<option value="'+item.id+'" selected="selected">'+item.username+'</option>';
               	    } else {
				    managerHTML += '<option value="'+item.id+'">'+item.username+'</option>';
				    }
               }); 
			   managerHTML += '</select>';


			   var statusHTML = '<select id="rec-status"><option value="ACTIVE"'; 
			   if(getstatus == 'ACTIVE') { statusHTML += 'selected="selected"' }
			   statusHTML += '>ACTIVE</option>';
			   statusHTML += '<option value="INACTIVE"';
			   if(getstatus == 'INACTIVE') { statusHTML += 'selected="selected"' }
			   statusHTML += '>INACTIVE</option></select>';



				// console.log(getrecruiter);
				$('#recruiter-name').html(getrecruiter);
				$('.rec-manager-area').html(managerHTML);
				$('.rec-status-area').html(statusHTML);
				$('#target-num').val(gettarget);
				$('#target-id').val(id);
            }
      });      
});

$('#error-message').hide();
$('.paulund_modal_box').hide();

$("#target-num").keyup(function(event){
    if(event.keyCode == 13){
        $("#set-target").click();
    }
});

$('#target-num').keyup(function () {     
  this.value = this.value.replace(/[^0-9\.]/g,'');
});



$("#set-target").click(function() {
	$('#error-message').css('color','red');
    $('#error-message').show().html('');
    var managerid = $('#rec-manager').find(':selected').val();
    var target = $.trim($('#target-num').val());
    var status = $('#rec-status').find(':selected').val();
    var id = $('#target-id').val();

    if(target == '') {
      $('#error-message').show().html('Make sure you enter some value for Target');
      return false;
    }

    // alert(managerid+' '+target+' '+status+' '+id);
    // return false;
    
    var dataString = 'managerid='+ managerid
				   + '&target='+ target
				   + '&status='+ status
                   + '&id='+ id;



    sendMessage(dataString);

    function sendMessage(dataString){
        var dataString = dataString;
        $('#error-message').css({'color':'#898989','visibility':'visible'}).fadeIn().html('Setting please wait...');

        // ajax
        $.ajax({
            type:"POST",
            url: window.location.origin+'/cds/index.php/cdsdashboard/update_recruiter',
            data: dataString,
            // dataType: "text",
            success: function(data)
            {
            	console.log(data);
            	if(data['status'] == 'success') {
                // alert('Manager: '+data['recruiter']['mid']+'Status: '+data['recruiter']['status']+' Target:'+data['recruiter']['targets']+' ID:'+data['id']);
                // return false;
                $('#target-num').val('');
                $('#error-message').css({'color':'green','visibility':'visible'}).fadeIn().html('Target set successfully');
                
                $(".paulund_modal_close").click();

                $('.target-recruiter-'+data['id']).find('td')[3].innerHTML = data['managername'];
                $('.target-recruiter-'+data['id']).find('td')[4].innerHTML = data['targets'];

                if(data['recstatus'] == 'ACTIVE') {
                $('.rec-status-'+data['id']).css('color','black');
                $('.target-recruiter-'+data['id']).find('td')[5].innerHTML = data['recstatus']; 
                } else {
                $('.rec-status-'+data['id']).css('color','red');
                $('.target-recruiter-'+data['id']).find('td')[5].innerHTML = data['recstatus']; 
                }

                $('html, body').animate({
			        scrollTop: $(".target-recruiter-"+data['id']).offset().top
			    }, 1000);
                
                $(".target-recruiter-"+data['id']).css("background-color","#CCFFCC");
				  setTimeout(function() {
				$(".target-recruiter-"+data['id']).css("background-color","white").effect("highlight", {}, 4000);
				 }, 9000);

				// $("#target-print-"+data['id']).css({"background-color":"#6666FF","color":"white"});
				//   setTimeout(function() {
				// $("#target-print-"+data['id']).css({"background-color":"white","color":"black"}).effect("highlight", {}, 4000);
				//  }, 9000);  

                $('#error-message').html(''); 
                }
                else {
                alert('Something went wrong');
                }
            }
        });
    }

    $('#error-message').css('color','red');
});
<?php

Class ReportsModel extends CI_Model {


	private $statusArray 		= array(
										'Submitted to Client' 	=> 0,
										'Interview' 			=> 0,
										'Placement' 			=> 0,
										'Rejected' 				=> 0
										);

	private $rejectionsArray 	= array(
										'Rejected (Reason: Comm Skills)',
										'Rejected (Reason: Position on Hold/Closed/Filled)',
										'Rejected (Reason: Technical Skills)',
										'Rejected (Reason: Rate Concern)',
										'Rejected (Reason: Location)',
										'Rejected',
										'Rejected After Interview'
									);


	private $statusKeys 		= array(
										'Submitted to Client' 	=> 'submitted_to_Client',
										'Interview' 			=> 'interviews',
										'Rejected' 				=> 'rejections'
										);


	private $result 			= array();

	function getClientContact($company) {
		$this->db->select('client_name');
		$this->db->from('user_clients');
		$this->db->where('client_company', $company);
		$query=$this->db->get();
		$row=$query->row();
		
		return $row;
	}

	function submissions_client_tracker_report($managerId, $reqruiterid, $date, $date1) { 
		
		$session_data = $this->session->userdata('logged_in');
		$userid = $session_data['id'];
		if ($managerId == 'All' && ($reqruiterid == 'All' || $reqruiterid='')) {
			if ($date != "" && $date1 != "") {
				
				$query = $this->db->query('SELECT s.company AS clientname, GROUP_CONCAT(s.cname) as cname, GROUP_CONCAT(case when s.rejections = 1 then cname else null end) as rejnames, GROUP_CONCAT(case when s.submitted_to_Client = 1 then cname else null end) as sub_client_names, GROUP_CONCAT(case when s.interviews = 1 then cname else null end) as interview_names, r.job_title, r.date as jobdate, count(s.id) as subcount, sum(case when s.rejections = 1 then 1 else 0 end) As rej_count, sum(case when s.submitted_to_Client = 1 then 1 else 0 end) As client_sub_count, sum(case when s.interviews = 1 then 1 else 0 end) as interview_count, sum(case when s.status = "Placement" then 1 else 0 end ) As placed_count, sum(case when s.status = "Backout" then 1 else 0 end ) as backout_count, sum(case when s.status = "Rejected after Interview" then 1 else 0 end ) as reject_after_interview_count, s.status FROM requirements r LEFT JOIN submissions s ON r.id = s.job_id WHERE s.userid = '.$managerId.' AND r.date BETWEEN "'.$date.'" AND "'.$date1.'" GROUP BY s.company, r.job_title, r.date order by r.date DESC');

			} else {
				/* By default current month */
				
				$date = date('Y-m-01');
				$date1 = date('Y-m-d');
				$query = $this->db->query('SELECT s.company AS clientname, GROUP_CONCAT(s.cname) as cname, GROUP_CONCAT(case when s.rejections = 1 then cname else null end) as rejnames, GROUP_CONCAT(case when s.submitted_to_Client = 1 then cname else null end) as sub_client_names, GROUP_CONCAT(case when s.interviews = 1 then cname else null end) as interview_names, r.job_title, r.date as jobdate, count(s.id) as subcount, sum(case when s.rejections = 1 then 1 else 0 end) As rej_count, sum(case when s.submitted_to_Client = 1 then 1 else 0 end) As client_sub_count, sum(case when s.interviews = 1 then 1 else 0 end) as interview_count, sum(case when s.status = "Placement" then 1 else 0 end ) As placed_count, sum(case when s.status = "Backout" then 1 else 0 end ) as backout_count, sum(case when s.status = "Rejected after Interview" then 1 else 0 end ) as reject_after_interview_count, s.status FROM requirements r LEFT JOIN submissions s ON r.id = s.job_id WHERE s.userid = '.$managerId.' AND r.date BETWEEN "'.$date.'" AND "'.$date1.'" GROUP BY s.company, r.job_title, r.date order by r.date DESC');

			}
			
			return $query->result();
		} else if($reqruiterid == 'All') {
			// manager wise			
				if ($date != "" && $date1 != "") {
					$query = $this->db->query('SELECT s.userid AS userID, s.company AS clientname, u.client_status AS client_status, GROUP_CONCAT(s.cname) as cname, GROUP_CONCAT(case when s.rejections = 1 then cname else null end) as rejnames, GROUP_CONCAT(case when s.submitted_to_Client = 1 then cname else null end) as sub_client_names, GROUP_CONCAT(case when s.interviews = 1 then cname else null end) as interview_names, r.job_title, r.date as jobdate, count(s.id) as subcount, sum(case when s.rejections = 1 then 1 else 0 end) As rej_count, sum(case when s.submitted_to_Client = 1 then 1 else 0 end) As client_sub_count, sum(case when s.interviews = 1 then 1 else 0 end) as interview_count, sum(case when s.status = "Placement" then 1 else 0 end ) As placed_count, sum(case when s.status = "Backout" then 1 else 0 end ) as backout_count, sum(case when s.status = "Rejected after Interview" then 1 else 0 end ) as reject_after_interview_count, s.status FROM requirements r LEFT JOIN submissions s ON r.id = s.job_id LEFT JOIN user_clients u ON u.client_company = s.company WHERE r.user_id = '.$managerId.' AND r.date BETWEEN "'.$date.'" AND "'.$date1.'" GROUP BY s.company, r.job_title, r.date ORDER BY FIELD(u.client_status, "ACTIVE", "RISING", "PASSIVE")');
				} else {
					/* By default current month */
					
					$date = date('Y-m-01');
					$date1 = date('Y-m-d');
					$query = $this->db->query('SELECT s.userid AS userID, s.company AS clientname, u.client_status AS client_status,  GROUP_CONCAT(s.cname) as cname, GROUP_CONCAT(case when s.rejections = 1 then cname else null end) as rejnames, GROUP_CONCAT(case when s.submitted_to_Client = 1 then cname else null end) as sub_client_names, GROUP_CONCAT(case when s.interviews = 1 then cname else null end) as interview_names, r.job_title, r.date as jobdate, count(s.id) as subcount, sum(case when s.rejections = 1 then 1 else 0 end) As rej_count, sum(case when s.submitted_to_Client = 1 then 1 else 0 end) As client_sub_count, sum(case when s.interviews = 1 then 1 else 0 end) as interview_count, sum(case when s.status = "Placement" then 1 else 0 end ) As placed_count, sum(case when s.status = "Backout" then 1 else 0 end ) as backout_count, sum(case when s.status = "Rejected after Interview" then 1 else 0 end ) as reject_after_interview_count, s.status FROM requirements r LEFT JOIN submissions s ON r.id = s.job_id LEFT JOIN user_clients u ON u.client_company = s.company WHERE r.user_id = '.$managerId.' AND r.date BETWEEN "'.$date.'" AND "'.$date1.'" GROUP BY s.company, r.job_title, r.date ORDER BY FIELD(u.client_status, "ACTIVE", "RISING", "PASSIVE")');
	
				}
				return $query->result();
			
		} else {	
				// recruiter wise			
				if ($date != "" && $date1 != "") {
					$query = $this->db->query('SELECT s.userid AS userID, s.company AS clientname, u.client_status AS client_status, GROUP_CONCAT(s.cname) as cname, GROUP_CONCAT(case when s.rejections = 1 then cname else null end) as rejnames, GROUP_CONCAT(case when s.submitted_to_Client = 1 then cname else null end) as sub_client_names, GROUP_CONCAT(case when s.interviews = 1 then cname else null end) as interview_names, r.job_title, r.date as jobdate, count(s.id) as subcount, sum(case when s.rejections = 1 then 1 else 0 end) As rej_count, sum(case when s.submitted_to_Client = 1 then 1 else 0 end) As client_sub_count, sum(case when s.interviews = 1 then 1 else 0 end) as interview_count, sum(case when s.status = "Placement" then 1 else 0 end ) As placed_count, sum(case when s.status = "Backout" then 1 else 0 end ) as backout_count, sum(case when s.status = "Rejected after Interview" then 1 else 0 end ) as reject_after_interview_count, s.status FROM requirements r LEFT JOIN submissions s ON r.id = s.job_id LEFT JOIN user_clients u ON u.client_company = s.company WHERE r.user_id = '.$managerId.' AND s.submittedby = '.$reqruiterid.' AND r.date BETWEEN "'.$date.'" AND "'.$date1.'" GROUP BY s.company, r.job_title, r.date ORDER BY FIELD(u.client_status, "ACTIVE", "RISING", "PASSIVE")');
				} else {
					/* By default current month */
					
					$date = date('Y-m-01');
					$date1 = date('Y-m-d');
					$query = $this->db->query('SELECT s.userid AS userID, s.company AS clientname, u.client_status AS client_status, GROUP_CONCAT(s.cname) as cname, GROUP_CONCAT(case when s.rejections = 1 then cname else null end) as rejnames, GROUP_CONCAT(case when s.submitted_to_Client = 1 then cname else null end) as sub_client_names, GROUP_CONCAT(case when s.interviews = 1 then cname else null end) as interview_names, r.job_title, r.date as jobdate, count(s.id) as subcount, sum(case when s.rejections = 1 then 1 else 0 end) As rej_count, sum(case when s.submitted_to_Client = 1 then 1 else 0 end) As client_sub_count, sum(case when s.interviews = 1 then 1 else 0 end) as interview_count, sum(case when s.status = "Placement" then 1 else 0 end ) As placed_count, sum(case when s.status = "Backout" then 1 else 0 end ) as backout_count, sum(case when s.status = "Rejected after Interview" then 1 else 0 end ) as reject_after_interview_count, s.status FROM requirements r LEFT JOIN submissions s ON r.id = s.job_id LEFT JOIN user_clients u ON u.client_company = s.company WHERE r.user_id = '.$managerId.' AND s.submittedby = '.$reqruiterid.' AND r.date BETWEEN "'.$date.'" AND "'.$date1.'" GROUP BY s.company, r.job_title, r.date ORDER BY FIELD(u.client_status, "ACTIVE", "RISING", "PASSIVE")');
	
				}
				return $query->result();
		}
	}

	function submissions_interview_tracker_report($managerId, $reqruiterid, $date, $date1) {

		if ($managerId == 'All' && ($reqruiterid == 'All' || $reqruiterid='')) {
			if ($date != "" && $date1 != "") {
			
				$query = $this->db->query("SELECT s.interview_date, s.interview_time, s.cname, s.psub, s.company, s.status, r.recruiter FROM submissions s INNER JOIN recruiters r ON r.id = s.submittedby WHERE s.userid = '".$managerId."' AND s.sdate BETWEEN '".$date."' AND '".$date1."' AND s.status NOT IN ('Submitted to Vendor','Submitted to Client') AND r.status='ACTIVE' order by s.interview_date desc");
			} else {
				/* By default current month */

				$date = date('Y-m-01');
				$date1 = date('Y-m-d');

				$query = $this->db->query("SELECT s.interview_date, s.interview_time, s.cname, s.psub, s.company, s.status, r.recruiter FROM submissions s INNER JOIN recruiters r ON r.id = s.submittedby WHERE s.userid = '".$managerId."' AND s.sdate BETWEEN '".$date."' AND '".$date1."' AND s.status NOT IN ('Submitted to Vendor','Submitted to Client') AND r.status='ACTIVE' order by s.interview_date desc");
			}
				
			return $query->result();
		} elseif ($reqruiterid == 'All') {
			// manager wise
				if ($date != "" && $date1 != "") {

					$query = $this->db->query("SELECT s.interview_date, s.interview_time, s.cname, s.psub, s.company, s.status, r.recruiter FROM submissions s INNER JOIN recruiters r ON r.id = s.submittedby WHERE s.userid = '".$managerId."' AND s.sdate BETWEEN '".$date."' AND '".$date1."' AND s.status NOT IN ('Submitted to Vendor','Submitted to Client') AND r.status='ACTIVE' order by s.interview_date desc");
	
				} else {
					/* By default current month */
	
					$date = date('Y-m-01');
					$date1 = date('Y-m-d');
					
					$query = $this->db->query("SELECT s.interview_date, s.interview_time, s.cname, s.psub, s.company, s.status, r.recruiter FROM submissions s INNER JOIN recruiters r ON r.id = s.submittedby WHERE s.userid = '".$managerId."' AND s.sdate BETWEEN '".$date."' AND '".$date1."' AND s.status NOT IN ('Submitted to Vendor','Submitted to Client') AND r.status='ACTIVE' order by s.interview_date desc");
				}
				return $query->result();
			
		} else {
			// recruiter wise
			
			if ($date != "" && $date1 != "") {

				$query = $this->db->query("SELECT s.interview_date, s.interview_time, s.cname, s.psub, s.company, s.status, r.recruiter FROM submissions s INNER JOIN recruiters r ON r.id = s.submittedby WHERE s.userid = '".$managerId."' AND submittedby = '".$reqruiterid."' AND s.sdate BETWEEN '".$date."' AND '".$date1."' AND s.status NOT IN ('Submitted to Vendor','Submitted to Client') AND r.status='ACTIVE' order by s.interview_date desc");

			} else {
				/* By default current month */

				$date = date('Y-m-01');
				$date1 = date('Y-m-d');
				
				$query = $this->db->query("SELECT s.interview_date, s.interview_time, s.cname, s.psub, s.company, s.status, r.recruiter FROM submissions s INNER JOIN recruiters r ON r.id = s.submittedby WHERE s.userid = '".$managerId."' AND submittedby = '".$reqruiterid."' AND s.sdate BETWEEN '".$date."' AND '".$date1."' AND s.status NOT IN ('Submitted to Vendor','Submitted to Client') AND r.status='ACTIVE' order by s.interview_date desc");
			}
			return $query->result();
		}
	}

	function submissions_stats_report($managerId, $reqruiterid, $date, $date1) {
		if ($managerId == 'All' && ($reqruiterid == 'All' || $reqruiterid='')) {
			if ($date != "" && $date1 != "") {
				//$date = date('Y-m-01');
				//$date1 = date('Y-m-d');
				$this->db->select("userid as managerId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('sdate BETWEEN "'.$date. '" and "'.$date1.'"');
				$this->db->group_by("userid"); 
				
				$query = $this->db->get();
				return $query->result();
			} else {
				$date = date('Y-m-01');
				$date1 = date('Y-m-d');

				$this->db->select("userid as managerId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('sdate BETWEEN "'.$date. '" and "'.$date1.'"');
				$this->db->group_by("userid"); 
				$query = $this->db->get();
				return $query->result();
			}
		} elseif ($reqruiterid == 'All') {
			// manager wise
			if ($date != "" && $date1 != "") {
				//$date = date('Y-m-01');
				//$date1 = date('Y-m-d');
				$this->db->select("userid as managerId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('sdate BETWEEN "'.$date. '" and "'.$date1.'"');
				$this->db->group_by("userid"); 
				
				$query = $this->db->get();
				return $query->result();
			} else {
				$date = date('Y-m-01');
				$date1 = date('Y-m-d');

				$this->db->select("userid as managerId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('sdate BETWEEN "'.$date. '" and "'.$date1.'"');
				$this->db->group_by("userid"); 
				$query = $this->db->get();
				return $query->result();
			}
		} else {
			// recruiter wise
			if ($date != "" && $date1 != "") {
				//$date = date('Y-m-01');
				//$date1 = date('Y-m-d');
				$this->db->select("userid as managerId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('submittedby = ' .$reqruiterid);
				$this->db->where('sdate BETWEEN "'.$date. '" and "'.$date1.'"');
				$this->db->group_by("submittedby"); 
				
				$query = $this->db->get();
				return $query->result();
			} else {
				$date = date('Y-m-01');
				$date1 = date('Y-m-d');

				$this->db->select("userid as managerId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('submittedby = ' .$reqruiterid);
				$this->db->where('sdate BETWEEN "'.$date. '" and "'.$date1.'"');
				$this->db->group_by("submittedby"); 
				$query = $this->db->get();
				return $query->result();
			}
		}
	}

	function submissions_comparision_report ($managerId, $reqruiterid, $date, $date1) {
		if ($managerId == 'All' && ($reqruiterid == 'All' || $reqruiterid='')) {
			//if ($date != "" && $date1 != "") {

				//$monday = strtotime("last monday");
				//$monday = date('W', $monday)==date('W') ? $monday-7*86400 : $monday;

				
				if (!is_numeric($date1))
				$date1 = strtotime($date1);
				
				if (date('w', $date1) == 1)
				$findLastMonday = $date1;		
				else
				$findLastMonday =  strtotime(
					'last monday',
					$date1
				);

				$monday = date('Y-m-d', $findLastMonday);

				//echo $monday;
				$friday = strtotime(date("Y-m-d",$findLastMonday)." +4 days");
				//$last_week_sd = date("Y-m-d", $monday);
				$last_week_sd = date('Y-m-d', strtotime('-1 week', strtotime($monday)));
				$last_week_ed = date('Y-m-d', strtotime('-1 week', strtotime(date('Y-m-d', $friday))));

				$last_last_week_sd = date('Y-m-d', strtotime('-1 week', strtotime($last_week_sd)));
				$last_last_week_ed = date('Y-m-d', strtotime('-1 week', strtotime($last_week_ed)));
				

				/* last week result */

				$this->db->select("userid as managerId, count(*) as submissioncount");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('sdate BETWEEN "'.$last_week_sd. '" and "'.$last_week_ed.'"');
				$this->db->group_by("userid"); 
				$query = $this->db->get();
				$data['last_week'] = $query->result();

				/* last last week result */

				$this->db->select("userid as managerId, count(*) as submissioncount");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('sdate BETWEEN "'.$last_last_week_sd. '" and "'.$last_last_week_ed.'"');
				$this->db->group_by("userid"); 
				$query = $this->db->get();
				$data['last_last_week'] = $query->result();

				//var_dump(sizeof($data['last_last_week']));exit;

				$data['last_week_sd'] = $last_week_sd;
				$data['last_week_ed'] = $last_week_ed;
				$data['last_last_week_sd'] = $last_last_week_sd;
				$data['last_last_week_ed'] = $last_last_week_ed;

				return $data;
				
			//}
		} elseif ($reqruiterid == 'All') {
				// manager wise
				if (!is_numeric($date1))
				$date1 = strtotime($date1);
				
				if (date('w', $date1) == 1)
				$findLastMonday = $date1;		
				else
				$findLastMonday =  strtotime(
					'last monday',
					$date1
				);

				$monday = date('Y-m-d', $findLastMonday);

				//echo $monday;
				$friday = strtotime(date("Y-m-d",$findLastMonday)." +4 days");
				//$last_week_sd = date("Y-m-d", $monday);
				$last_week_sd = date('Y-m-d', strtotime('-1 week', strtotime($monday)));
				$last_week_ed = date('Y-m-d', strtotime('-1 week', strtotime(date('Y-m-d', $friday))));

				$last_last_week_sd = date('Y-m-d', strtotime('-1 week', strtotime($last_week_sd)));
				$last_last_week_ed = date('Y-m-d', strtotime('-1 week', strtotime($last_week_ed)));
				

				/* last week result */

				$this->db->select("userid as managerId, count(*) as submissioncount");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('sdate BETWEEN "'.$last_week_sd. '" and "'.$last_week_ed.'"');
				$this->db->group_by("userid"); 
				$query = $this->db->get();
				$data['last_week'] = $query->result();

				/* last last week result */

				$this->db->select("userid as managerId, count(*) as submissioncount");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('sdate BETWEEN "'.$last_last_week_sd. '" and "'.$last_last_week_ed.'"');
				$this->db->group_by("userid"); 
				$query = $this->db->get();
				$data['last_last_week'] = $query->result();

				//var_dump(sizeof($data['last_last_week']));exit;

				$data['last_week_sd'] = $last_week_sd;
				$data['last_week_ed'] = $last_week_ed;
				$data['last_last_week_sd'] = $last_last_week_sd;
				$data['last_last_week_ed'] = $last_last_week_ed;

				return $data;

		} else {
			// recruiter wise
			if (!is_numeric($date1))
				$date1 = strtotime($date1);
				
				if (date('w', $date1) == 1)
				$findLastMonday = $date1;		
				else
				$findLastMonday =  strtotime(
					'last monday',
					$date1
				);

				$monday = date('Y-m-d', $findLastMonday);

				//echo $monday;
				$friday = strtotime(date("Y-m-d",$findLastMonday)." +4 days");
				//$last_week_sd = date("Y-m-d", $monday);
				$last_week_sd = date('Y-m-d', strtotime('-1 week', strtotime($monday)));
				$last_week_ed = date('Y-m-d', strtotime('-1 week', strtotime(date('Y-m-d', $friday))));

				$last_last_week_sd = date('Y-m-d', strtotime('-1 week', strtotime($last_week_sd)));
				$last_last_week_ed = date('Y-m-d', strtotime('-1 week', strtotime($last_week_ed)));
				

				/* last week result */

				$this->db->select("userid as managerId, count(*) as submissioncount");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('submittedby = ' .$reqruiterid);
				$this->db->where('sdate BETWEEN "'.$last_week_sd. '" and "'.$last_week_ed.'"');
				$this->db->group_by("submittedby"); 
				$query = $this->db->get();
				$data['last_week'] = $query->result();

				/* last last week result */

				$this->db->select("userid as managerId, count(*) as submissioncount");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('submittedby = ' .$reqruiterid);
				$this->db->where('sdate BETWEEN "'.$last_last_week_sd. '" and "'.$last_last_week_ed.'"');
				$this->db->group_by("submittedby"); 
				$query = $this->db->get();
				$data['last_last_week'] = $query->result();

				//var_dump(sizeof($data['last_last_week']));exit;

				$data['last_week_sd'] = $last_week_sd;
				$data['last_week_ed'] = $last_week_ed;
				$data['last_last_week_sd'] = $last_last_week_sd;
				$data['last_last_week_ed'] = $last_last_week_ed;

				return $data;

		}
	}

	function submissions_recruiterdata_report($managerId, $reqruiterid, $date, $date1) {
		if ($managerId == 'All' && ($reqruiterid == 'All' || $reqruiterid='')) {
			if ($date != "" && $date1 != "") {

				$this->db->select("submittedby as recruiterId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
				$this->db->group_by("submittedby"); 
				
				$query = $this->db->get();
				//var_dump($query->result());exit;
				return $query->result();
			} else {
	
				$date = date('Y-m-01');
				$date1 = date('Y-m-d');
	
				$this->db->select("submittedby as recruiterId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
				$this->db->group_by("submittedby"); 
				
				$query = $this->db->get();
				//var_dump($query->result());exit;
				return $query->result();
			}
		} elseif ($reqruiterid == 'All') {
			// manger wise
			// $this->load->model('user','',TRUE);
			$recruiter_ids = $this->user->get_reqruiter($managerId);
			if ($date != "" && $date1 != "") {
				foreach ($recruiter_ids as $recruiter) {
					$this->db->select("submittedby as recruiterId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
					$this->db->from("submissions");
					$this->db->where('submittedby', $recruiter->recruiter_id);
					$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
					// $this->db->group_by("submittedby");
					$query = $this->db->get();
					// echo $this->db->last_query();
					$recruiters_data[] = $query->result();
					// return $query->result();
				}
				return $recruiters_data;
			} else {
	
				$date = date('Y-m-01');
				$date1 = date('Y-m-d');
	
				$this->db->select("submittedby as recruiterId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
				$this->db->group_by("submittedby"); 
				
				$query = $this->db->get();
				//var_dump($query->result());exit;
				return $query->result();
			}
		} else {
			// recruiter wise
			if ($date != "" && $date1 != "") {

				$this->db->select("submittedby as recruiterId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('submittedby = ' .$reqruiterid);
				$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
				$this->db->group_by("submittedby"); 
				
				$query = $this->db->get();
				//var_dump($query->result());exit;
				return $query->result();
			} else {
	
				$date = date('Y-m-01');
				$date1 = date('Y-m-d');
	
				$this->db->select("submittedby as recruiterId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$managerId);
				$this->db->where('submittedby = ' .$reqruiterid);
				$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
				$this->db->group_by("submittedby"); 
				
				$query = $this->db->get();
				//var_dump($query->result());exit;
				return $query->result();
			}
		}
		
	}

	function getRecId($username) {
		$query = $this->db->query("SELECT id FROM recruiters WHERE recruiter='".$username."' AND status='ACTIVE'")->row()->id;
		return $query;
	}

	// if user recruiter

	function rsubmissions_client_tracker_report($reqruiterid, $date, $date1) { 
		//echo $managerId, $reqruiterid; exit;
				
		if ($date != "" && $date1 != "") {
			$query = $this->db->query('SELECT s.userid AS userID, s.company AS clientname, GROUP_CONCAT(s.cname) as cname, GROUP_CONCAT(case when s.rejections = 1 then cname else null end) as rejnames, GROUP_CONCAT(case when s.submitted_to_Client = 1 then cname else null end) as sub_client_names, GROUP_CONCAT(case when s.interviews = 1 then cname else null end) as interview_names, r.job_title, r.date as jobdate, count(s.id) as subcount, sum(case when s.rejections = 1 then 1 else 0 end) As rej_count, sum(case when s.submitted_to_Client = 1 then 1 else 0 end) As client_sub_count, sum(case when s.interviews = 1 then 1 else 0 end) as interview_count, sum(case when s.status = "Placement" then 1 else 0 end ) As placed_count, sum(case when s.status = "Backout" then 1 else 0 end ) as backout_count, sum(case when s.status = "Rejected after Interview" then 1 else 0 end ) as reject_after_interview_count, s.status FROM requirements r LEFT JOIN submissions s ON r.id = s.job_id WHERE s.submittedby = '.$reqruiterid.' AND r.date BETWEEN "'.$date.'" AND "'.$date1.'" GROUP BY s.company, r.job_title, r.date order by r.date DESC');
		} else {
			/* By default current month */
			
			$date = date('Y-m-01');
			$date1 = date('Y-m-d');
			$query = $this->db->query('SELECT s.userid AS userID, s.company AS clientname, GROUP_CONCAT(s.cname) as cname, GROUP_CONCAT(case when s.rejections = 1 then cname else null end) as rejnames, GROUP_CONCAT(case when s.submitted_to_Client = 1 then cname else null end) as sub_client_names, GROUP_CONCAT(case when s.interviews = 1 then cname else null end) as interview_names, r.job_title, r.date as jobdate, count(s.id) as subcount, sum(case when s.rejections = 1 then 1 else 0 end) As rej_count, sum(case when s.submitted_to_Client = 1 then 1 else 0 end) As client_sub_count, sum(case when s.interviews = 1 then 1 else 0 end) as interview_count, sum(case when s.status = "Placement" then 1 else 0 end ) As placed_count, sum(case when s.status = "Backout" then 1 else 0 end ) as backout_count, sum(case when s.status = "Rejected after Interview" then 1 else 0 end ) as reject_after_interview_count, s.status FROM requirements r LEFT JOIN submissions s ON r.id = s.job_id WHERE s.submittedby = '.$reqruiterid.' AND r.date BETWEEN "'.$date.'" AND "'.$date1.'" GROUP BY s.company, r.job_title, r.date order by r.date DESC');

		}

		return $query->result();
	}

	function rsubmissions_interview_tracker_report($reqruiterid, $date, $date1) {
		if ($date != "" && $date1 != "") {

			$query = $this->db->query("SELECT s.interview_date, s.interview_time, s.cname, s.psub, s.company, s.status, r.recruiter FROM submissions s INNER JOIN recruiters r ON r.id = s.submittedby WHERE s.submittedby = '".$reqruiterid."' AND s.sdate BETWEEN '".$date."' AND '".$date1."' AND s.status NOT IN ('Submitted to Vendor','Submitted to Client') AND r.status='ACTIVE' order by s.interview_date desc");

		} else {
			/* By default current month */

			$date = date('Y-m-01');
			$date1 = date('Y-m-d');
			
			$query = $this->db->query("SELECT s.interview_date, s.interview_time, s.cname, s.psub, s.company, s.status, r.recruiter FROM submissions s INNER JOIN recruiters r ON r.id = s.submittedby WHERE s.submittedby = '".$reqruiterid."' AND s.sdate BETWEEN '".$date."' AND '".$date1."' AND s.status NOT IN ('Submitted to Vendor','Submitted to Client') AND r.status='ACTIVE' order by s.interview_date desc");
		}
		return $query->result();
	}

	function rsubmissions_stats_report($reqruiterid, $date, $date1) {
		if ($date != "" && $date1 != "") {
			//$date = date('Y-m-01');
			//$date1 = date('Y-m-d');
			$this->db->select("userid as managerId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
			$this->db->from("submissions");
			$this->db->where('submittedby = ' .$reqruiterid);
			$this->db->where('sdate BETWEEN "'.$date. '" and "'.$date1.'"');
			$this->db->group_by("submittedby"); 
			
			$query = $this->db->get();
			return $query->result();
		} else {
			$date = date('Y-m-01');
			$date1 = date('Y-m-d');

			$this->db->select("userid as managerId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
			$this->db->from("submissions");
			$this->db->where('submittedby = ' .$reqruiterid);
			$this->db->where('sdate BETWEEN "'.$date. '" and "'.$date1.'"');
			$this->db->group_by("submittedby"); 
			$query = $this->db->get();
			return $query->result();
		}
	}

	function rsubmissions_comparision_report ($reqruiterid, $date, $date1) {
		if (!is_numeric($date1))
		$date1 = strtotime($date1);
		
		if (date('w', $date1) == 1)
		$findLastMonday = $date1;		
		else
		$findLastMonday =  strtotime(
			'last monday',
			$date1
		);

		$monday = date('Y-m-d', $findLastMonday);

		//echo $monday;
		$friday = strtotime(date("Y-m-d",$findLastMonday)." +4 days");
		//$last_week_sd = date("Y-m-d", $monday);
		$last_week_sd = date('Y-m-d', strtotime('-1 week', strtotime($monday)));
		$last_week_ed = date('Y-m-d', strtotime('-1 week', strtotime(date('Y-m-d', $friday))));

		$last_last_week_sd = date('Y-m-d', strtotime('-1 week', strtotime($last_week_sd)));
		$last_last_week_ed = date('Y-m-d', strtotime('-1 week', strtotime($last_week_ed)));
		

		/* last week result */

		$this->db->select("userid as managerId, count(*) as submissioncount");
		$this->db->from("submissions");
		$this->db->where('submittedby = ' .$reqruiterid);
		$this->db->where('sdate BETWEEN "'.$last_week_sd. '" and "'.$last_week_ed.'"');
		$this->db->group_by("submittedby"); 
		$query = $this->db->get();
		$data['last_week'] = $query->result();

		/* last last week result */

		$this->db->select("userid as managerId, count(*) as submissioncount");
		$this->db->from("submissions");
		$this->db->where('submittedby = ' .$reqruiterid);
		$this->db->where('sdate BETWEEN "'.$last_last_week_sd. '" and "'.$last_last_week_ed.'"');
		$this->db->group_by("submittedby"); 
		$query = $this->db->get();
		$data['last_last_week'] = $query->result();

		//var_dump(sizeof($data['last_last_week']));exit;

		$data['last_week_sd'] = $last_week_sd;
		$data['last_week_ed'] = $last_week_ed;
		$data['last_last_week_sd'] = $last_last_week_sd;
		$data['last_last_week_ed'] = $last_last_week_ed;

		return $data;
	}

	function rsubmissions_recruiterdata_report($reqruiterid, $date, $date1) {
		if ($date != "" && $date1 != "") {

			$this->db->select("submittedby as recruiterId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
			$this->db->from("submissions");
			$this->db->where('submittedby = ' .$reqruiterid);
			$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
			$this->db->group_by("submittedby"); 
			
			$query = $this->db->get();
			
			//var_dump($query->result());exit;
			return $query->result();
		} else {

			$date = date('Y-m-01');
			$date1 = date('Y-m-d');

			$this->db->select("submittedby as recruiterId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
			$this->db->from("submissions");
			$this->db->where('submittedby = ' .$reqruiterid);
			$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
			$this->db->group_by("submittedby"); 
			
			$query = $this->db->get();
			//var_dump($query->result());exit;
			return $query->result();
		}
		
	}
	// if All directors
	function all_submissions_client_tracker_report($date, $date1) {

		if ($date != "" && $date1 != "") {
			$this->load->model('AssignManagersToDirector', 'assignmanagerstodirector');
			$allDirectors = $this->assignmanagerstodirector->get_all_directors();
			$all_Dirtector_clientTracker = [];
			foreach($allDirectors as $director) {
				$get_username = $this->assignmanagerstodirector->get_username($director->id);	
				$getallmanagers = $this->assignmanagerstodirector->get_all_managersByDirectorID($director->id);
				$mng = [];
				foreach($getallmanagers as $manager) {
					$query = $this->db->query('SELECT s.company AS clientname, GROUP_CONCAT(s.cname) as cname, GROUP_CONCAT(case when s.rejections = 1 then cname else null end) as rejnames, GROUP_CONCAT(case when s.submitted_to_Client = 1 then cname else null end) as sub_client_names, GROUP_CONCAT(case when s.interviews = 1 then cname else null end) as interview_names, r.job_title, r.date as jobdate, count(s.id) as subcount, sum(case when s.rejections = 1 then 1 else 0 end) As rej_count, sum(case when s.submitted_to_Client = 1 then 1 else 0 end) As client_sub_count, sum(case when s.interviews = 1 then 1 else 0 end) as interview_count, sum(case when s.status = "Placement" then 1 else 0 end ) As placed_count, sum(case when s.status = "Backout" then 1 else 0 end ) as backout_count, sum(case when s.status = "Rejected after Interview" then 1 else 0 end ) as reject_after_interview_count, s.status FROM requirements r LEFT JOIN submissions s ON r.id = s.job_id WHERE s.userid = '.$manager->manager_id.' AND r.date BETWEEN "'.$date.'" AND "'.$date1.'" GROUP BY s.company, r.job_title, r.date order by r.date DESC');

					//$get_username = $this->assignmanagerstodirector->get_username($manager->manager_id);	
					//var_dump($get_username->username);
					$mng[] = $query->result();
					//$all_Dirtector_clientTracker[$get_username->username] = $query->result();
				}
				$all_Dirtector_clientTracker[$get_username->username] = $mng;
			}
			
			//var_dump($all_Dirtector_clientTracker);
			return $all_Dirtector_clientTracker;
		} else {
			$date = date('Y-m-01');
			$date1 = date('Y-m-d');
			$this->load->model('AssignManagersToDirector', 'assignmanagerstodirector');
			$allDirectors = $this->assignmanagerstodirector->get_all_directors();
			$all_Dirtector_clientTracker = [];
			foreach($allDirectors as $director) {
				$get_username = $this->assignmanagerstodirector->get_username($director->id);	
				$getallmanagers = $this->assignmanagerstodirector->get_all_managersByDirectorID($director->id);
				$mng = [];
				foreach($getallmanagers as $manager) {
					$query = $this->db->query('SELECT s.company AS clientname, GROUP_CONCAT(s.cname) as cname, GROUP_CONCAT(case when s.rejections = 1 then cname else null end) as rejnames, GROUP_CONCAT(case when s.submitted_to_Client = 1 then cname else null end) as sub_client_names, GROUP_CONCAT(case when s.interviews = 1 then cname else null end) as interview_names, r.job_title, r.date as jobdate, count(s.id) as subcount, sum(case when s.rejections = 1 then 1 else 0 end) As rej_count, sum(case when s.submitted_to_Client = 1 then 1 else 0 end) As client_sub_count, sum(case when s.interviews = 1 then 1 else 0 end) as interview_count, sum(case when s.status = "Placement" then 1 else 0 end ) As placed_count, sum(case when s.status = "Backout" then 1 else 0 end ) as backout_count, sum(case when s.status = "Rejected after Interview" then 1 else 0 end ) as reject_after_interview_count, s.status FROM requirements r LEFT JOIN submissions s ON r.id = s.job_id WHERE s.userid = '.$manager->manager_id.' AND r.date BETWEEN "'.$date.'" AND "'.$date1.'" GROUP BY s.company, r.job_title, r.date order by r.date DESC');

					//$get_username = $this->assignmanagerstodirector->get_username($manager->manager_id);	
					//var_dump($get_username->username);
					$mng[] = $query->result();
					//$all_Dirtector_clientTracker[$get_username->username] = $query->result();
				}
				$all_Dirtector_clientTracker[$get_username->username] = $mng;
			}
			
			//var_dump($all_Dirtector_clientTracker);
			return $all_Dirtector_clientTracker;
		}	
	
	}
	function all_submissions_interview_tracker_report($date, $date1) {
		if ($date != "" && $date1 != "") {
			$this->load->model('AssignManagersToDirector', 'assignmanagerstodirector');
			$allDirectors = $this->assignmanagerstodirector->get_all_directors();
			$all_Dirtector_interviewTracker = [];
			foreach($allDirectors as $director) {
				$get_username = $this->assignmanagerstodirector->get_username($director->id);	
				$getallmanagers = $this->assignmanagerstodirector->get_all_managersByDirectorID($director->id);
				$mng = [];
				foreach($getallmanagers as $manager) {
					$query = $this->db->query("SELECT s.interview_date, s.interview_time, s.cname, s.psub, s.company, s.status, r.recruiter FROM submissions s INNER JOIN recruiters r ON r.id = s.submittedby WHERE s.userid = '".$manager->manager_id."' AND s.sdate BETWEEN '".$date."' AND '".$date1."' AND s.status NOT IN ('Submitted to Vendor','Submitted to Client') AND r.status='ACTIVE' order by s.interview_date desc");
					//$get_username = $this->assignmanagerstodirector->get_username($manager->manager_id);	
					//var_dump($get_username->username);
					$mng[] = $query->result();
					//$all_Dirtector_clientTracker[$get_username->username] = $query->result();
				}
				$all_Dirtector_interviewTracker[$get_username->username] = $mng;
			}
			
			//var_dump($all_Dirtector_clientTracker);
			return $all_Dirtector_interviewTracker;
		} else {
			$date = date('Y-m-01');
			$date1 = date('Y-m-d');
			$this->load->model('AssignManagersToDirector', 'assignmanagerstodirector');
			$allDirectors = $this->assignmanagerstodirector->get_all_directors();
			$all_Dirtector_interviewTracker = [];
			foreach($allDirectors as $director) {
				$get_username = $this->assignmanagerstodirector->get_username($director->id);	
				$getallmanagers = $this->assignmanagerstodirector->get_all_managersByDirectorID($director->id);
				$mng = [];
				foreach($getallmanagers as $manager) {
					$query = $this->db->query("SELECT s.interview_date, s.interview_time, s.cname, s.psub, s.company, s.status, r.recruiter FROM submissions s INNER JOIN recruiters r ON r.id = s.submittedby WHERE s.userid = '".$manager->manager_id."' AND s.sdate BETWEEN '".$date."' AND '".$date1."' AND s.status NOT IN ('Submitted to Vendor','Submitted to Client') AND r.status='ACTIVE' order by s.interview_date desc");

					//$get_username = $this->assignmanagerstodirector->get_username($manager->manager_id);	
					//var_dump($get_username->username);
					$mng[] = $query->result();
					//$all_Dirtector_clientTracker[$get_username->username] = $query->result();
				}
				$all_Dirtector_interviewTracker[$get_username->username] = $mng;
			}
			
			//var_dump($all_Dirtector_clientTracker);
			return $all_Dirtector_interviewTracker;
		}
	}
	function all_submissions_stats_tracker_report($date, $date1) {
		if ($date != "" && $date1 != "") {
			$this->load->model('AssignManagersToDirector', 'assignmanagerstodirector');
			$allDirectors = $this->assignmanagerstodirector->get_all_directors();
			$all_Dirtector_statsTracker = [];
			foreach($allDirectors as $director) {
				$get_username = $this->assignmanagerstodirector->get_username($director->id);	
				$getallmanagers = $this->assignmanagerstodirector->get_all_managersByDirectorID($director->id);
				$mng = [];
				foreach($getallmanagers as $manager) {
					$this->db->select("userid as managerId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
				$this->db->from("submissions");
				$this->db->where('userid = ' .$manager->manager_id);
				$this->db->where('sdate BETWEEN "'.$date. '" and "'.$date1.'"');
				$this->db->group_by("userid"); 
				
				$query = $this->db->get();
					//$get_username = $this->assignmanagerstodirector->get_username($manager->manager_id);	
					//var_dump($get_username->username);
					$mng[] = $query->result();
					//$all_Dirtector_clientTracker[$get_username->username] = $query->result();
				}
				$all_Dirtector_statsTracker[$get_username->username] = $mng;
			}
			
			//var_dump($all_Dirtector_clientTracker);
			return $all_Dirtector_statsTracker;
		} else {
			$date = date('Y-m-01');
			$date1 = date('Y-m-d');
			$this->load->model('AssignManagersToDirector', 'assignmanagerstodirector');
			$allDirectors = $this->assignmanagerstodirector->get_all_directors();
			$all_Dirtector_statsTracker = [];
			$totalSubmissions = 0;
			$totalPlacements = 0;
			$totalSubtoClient = 0;
			$totalInterviews = 0;
			$totalRejections = 0;
			$data = [];
			$mgrIds = [];
			$dir = [];
			foreach($allDirectors as $director) {
				$get_username = $this->assignmanagerstodirector->get_username($director->id);	
				$getallmanagers = $this->assignmanagerstodirector->get_all_managersIdByDirectorID($director->id);

				
				if (sizeof($getallmanagers) > 0) {
					$this->db->select("count(*) as submissioncount,sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
					$this->db->from("submissions");
					//$this->db->where('userid = ' .$manager->manager_id);
					$this->db->where_in('userid', $getallmanagers);
					$this->db->where('sdate BETWEEN "'.$date. '" and "'.$date1.'"');
					
					$query = $this->db->get();
					
					$all_Dirtector_statsTracker[$director->id] = $query->result();
				} else {
					$all_Dirtector_statsTracker[$director->id] = [];
				}
				
				
				
				// $mng = [];
				/*if (sizeof($getallmanagers)>0) {
					foreach($getallmanagers as $manager) {
						//var_dump($manager);
						if ($director->id == $manager->director_id) {
							$mgrIds[$director->id] = $manager->manager_id;
							$dir[$director->id] = $mgrIds;
							
						}else {
							echo "1";
						}
						
					}
					
				}
				var_dump($mgrIds);*/
				
				
				/*foreach($getallmanagers as $manager) {
					$this->db->select("userid as managerId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
					$this->db->from("submissions");
					$this->db->where('userid = ' .$manager->manager_id);
					$this->db->where('sdate BETWEEN "'.$date. '" and "'.$date1.'"');
					$this->db->group_by("userid"); 
					
					$query = $this->db->get();
					//var_dump($query->result());
						$all_Dirtector_statsTracker[] = $query->result();
				}
				
				foreach($all_Dirtector_statsTracker as $key => $Dirtector_statsTracker) {
					foreach($Dirtector_statsTracker as $Dirtector_statTracker) {
						
						$totalSubmissions+=$Dirtector_statTracker->submissioncount;
						$totalPlacements+=$Dirtector_statTracker->placements;
						$totalSubtoClient+=$Dirtector_statTracker->submitted_to_client;
						$totalInterviews+=$Dirtector_statTracker->interviews;
						$totalRejections+=$Dirtector_statTracker->rejections;
					}
				}*/

				//$data[$director->id] = ['submissions' => $totalSubmissions, 'interviews' => $totalInterviews, 'rejections' => $totalRejections, 'placements' => $totalPlacements, "subtoclient" => $totalSubtoClient];

			}
			//var_dump($all_Dirtector_statsTracker);
			
			return $all_Dirtector_statsTracker;
		}
	}
	function all_submissions_recruiterdata_report($date, $date1) {
		if ($date != "" && $date1 != "") {
			$this->load->model('AssignManagersToDirector', 'assignmanagerstodirector');
			$allDirectors = $this->assignmanagerstodirector->get_all_directors();
			$all_Dirtector_recruiterdataTracker = [];
			foreach($allDirectors as $director) {
				$get_username = $this->assignmanagerstodirector->get_username($director->id);	
				$getallmanagers = $this->assignmanagerstodirector->get_all_managersByDirectorID($director->id);
				$mng = [];
				foreach($getallmanagers as $manager) {
					$this->db->select("submittedby as recruiterId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
						$this->db->from("submissions");
						$this->db->where('userid = ' .$manager->manager_id);
						$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
						$this->db->group_by("submittedby"); 
						
						$query = $this->db->get();
						$mng[$manager->manager_id] = $query->result();
				}
				$all_Dirtector_recruiterdataTracker[$get_username->username] = $mng;
			}
			return $all_Dirtector_recruiterdataTracker;
		} else {

			$date = date('Y-m-01');
			$date1 = date('Y-m-d');
			$this->load->model('AssignManagersToDirector', 'assignmanagerstodirector');
			$allDirectors = $this->assignmanagerstodirector->get_all_directors();
			$all_Dirtector_recruiterdataTracker = [];
			foreach($allDirectors as $director) {
				$get_username = $this->assignmanagerstodirector->get_username($director->id);	
				$getallmanagers = $this->assignmanagerstodirector->get_all_managersByDirectorID($director->id);
				$mng = [];
				foreach($getallmanagers as $manager) {
					$this->db->select("submittedby as recruiterId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
						$this->db->from("submissions");
						$this->db->where('userid = ' .$manager->manager_id);
						$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
						$this->db->group_by("submittedby");
						
						$query = $this->db->get();
						$mng[] = $query->result();
				}
				$all_Dirtector_recruiterdataTracker[$get_username->username] = $mng;
			}
			return $all_Dirtector_recruiterdataTracker;
		}

	}

	/**get client details by company name and user id */

	function get_clientDetailsByName_userid($company,$userID){
		$this->db->select('client_name, client_status');
		$this->db->from('user_clients');
		$this->db->where('user_id', $userID);
		$this->db->where('client_company', $company);
		// $this->db->order_by('client_status', 'DSC');
		$result = $this->db->get();
		// echo $this->db->last_query();
		return $result->result();
	}




}
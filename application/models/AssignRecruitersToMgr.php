<?php
Class AssignRecruitersToMgr extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->library('session');
	}

    function get_all_recruiters(){
        $this->db->select('id, recruiter');
        $this->db->from('recruiters');
        $query = $this->db->get();
        return $query->result();
    }

    function insert_assigned_mgrs_to_dir($insert_data){
        $this->db->insert('map_mgr_recruiter', $insert_data);
        return 'Recruiters assigned to Manager successfully';
    }

    function view_all(){
        $this->db->select('*');
        $this->db->from('map_mgr_recruiter');
        $this->db->order_by('isActive', "DESC");
        $query = $this->db->get();
        return $query->result();
    }

    function get_recruitername($id){
        return $this->db->select('recruiter')
		                ->from('recruiters')
		                ->where('id', $id)
		                ->limit(1)
		                ->get()->row();
    }

    function get_all_recruitersBymgrID($id){
        $this->db->select('*');
        $this->db->from('map_mgr_recruiter');
        $this->db->where('manager_id', $id);
        $this->db->order_by('isActive', "DESC");
        $query = $this->db->get();
        return $query->result();
    }

    function find_mgrData_by_dirid($recrID, $mgrID){
        return $this->db->select('*')
		                ->from('map_mgr_recruiter')
		                ->where('recruiter_id', $recrID)
                        ->where('manager_id', $mgrID)
		                ->limit(1)
		                ->get()->row();
    }

    function update_isActive($recr_id, $manager_id, $active_status){
        $data = array(
            'isActive'    => $active_status
        );
        $this->db->where('recruiter_id', $recr_id);
        $this->db->where('manager_id', $manager_id);
        $this->db->update('map_mgr_recruiter', $data);
        return 'Recruiters Status changed succesfully';
    }

    function get_selected_recrs_by_MgrID($mgrID, $recrID){
        return $this->db->select('isActive')
		                ->from('map_mgr_recruiter')
		                ->where('manager_id', $mgrID)
		                ->where('recruiter_id', $recrID)
		                ->limit(1)
		                ->get()->row();
    }
}
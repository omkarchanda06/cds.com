<?php
Class AssignManagersToDirector extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->library('session');
	}

    function get_all_directors(){
        $this->db->select('id, username');
        $this->db->from('users');
        $this->db->where('usertype', 'DIRECTOR');
        $query = $this->db->get();
        return $query->result();
    }

    function get_all_managers(){
        
        $this->db->select('id, username');
        $this->db->from('users');
        $this->db->where('usertype', 'MANAGER');
        $query = $this->db->get();
        return $query->result();
    }

    function insert_assigned_mgrs_to_dir($insert_data){
        $this->db->insert('map_dir_mgr', $insert_data);
        return 'Managers assigned to director successfully';
    }

    function view_all(){
        $this->db->select('*');
        $this->db->from('map_dir_mgr');
        $this->db->order_by('isActive', "DESC");
        $query = $this->db->get();
        return $query->result();
    }
    function get_username($id){
        return $this->db->select('username')
		                ->from('users')
		                ->where('id', $id)
		                ->limit(1)
		                ->get()->row();
    }

    function get_all_managersByDirectorID($id){
        $this->db->select('*');
        $this->db->from('map_dir_mgr');
        $this->db->where('director_id', $id);
        $this->db->order_by('isActive', "DESC");
        $query = $this->db->get();
        return $query->result();
    }

    function get_all_managersIdByDirectorID($id) {
       
        $this->db->select('manager_id');
        $this->db->from('map_dir_mgr');
        $this->db->where('director_id', $id);
        $this->db->order_by('isActive', "DESC");
        $query = $this->db->get();
        $mngrs = $query->result();
        $mgrIds = [];
        foreach($mngrs as $mngrid) {
           $mgrIds[] = $mngrid->manager_id;
        }
       //var_dump($mgrIds);
        return $mgrIds;
    }

    function find_mgrData_by_dirid($mgrID, $dirID){
        return $this->db->select('*')
		                ->from('map_dir_mgr')
		                ->where('director_id', $dirID)
                        ->where('manager_id', $mgrID)
		                ->limit(1)
		                ->get()->row();
    }

    function update_isActive($mgr_id, $director_id, $active_status){
        $data = array(
            'isActive'    => $active_status
        );
        $this->db->where('director_id', $director_id);
        $this->db->where('manager_id', $mgr_id);
        $this->db->update('map_dir_mgr', $data);
        return 'Managers Status changed succesfully';
    }

    function get_selected_mgrs_by_dirID($dirID, $mgrID){
        return $this->db->select('isActive')
		                ->from('map_dir_mgr')
		                ->where('director_id', $dirID)
		                ->where('manager_id', $mgrID)
		                ->limit(1)
		                ->get()->row();
    }
   
}
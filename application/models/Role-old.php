<?php

Class Role extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->library('session');
	}

	function filter() {
		
		if(!$this->session->userdata('logged_in')){
			redirect('cdslogpage', 'refresh');
		}
	}
	
	// Candidates
	function get_role() {
	
		$query=$this->db->get('role_mgmt');
		return $query->result();
	}
	
	function get_menu() {
		$this->db->select('*');
		$this->db->where('isActive', 1); 
		$this->db->order_by('id','ASC');
		$query=$this->db->get('menu');
		return $query->result();
	}
	
	function updateRoleManagement($roleID, $menuIDs,$user_category){
		$this->db->delete('map_role_menu', array('role_id' => $roleID));
		foreach($menuIDs as $menuID){
			$data = array(
				'role_id' => $roleID, 
				'menu_id' => $menuID,
				'isActive' => 1
			);
			$this->db->insert('map_role_menu', $data);
		}
		return true;
	}


}
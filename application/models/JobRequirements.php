<?php

Class JobRequirements extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->library('session');
	}
    
    function insert_job_details($data){
		//print_r($data);die();
		$this->db->insert('requirements', $data);
		return 'Job Posting Added Successfully';
	}

    function JobRequirementDetailsByUserID($userid){
        return $this->db->select('*')
		                ->from('requirements')
		                ->where('user_id', $userid)
						->order_by("job_title", "ASC")
		                ->get()->result();
    }
	function JobRequirementDetailsByID($id){
        return $this->db->select('*')
		                ->from('requirements')
		                ->where('id', $id)
		                ->limit(1)
		                ->get()->row();
    }

	function update_job_details($id, $data){
		$this->db->where('id', $id);
		$this->db->update('requirements', $data);
		return 'Job Posting updated Successfully';
	}
}
<?php

Class Manager extends CI_Model {


	// function getAllManagers(){
	// 	$result = $this->db->query('SELECT id, username FROM users WHERE view=0 ORDER BY position ASC');
	// 	return $result->result();
	// }

	function getAllManagers(){
		$this->db->select('id, username');
		$this->db->from('users');
		$this->db->where('usertype', 'MANAGER');
		$result = $this->db->get();
		return $result->result();
	}

	function getManagersByCategory($catID){
		$result = $this->db->query('SELECT id, username FROM users WHERE view=0 AND user_category='.$catID.'  ORDER BY position ASC');
		return $result->result();
	}

 	function getManagerByID($managerID){
		$result = $this->db->query('SELECT id, username FROM users WHERE view=0 AND id='.$managerID);
		return $result->result();
	}


	// function getManagerTotalTarget($managerID) {
	// 	$query 		= 	mysql_query("SELECT sum(targets) FROM recruiters WHERE mid={$managerID} AND status='ACTIVE'");
	// 	return 			mysql_result($query, 0);
	// }

	function getManagerTotalTarget($managerID, $fromDate, $toDate, $totalDates) {
		$result['sumtarget'] 	= 0;
		$result['targets'] 		= 0;

		$query 		= $this->db->query("SELECT targets, recruiter FROM recruiters WHERE mid={$managerID} AND status='ACTIVE'");
		$qResult 	= $query->result();

		foreach($qResult as $value) {

			$result['sumtarget']   = $result['sumtarget'] + (int)$value->targets;
			
			$defaultTarget 			= (int)$value->targets;
			$absentDays 			= $this->countAbsentAttendanceDates($value->recruiter, $fromDate, $toDate)[0]->count;

			if($absentDays == 0) {
				$result['targets'] = $result['targets'] + $defaultTarget;
			} else {
				$reducedTarget 	= $defaultTarget - $absentDays*($defaultTarget/$totalDates);				
				$result['targets'] = $result['targets'] + $reducedTarget;				
			}
		}

		$result['targets'] = ($result['targets']/5)*$totalDates;				

		return $result;

	}

	function getRecruiterTotalTarget($username, $fromDate, $toDate, $totalDates) {
		$result['sumtarget'] 	= 0;
		$result['targets'] 		= 0;

		$query 		= $this->db->query("SELECT targets, recruiter FROM recruiters WHERE recruiter='".$username."' AND status='ACTIVE'");
		$qResult 	= $query->result();

		foreach($qResult as $value) {

			$result['sumtarget']   = $result['sumtarget'] + (int)$value->targets;
			
			$defaultTarget 			= (int)$value->targets;
			$absentDays 			= $this->countAbsentAttendanceDates($value->recruiter, $fromDate, $toDate)[0]->count;

			if($absentDays == 0) {
				$result['targets'] = $result['targets'] + $defaultTarget;
			} else {
				$reducedTarget 	= $defaultTarget - $absentDays*($defaultTarget/$totalDates);				
				$result['targets'] = $result['targets'] + $reducedTarget;				
			}
		}

		$result['targets'] = ($result['targets']/5)*$totalDates;				

		return $result;

	}


	function countAbsentAttendanceDates($recruiterName, $fromDate, $toDate) {
		$query 	= $this->db->query("SELECT count(*) as count FROM recruiters_attendance WHERE rec='{$recruiterName}' AND status=1 AND date BETWEEN '{$fromDate}' AND '{$toDate}'");
		return $query->result();

	}




}
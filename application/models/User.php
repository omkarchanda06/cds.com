<?php
Class User extends CI_Model {

	private $rejectionArray = array(
								'Rejected (Reason: Comm Skills)',
								'Rejected (Reason: Position on Hold/Closed/Filled)',
								'Rejected (Reason: Technical Skills)',
								'Rejected (Reason: Rate Concern)',
								'Rejected (Reason: Location)',
								'Rejected',
								'Rejected After Interview'
								);

	function login($pt_no, $password) {
		//print_r('hi');exit;
		//echo $pt_no." ".$password;
		$this->db->select('id, user_category, username, usertype, email, ptno');
		$this->db->from('users');
		$this->db->where('ptno = ' ."'".$pt_no."'"); 
		$this->db->where('password = ' ."'".MD5($password)."'"); 
		$this->db->limit(1);

		$query = $this->db->get();
		//echo $query->num_rows(); die;
		if($query -> num_rows() == 1){
			return $query->result();
		}else{
			return false;
		}
	}

	function change_pwd(){
		
	}

	function add_recruiter_insert(){
		$session_data = $this->session->userdata('logged_in');
		$userId = $session_data['id'];
		$data = array(
			'recruiter' => $this->input->post('rname'), 
			'mid' => $userId,
			'status' => $this->input->post('status')
		);	
		return $this->db->insert('recruiters', $data);
	}
	
	function updateview($view, $submisionid){
		$data = array(
				'view' => $view
		);		
		$this->db->where('id', $submisionid);
		$this->db->update('submissions', $data);	 
	}
	
	function add_recruiters_insert() {
		$session_data = $this->session->userdata('logged_in');
		$userId = $this->input->post('userid');
		$data = array(
			'recruiter' => $this->input->post('rname'), 
			'mid' => $userId,
			'status' => $this->input->post('status'),
			'targets' => $this->input->post('target')
		);	
		return $this->db->insert('recruiters', $data);
	}

    function add_user_insert() {
		$uname = $this->input->post('uname');
		$pwd = md5($this->input->post('pwd'));
		$cpwd = md5($this->input->post('cpwd'));
		$email = $this->input->post('email');
		$ptno = $this->input->post('ptno');
		$ucat = $this->input->post('ucat');
		$categoryName = $this->getCategoryName($ucat);

		$utype=$this->input->post('utype');
		$udepartment=$this->input->post('udepartment');
		$status=$this->input->post('status');
		if($this->input->post('adduser')== 'adduser'){
			$this->db->select('*');
			$this->db->where('username',$uname);
			$query=$this->db->get('users');
			//if($query->num_rows != 0){
			if($query->result_id->num_rows != 0){
				return "Exists or Invalid User Name, Please try again";
			} else if($pwd =='' || $pwd != $cpwd){
				return "Empty Password or Passwords does not match, Please try again";
			} else {
				$data = array(
					  'user_category' => $ucat,
					  'username' => $uname,
					  'password' => $pwd,
					  'email' => $email,
					  'ptno' => strtoupper($ptno),
					  'usertype' => $utype,
					  'department' => $udepartment,
				      'status' => $status
				);
				// print_r($data);exit;
				$this->db->insert('users', $data);
				$insertId = $this->db->insert_id();
				if ($utype == "RECRUITER") {
					$recruiter_data = array(
						'urid' => $insertId,
						'recruiter' => $uname,
						'status' => $status
					);
					$this->db->insert('recruiters', $recruiter_data);
				}
				return "User Added Successfully";			  
			}		
		}	
	}

	function getCategoryName($catID) {
        $this->db->select('*');
        $this->db->from('categories');
		$this->db->where('category_id',$catID);
		$results = $this->db->get()->result();

		$categoryName = '';
		foreach($results as $cat) {
			$categoryName = $cat->category_name;
		}

		return $categoryName;
	}
	
	function users_list_model(){
		$session_data = $this->session->userdata('logged_in');
		$userId = $session_data['id'];
		$this->db->select('*');
		$this->db->where('status','ACTIVE');
		$this->db->where('id !=',1);
		$this->db->where('id !=',$userId);
		$query=$this->db->get('users');
		return $query->result();
	}


	function get_userslist($cat_id){
		$session_data = $this->session->userdata('logged_in');
		$userId = $session_data['id'];
		$this->db->select('*');
		$this->db->where('status','ACTIVE');
		$this->db->where('user_category', $cat_id);
		$this->db->where('id !=',1);
		$this->db->where('id !=',$userId);
		$query=$this->db->get('users');
		return $query->result();
	}

	function user_curr_id(){
		$session_data = $this->session->userdata('logged_in');
		$userId = $session_data['id'];
		$this->db->select('*');
		$this->db->where('status','ACTIVE');
		$this->db->where('id',$userId);
		$query=$this->db->get('users');
		foreach($query->result() as $utype)
		{
			$_SESSION['utype']=$utype->usertype;
		}
	}
	
	function inactive_users_list_model(){
		$session_data = $this->session->userdata('logged_in');
		$userId = $session_data['id'];
		$this->db->select('*');
		$this->db->where('status','INACTIVE');
		$this->db->where('id !=',1);
		$this->db->where('id !=',$userId);
		$query=$this->db->get('users');
		return $query;
	}

	function get_inactive_list($cat_id){
		$session_data = $this->session->userdata('logged_in');
		$userId = $session_data['id'];
		$this->db->select('*');
		$this->db->where('status','INACTIVE');
		$this->db->where('user_category', $cat_id);
		$this->db->where('id !=',1);
		$this->db->where('id !=',$userId);
		$query=$this->db->get('users');
		return $query;
	}
	
	function addsub_users_location(){
		$this->db->select('*');
		$this->db->order_by("location", "ASC"); 
		$query=$this->db->get('location');
		//print_r($query->result());exit;
		return $query;
	}


	function get_addsub_users_location($catID){
        
		$this->db->select('*');

	    if($catID == 1 || $catID == 2 || $catID == 4) {
			$catID = 1;
		}
		$this->db->where("loc_cat_id", $catID); 
		$this->db->order_by("location", "ASC"); 
		$query=$this->db->get('location');
		//print_r($query->result());exit;
		return $query;
	}

    function addsub_users_recruiters(){
		$this->db->select('*');
		$this->db->where('status','ACTIVE');
		$this->db->order_by("recruiter", "ASC"); 
		$query=$this->db->get('recruiters');
		
		return $query;
	}

	function get_addsub_users_recruiters($catID){

		$result =$this->db->query('SELECT r.id, r.mid, r.recruiter, r.status, r.interviews, r.targets FROM recruiters r LEFT OUTER JOIN users u ON r.mid = u.id WHERE r.status = "ACTIVE" AND u.user_category='.$catID.' ORDER BY r.recruiter ASC');
		return $result;
	}
	
	function addsub_users_visa() {
		$this->db->select('*');
		$query=$this->db->get('visa');
		//print_r($query->result());exit;
		return $query;
	}

	function get_addsub_users_visa($catID) {

		if($catID == 1 || $catID == 2) {
			$catID = 1;
		}
		$this->db->select('*');
		$this->db->where('category_id',$catID);
		$query=$this->db->get('visa');
		//print_r($query->result());exit;
		return $query;
	}

	function get_submissions_count($catID) {
		$result =$this->db->query('SELECT count(s.id) as countsub FROM submissions s WHERE s.view=0 AND s.submission_category='.$catID);
		return $result->result();
	}
	
	function usubmissions_count(){
		$result =$this->db->query('SELECT count(id) as countsub FROM submissions WHERE view=1');
		return $result->result();
	}
	function submissions_count() {
		$result =$this->db->query('SELECT count(id) as countsub FROM submissions WHERE view=0');
		return $result->result();
	}
	
	function get_usubmissions_count($catID) {
		$result =$this->db->query('SELECT count(s.id) as countsub FROM submissions s WHERE s.view=1 AND s.submission_category='.$catID);
		return $result->result();
	}
	
	function submissions_result($limit, $offset){
		$result =$this->db->query('SELECT s.id, s.userid, s.sdate, s.cname, s.psub, s.totalit, s.company, l.st, s.vcompany, s.vcontact, s.crate, v.visatype, s.relocate, s.srate, sa.resume, s.status, s.employerdetails, (SELECT username FROM `users` WHERE `id` = s.userid) manager, (SELECT recruiter FROM `recruiters` WHERE `id` = s.submittedby) recruiter, s.view FROM submissions s LEFT OUTER JOIN visa v ON s.visa = v.vid LEFT OUTER JOIN submissions_attachment sa ON s.id = sa.submissionsId LEFT OUTER JOIN location l ON s.locationid = l.lid ORDER BY s.sdate DESC Limit '.$offset.','.$limit);

		return $result;
		
		// $this->db->select('s.id, s.userid, s.sdate, s.cname, s.psub, s.totalit, s.company, l.st, s.vcompany, s.vcontact, s.crate, v.visatype, s.relocate, s.srate, sa.resume, s.status, s.employerdetails, u.username as manager, r.recruiter as recruiter, s.view');  
		// $this->db->from('submissions AS s'); // I use aliasing make joins easier  
		// $this->db->join('users AS u', 'u.id = s.userid', 'LEFT OUTER');  
		// $this->db->join('recruiters AS r', 'r.id = s.submittedby', 'LEFT OUTER');  
		// $this->db->join('visa AS v', 'v.vid = s.visa', 'LEFT OUTER');
		// $this->db->join('submissions_attachment AS sa', 'sa.submissionsId = s.id', 'LEFT OUTER');
		// $this->db->join('location AS l', 'l.lid = s.locationid', 'LEFT OUTER');
		// $this->db->order_by("s.sdate", "DESC");
		// $this->db->limit($limit, $offset);
		// $query  = $this->db->get();

		// return $query;
	}

	function get_submissions_result($limit, $offset, $catID){
		
		$result =$this->db->query('SELECT s.id, s.userid, s.sdate, s.cname, s.psub, s.totalit, s.company, l.st, s.vcompany, s.vcontact, s.crate, v.visatype, s.relocate, s.srate, sa.resume, s.status, s.employerdetails, (SELECT username FROM `users` WHERE `id` = s.userid) manager, (SELECT recruiter FROM `recruiters` WHERE `id` = s.submittedby) recruiter, s.view FROM submissions s LEFT OUTER JOIN visa v ON s.visa = v.vid LEFT OUTER JOIN submissions_attachment sa ON s.id = sa.submissionsId LEFT OUTER JOIN location l ON s.locationid = l.lid WHERE s.view = 0 AND s.submission_category = '.$catID.' ORDER BY s.sdate DESC Limit '.$offset.','.$limit);
		
		return $result;

		// $this->db->select('s.id, s.userid, s.sdate, s.cname, s.psub, s.totalit, s.company, l.st, s.vcompany, s.vcontact, s.crate, v.visatype, s.relocate, s.srate, sa.resume, s.status, s.employerdetails, u.username as manager, r.recruiter as recruiter, s.view');  
		// $this->db->from('submissions AS s');// I use aliasing make joins easier  
		// $this->db->join('users AS u', 'u.id = s.userid', 'LEFT OUTER');  
		// $this->db->join('recruiters AS r', 'r.id = s.submittedby', 'LEFT OUTER');  
		// $this->db->join('visa AS v', 'v.vid = s.visa', 'LEFT OUTER');
		// $this->db->join('submissions_attachment AS sa', 'sa.submissionsId = s.id', 'LEFT OUTER');
		// $this->db->join('location AS l', 'l.lid = s.locationid', 'LEFT OUTER');
		// $this->db->where('s.submission_category', $catID);
		// $this->db->order_by("s.sdate", "DESC");
		// $this->db->limit($limit, $offset);
		// $query  = $this->db->get();
		// return $query;
	}
	
	function usubmissions_result($limit, $offset){
		$result =$this->db->query('SELECT s.id, s.userid, s.sdate, s.cname, s.psub, s.totalit, s.company, l.st, s.vcompany, s.vcontact, s.crate, v.visatype, s.relocate, s.srate, sa.resume, s.status, s.employerdetails, (SELECT username FROM `users` WHERE `id` = s.userid) manager , (SELECT recruiter FROM `recruiters` WHERE `id` = s.submittedby) recruiter, s.view FROM submissions s LEFT OUTER JOIN visa v ON s.visa = v.vid LEFT OUTER JOIN submissions_attachment sa ON s.id = sa.submissionsId LEFT OUTER JOIN location l ON l.lid=s.locationid WHERE s.view = 1 ORDER BY s.sdate DESC Limit '.$offset.','.$limit);
		return $result;
	}

	function get_usubmissions_result($limit, $offset, $catID){
		$result =$this->db->query('SELECT s.id, s.userid, s.sdate, s.cname, s.psub, s.totalit, s.company, l.st, s.vcompany, s.vcontact, s.crate, v.visatype, s.relocate, s.srate, sa.resume, s.status, s.employerdetails, (SELECT username FROM `users` WHERE `id` = s.userid) manager , (SELECT recruiter FROM `recruiters` WHERE `id` = s.submittedby) recruiter, s.view FROM submissions s LEFT OUTER JOIN visa v ON s.visa = v.vid LEFT OUTER JOIN submissions_attachment sa ON s.id = sa.submissionsId LEFT OUTER JOIN location l ON l.lid=s.locationid WHERE s.view = 1 AND s.submission_category = '.$catID.' ORDER BY s.sdate DESC Limit '.$offset.','.$limit);
		return $result;
	}
	
	function submissions_export_result($managerId, $reqruiterid, $date1, $date2, $status){

		$statusQuery = '';
		if($managerId != '') {
		  $statusQuery .= ' userId ="'.$managerId.'" AND ';
		}

		if(!empty($status) && $status != 'All') {
			$statusQuery .= " status = '{$status}' AND "; 
		}
		
		if($reqruiterid == "All") {
			$result = 	$this->db->query("SELECT id, sdate, cname, psub, totalit, visa, status, (SELECT username FROM `users` WHERE `id` = submissions.userid) manager, (SELECT recruiter FROM `recruiters` WHERE `id` = submissions.submittedby) recruiter, (SELECT st FROM `location` WHERE `lid` = submissions.locationid) st FROM submissions WHERE ".$statusQuery." view = 0 AND  sdate BETWEEN '".$date1."' AND '".$date2."' order by sdate");
		} elseif(!empty($reqruiterid)) {
			$result =	$this->db->query("SELECT id, sdate, cname, psub, totalit, visa, status, (SELECT username FROM `users` WHERE `id` = submissions.userid) manager, (SELECT recruiter FROM `recruiters` WHERE `id` = submissions.submittedby) recruiter, (SELECT st FROM `location` WHERE `lid` = submissions.locationid) st FROM submissions WHERE ".$statusQuery." view = 0 AND  submittedby =".$reqruiterid." AND sdate BETWEEN '".$date1."' AND '".$date2."' order by sdate");

		}else{
			$result =	$this->db->query("SELECT id, sdate, cname, psub, totalit, visa, status, (SELECT username FROM `users` WHERE `id` = submissions.userid) manager, (SELECT recruiter FROM `recruiters` WHERE `id` = submissions.submittedby) recruiter, (SELECT st FROM `location` WHERE `lid` = submissions.locationid) st FROM submissions WHERE ".$statusQuery." view = 0 AND sdate BETWEEN '".$date1."' AND '".$date2."' order by sdate");
		}
		return $result;	
	}
	
	function mysubmissions_count(){
		$session_data = $this->session->userdata('logged_in');
	  	
	    $userId = $session_data['id'];
		$result =$this->db->query('SELECT count(id) as countsub FROM submissions where userid='.$userId.' AND view = 0');
		return $result->result();
	}
	
	
	function mysubmissions_result($limit, $offset){
		$session_data = $this->session->userdata('logged_in');
	    $userId = $session_data['id'];

		$result =$this->db->query("SELECT id, userid, sdate, cname, psub, totalit, company, st, vcompany, vcontact, vemail, crate, visatype, srate, status, employerdetails, (SELECT recruiter FROM `recruiters` WHERE `id` = s.submittedby) recruiter FROM submissions s, visa v, location l where s.userid='".$userId."' AND v.vid=s.visa AND s.view = 0 GROUP BY s.id ORDER BY s.sdate DESC limit ".$offset.",".$limit);
		return $result;
	}

	function mysub_val_location(){
		$this->db->select('*');
		$query=$this->db->get('location');
		return $query->result();
	}

	function get_mysub_val_location($catID){
		
		if($catID == 2) {
			$catID = 1;
		}
		$this->db->select('*');
		$this->db->where('loc_cat_id = '.$catID);
		$query=$this->db->get('location');
		return $query->result();
	}

	function mysub_val_recruiters(){
		$this->db->select('*');
		$query=$this->db->get_where('recruiters',array('status'=>'ACTIVE'));
		return $query->result();
	}

	function get_all_recruiters(){
		$this->db->select('*');
		$this->db->order_by('status','ASC');
		$query=$this->db->get('recruiters');
		return $query->result();
	}

	// function get_mysub_val_recruiters($managerID){
	// 	$this->db->select('*');
	// 	$this->db->where('mid = '.$managerID);
	// 	$query=$this->db->get_where('recruiters',array('status'=>'ACTIVE'));
	// 	return $query->result();
	// }

	function get_mysub_val_recruiters($catID){
		$query =$this->db->query("SELECT r.id, r.recruiter FROM recruiters r LEFT OUTER JOIN users u ON r.mid = u.id WHERE r.status = 'ACTIVE' AND u.user_category = ".$catID);
		return $query->result();
	}
	
	function mysub_val_visa(){
		$this->db->select('*');
		$query=$this->db->get('visa');
		return $query->result();
	}

	function get_mysub_val_visa($catID){

		if($catID == 1 || $catID == 2) {
			$catID = 1;
		}
		
		$this->db->select('*');
		$this->db->where('category_id = '.$catID);
		$query=$this->db->get('visa');
		return $query->result();
	}
	
	function mysub_val($id){
		$result =$this->db->query("SELECT * FROM submissions s, visa v, location l where s.id='".$id."' AND v.vid=s.visa AND l.lid=s.locationid");
		//print_r($result->result());exit;
		return $result;
	}

	/**fetch submission_attachment table from submission id */

	function my_sub_attach($id){
		//$result = $this->db->query("SELECT * FROM submissions_attachment WHERE submissionsId = $id");
		return $this->db->select('*')
		                ->from('submissions_attachment')
		                ->where('submissionsId', $id)
		                ->limit(1)
		                ->get()->row();
	}
	
	function delete_mysubs($id){
		//print_r('123');exit;
		$this->db->delete('submissions', array('id' => $id));
	}

    
     
 //     function users_edit_model($id){

	// 	$pwd=md5($this->input->post('pwd'));
	// 	$cpwd=md5($this->input->post('cpwd'));
        
 //        $username = $this->input->post('username');

	// 	$ucat = $this->input->post('ucat');
	// 	$categoryName = $this->getCategoryName($ucat);

	// 	$utype=$this->input->post('utype');
	// 	$status=$this->input->post('status');
	// 	$uid=$this->input->post('uid');
	// 	if($pwd =='' || $pwd != $cpwd){
	// 		//print_r('asdadsad');exit;
	// 		return "Empty Password or Passwords does not match, Please try again";
	// 	}else{
	// 		$data = array(
	// 			    'user_category' => $ucat,
	// 			    'username' => $username,
	// 				'password' => $pwd,
	// 				'usertype' => $utype,
	// 				'status' =>$status
	// 		);		
	// 		$this->db->where('id', $uid);
	// 		$this->db->update('users', $data);
	// 		return 'User Updated Successfully';
	// 	}
	// }



	
    function users_edit_model($id){
        
        $username = $this->input->post('username');
		$ucat = $this->input->post('ucat');
		$categoryName = $this->getCategoryName($ucat);
		$ptno = $this->input->post('ptno');
		$utype=$this->input->post('utype');
		$udepartment=$this->input->post('udepartment');
		$status=$this->input->post('status');
		$uid=$this->input->post('uid');
		$email = $this->input->post('email');
        $data = array();

        if(trim($this->input->post('pwd')) == '' && trim($this->input->post('cpwd')) == '') {
		$data = array(
				    'user_category' => $ucat,
					'username'      => $username,
					'email'         => $email,
				    'ptno'          => strtoupper($ptno),
					'usertype'      => $utype,
					'department'      => $udepartment,
					'status'        => $status
			);

		} else {
        
        $pwd=md5($this->input->post('pwd'));
		$cpwd=md5($this->input->post('cpwd'));

			if($pwd != $cpwd) {
				return "Empty Password or Passwords does not match, Please try again";
			} else {

				$data = array(
				    'user_category' => $ucat,
				    'username' => $username,
					'password' => $pwd,
					'email' => $email,
					'ptno' => strtoupper($ptno),
					'usertype' => $utype,
					'department' => $udepartment,
					'status' =>$status
				);
			}
		}
     
			$this->db->where('id', $uid);
			$this->db->update('users', $data);
			return 'User Updated Successfully';
		}
	
	function users_update($id){
	
		$this->db->select("*");
		$this->db->from("users");
		$this->db->where("id = ". "'". $id . "'");
		$query = $this->db->get();
		//print_r($query);exit;
		if($query->num_rows()>0){
			
			foreach($query->result() as $rows){
	           $data1[] = $rows;
	        }
	        return $data1;          
		}
	}
	
	function all_recruiters_data(){
		$session_data = $this->session->userdata('logged_in');
		$userId = $session_data['id'];
		$this->db->order_by('status','ASC');
		$query=$this->db->get('recruiters');
		return $query->result();
	}

	function recruiters_data(){
		$session_data = $this->session->userdata('logged_in');
		$userId = $session_data['id'];
		// if ($session_data['usertype'] == "DIRECTOR") {
		// 	echo $userId;
		// } else {
		// 	echo $userId;
		// }
		
		$this->db->where('mid', $userId);
		$this->db->order_by('status','ASC');
		$query=$this->db->get('recruiters');
		return $query->result();
	}

	function get_recruiters_data($catID){
        $this->db->select('*');
	    $this->db->from('recruiters');
        $this->db->join('users', 'recruiters.mid = users.id');
		$this->db-> where("users.user_category", $catID);
		$query = $this->db->get();
		return $query->result();
	}
	
	function rec_attendence_data(){
		$session_data = $this->session->userdata('logged_in');
		$userId = $session_data['id'];
		$this->db->where('mid', $userId);
		$this->db->where('status','ACTIVE');
		$this->db->order_by('status','ASC');
		$query=$this->db->get('recruiters');
		//print_r($query);exit;
		return $query->result();
	}

	function attendence_data_all(){
		$query1=$this->db->query("SELECT recruiters.recruiter as recruiter, users.username as manager FROM recruiters JOIN users ON recruiters.mid = users.id WHERE recruiters.status = 'ACTIVE' ");
		return $query1->result();
	}
 	
	function rec_attendence_data_val(){
		$session_data = $this->session->userdata('logged_in');
		$userId = $session_data['id'];
		$query1=$this->db->query("SELECT recruiter FROM recruiters WHERE mid= '".$userId."' AND status = 'ACTIVE'");
		return $query1->result();
	}

	function get_attendence_data($catID){
		$this->db->select('*');
	    $this->db->from('recruiters');
        $this->db->join('users', 'recruiters.mid = users.id');
		$this->db-> where("users.user_category", $catID);
		$query = $this->db->get();
		return $query->result();
	}
	
	function status_recruiters($status,$id){
		if($status == 'ACTIVE'){
			$data = array('status' => 'INACTIVE');
			$this->db->where('id', $id);
			$this->db->update('recruiters', $data);
			return 'Recruiter Status changed succesfully';
		}else{
			$data = array('status' => 'ACTIVE');
			$this->db->where('id', $id);
			$this->db->update('recruiters', $data);
			return 'Recruiter Status changed succesfully';
		}
	}
	
	function status_attendence_model($status,$rname){
		$this -> db -> select('rec, date');
		$this -> db -> from('recruiters_attendance');
		$this -> db -> where('rec = ' . "'" .$rname. "'"); 
		$this -> db -> where('date = ' . "'" . date("Y-m-d") . "'"); 
		$this -> db -> limit(1);
		$query = $this -> db -> get();
		
		if($query -> num_rows() == 1){
			
				$data = array('status' => $status);
				$this->db->where('rec', $rname);
				$this->db->where('date', date("Y-m-d"));
				$this->db->update('recruiters_attendance', $data);
				echo 'Recruiter Status changed succesfully';
			
			
		}else{
			$data = array(
				'status' => '1',
				'rec' => $rname,
				'date' => date("Y-m-d"),
			);
			$this->db->insert('recruiters_attendance', $data);
			echo 'Recruiter Status changed succesfully3';
		}
		
	}
	
	function submissions_insert($filename){
		//$file_name = preg_replace("/\s+/", "_", $_FILES["userfile"]["name"]);
		$submitted_to_client = 0;
		$rejections = 0;
		$interviews = 0;
		$status = $this->input->post('status');
		if(($status == "Submitted to Client")){
			$submitted_to_client = 1;
		}elseif($status == "Interview"){
			$submitted_to_client = 1;
			$interviews = 1;
		}elseif($status == "Rejected"){
			$rejections = 1;
		}elseif($status == "Rejected after Interview"){
			$submitted_to_client = 1;
			$interviews = 1;
			$rejections = 1;
		}
		$data = array(
				'userid' => $this->input->post('userid'),
				'submission_category' => $this->session->userdata('logged_in')['category'],
				'sdate' => $this->input->post('sdate'),
				'cname' => $this->input->post('cname'),
				'psub' => $this->input->post('psub'),
				'job_submitted_to' => $this->input->post('jsub'),
				'totalit' => $this->input->post('totalit'),
				'company' => $this->input->post('company'),
				'submittedby' => $this->input->post('submittedby'),
				//'vcompany' => $this->input->post('vcompany'),
				//'vcontact' => $this->input->post('vcontact'),
				//'vemail' => $this->input->post('vemail'),
				'crate' => $this->input->post('crate'),
				'srate' => $this->input->post('srate'),
				'status' => $this->input->post('status'),
				//'resume' => 'resumes/'.$filename,
				'visa' => $this->input->post('visa'),
				'relocate' => $this->input->post('relocate'),
				'locationid' => $this->input->post('location'),
				'employerdetails' => $this->input->post('employerdetails'),
				//'resumecopy' => $this->input->post('copypaste'),
				'submitted_to_client' => $submitted_to_client,
				'interviews' => $interviews,
				'rejections' => $rejections,
				'job_id'  => $this->input->post('jobid'),
		);
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";die();
		$this->db->insert('submissions', $data);
		$attachment_data = array(
			'submissionsId' => $this->db->insert_id(),
			'resume' => 'resumes/'.$filename,
			'resumecopy' => $this->input->post('copypaste')
		);
		$this->db->insert('submissions_attachment', $attachment_data);
		return 'Submission Successfully Added';
	}
	
	
	function jobs_data(){
		
		if($this->input->post('Submit')=='Submit'){
		
			$start = $this->input->post('datepicker');
			$msg = $this->input->post('txtSubject');
			$end = $this->input->post('deadline');
			$cmbFrom = $this->input->post('cmbFrom');
			$cmbStatus = $this->input->post('cmbStatus');
		 	if (!empty($start) && !empty($end)){
		
				$query = $this->db->query("SELECT * FROM jobs WHERE datepicker >= '$start' AND datepicker <= '$end' OR deadline >= '$start' AND deadline <= '$end' AND status_delete='1'");
		
				return $query; 
			}else{
				$query = $this->db->query("SELECT * FROM jobs WHERE `subject` LIKE ('$msg') OR `from` LIKE ('$cmbFrom') OR `status` LIKE ('$cmbStatus') AND status_delete='1'");
				$result=$query->num_rows;
				return $query;	
			}  
			$query = $this->db->query("SELECT * FROM jobs WHERE (datepicker >= '$start' AND datepicker <= '$end' OR deadline >= '$start' AND deadline <= '$end' AND `from` LIKE '%".$msg."%') OR (`from` LIKE '%".$msg."%') AND status_delete='1' "); 
			return $query;
		}else{
			$this->db->select("*");
			$this->db->from("jobs");
			$this->db->where("status_delete",'1');
		
			$query = $this->db->get();
			
			if ($query->num_rows() > 0){ 
                //true if there are rows in the table
                return $query->result_array(); //returns an object of data
            }
			return false;
		}			
	}
	
	public function record_count() {
        return $this->db->count_all("jobs");
    }
	
	function admin_users(){
		$query = $this->db->get('admin');
		//print_r($query);exit;
		return $query->result();
	}
	function admin_status(){
		$query = $this->db->get('status');
		//print_r($query);exit;
		return $query->result();
	}
	
	function update_data($id){
	
		$this->db->select("*");
		$this->db->from("jobs");
		$this->db->where("id = ". "'". $id . "'");
		$query = $this->db->get();
		if($query->num_rows()>0){
			foreach($query->result() as $rows){
				$data1[] = $rows;
			}
			return $data1;          
		}
	}
	
	function update_attachments($id){
	
		$this->db->select("*");
		$this->db->from("attachments");
		$this->db->where("sid = ". "'". $id . "'");
		//$this->db->or_where("id = ". "'". $sid . "'");
		$query = $this->db->get();
		if($query->num_rows()>0){
			foreach($query->result() as $rows){
				$data1[] = $rows;
			}
            return $data1;          
		}
	}

	function update_attachments1($id){
	
		$this->db->select("*");
		$this->db->from("attachments");
		$this->db->where("sid = ". "'". $id . "'");
		$query = $this->db->get();
		if($query->num_rows()>0){
			foreach($query->result() as $rows){
				$data1[] = $rows;
			}
			return $data1;          
		}
	}
	   
	function commments_insert(){

		$data = array(
				'sid' => $this->input->post('sid'),
				'uid' => $this->input->post('uid'),
				'comment' => $this->input->post('comment')
		);
		$this->db->insert('comments', $data);
		return "Your comment added successfully";	
	}   
	
	function comments_data(){
		$sid=$_SESSION['id'];
		$query=$this->db->query("select comment, username, ondate from comments, users where users.id=comments.uid AND comments.sid= $sid order by comments.ondate DESC");
		return $query->result();
	}
	
	// function edit_submissions_model($filename){
	// 	//$file_name = preg_replace("/\s+/", "_", $_FILES["userfile"]["name"]);
	// 	$submitted_to_client = 0;
	// 	$rejections = 0;
	// 	$interviews = 0;
	// 	$status = $this->input->post('status');
	// 	if(($status == "Submitted to Client")){
	// 		$submitted_to_client = 1;
	// 	}elseif($status == "Interview"){
	// 		$submitted_to_client = 1;
	// 		$interviews = 1;
	// 	}elseif($status == "Rejected"){
	// 		$rejections = 1;
	// 	}elseif($status == "Rejected After Interview"){
	// 		$submitted_to_client = 1;
	// 		$interviews = 1;
	// 		$rejections = 1;
	// 	}elseif(($status == "Placement") || ($status == "Backout")){
	// 		$submitted_to_client = 1;
	// 		$interviews = 1;			
	// 	}
		
	// 	$data = array(
	// 			'sdate' => $this->input->post('sdate'),
	// 			'cname' => $this->input->post('cname'),
	// 			'psub' => $this->input->post('psub'),
	// 			'totalit' => $this->input->post('totalit'),
	// 			'company' => $this->input->post('company'),
	// 			'submittedby' =>$this->input->post('submittedby'),
	// 			'vcompany' =>$this->input->post('vcompany'),
	// 			'vcontact' =>$this->input->post('vcontact'),
	// 			'vemail' =>$this->input->post('vemail'),
	// 			'crate' =>$this->input->post('crate'),
	// 			'srate' =>$this->input->post('srate'),
	// 			'status' =>$this->input->post('status'),
	// 			'interview_date' =>$this->input->post('interview_date'),
	// 			'visa' =>$this->input->post('visa'),
	// 			'locationid' =>$this->input->post('location'),
	// 			'employerdetails' =>$this->input->post('employerdetails'),
	// 			'resume' =>'resumes/'.$filename,
	// 			'resumecopy' =>$this->input->post('copypaste'),
	// 			'mdate' =>$this->input->post('mdate'),
	// 			'submitted_to_client' => $submitted_to_client,
	// 			'interviews' =>$interviews,
	// 			'rejections' =>$rejections
	// 	);
		
	// 	$this->db->where('id', $this->input->post('sid'));
	// 	$this->db->update('submissions', $data);
	// 	//print_r($this->db); die;
	// 	return 'Submission Updated Successfully';
	// 	//redirect('home/edit/'.$id.'/ok'); 	
	// }



	function edit_submissions_model($filename){

	 	$submitted_to_client 	= 0;
		$rejections 			= 0;
		$interviews 			= 0;
		$status 				= $this->input->post('status');	

	 	$data 					= array(
									'sdate' 				=> $this->input->post('sdate'),
									'cname' 				=> $this->input->post('cname'),
									'psub' 					=> $this->input->post('psub'),
									'totalit' 				=> $this->input->post('totalit'),
									'company' 				=> $this->input->post('company'),
									'submittedby' 			=> $this->input->post('submittedby'),
									// 'vcompany' 				=> $this->input->post('vcompany'),
									// 'vcontact' 				=> $this->input->post('vcontact'),
									// 'vemail' 				=> $this->input->post('vemail'),
									'crate' 				=> $this->input->post('crate'),
									'srate' 				=> $this->input->post('srate'),
									'status' 				=> $this->input->post('status'),
									'interview_date' 		=> $this->input->post('interview_date'),
									'interview_time' 		=> $this->input->post('interview_time'),
									'time_zone' 		    => $this->input->post('interview_time_zone'),
									'mode_of_interview' 	=> $this->input->post('modeofinterview'),
									'visa' 					=> $this->input->post('visa'),
									'relocate' 				=> $this->input->post('relocate'),
									'locationid' 			=> $this->input->post('location'),
									'employerdetails' 		=> $this->input->post('employerdetails'),
									'mdate' 				=> $this->input->post('mdate')
								);

		$submission_attach_data = array(
			'resume' 				=> 'resumes/'.$filename,
			'resumecopy' 			=> $this->input->post('copypaste'),
		);

		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		// die();

		if(($status == "Submitted to Client")) {

			$data['submitted_to_client'] 	= 1;

		} elseif($status == "Interview") {

		    $data['interviews'] 			= 1;

		} else if(in_array($status, $this->rejectionArray)) {	

			$data['rejections'] 			= 1;

		} 
	
		
		$this->db->where('id', $this->input->post('sid'));
		$this->db->update('submissions', $data);
		$this->db->where('submissionsId', $this->input->post('sid'));
		$this->db->update('submissions_attachment', $submission_attach_data);
		return 'Submission Updated Successfully';
	}  
	
	function delete_users($id){
	
		$this->db->delete('users', array('id' => $id)); 
	} 	
	function getmanager(){
	
		$this->db->select("id as managerId, username as managerName, targets as submissionTargets");
		$this->db->from("users");
		$this->db->where("view = 0");
		$this->db->order_by("position","asc");
		$query = $this->db->get();
		return $query->result();
	}

	function getmanager_from_category($catID){
	
		$this->db->select("id as managerId, username as managerName, targets as submissionTargets");
		$this->db->from("users");
		$this->db->where("view = 0");
		$this->db->where("user_category = ".$catID);
		$this->db->order_by("position","asc");
		$query = $this->db->get();
		return $query->result();
	}

	function getmanagerbyID($userID, $catID){
	
		$this->db->select("id as managerId, username as managerName, targets as submissionTargets");
		$this->db->from("users");
		$this->db->where("view = 0");
		$this->db->where("id = ".$userID);
		$this->db->where("user_category = ".$catID);
		$query = $this->db->get();
		return $query->result();
	}


	function getrecruiter(){
	
		$this->db->select("id as recruiterId, mid as managerId, recruiter as recruiterName, targets as rsubmissionTargets");
		$this->db->from("recruiters");
		$this->db->where("status = 'ACTIVE'");
		$query = $this->db->get();
		return $query->result();
	}

	function getrecruiter_by_category($catID = 1){
	
		$this->db->select("id as recruiterId, mid as managerId, recruiter as recruiterName, targets as rsubmissionTargets");
		$this->db->from("recruiters");
		$this->db->where("status = 'ACTIVE'");
		$query = $this->db->get();
		return $query->result();
	}

	function getmsubmission($date, $date1){
		$this->db->select("userid as managerId, count(*) as submissioncount, sdate");
		$this->db->from("submissions");
		$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
		//$this->db->where('sdate BETWEEN "2013-02-18" and "2013-02-24"');
		$this->db->group_by("userid, sdate"); 
		
		$query = $this->db->get();
		return $query->result();
	}

	function getmsubmission_category($date, $date1, $catID){
		$this->db->select("userid as managerId, count(*) as submissioncount, sdate");
		$this->db->from("submissions");
		$this->db->where('submission_category = ' .$catID);
		$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
		//$this->db->where('sdate BETWEEN "2013-02-18" and "2013-02-24"');
		$this->db->group_by("userid, sdate"); 
		
		$query = $this->db->get();
		return $query->result();
	}

	function getmsubmission_manager_category($date, $date1, $userID, $catID){
		$result =$this->db->query('SELECT s.userid AS managerId, count(*) as submissioncount, s.sdate FROM submissions s LEFT OUTER JOIN recruiters r ON s.userid = r.id WHERE s.submission_category = '.$catID.' AND   r.mid = '.$userID.' AND   s.sdate BETWEEN "'.$date.'" AND "'.$date1.'" GROUP BY  s.userid, s.sdate');
        return $result;
	}
		
	function getrsubmission($date, $date1){
		$this->db->select("userid as managerId, count(*) as submissioncount, submittedby as recruiterId, sdate");
		$this->db->from("submissions");
		$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
		//$this->db->where('sdate BETWEEN "2013-02-18" and "2013-02-24"');
		$this->db->group_by("submittedby, sdate"); 
		
		$query = $this->db->get();
		//print_r($this->db); die;
		return $query->result();
	}

	function getrsubmission_category($date, $date1, $catID){
		$this->db->select("userid as managerId, count(*) as submissioncount, submittedby as recruiterId, sdate");
		$this->db->from("submissions");
		$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
		$this->db->where('submission_category ='.$catID);
		//$this->db->where('sdate BETWEEN "2013-02-18" and "2013-02-24"');
		$this->db->group_by("submittedby, sdate"); 
			
		$query = $this->db->get();
		return $query->result();
	}

	function getrsubmission_manager_category($date, $date1, $userID, $catID){
		$result =$this->db->query('SELECT s.userid AS managerId, count(*) as submissioncount, s.submittedby as recruiterId, s.sdate FROM submissions s LEFT OUTER JOIN recruiters r ON s.userid = r.id WHERE s.submission_category = '.$catID.' AND   r.mid = '.$userID.' AND   s.sdate BETWEEN "'.$date.'" AND "'.$date1.'" GROUP BY  s.submittedby, s.sdate');
		
        return $result;
	}
	
	//search with kewords
	function getmsubmissiontoall($searchtype, $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $limit, $offset, $visa, $relocate){
		
	    $keySearchCond = '';
		if(trim($ksearch1)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch1."%' AND ";
		if(trim($ksearch2)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch2."%' AND ";
		if(trim($ksearch3)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch3."%' AND ";
		if(trim($ksearch4)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch4."%' AND ";
		if(trim($cname)!='')
		$keySearchCond .= "s.cname LIKE '%".$cname."%' AND ";
		if(trim($yrsexp)!='')
		$keySearchCond .= "s.totalit >= '".$yrsexp."' AND ";
		// if(trim($location)!='')
		// $keySearchCond .= "s.locationid = '".$location."' AND ";
		if(trim($location)!='')
		$keySearchCond .= "s.locationid IN (".$location.") AND ";
		// if($visa!='')
		// $keySearchCond .= "s.visa = '".$visa."' AND ";
		if(trim($visa)!='')
		$keySearchCond .= "s.visa IN (".$visa.") AND ";
		if(trim($relocate)!='')
		$keySearchCond .= "s.relocate LIKE '%".$relocate."%' AND ";
		if(trim($date1)!='')
		$keySearchCond .= "s.sdate BETWEEN '". $date1. "' and '". $date."' AND";
		
		$session_data = $this->session->userdata('logged_in');
	  	
	    $userId = $session_data['id'];

		if($searchtype == "totalsubmissions"){
		$query = $this->db->query("SELECT id, (SELECT recruiter FROM `recruiters` WHERE `id` = s.submittedby) recruiter FROM submissions s, submissions_attachment sa, visa v, location l where v.vid=s.visa AND l.lid=s.locationid AND s.id=sa.submissionsid AND ".$keySearchCond." `psub` LIKE '%".$tsearch."%' AND s.view=0 ORDER BY s.sdate DESC ");	
		
		}else{
		
		$query = $this->db->query("SELECT *, (SELECT recruiter FROM `recruiters` WHERE `id` = s.submittedby) recruiter FROM submissions s, submissions_attachment sa, visa v, location l where v.vid=s.visa AND l.lid=s.locationid AND s.id=sa.submissionsid AND ".$keySearchCond." `psub` LIKE '%".$tsearch."%' AND userid='".$userId."' AND s.view=0 ORDER BY s.sdate DESC");
		}
		//var_dump($query->result()[0]->vcompany);die();
		return $query->result();
	}



	function getmsubmissiontoall_category($searchtype, $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $limit, $offset, $visa, $relocate, $catID){
		
	    $keySearchCond = '';
		if(trim($ksearch1)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch1."%' AND ";
		if(trim($ksearch2)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch2."%' AND ";
		if(trim($ksearch3)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch3."%' AND ";
		if(trim($ksearch4)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch4."%' AND ";
		if(trim($cname)!='')
		$keySearchCond .= "s.cname LIKE '%".$cname."%' AND ";
		if(trim($yrsexp)!='')
		$keySearchCond .= "s.totalit >= '".$yrsexp."' AND ";
		// if(trim($location)!='')
		// $keySearchCond .= "s.locationid = '".$location."' AND ";
		if(trim($location)!='')
		$keySearchCond .= "s.locationid IN (".$location.") AND ";
		// if($visa!='')
		// $keySearchCond .= "s.visa = '".$visa."' AND ";
		if(trim($visa)!='')
		$keySearchCond .= "s.visa IN (".$visa.") AND ";
		if(trim($relocate)!='')
		$keySearchCond .= "s.relocate LIKE '%".$relocate."%' AND ";
		if(trim($date1)!='')
		$keySearchCond .= "s.sdate BETWEEN '". $date1. "' and '". $date."' AND ";

	    $keySearchCond .= "submission_category =".$catID." AND";
		
		//if($keySearchCond!='')
		//$keySearchCond = " AND (".substr($keySearchCond, 0, -4).") ";
		$session_data = $this->session->userdata('logged_in');
	  	
	    $userId = $session_data['id'];
		
		if($searchtype == "totalsubmissions"){
		$query = $this->db->query("SELECT *, (SELECT recruiter FROM `recruiters` WHERE `id` = s.submittedby) recruiter FROM submissions s, submissions_attachment sa, visa v, location l where v.vid=s.visa AND s.id=sa.submissionsid AND l.lid=s.locationid AND ".$keySearchCond." `psub` LIKE '%".$tsearch."%' AND s.view=0 ORDER BY s.sdate DESC");	
		
		}else{
		
		$query = $this->db->query("SELECT *, (SELECT recruiter FROM `recruiters` WHERE `id` = s.submittedby) recruiter FROM submissions s, submissions_attachment sa,visa v, location l where v.vid=s.visa AND s.id=sa.submissionsid AND l.lid=s.locationid AND ".$keySearchCond." `psub` LIKE '%".$tsearch."%' AND userid='".$userId."' AND s.view=0 ORDER BY s.sdate DESC ");
		}
		
		return $query->result();
	}
	
	function getconmsubmission($consultantlist){
		$query = $this->db->query("SELECT sdate, cname, psub, totalit, company, resume, st, crate,  (SELECT recruiter FROM `recruiters` WHERE `id` = s.submittedby) as recruiter FROM submissions s, visa v, location l where v.vid=s.visa AND l.lid=s.locationid AND id =".$consultantlist);
		return $query->result();
	}
	function getcondetails($consultantlist){
		$query = $this->db->query("SELECT sdate, cname, psub, resumecopy, location FROM submissions s, visa v, location l where v.vid=s.visa AND l.lid=s.locationid AND id =".$consultantlist);
		return $query->result();
	}
	function gettotsubmissions_count($searchtype, $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $visa, $relocate){
		$keySearchCond = '';
		if(trim($ksearch1)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch1."%' AND ";
		if(trim($ksearch2)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch2."%' AND ";
		if(trim($ksearch3)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch3."%' AND ";
		if(trim($ksearch4)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch4."%' AND ";
		if(trim($cname)!='')
		$keySearchCond .= "s.cname LIKE '%".$cname."%' AND ";
		if(trim($yrsexp)!='')
		$keySearchCond .= "s.totalit >= '".$yrsexp."' AND ";
		// if(trim($visa)!='')
		// $keySearchCond .= "visa = '".$visa."' AND ";
	    if(trim($visa)!='')
	    $keySearchCond .= "s.visa IN (".$visa.") AND ";
		if(trim($relocate)!='')
		$keySearchCond .= "s.relocate LIKE '%".$relocate."%' AND ";
		// if(trim($location)!='')
		// $keySearchCond .= "locationid = '".$location."' AND ";
	    if(trim($location)!='')
	    $keySearchCond .= "s.locationid IN (".$location.") AND ";
		if(trim($date1)!='')
		$keySearchCond .= "s.sdate BETWEEN '". $date1. "' and '". $date."' AND";
		
		//if($keySearchCond!='')
		//$keySearchCond = " AND (".substr($keySearchCond, 0, -4).") ";
		$session_data = $this->session->userdata('logged_in');
	  	
	    $userId = $session_data['id'];
		if($searchtype == "totalsubmissions"){
		
		$query = $this->db->query("SELECT count(*) as coutsub FROM submissions s, submissions_attachment sa where s.id=sa.submissionsid AND ".$keySearchCond." `psub` LIKE '%".$tsearch."%'");			
		}else{
		
		$query = $this->db->query("SELECT count(*) as coutsub FROM submissions s, submissions_attachment sa where s.id=sa.submissionsid AND ".$keySearchCond."  `psub` LIKE '%".$tsearch."%' AND s.userid='".$userId."'");
		}
		
		return $query->result();
	}


	function gettotsubmissions_count_category($searchtype, $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $visa, $relocate, $catID){
		$keySearchCond = '';
		if(trim($ksearch1)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch1."%' AND ";
		if(trim($ksearch2)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch2."%' AND ";
		if(trim($ksearch3)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch3."%' AND ";
		if(trim($ksearch4)!='')
		$keySearchCond .= "sa.resumecopy LIKE '%".$ksearch4."%' AND ";
		if(trim($cname)!='')
		$keySearchCond .= "s.cname LIKE '%".$cname."%' AND ";
		if(trim($yrsexp)!='')
		$keySearchCond .= "s.totalit >= '".$yrsexp."' AND ";
		// if(trim($visa)!='')
		// $keySearchCond .= "visa = '".$visa."' AND ";
		if(trim($visa)!='')
		$keySearchCond .= "s.visa IN (".$visa.") AND ";
		if(trim($relocate)!='')
		$keySearchCond .= "s.relocate LIKE '%".$relocate."%' AND ";
		// if(trim($location)!='')
		// $keySearchCond .= "locationid = '".$location."' AND ";
		if(trim($location)!='')
		$keySearchCond .= "s.locationid IN (".$location.") AND ";
		if(trim($date1)!='')
		$keySearchCond .= "s.sdate BETWEEN '". $date1. "' and '". $date."' AND ";

	    $keySearchCond .= "s.submission_category =".$catID." AND";
		
		//if($keySearchCond!='')
		//$keySearchCond = " AND (".substr($keySearchCond, 0, -4).") ";
		$session_data = $this->session->userdata('logged_in');
	  	
	    $userId = $session_data['id'];
		if($searchtype == "totalsubmissions"){
		
		$query = $this->db->query("SELECT count(*) as coutsub FROM submissions s, submissions_attachment sa where s.id=sa.submissionsid AND ".$keySearchCond." `psub` LIKE '%".$tsearch."%'");			
		}else{
		
		$query = $this->db->query("SELECT count(*) as coutsub FROM submissions s, submissions_attachment sa where s.id=sa.submissionsid AND ".$keySearchCond."  `psub` LIKE '%".$tsearch."%' AND s.userid='".$userId."'");
		}
		
		return $query->result();
	}

	
	function addlog($username, $count, $message, $datatime){
		$data = array(
				'manager_name' => $username,
				'count' => $count,
				'log_msg' => $message,
				'date_time' => $datatime
		);
		$this->db->insert('log', $data);
	}
	function log_count(){
		$result =$this->db->query('SELECT count(*) as countlog FROM log');
		return $result->result();
	}
	function log_result($limit, $offset){
		$result =$this->db->query('SELECT * FROM LOG ORDER BY date_time DESC Limit '.$offset.','.$limit);
		//print_r($result); die;
		return $result;
	}
	
	function log_view($logid){
		$result =$this->db->query('SELECT log_msg FROM log where logid = '.$logid);
		return $result->result();
	}

	function get_manager(){
		$result =$this->db->query('SELECT id, username FROM users where view=0');
		return $result->result();
	}

	function get_all_manager(){
		$result =$this->db->query('SELECT * FROM users where view=0');
		return $result->result();
	}

	function get_manager_category($catID){
		$result =$this->db->query('SELECT id, username FROM users where view=0 AND user_category='.$catID);
		return $result->result();
	}

	function get_manager_from_id($ID){
		$result =$this->db->query('SELECT username FROM users where id='.$ID);
		$managers = $result->result();

		$managerName = '';
		foreach($managers as $manager) {
			$managerName = $manager->username;
		}

		return $managerName;
	}

	function get_manager_from_category($catID){
		$result =$this->db->query('SELECT id, username FROM users where view=0 AND user_category='.$catID);
		return $result->result();
	}

	/**retrive managers list by director id */

	function get_managers($directorId){
		$this->db->select('manager_id');
		$this->db->from('map_dir_mgr');
		$this->db->where('director_id', $directorId);
		$this->db->where('isActive', 1);
		$result = $this->db->get();
		return $result->result();	
	}

	/*rectrive directors list */

	function get_directors(){
		$this->db->select('id, username');
		$this->db->from('users');
		$this->db->where('usertype', 'DIRECTOR');
		$result = $this->db->get();
		return $result->result();	
	}


	/**Old get recruiter function */

	// function get_reqruiter($managerId){
	// 	$condition = '';
	// 	if($managerId != '') {
	// 		$condition = ' AND mid ='.$managerId;
	// 	}
	// 	$result =$this->db->query("SELECT id, recruiter FROM recruiters WHERE status='ACTIVE'".$condition);
	// 	return $result->result();
		
	// }

	/*retrieve recruiters by managerid from map_mgr_recruiter table*/
	function get_reqruiter($managerId){
		$this->db->select('recruiter_id');
		$this->db->from('map_mgr_recruiter');
		$this->db->where('manager_id', $managerId);
		$this->db->where('isActive', 1);
		$result = $this->db->get();
		return $result->result();	
	}

	/**retrieve recruiter name by id */

	function get_recruiter_from_id($ID){
		$this->db->select('recruiter');
		$this->db->from('recruiters');
		$this->db->where('id', $ID);
		$result =$this->db->get();
		$recruiters = $result->result();

		$recruiterName = '';
		foreach($recruiters as $recruiter) {
			$recruiterName = $recruiter->recruiter;
		}

		return $recruiterName;
	}
	
	function gerecruitername($id){
		
		$this->db->select('mid, recruiter');
		$this->db->from('recruiters');
		$this->db->where("id", $id);
		$query=$this->db->get();
		
		//$result =$this->db->query("SELECT mid, recruiter FROM  recruiters WHERE id=".$id);
		return $query->row();	
	}
	
	function addinterviews($date, $intval, $plaval, $bacval, $recruiterId, $managerId){
		$data = array(
				'recruiterId' => $recruiterId,
				'managerid' => $managerId,
				'date' => $date,
				'intval' => $intval,
				'plaval' => $plaval,
				'bacval' => $bacval
		);
		$this->db->insert('interview_value', $data);
	}

	function getLocationByID($id) {
		$query_result =  $this->db->select('*')
		                		  ->from('location')
		                		  ->where('lid', $id)
		                		  ->limit(1)
		                		  ->get()->result(); 
        $location = ''  ;
        foreach ($query_result as $value) {
        	// return $value->location;
        	return $value->st;
        }
        return $location;
	}

	function getEmailByManagerID($id) {
		$query_result =  $this->db->select('*')
		                		  ->from('users')
		                		  ->where('id', $id)
		                		  ->limit(1)
		                		  ->get()->result(); 
        $email = ''  ;
        foreach ($query_result as $value) {
        	return $value->email;
        }
        return $email;
	}



// Requirement Section Local

function getAllRequirements() {
		return $this->db->select('*')
		                ->from('requirements')
		                ->order_by("id", "DESC")
		                ->get()->result(); 
	}

	function getRequirementsByUserID($userID) {
		return $this->db->select('*')
		                ->from('requirements')
		                ->where('posted_by', $userID)
		                ->order_by("id", "DESC")
		                ->get()->result(); 
	}

	function getAllLocations() {
		return $this->db->select('*')
		                ->from('location')
		                ->order_by("location", "ASC")
		                ->get()->result();
	}
    
    function insertReqiurement($row) {
	$this->db->insert('requirements', $row);
    }

    function getRequirementByID($id) {
    	return $this->db->select('*')->from('requirements')->where('id',$id)->limit(1)->get()->result();
    }

    function updateRequirement($req_id, $row) {		
		$this->db->where('id', $req_id);
		$this->db->update('requirements', $row);	 
	}

	function deleteRequirement($id) {
        
        $this->db->trans_start();
		$query_result = $this->db->select('*')
		                		 ->from('requirements')
						         ->where('id ',$id)
						         ->limit(1)->get()->result();
        $this->db->trans_complete(); 
        foreach ($query_result as  $value) {

        $session_data = $this->session->userdata('logged_in');
        	
      if($value->posted_by == $session_data['id'] || $session_data['usertype'] == 'SUPERADMIN') { 
                 
                 $getpermalink = $this->getPermalinkByID($id);
			 	 if($getpermalink != '') {
			 	 $this->deleteRequirementByPermalinkLive($getpermalink);
			 	 }

			 	 $this->db->trans_start();
        		 $this->db->where('id', $id);
                 $this->db->delete('requirements');
                 $this->db->trans_complete();

                 if($value->posted_by == $session_data['id']) {
                 redirect('requirement', 'refresh'); 		
                 } else {
                 redirect('requirement/all', 'refresh');
                 }
        	}
        }
        redirect('cdslogpage', 'refresh'); 
	}


	function getPermalinkByID($id) {
       
		$permalink = '';
		
		$this->db->trans_start();
		$query_result =  $this->db->select('permalink')
		                		  ->from('requirements')
		                          ->where('id', $id)
		                          ->get()->result(); 
		$this->db->trans_complete();                          

		if(sizeof($query_result) > 0) {
			foreach ($query_result as $value) {
				$permalink = $value->permalink;
			}
		}

		return $permalink;                
	}

	function generatePermalink($keyword) {
		$this->db->trans_start();
		$query_result = $this->db->select('*')
		                		 ->from('requirements')
						         ->where('permalink =',$keyword)
						         ->limit(1)->get()->result();
        $this->db->trans_complete(); 
		if(sizeof($query_result) > 0) {
            
            $i           = 1;
            $generate    = FALSE;
            $new_keyword = $keyword;

            while ($generate == FALSE) {

                $combinekeyword = $new_keyword.'-'.$i;
                
                $this->db->trans_start();
            	$check_result = $this->db->select('*')
		                		         ->from('requirements')
						                 ->where('permalink =',$combinekeyword)
						                 ->limit(1)->get()->result();
				$this->db->trans_complete(); 		                 

				if(sizeof($check_result) > 0) {		                 
				$generate == FALSE;
			    } else {
			    $generate == TRUE;
                return $combinekeyword;
			    }

			   $i++;		       
            }

		} else {
			return $keyword;
		}				
	}

// End of Requirement Section Local


// Requirement Section Live

    
    function insertReqiurementLive($row) {

	$dsn2 = 'mysql://C270751_panzerso:pCvX1dRg4c@mysql903.ixwebhosting.com:3306/C270751_panzersolutions';
    $this->db2 = $this->load->database($dsn2, true);

	$this->db2->insert('requirements', $row);
    }


    function updateRequirementLive($req_id, $row) {	

        $dsn2 = 'mysql://C270751_panzerso:pCvX1dRg4c@mysql903.ixwebhosting.com:3306/C270751_panzersolutions';
        $this->db2 = $this->load->database($dsn2, true);

		$this->db2->where('id', $req_id);
		$this->db2->update('requirements', $row);	 
	}

	function updateRequirementByPermalinkLive($permalink, $row) {	

        $dsn2 = 'mysql://C270751_panzerso:pCvX1dRg4c@mysql903.ixwebhosting.com:3306/C270751_panzersolutions';
        $this->db2 = $this->load->database($dsn2, true);

		$this->db2->where('permalink', $permalink);
		$this->db2->update('requirements', $row);	 
	}

	function deleteRequirementLive($id) {

		$dsn2 = 'mysql://C270751_panzerso:pCvX1dRg4c@mysql903.ixwebhosting.com:3306/C270751_panzersolutions';
        $this->db2 = $this->load->database($dsn2, true);
        
        $this->db2->trans_start();
		$query_result = $this->db2->select('*')
		                		  ->from('requirements')
						          ->where('id ',$id)
						          ->limit(1)->get()->result();
        $this->db2->trans_complete(); 
        foreach ($query_result as  $value) {

        $session_data = $this->session->userdata('logged_in');
        	
      if($value->posted_by == $session_data['id'] || $session_data['usertype'] == 'SUPERADMIN') { 
        		 $this->db2->trans_start();
        		 $this->db2->where('id', $id);
                 $this->db2->delete('requirements');
                 $this->db2->trans_complete();

                 if($value->posted_by == $session_data['id']) {
                 redirect('requirement', 'refresh'); 		
                 } else {
                 redirect('requirement/all', 'refresh');
                 }
        	}
        }
        redirect('cdslogpage', 'refresh'); 
	}


	function deleteRequirementByPermalinkLive($permalink) {
		$dsn2 = 'mysql://C270751_panzerso:pCvX1dRg4c@mysql903.ixwebhosting.com:3306/C270751_panzersolutions';
        $this->db2 = $this->load->database($dsn2, true);
        $this->db2->trans_start();
	    $this->db2->where('permalink', $permalink);
        $this->db2->delete('requirements');
        $this->db2->trans_complete();
	}

// End of Requirement Section Live




// Job Title Section

	function filterSuperAdmin() {

		$result = false;
		if($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			if($session_data['usertype'] == 'SUPERADMIN') {
					$result = true;
			}
		}

		if(!$result)
		redirect('cdslogpage', 'refresh');	
	}

	function getAllJobTitles() {
		return $this->db->select('*')
		                ->from('job_title')
		                ->order_by("id", "DESC")
		                ->get()->result(); 
	}

	function getJobTitleByID($id) {
		return $this->db->select('*')
		                ->from('job_title')
		                ->where('id', $id)
		                ->limit(1)
		                ->get()->result(); 
	}

	function getJobTitleByTitle($title) {
		return $this->db->select('*')
		                ->from('job_title')
		                ->where('title', $title)
		                ->limit(1)
		                ->get()->result(); 
	}

    
    function insertJobTitle($row) {
	$this->db->insert('job_title', $row);
    }

    function updateJobTitle($id, $row) {		
		$this->db->where('id', $id);
		$this->db->update('job_title', $row);	 
	}

	function deleteJobTitle($id) {
        
        $this->db->trans_start();
		$this->db->where('id', $id);
        $this->db->delete('job_title');
        $this->db->trans_complete();
        
        redirect('jobtitle', 'refresh'); 
	}


// End of Job Title Section




// For search

	function getJobTitlesBySearchLimit($search, $limit){

		$searchArray   = explode(' ', $search); 

		if($search != '' && sizeof($searchArray) > 0) {

			$this->db->select("*");
			$this->db->from("job_title");

			if(sizeof($searchArray) > 1) {

						$this->db->like('title', $search);

				foreach($searchArray as $term) {

						$this->db->like('title', $term);
				}

			} else {
				$this->db->like('title', $search);
			}


			$this->db->limit($limit);
			$query = $this->db->get();
			return $query->result();
		}


	}



	function getRecruiterBySearchLimit($search, $limit){

		$searchArray   = explode(' ', $search); 

		if($search != '' && sizeof($searchArray) > 0) {

			$this->db->select("*");
			$this->db->from("recruiters");

			if(sizeof($searchArray) > 1) {

						$this->db->like('recruiter', $search);

				foreach($searchArray as $term) {

						$this->db->like('recruiter', $term);
				}

			} else {
				$this->db->like('recruiter', $search);
			}

			$this->db->where('status','ACTIVE');
			$this->db->order_by("recruiter", "ASC"); 
			$this->db->limit($limit);
			$query = $this->db->get();
			return $query->result();
		}


	}



	function getSubmissionByID($id){
		$this->db->select("*");
		$this->db->from("submissions");
		$this->db->where('id = '. $id);
		
		$query = $this->db->get();
		return $query->result();
	}


	/*function calculatePercentage($val, $total) {
		return round((int)$val * 100 / $total, 2);
	}

	function averagePercentage($total, $number) {
		return round((int)$total/$number, 2);
	}*/
	function calculatePercentage($val, $total) {
		if($total == 0){
			return 0;
		} else{
			return round((int)$val * 100 / $total, 2);
		}
	}

	function averagePercentage($total, $number) {
		if($number == 0){
			return 0;
		} else{
			return round((int)$total/$number, 2);
		}
		
	}
	

// End for search


function updateSubmissionsStatus($id, $status) {

		$data = array('status' => $status);

		if($status == 'Submitted to Client') {
			$data['submitted_to_Client'] 	= 1;
		}

		else if($status == 'Interview') {
			$data['interviews'] 			= 1;
		}

		else if(in_array($status, $this->rejectionArray)) {
			$data['rejections'] 			= 1;
		}

	    $this->db->where('id', $id);
	    $this->db->update('submissions', $data);   

  }
  function commentsBySubmissionsID($id){
		$query=$this->db->query("select comment, username, ondate from comments, users where users.id=comments.uid AND comments.sid={$id} order by comments.ondate DESC");
		return $query->result();
	}

  function totalsubmissions() {
  	$query 	= $this->db->query('SELECT COUNT(*) AS total from submissions');
  	return $query->result_array()[0]['total'];
  }	

  /*fetching client names*/

	function getClientNamesBySearchLimit($search, $user_id, $limit){

		$searchArray   = explode(' ', $search); 

		if($search != '' && sizeof($searchArray) > 0) {

			$this->db->select("*");
			$this->db->from("user_clients");
			$this->db->where("user_id", $user_id);
			if(sizeof($searchArray) > 1) {
				$this->db->like('client_name', $search);
				foreach($searchArray as $term) {
					$this->db->like('client_name', $term);
				}

			} else {
				$this->db->like('client_name', $search);
			}
			$this->db->limit($limit);
			$query = $this->db->get();
			return $query->result();
		}
	}

	/*fetching client name with Job title*/

	function getClientWithjobTitle($search, $limit){

		$searchArray   = explode(' ', $search); 

		$session_data = $this->session->userdata('logged_in');
		$userId = $session_data['id'];

		if($search != '' && sizeof($searchArray) > 0) {

			$this->db->select("job_title, client_company");
			$this->db->from("requirements");
			$this->db->where('user_id ', $userId);
			if(sizeof($searchArray) > 1) {
				$this->db->like('job_title', $search);
				$this->db->or_like('client_company', $search);
				foreach($searchArray as $term) {
					$this->db->like('job_title', $term);
					$this->db->or_like('client_company', $term);
				}

			} else {
				$this->db->like('job_title', $search);
				$this->db->or_like('client_company', $search);
			}
			$this->db->limit($limit);
			$query = $this->db->get();
			return $query->result();
		}
	}


	/*fetching cities names*/

	function getCitiesNamesBySearchLimit($search, $limit){
		$searchArray   = explode(' ', $search);
		if($search != '' && sizeof($searchArray) > 0) {

			$this->db->select("*");
			$this->db->from("cities");
			
			if(sizeof($searchArray) > 1) {
				$this->db->like('city', $search);
				foreach($searchArray as $term) {
					$this->db->like('city', $term);
				}

			} else {
				$this->db->like('city', $search);
			}
			$this->db->limit($limit);
			$query = $this->db->get();
			return $query->result();
		}
	}

	function getStateCode($selected_city){
		$selected_city = "'$selected_city'";
		$this->db->select("state_code");
		$this->db->from("cities");
		$this->db->where('city LIKE '. $selected_city);
		$query = $this->db->get();
		return $query->result();
	}

	function set_location($state_code){
		$state_code = "'$state_code'";
		$this->db->select("*");
		$this->db->from("location");
		$this->db->where('st LIKE '. $state_code);
		$query = $this->db->get();
		//return $query->result();
		return $query->result();
	}

	/**get submissions by recruiterid */

	function get_submissions_result_by_recrID($limit, $offset, $recrID){

		//$result =$this->db->query('SELECT s.id, s.userid, s.sdate, s.cname, s.psub, s.totalit, s.company, l.st, s.vcompany, s.vcontact, s.crate, v.visatype, s.relocate, s.srate, s.status, s.employerdetails, (SELECT username FROM `users` WHERE `id` = s.userid) manager, (SELECT recruiter FROM `recruiters` WHERE `id` = s.submittedby) recruiter, s.view FROM submissions s LEFT OUTER JOIN visa v ON s.visa = v.vid LEFT OUTER JOIN location l ON s.locationid = l.lid WHERE s.view = 0 AND s.submittedby = '.$recrID.' ORDER BY s.sdate DESC Limit '.$offset.','.$limit);

		$this->db->select('s.id, s.userid, s.sdate, s.cname, s.psub, s.totalit, s.company, l.st, s.vcompany, s.vcontact, s.crate, v.visatype, s.relocate, s.srate, s.status, s.employerdetails, (SELECT username FROM `users` WHERE `id` = s.userid) manager, (SELECT recruiter FROM `recruiters` WHERE `id` = s.submittedby) recruiter, s.view');
		$this->db->from('submissions as s');
		$this->db->join('visa as v', 'v.vid = s.visa', 'inner');
		$this->db->join('location as l', 'l.lid = s.locationid', 'inner');
		$this->db->where('s.view', 0);
		$this->db->where_in( 's.submittedby', $recrID);
		$this->db->order_by('s.sdate', 'DESC');
		$this->db->limit($limit, $offset);
		$result = $this->db->get();
		
		return $result;
	}

	/**get submissions by manager ID */

	function get_submissions_result_by_mgrID($limit, $offset, $mgrIDS){
		//print_r($mgrID);exit;
		//$result =$this->db->query('SELECT s.id, s.userid, s.sdate, s.cname, s.psub, s.totalit, s.company, l.st, s.vcompany, s.vcontact, s.crate, v.visatype, s.relocate, s.srate, s.status, s.employerdetails, (SELECT username FROM `users` WHERE `id` = s.userid) manager, (SELECT recruiter FROM `recruiters` WHERE `id` = s.submittedby) recruiter, s.view FROM submissions s LEFT OUTER JOIN visa v ON s.visa = v.vid LEFT OUTER JOIN location l ON s.locationid = l.lid WHERE s.view = 0 AND s.userid = array('.$mgrID.') ORDER BY s.sdate DESC Limit '.$offset.','.$limit);

		$this->db->select('s.id, s.userid, s.sdate, s.cname, s.psub, s.totalit, s.company, l.st, s.vcompany, s.vcontact, s.crate, v.visatype, s.relocate, s.srate, s.status, s.employerdetails, (SELECT username FROM `users` WHERE `id` = s.userid) manager, (SELECT recruiter FROM `recruiters` WHERE `id` = s.submittedby) recruiter, s.view');
		$this->db->from('submissions as s');
		$this->db->join('visa as v', 'v.vid = s.visa', 'inner');
		$this->db->join('location as l', 'l.lid = s.locationid', 'inner');
		$this->db->where('s.view', 0);
		$this->db->where_in( 's.userid', $mgrIDS);
		$this->db->order_by('s.sdate', 'DESC');
		$this->db->limit($limit, $offset);
		$result = $this->db->get();
		return $result;
	}

	/**adding interview details */
	function insert_interview_details($submission_id, $data){
		//print_r($data);die();
		$this->db->where('id', $submission_id);
		$this->db->update('submissions', $data);
		return "interview details added successfully";
	}

	/*fetchig usertype data from users table*/
	function get_usertype_data(){
		$this->db->select('usertype');
		$this->db->distinct('usertype');
		$this->db->from('users');
		$result = $this->db->get();
		return $result->result();

		// $sql = "SHOW COLUMNS FROM `users` LIKE 'usertype'";
		// $result = $this->db->query($sql);
		// $row = $result->result();
		// $type = $row[0]->Type;
		// preg_match('/enum\((.*)\)$/', $type, $matches);
		// $vals = explode(',', $matches[1]);
		// return $vals;
	}

	function update_recruiter_data($data, $id){
		$this->db->where('id', $id);
    	$this->db->update('recruiters', $data);
		return "updated";
	}

	/**get menu usertype */

	function getMenuAsUserType($userType){
		$query 	= $this->db->query("SELECT mrm.role_id, me.manu_name, me.menu_url, me.id, me.category, me.parent_id FROM map_role_menu as mrm JOIN menu AS me ON me.id = mrm.menu_id JOIN role_mgmt AS rmgmt ON rmgmt.id = mrm.role_id WHERE rmgmt.user_category= ".$userType." AND mrm.isActive = 1 AND me.isActive = 1 ORDER BY me.id");
		//print_r($this->db);
		  return $query->result();
	  }
}
?>
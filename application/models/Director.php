<?php

Class Director extends CI_Model {
    
    function getAllDirectors(){
		$this->db->select('id, username');
		$this->db->from('users');
		$this->db->where('usertype', 'DIRECTOR');
		$result = $this->db->get();
		return $result->result();
	}
    /**getting all directors, associate directors and managers data */
    function getDirs_Assodir_Mgrs(){
        $usertypes = array("DIRECTOR","ASSOCIATEDIRECTOR","MANAGER");
		$this->db->select('id, username');
		$this->db->from('users');
		$this->db->where_in('usertype', $usertypes);
		$result = $this->db->get();
		return $result->result();
	}
}
<?php

class Recruiter extends CI_Model
{


	function getAllRecruiters()
	{
		$result = $this->db->query('SELECT id, recruiter FROM recruiters WHERE status="ACTIVE"');
		return $result->result();
	}

	// function getRecruitersByManagerID($managerID){
	// 	$result = $this->db->query('SELECT id, recruiter FROM recruiters WHERE status="ACTIVE" AND mid='.$managerID);
	// 	return $result->result();
	// }

	// function getRecruitersByManagerID($managerID){
	// 	$this->db->select('id, recruiter');
	// 	$this->db->from('recruiters');
	// 	$this->db->where('status', 'ACTIVE');
	// 	$this->db->where('mid', $managerID);

	// 	$result = $this->db->get();
	// 	return $result->result();
	// }

	function getRecruitersByManagerID($managerID)
	{
		$this->db->select('recruiter_id');
		$this->db->from('map_mgr_recruiter');
		$this->db->where('isActive', 1);
		$this->db->where('manager_id', $managerID);

		$result = $this->db->get();
		return $result->result();
	}

	function getRecruiterByID($recruiterID)
	{
		$result = $this->db->query('SELECT id, recruiter FROM recruiters WHERE status="ACTIVE" AND id=' . $recruiterID);
		return $result->result();
	}


	/*function getRecruiterTarget($recruiterID) {
		$query 		= 	mysql_query("SELECT sum(targets) FROM recruiters WHERE id={$recruiterID} AND status='ACTIVE'");
		return 			mysql_result($query, 0);
	}

	function getRecruiterAttendance($recruiterName, $date) {
		$query  = 	mysql_query("SELECT id FROM recruiters_attendance WHERE rec='{$recruiterName}' AND date = '{$date}' AND status = 1");
		return 		mysql_num_rows($query);
	}*/
	/*function getRecruiterTarget($recruiterID) {
		$query 		= 	$this->db->query("SELECT sum(targets) FROM recruiters WHERE id={$recruiterID} AND status='ACTIVE'");
		return 			$query->result();
	}

	function getRecruiterAttendance($recruiterName, $date) {
		$query  = 	$this->db->query("SELECT id FROM recruiters_attendance WHERE rec='{$recruiterName}' AND date = '{$date}' AND status = 1");
		return 		$query->result();
	}

	function getRecruiter($recruiterID) {

		$name 		= '';
		$query 		= $this->db->query('SELECT recruiter FROM recruiters WHERE id='.$recruiterID);
		$recruiters = $query->result();

		foreach ($recruiters as $value) {
			$name = $value->recruiter;
		}

		return $name;

	}


	function getRecruiterTotalTarget($recruiterID, $fromDate, $toDate, $totalDates) {

		$result['sumtarget'] 	= 0;
		$result['targets'] 		= 0;

		$query 		= $this->db->query("SELECT targets, recruiter FROM recruiters WHERE id={$recruiterID} AND status='ACTIVE'");
		$qResult 	= $query->result();

		foreach($qResult as $value) {

			$result['sumtarget']   	= $result['sumtarget'] + (int)$value->targets;
			
			$defaultTarget 			= (int)$value->targets;
			$absentDays 			= $this->countAbsentAttendanceDates($value->recruiter, $fromDate, $toDate)[0]->count;

			if($absentDays == 0) {
				$result['targets'] 	= $result['targets'] + $defaultTarget;
			} else {
				$reducedTarget 		= $defaultTarget - $absentDays*($defaultTarget/$totalDates);				
				$result['targets'] 	= $result['targets'] + $reducedTarget;				
			}
		}

		$result['targets'] = ($result['targets']/5)*$totalDates;				

		return $result;

	}


	function countAbsentAttendanceDates($recruiterName, $fromDate, $toDate) {
		$query 	= $this->db->query("SELECT count(*) as count FROM recruiters_attendance WHERE rec='{$recruiterName}' AND status=1 AND date BETWEEN '{$fromDate}' AND '{$toDate}'");
		return $query->result();

	}*/
	function getRecruiterTarget($recruiterID)
	{
		$query 		= 	$this->db->query("SELECT sum(targets) as targets FROM recruiters WHERE id={$recruiterID} AND status='ACTIVE'");
		//return 			$query->result();
		foreach ($query->result() as $row) {
			return $row->targets;
		}
	}

	function getRecruiterAttendance($recruiterName, $date)
	{
		//echo "SELECT id FROM recruiters_attendance WHERE rec='{$recruiterName}' AND date = '{$date}' AND status = 1";
		$query  = 	$this->db->query("SELECT id FROM recruiters_attendance WHERE rec='{$recruiterName}' AND date = '{$date}' AND status = 1");
		foreach ($query->result() as $row) {
			return $row->id;
		}
		//return 		$query->result();
	}

	function getRecruiter($recruiterID)
	{

		$name 		= '';
		$query 		= $this->db->query('SELECT recruiter FROM recruiters WHERE id=' . $recruiterID);
		$recruiters = $query->result();

		foreach ($recruiters as $value) {
			$name = $value->recruiter;
		}

		return $name;
	}


	function getRecruiterTotalTarget($recruiterID, $fromDate, $toDate, $totalDates)
	{

		
		$result['sumtarget'] 	= 0;
		$result['targets'] 		= 0;

		$query 		= $this->db->query("SELECT targets, recruiter FROM recruiters WHERE id={$recruiterID} AND status='ACTIVE'");
		$qResult 	= $query->result();

		foreach ($qResult as $value) {

			$result['sumtarget']   	= $result['sumtarget'] + (int)$value->targets;

			$defaultTarget 			= (int)$value->targets;
			$absentDays 			= $this->countAbsentAttendanceDates($value->recruiter, $fromDate, $toDate)[0]->count;

			if ($absentDays == 0) {
				$targets 	= $result['targets'] + $defaultTarget;
			} else {
				$reducedTarget 		= $defaultTarget - $absentDays * ($defaultTarget / $totalDates);
				$targets 	= $result['targets'] + $reducedTarget;
			}
		}

		$result['targets'] = ($targets / 5) * $totalDates;
		return $result;
	}


	function countAbsentAttendanceDates($recruiterName, $fromDate, $toDate)
	{
		$query 	= $this->db->query("SELECT count(*) as count123 FROM recruiters_attendance WHERE rec='{$recruiterName}' AND status=1 AND date BETWEEN '{$fromDate}' AND '{$toDate}'");
		$result =  $query->result();
		foreach ($result as $value) {
			$count = $value->count123;
		}
		return $count;
	}

	/**get recruiter attendance */
	function get_recruiter_attendance($recruiter_name, $date)
	{
		$this->db->select('status');
		$this->db->from('recruiters_attendance');
		$this->db->where('rec', $recruiter_name);
		$this->db->where('date', $date);
		$result = $this->db->get();
		//echo $this->db->last_query();exit;
		return $result;
	}

	function getRecruiterArrayTotalTarget($recruiterIDArray, $fromDate, $toDate, $totalDates) {
		$result['sumtarget'] 	= 0;
		$result['targets'] 		= 0;

		//$query 		= $this->db->query("SELECT targets, recruiter FROM recruiters WHERE id={$recruiterID} AND status='ACTIVE'");

		$this->db->select("SUM(targets) as targets, recruiter");
					$this->db->from("recruiters");
					
					$this->db->where_in('id', $recruiterIDArray);
					$this->db->where('status', "ACTIVE");
					
					$query = $this->db->get();

		$qResult 	= $query->result();

		foreach ($qResult as $value) {

			$result['sumtarget']   	= $result['sumtarget'] + (int)$value->targets;

			$defaultTarget 			= (int)$value->targets;
			$absentDays 			= $this->countAbsentAttendanceDates($value->recruiter, $fromDate, $toDate)[0]->count;

			if ($absentDays == 0) {
				$targets 	= $result['targets'] + $defaultTarget;
			} else {
				$reducedTarget 		= $defaultTarget - $absentDays * ($defaultTarget / $totalDates);
				$targets 	= $result['targets'] + $reducedTarget;
			}
		}

		$result['targets'] = ($targets / 5) * $totalDates;
		
		return $result;
	}
}

<?php

Class ClientDetails extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->library('session');
	}
	function insert_client_details($data){
		$this->db->insert('user_clients', $data);
		return 'Clients Details Added Successfully';
	}

	function getAllClientsDetails() {
		return $this->db->select('*')
		                ->from('user_clients')
		                ->order_by("id", "DESC")
		                ->get()->result(); 
	}

	function getAllClientsDetailsByID($id) {
		return $this->db->select('*')
		                ->from('user_clients')
						->where('user_id', $id)
		                ->order_by("client_name", "ASC")
		                ->get()->result();
	}

	function getClientDetailsByID($id) {
		return $this->db->select('*')
		                ->from('user_clients')
		                ->where('id', $id)
		                ->limit(1)
		                ->get()->result();
	}

	function updateclientdetails($id, $data) {		
		$this->db->where('id', $id);
		$this->db->update('user_clients', $data);
		return 'Clients Details Updated Successfully';	 
	}

	function getClientDetailsByUserID($user_id) {
		return $this->db->select('*')
		                ->from('user_clients')
		                ->where('user_id', $user_id)
		                ->get()->result();
	}

	function getClientNameByCompanyname($client_company) {
		return $this->db->select('client_name')
		                ->from('user_clients')
		                ->where('client_company', $client_company)
		                ->limit(1)
		                ->get()->result();
	}
}
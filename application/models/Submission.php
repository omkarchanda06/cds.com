<?php

Class Submission extends CI_Model {


	private $statusArray 		= array(
										'Submitted to Client' 	=> 0,
										'Interview' 			=> 0,
										'Placement' 			=> 0,
										'Rejected' 				=> 0
										);

	private $rejectionsArray 	= array(
										'Rejected (Reason: Comm Skills)',
										'Rejected (Reason: Position on Hold/Closed/Filled)',
										'Rejected (Reason: Technical Skills)',
										'Rejected (Reason: Rate Concern)',
										'Rejected (Reason: Location)',
										'Rejected',
										'Rejected After Interview'
									);


	private $statusKeys 		= array(
										'Submitted to Client' 	=> 'submitted_to_Client',
										'Interview' 			=> 'interviews',
										'Rejected' 				=> 'rejections'
										);


	private $result 			= array();



	public function getSubmissionsStat($conditionQuery, $fromDate, $toDate) {

		$result['graphTitle']	= 'Submission graph from '.$fromDate.' to '.$toDate;

		foreach($this->statusArray as $key => $value) {

				$statusQuery = " status = '".$key."'";

		if($key != 'Placement') {
				$statusQuery = " ".$this->statusKeys[$key]." = 1";				
			}

		// count number of rows with this status	
		$totalStatus = $this->db->query("SELECT count(status) FROM submissions WHERE ".$statusQuery.$conditionQuery." AND sdate BETWEEN '". $fromDate. "' AND '". $toDate."'");

		// update key with count value
		
		//$this->statusArray[$key]   = $query->result();
		$this->statusArray[$key]   = $totalStatus->result();

		}
		
		$result['statusArray'] 	= $this->statusArray;

		return $result;

	}




	public function getDatesArray($fromDate, $toDate) {

		$dates 	= array();
		$diff	= round((strtotime($toDate)-strtotime($fromDate))/(60*60*24))+1;
		
		for ($i = 0; $i < $diff; $i++) {
			$d  	= date("Y-m-d", (strtotime($fromDate) + ($i * 86400)));
			$t  	= strtotime($d); 
			$day 	= date('w', $t);
			
			if($day == 0 || $day == 6){
			}	else 	{
			$dates[] = $d;
			}
			
		}
		
		return $dates;

	}


	// Managers Submissions
	function getAllSubmissions($date, $date1){
		$this->db->select("userid as managerId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
		$this->db->from("submissions");
		$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
		$this->db->group_by("userid, sdate"); 
		
		$query = $this->db->get();
		return $query->result();
	}


	function getAllSubmissionsOfCategory($date, $date1, $catID){
		$this->db->select("userid as managerId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
		$this->db->from("submissions");
		$this->db->where('submission_category = ' .$catID);
		$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
		$this->db->group_by("userid, sdate"); 
		
		$query = $this->db->get();
			
		return $query->result();
	}


	function getAllSubmissionsOfManager($date, $date1, $managerID){
	
		$this->db->select("userid as managerId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
		$this->db->from("submissions");
		$this->db->where('userid = ' .$managerID);
		$this->db->where('sdate BETWEEN "'.$date. '" and "'.$date1.'"');
		$this->db->group_by("userid, sdate"); 
		
		$query = $this->db->get();
		return $query->result();
	}


	// Recruiter Submissions
	function getAllSubmissionsOfRecruiters($date, $date1){
		//echo $date." ".$date1;
		$this->db->select("submittedby as recruiterId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when status = 'No Feedback' then 1 else 0 end) as nofeedbacks, sum(case when status = 'Onhold' then 1 else 0 end) as onholds, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
		$this->db->from("submissions");
		$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
		$this->db->group_by("submittedby, sdate"); 
		
		$query = $this->db->get();
		//print_r($query->result());
		return $query->result();
	}

	// one Recruiter Submissions
	function getAllSubmissionsOfRecruiter($date, $date1, $recruiter_id){
		//echo $date." ".$date1." ".$recruiter_id;
		$this->db->select("count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when status = 'No Feedback' then 1 else 0 end) as nofeedbacks, sum(case when status = 'Onhold' then 1 else 0 end) as onholds, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
		$this->db->from("submissions");
		$this->db->where('submittedby = ' .$recruiter_id);
		$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
		$this->db->group_by("submittedby, sdate"); 
		$query = $this->db->get();
		return $query->result();
	}


	function getAllSubmissionsOfCategoryOfRecruiters($date, $date1, $catID){
		$this->db->select("submittedby as recruiterId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
		$this->db->from("submissions");
		$this->db->where('submission_category = ' .$catID);
		$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
		$this->db->group_by("submittedby, sdate"); 
		
		$query = $this->db->get();
		return $query->result();
	}


	function getAllSubmissionsOfRecruitersByManagerID($date, $date1, $managerID){
		$this->db->select("submittedby as recruiterId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when status = 'No Feedback' then 1 else 0 end) as nofeedbacks, sum(case when status = 'Onhold' then 1 else 0 end) as onholds, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
		$this->db->from("submissions");
		$this->db->where('userid = ' .$managerID);
		$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
		$this->db->group_by("submittedby, sdate"); 
		
		$query = $this->db->get();
		return $query->result();
	}


	function getInterviewListofManager($fromDate, $toDate, $managerID) {
		$this->db->select("*");
		$this->db->from("submissions");
		$this->db->where('userid = ' .$managerID);
		// $this->db->where('interviews = 1');
		$this->db->where('status', 'Interview');
		$this->db->where('interview_date BETWEEN "'. $fromDate. '" and "'. $toDate.'"');
		
		$query = $this->db->get();
		return $query->result();	
	}


	function countInterviewOfRecruiter($fromDate, $toDate, $recruiterID) {
		$this->db->select("*");
		$this->db->from("submissions");
		$this->db->where('submittedby = ' .$recruiterID);
		$this->db->where('interviews = 1');
		$this->db->where('interview_date BETWEEN "'. $fromDate. '" and "'. $toDate.'"');
		
		$query = $this->db->get();
		return $query->num_rows();
	}


	function countInterviewOfManager($fromDate, $toDate, $managerID) {
		$this->db->select("*");
		$this->db->from("submissions");
		$this->db->where('userid = ' .$managerID);
		$this->db->where('interviews = 1');
		$this->db->where('interview_date BETWEEN "'. $fromDate. '" and "'. $toDate.'"');
		
		$query = $this->db->get();
		return $query->num_rows();
	}



	function createSubmissionsInfoArrayForManager($submissions) {
			//$this->dumpDie($submissions);

			$subcount = array();

			foreach($submissions as $sub):
			
			$subcount[$sub->managerId][$sub->sdate]['submissioncount'] 		= $sub->submissioncount;

			if(!isset($subcount[$sub->managerId]['placements'])):
			$subcount[$sub->managerId]['placements'] 						= (int)$sub->placements;
			else:
			$subcount[$sub->managerId]['placements'] 						= $subcount[$sub->managerId]['placements'] + (int)$sub->placements;
			endif;

			if(!isset($subcount[$sub->managerId]['submitted_to_client'])):
			$subcount[$sub->managerId]['submitted_to_client'] 				= (int)$sub->submitted_to_client;
			else:
			$subcount[$sub->managerId]['submitted_to_client'] 				= $subcount[$sub->managerId]['submitted_to_client'] + (int)$sub->submitted_to_client;
			endif;

			if(!isset($subcount[$sub->managerId]['interviews'])):
			$subcount[$sub->managerId]['interviews'] 						= (int)$sub->interviews;
			else:
			$subcount[$sub->managerId]['interviews'] 						= $subcount[$sub->managerId]['interviews'] + (int)$sub->interviews;
			endif;

			if(!isset($subcount[$sub->managerId]['rejections'])):
			$subcount[$sub->managerId]['rejections'] 						= (int)$sub->rejections;
			else:
			$subcount[$sub->managerId]['rejections'] 						= $subcount[$sub->managerId]['rejections'] + (int)$sub->rejections;
			endif;

			endforeach;

			return $subcount;

	}



	function createSubmissionsInfoArrayForRecruiter($submissions) {

			$subcount = array();

			//$this->dumpDie($submissions);

			foreach($submissions as $sub):
			$subcount[$sub->recruiterId][$sub->sdate]['submissioncount'] 	= $sub->submissioncount;

			if(!isset($subcount[$sub->recruiterId]['placements'])):
			$subcount[$sub->recruiterId]['placements'] 						= (int)$sub->placements;
			else:
			$subcount[$sub->recruiterId]['placements'] 						= $subcount[$sub->recruiterId]['placements'] + (int)$sub->placements;
			endif;

			if(!isset($subcount[$sub->recruiterId]['submitted_to_client'])):
			$subcount[$sub->recruiterId]['submitted_to_client'] 			= (int)$sub->submitted_to_client;
			else:
			$subcount[$sub->recruiterId]['submitted_to_client'] 			= $subcount[$sub->recruiterId]['submitted_to_client'] + (int)$sub->submitted_to_client;
			endif;

			if(!isset($subcount[$sub->recruiterId]['interviews'])):
			$subcount[$sub->recruiterId]['interviews'] 						= (int)$sub->interviews;
			else:
			$subcount[$sub->recruiterId]['interviews'] 						= $subcount[$sub->recruiterId]['interviews'] + (int)$sub->interviews;
			endif;

			if(!isset($subcount[$sub->recruiterId]['rejections'])):
			$subcount[$sub->recruiterId]['rejections'] 						= (int)$sub->rejections;
			else:
			$subcount[$sub->recruiterId]['rejections'] 						= $subcount[$sub->recruiterId]['rejections'] + (int)$sub->rejections;
			endif;

			if(!isset($subcount[$sub->recruiterId]['nofeedbacks'])):
			$subcount[$sub->recruiterId]['nofeedbacks'] 						= (int)$sub->nofeedbacks;
			else:
			$subcount[$sub->recruiterId]['nofeedbacks'] 						= $subcount[$sub->recruiterId]['nofeedbacks'] + (int)$sub->nofeedbacks;
			endif;

			if(!isset($subcount[$sub->recruiterId]['onholds'])):
			$subcount[$sub->recruiterId]['onholds'] 						= (int)$sub->onholds;
			else:
			$subcount[$sub->recruiterId]['onholds'] 						= $subcount[$sub->recruiterId]['onholds'] + (int)$sub->onholds;
			endif;

			endforeach;
			return $subcount;

	}


	function dumpDie($variable) {
		echo "<pre>";
		var_dump($variable);
		echo "</pre>"; 
		die();
	}

	/*get managers list as per director* */

	function getManagersByDirector($dirID){
		$this->db->select('manager_id');
		$this->db->from('map_dir_mgr');
		$this->db->where('director_id', $dirID);
		$this->db->where('isActive', 1);
		$query = $this->db->get();
		return $query->result();
	}

	/**get all submissions of recruiters by managerid */

	function getAllSubmissionsOfManagerOfRecruiters($date, $date1, $mgrID){
		$this->db->select("submittedby as recruiterId, count(*) as submissioncount, sdate, sum(case when status = 'Placement' then 1 else 0 end) as placements, sum(case when submitted_to_Client = 1 then 1 else 0 end) as submitted_to_client, sum(case when interviews = 1 then 1 else 0 end) as interviews, sum(case when rejections = 1 then 1 else 0 end) as rejections");
		$this->db->from("submissions");
		$this->db->where('submission_category = ' .$mgrID);
		$this->db->where('sdate BETWEEN "'. $date. '" and "'. $date1.'"');
		$this->db->group_by("submittedby, sdate"); 
		
		$query = $this->db->get();
		return $query->result();
	}

// sum(case when interviews = 1 then 1 else 0 end) as interviews	

}
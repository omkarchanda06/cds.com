<?php
class Visa extends CI_Model {

	private $uploadFolder 	= 'visacopy';
	private $table 			= 'visa_copies';
	private $sort   		= 'desc';
	
	public function uploadVisaCopy($jobTitle, $candidateName, $fileSource) {

	  $fileName 	= date('Y-m-d-H-i-s').$fileSource['name'];		
	  $path 		= $this->uploadFolder.'/'.$fileName;

	  $row 			= array(
		  				'job_title' 		=> $jobTitle,
		  				'candidate_name' 	=> $candidateName,
		  				'path' 				=> $path
	  					);	

	  move_uploaded_file($fileSource["tmp_name"], $path);
	  $this->addVisaCopy($row);

	}

	public function addVisaCopy($row) {
		$this->db->insert($this->table, $row);
	}

	public function getVisaName($id) {
		$result = $this->db->where('vid', $id)->get('visa')->result(); 
		if(sizeof($result)>0) {
			foreach ($result as $res) {
				return $res->visatype;
			}
		}
	}



	public function getPaginateSearchResult($id, $jobtitle, $cname, $limit, $page) {
		
		if($this->sort == 'desc') {
        $this->db->order_by('id', 'desc');
        }


      	if($id != '') {
      	$this->db->where('id', $id);   
      	}

      	if($jobtitle != '') {
      	$this->db->like('job_title', $jobtitle);   
      	}

      	if($cname != '') {
      	$this->db->or_like('candidate_name', $cname);   
      	}

        if($page == 1) {

		$query = $this->db->get($this->table, $limit); 

	 	} else {

	 	$page = $page - 1;
	 	$page = $page*$limit;

	 	$query = $this->db->get($this->table, $limit, $page); 

	 	}

	 	//var_dump($this->db->last_query()); die();

		if(!$query->num_rows()){
			return FALSE;
		} else {
			return $query->result();
		}

	}


}

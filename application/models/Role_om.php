<?php

Class Role extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->library('session');
	}

	function filter() {
		
		if(!$this->session->userdata('logged_in')){
			redirect('cdslogpage', 'refresh');
		}
	}
	
	// Candidates
	function get_role() {
	
		$query=$this->db->get('role_mgmt');
		return $query->result();
	}
	
	function get_menu() {
		$this->db->select('*');
		$this->db->where('isActive', 1); 
		$this->db->order_by('id','ASC');
		$query=$this->db->get('menu');
		return $query->result();
	}

	function get_selected_menu($roleId) {
		$this->db->select('*');
		$this->db->where('role_id', $roleId); 
		$this->db->order_by('id','ASC');
		$query=$this->db->get('map_role_menu');
		return $query->result();
	}



}
<?php $this->load->view('includes/header'); ?>

<div id="page-heading"><h1>Add Job Title</h1></div>

<table width="100%" cellspacing="0" cellpadding="0" border="0" id="content-table">
<tbody>
<tr>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowleft.jpg"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowright.jpg"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo form_open('jobtitle/add'); ?>
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
	<tr valign="top" height="30">
		<td><span style="color:blue; line-height: 23px;"><?php if($message!=''){ echo $message; }?></span>	
			<table cellspacing="0" cellpadding="0" border="0" id="id-form" width="100%" >
			<tbody>
				<tr>
					<th valign="top">Job Title :</th>
					<td><input type="text" class="inp-form"  name="job_title" value="<?php echo $job_title;?>"
					     style="width:500px;" placeholder="Job Title"> 
					</td>
					<td style="color:red;"><?php echo form_error('job_title') ?></td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td valign="top" style="padding-top: 10px;">
						<input type="submit" name="addjobtitle" class="form-submit" value="Add Job Title"/>
						<input type="reset" class="form-reset" value="">
					</td>
					<td></td>
				</tr>
			
			</tbody>
			</table>
			<!-- end id-form  -->

		</td>
	</tr>
<!--<tr>
<td><img width="695" height="1" alt="blank" src="images/shared/blank.gif"></td>

</tr> -->
	</tbody>
	</table>
	</form>
	
	<div class="clear"></div>
	 
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</tbody>
</table>

<script type="text/javascript">
// 	$('#bill_rate').keydown(function (event) {
//     if (event.shiftKey == true) {
//         event.preventDefault();
//     }

//     if ((event.keyCode >= 48 && event.keyCode <= 57) || 
//         (event.keyCode >= 96 && event.keyCode <= 105) || 
//         event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
//         event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

//     } else {
//         event.preventDefault();
//     }

//     if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
//         event.preventDefault(); 
//     //if a decimal has been added, disable the "."-button
// });
</script>

<?php $this->load->view('includes/footer'); ?>

<?php $this->load->view('includes/header'); ?>

<div id="page-heading"><h1>Add / Edit User Form</h1></div>

<table width="100%" cellspacing="0" cellpadding="0" border="0" id="content-table">
<tbody>
<tr>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url()  ?>images/shared/side_shadowleft.jpg"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url()  ?>images/shared/side_shadowright.jpg"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
<?php //var_dump($keys);
				foreach($keys as $result) {
				//print_r($result);exit;
				?>	
	<form method="POST" action="<?php echo base_url()?>index.php/add_user/users_edit/<?php echo $result->id;?>">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
	<tr valign="top">
		<td>
		<?php
			//$error='';
			if($error!=''){
			echo $error;	
			}
			?>
			<!-- start id-form -->
			<table cellspacing="0" cellpadding="0" border="0" id="id-form" width="100%" >
			<tbody>
				
					<tr>
					<th colspan="2">If you are not changing the password leave the password fileds empty</th>					 
				</tr>				
				<tr>
					<th valign="top">User Name :</th>
					<td><?php echo $result->username;?>
					<input type="hidden" class="inp-form" name="uid" value="<?php echo $result->id;?>">

					<input type="hidden" class="inp-form" 
					       name="username" 
					       value="<?php
					        $username = str_replace(' Panzer', '', $result->username);
					        $username = str_replace(' RPO', '', $username);
					        $username = str_replace(' Canada', '', $username);
					        echo $username;?>">
					</td>
					<td></td>
				</tr>
				<tr>
					 <th>Password :</th>
					 <td><input type="password" class="inp-form" name="pwd"></td>
				</tr>
				<tr>
					 <th>Confirm Password :</th>
					 <td><input type="password" class="inp-form" name="cpwd"></td>
				</tr>

				<tr>
					<th>User Category :</th>
					 <td>
						<select name="ucat" class="styledselect_user">
						    <option <?php if($result->user_category == 1){ echo 'selected="selected"';} ?> value="1">Panzer</option>							
							<option <?php if($result->user_category == 2){ echo 'selected="selected"';} ?> value="2">RPO</option>
							<option <?php if($result->user_category == 3){ echo 'selected="selected"';} ?> value="3">Canada</option>							
							<option <?php if($result->user_category == 4){ echo 'selected="selected"';} ?> value="4">All (Super Admin)</option>							
						</select>
					 </td>
				</tr>
			
				<tr>
					<th>User Type :</th>
					 <td>
						<select name="utype" class="styledselect_user">
						    <option <?php if($result->usertype=='SUPERADMIN'){ echo 'selected="selected"';} ?> value="1">Super Admin</option>							
							<option <?php if($result->usertype=='DIRECTOR'){ echo 'selected="selected"';} ?> value="2">DIRECTOR</option>
							<option <?php if($result->usertype=='TA MANAGER'){ echo 'selected="selected"';} ?> value="4">TA MANAGER</option>							
						</select>
					 </td>
				</tr>
				<tr>	
					<th>Status :</th>
					 <td>
						<select name="status" class="styledselect_user">
							<option <?php if($result->status =='ACTIVE'){ echo 'selected="selected"';} ?> value="1">Active</option>
							<option <?php if($result->status=='INACTIVE'){ echo 'selected="selected"';} ?> value="2">Inactive</option>							
						</select>
					 </td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td valign="top">
						<input type="submit" name="updateuser" class="form-submit" value="updateuser">
						<input type="reset" class="form-reset" value="">
					</td>
					<td></td>
				</tr>
			<?php
			}
			?>
				
			</tbody>
			</table>
			<!-- end id-form  -->

		</td>
	</tr>
<!--<tr>
<td><img width="695" height="1" alt="blank" src="images/shared/blank.gif"></td>

</tr> -->
	</tbody>
	</table>
	</form>
	
	<div class="clear"></div>
	 
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</tbody>
</table>
<?php $this->load->view('includes/footer'); ?>

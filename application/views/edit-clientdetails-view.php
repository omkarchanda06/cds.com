<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url()?>css/profile.css" />
<link rel="stylesheet" href="<?php echo base_url()?>css/add-edit-user.css" />
<!-- <body> -->
  <!-- default menu -->
  <div class="default_nav">
    <div class="logo">
      <img src="<?php echo base_url()?>images/logo.png" alt="Panzer" />
    </div>
  </div>
  <div class="background">
    <div class="title-div">
      <p class="title">
      <?php if(!empty($result)){
        echo $result;
        }else{
            echo "Update Client Details";
        } ?>
      </p>
      <?php if($message!=''){ echo $message; }?>
    </div>
    <?php if(sizeof($clientdetails) > 0) { foreach ($clientdetails as $detail) { ?>
    <?php $attributes = array('name' => 'update-client-form', 'id' => 'update-client-form'); echo form_open('client/update', $attributes); ?>
        <div class="input-container">
        <div class="input-div">
            <div class="input-name">Contact Name :</div>
            <input class="selectpicker" placeholder="Contact Name" name="client_name" value="<?php echo $detail->client_name;?>" required/>
        </div>
        <div class="input-div">
            <div class="input-name">Company Name :</div>
            <input class="selectpicker" placeholder="Company Name" name="client_company" value="<?php echo $detail->client_company;?>" required/>
        </div>
        </div>
        <div class="input-container">
        <div class="input-div">
            <div class="input-name">Contact Number :</div>
            <input class="selectpicker" placeholder="Contact Number" name="client_contact" id="client_contact" value="<?php echo $detail->client_contact;?>" required/>
        </div>
        <span id="lblError" style="color: red; position: absolute;"></span>
        <div class="input-div">
            <div class="input-name">Email :</div>
            <input class="selectpicker"  placeholder="Email" type="email" name="client_email" value="<?php echo $detail->client_email;?>" required/>
        </div>
        <div class="input-div">
        <div class="input-name">Status :</div>
        <select name="client_status" class="selectpicker" placeholder="Status" id="client_status" required>
            <option value="">Select Status</option>
            <option value="ACTIVE" <?php if($detail->client_status == "ACTIVE"){echo "selected";} ?>>Active</option>
            <option value="PASSIVE" <?php if($detail->client_status == "PASSIVE"){echo "selected";} ?>>Passive</option>
            <option value="RISING" <?php if($detail->client_status == "RISING"){echo "selected";} ?>>Rising</option>
        </select>
        </div>
        </div>
        <div class="input-div" style="margin: auto; margin-bottom: 20px; width: 70%">
        <div class="input-name">Comments/ Signature :</div>
        <textarea class="selectpicker" name="comments"    placeholder="Comments" style="
                width: 500px;
                height: 150px;
                border: 2px solid #0087c4;
                border-radius: 10px;
            " required><?php echo $detail->comments;?></textarea>
        </div>
        <div class="form-btn">
            <input type="hidden" name="id" value="<?=$detail->id?>">
            <button class="cssbuttons-io-button" name="updateclientdetails">
                Update
                <div class="icon">
                <svg height="24" width="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z"
                    fill="currentColor"></path>
                </svg>
                </div>
            </button>
        <!-- clear form btn -->
        <!-- <button class="button">
            <div class="trash">
            <div class="top">
                <div class="paper"></div>
            </div>
            <div class="box"></div>
            <div class="check">
                <svg viewBox="0 0 8 6">
                <polyline points="1 3.4 2.71428571 5 7 1"></polyline>
                </svg>
            </div>
            </div>
            <span>Clear Form</span>
        </button> -->
        <!-- js for btn -->
        <!-- <script>
            document.querySelectorAll(".button").forEach((button) =>
            button.addEventListener("click", (e) => {
                if (!button.classList.contains("delete")) {
                    button.classList.add("delete");
                    document.getElementById("update-client-form").reset();
                    setTimeout(() => button.classList.remove("delete"), 3200);
                }
                e.preventDefault();
            })
            );
        </script> -->
        </div>
    <?php echo form_close(); }}?>
  </div>
  <!-- additional css -->
  <style>
    .input-div {
      width: 400px;
    }
  </style>

</html>
<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url()?>css/profile.css" />
<link rel="stylesheet" href="<?php echo base_url()?>css/add-edit-user.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/resume_db.css" />
<script src="<?php echo base_url() ?>assets/js/libs/jquery.min.js"></script>
<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script>
    function get_requiter(val) {
        $.post("<?php echo base_url(); ?>index.php/recruitercount/requiterlist", {
            mid: val
        }, function(result) {
            //alert(result);

            $("#reqruiterid").html(result);
        });
    }

    function get_manager(val) {
        $.post("<?php echo base_url(); ?>index.php/managercount/managerlist", {
            Dirid: val
        }, function(result) {
            //alert(result);
            $("#managerid").html(result);
        });
    }
    // get reports

    function get_report() {
        var directorId = $('#directorid').val();
        var mngrid = $('#managerid').val();
        var recid = $('#reqruiterid').val();
        var fromdate = $('#fromdate').val();
        var todate = $('#todate').val();
        //var status = $('#status').val();
        //if (mngrid != "" && recid != "" && fromdate != "" && todate != "") {
        $.post("<?php echo base_url(); ?>index.php/reports/getreports", {
            directorId: directorId,
            mngrid: mngrid,
            recid: recid,
            fromdate: fromdate,
            todate: todate
        }, function(result) {
            //alert(result);

            $("#res").html(result);
        });
        /*} else {
            alert('Please select All inputs');
        }*/

    }
</script>
<body style="font-family: Calibri;font-size: 12px;">
    <div class="reports-div">
        <div class="reports-inputs">
            <div class="container-fluid">
                <h2>
                    Reports
                    <!--small>Statistics Overview</small-->
                </h2>
                <div class="row">
                    <?php if ($usertype == "SUPERADMIN" || $usertype == "HR" || $usertype == "MIS") { ?>
                        <div class="col-md-2">
                            <!-- <label for="inputState">Directors</label> -->
                            <label>Select Director: </label>
                            <select name="directorid" id="directorid" class="inp-form" onchange="get_manager(this.value);" style="padding:5px; width:150px;">
                                <?php
                                if (!empty($directors)) {
                                    // echo '<option value="All">Select Director</option>';
                                    echo '<option value="All">All</option>';
                                    foreach ($directors as $dir) {
                                        $directorid = $dir->id;
                                        $directorname = $dir->username;
                                        echo '<option value="' . $directorid . '">' . $directorname . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    <?php } ?>
                    <?php if ($usertype == "SUPERADMIN" || $usertype == "DIRECTOR" || $usertype == "HR" || $usertype == "MIS") { ?>
                        <div class="col-md-2">
                        <label>Select Manager: </label>
                            <select name="managerid" id="managerid" class="inp-form" onchange="get_requiter(this.value);" style="padding:5px; width:150px;">
                                <!-- <option value="">Select Manager</option> -->
                                <option value="All">All</option>
                                <?php
                                if (!empty($managerslist)) {
                                    foreach ($managerslist as $mgr) {
                                        $managerid = $mgr->manager_id;
                                        $managername = $this->user->get_manager_from_id($mgr->manager_id);
                                        echo '<option value="' . $managerid . '">' . $managername . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    <?php } ?>
                    <?php if ($usertype == "SUPERADMIN" || $usertype == "DIRECTOR" || $usertype == 'MANAGER' || $usertype == "HR" || $usertype == "MIS") { ?>
                        <div class="col-md-2">
                        <label>Select Recruiter: </label>
                            <select name="reqruiterid" id="reqruiterid" class="inp-form" style="padding:5px; width:150px;">
                                <!-- <option value="">Select Recruiter</option> -->
                                <option value="All">All</option>
                                <?php
                                if (!empty($recruiterlist)) {
                                    foreach ($recruiterlist as $rcr) {
                                        // $recruiterid = $rcr->recruiter_id;
                                        $recruitername = $this->user->get_recruiter_from_id($rcr->recruiter_id);
                                        echo '<option value="' . $rcr->recruiter_id . '">' . $recruitername . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    <?php } ?>
                    <div class="col-md-2">
                        <label for="inputState">From : </label>
                        <input type="date" id="fromdate">
                    </div>
                    <div class="col-md-2">
                        <label for="inputState">To : </label>
                        <input type="date" id="todate">
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary form-control" id="getReports" onclick="get_report();">Submit</button>
                    </div>
                </div><br>
                <div id="res">
                    <div class="row">
                        <div class="col-lg-12">

                            <div id="myTabContent" class="tab-content">
                                <div class="tab-pane fade in active" id="home">
                                    <div class="content_accordion">
                                        <div class="panel-group" id="res">

                                        </div>
                                    </div>
                                    <!--accordion end-->
                                </div>
                                <div class="tab-pane fade" id="a">
                                    <div class="content_accordion">
                                        <div class="panel-group" id="ga">
                                            <p>No data Available</p>

                                        </div>
                                    </div>
                                    <!--accordion end-->
                                </div>
                                <div class="tab-pane fade" id="b">
                                    <div class="content_accordion">
                                        <div class="panel-group" id="gb">
                                            <p>No data Available</p>
                                        </div>
                                    </div>
                                    <!--accordion end-->
                                </div>
                                <!-- <div class="tab-pane fade" id="c">
                                <div class="content_accordion">
                                    <div class="panel-group" id="gc">
                                    <p>No data Available</p>
                                    </div>
                             </div>
accordion end
                           </div> -->
                                <div class="tab-pane fade" id="d">
                                    <div class="content_accordion">
                                        <div class="panel-group" id="gd">
                                            <p>No data Available</p>
                                        </div>
                                    </div>
                                    <!--accordion end-->
                                </div>
                                <div class="tab-pane fade" id="e">
                                    <div class="content_accordion">
                                        <div class="panel-group" id="ge">
                                            <p>No data Available</p>
                                        </div>
                                    </div>
                                    <!--accordion end-->
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>

                </div>
            </div>
        </div>
        <style>
        .nav-tabs>li.active>a,
        .nav-tabs>li.active>a:hover,
        .nav-tabs>li.active>a:focus {
            background-color: #0087C4 !important;
            border-radius: 10px;
            color: #fff !important;
        }

        .nav-tabs {
            padding: 15px;
            justify-content: center;
            display: flex !important;
            background-color: #f5f5f5 !important;
            border-radius: 15px;
            align-content: center;
            width: max-content;
            margin-left: auto;
            margin-right: auto
        }

        .nav-tabs li {
            border: none !important;
            border-radius: 10px !important;
        }

        .nav-tabs li a:hover {
            border: 1px solid #0087C4 !important;
            border-radius: 10px !important;
            background-color: #EDE6D6;
        }

        .nav-tabs li a {
            position: relative;
            border: none !important;
            -webkit-transition: .5s;
            transition: .5s;
            max-height: 38px !important;
            margin-left: 2px;
            margin-right: 2px;
        }

        .nav-item:not(.active):hover:before {
            opacity: 1;
            bottom: 0;
        }

        .nav-item:not(.active):hover {
            color: #333;
        }

        .panel-group {
            margin-left: auto;
            margin-right: auto;
        }
        label{
            color: #000;
        }
    </style>
        <script src="<?php echo base_url() ?>assets/js/checkbox.js"></script>
<?php 
	$this->load->view('includes/header'); 
	error_reporting(0);
?>
<style type="text/css">
label	{
color: #F00;
line-height: 2px;
font-size: 12px;
position: absolute;
margin: -10px 0px 0px -130px;
}
.comment-view-section {
	margin: -32px 210px 0px 0px;
}

</style>
<link rel="stylesheet" href="<?php echo base_url()?>css/profile.css" />
<link rel="stylesheet" href="<?php echo base_url()?>css/add-edit-user.css" />
<link rel="stylesheet" href="<?php echo base_url()?>css/resume_db.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/tcal.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery/tcal.js"></script> 
<!-- <script type="text/javascript" src="<?php echo base_url()?>js/jquery/jquery-1.4.1.min.js"></script> -->
<!-- <script type="text/javascript" src="<?php echo base_url()?>assets/js/libs/jquery.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url()?>js/jquery/jquery.validate.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>js/custom.js"></script> 
<script>
$().ready(function() {
	// validate the comment form when it is submitted
	$("#exportForm").validate();
});
function PopupCenter(pageURL, title, w, h) {
var left 		= (screen.width/2)-(w/2);
var top 		= (screen.height/2)-(h/2);
var targetWin 	= window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}

function get_requiter(val){
	$.post("<?php echo base_url();?>index.php/recruitercount/requiterlist",{mid:val},function(result){
			//alert(result);
    	    $("#reqruiterid").html(result);
    	});	
}
function get_manager(val){
	$.post("<?php echo base_url();?>index.php/managercount/managerlist",{Dirid:val},function(result){
			//alert(result);
    	    $("#managerid").html(result);
    	});	
}

var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })() /*document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();*/
</script>
    <?php $session_data = $this->session->userdata('logged_in'); ?>
    <div class="search-resume-div" style="width: fit-content;padding-right: 60px;">
        <div class="search-inputs">
            <div class="search-text">Total Submissions</div>
        </div>
        <?php if($session_data['ptno'] == 'PT 005'): ?>
        <!-- <div style="float: right; padding: 5px 80px 0px 5px; font-size: 15px;">
                Total Resumes: <strong><?php echo $totalResumes; ?></strong>
        </div> -->
        <?php endif; ?>
    </div>
    <?php $attributes = array('name' => 'registration', 'id' => 'exportForm', 'onsubmit' => 'return v.exec()');
    echo form_open('totalsubmissionsreport', $attributes); ?>
    <div class="table-search" style="justify-content:flex-end !important; margin-top:20px !important;">
    <?php if($session_data['usertype'] == 'SUPERADMIN'){ ?>
        <div class="input-div">
            <select name="directorid" id="directorid" class="selectpicker" onchange="get_manager(this.value);"><option value="">Select Director</option>
				<?php
				if (!empty($directors)) {
					foreach($directors as $dir){
						$directorid = $dir->id;
						$directorname = $dir->username;
						echo '<option value="'.$directorid.'">'.$directorname.'</option>';
					}
				}
			?></select>
        </div>
        <div class="input-div">
            <select name="managerid" id="managerid" class="selectpicker" onchange="get_requiter(this.value);"><option value="">Select Manager</option>
				<?php
				if (!empty($managerslist)) {
					foreach($managerslist as $mgr){
						$managerid = $mgr->manager_id;
						$managername = $this->user->get_manager_from_id($mgr->manager_id);
						echo '<option value="'.$managerid.'">'.$managername.'</option>';
					}
				}
				?>
			</select>
        </div>
        <div class="input-div">
            <select name="reqruiterid" id="reqruiterid" class="selectpicker">
				<option value="All">Select Recruiter</option>
				<?php
				if (!empty($recruiterlist)) {
					foreach($recruiterlist as $rcr){
						$recruiterid = $rcr->recruiter_id;
						$recruitername = $this->user->get_recruiter_from_id($rcr->recruiter_id);
						echo '<option value="'.$recruiterid.'">'.$recruitername.'</option>';
					}
				}elseif(!empty($recruiterID)){
					$recruitername = $this->user->get_recruiter_from_id($recruiterID);
					echo '<option value="'.$recruiterID.'" selected = selected>'.$recruitername.'</option>';
				}
				?>
			</select>
        </div>
        <div class="input-div">
            <select name="status" class="selectpicker">
                <option value="All">Select Status</option>
                <option value="Submitted to Vendor">Submitted to Vendor</option>
                <option value="Rejected">Rejected</option>
                <option value="Submitted to Client">Submitted to Client</option>
                <option value="Interview">Interview</option>
                <option value="Rejected after Interview">Rejected after Interview</option>
                <option value="Placement">Placement</option>
			</select>
        </div>
        <div class="input-div">
            Start
            <input style="margin-left: 5px" type="date" class="form-control ppDate" name="date1" id="date1" placeholder="Start Date" />
        </div>
        <div class="input-div">
            End
            <input style="margin-left: 5px" type="date" class="form-control ppDate" name="date2" id="date2" placeholder="End Date" />
        </div>
        <button type="submit" class="cssbuttons-io-button">
            <input type="hidden" name="submit" value="submit">
            Submit
            <div class="icon">
                <svg height="24" width="24" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z"
                        fill="currentColor"></path>
                </svg>
            </div>
        </button>
         <!-- Download excel btn -->
         <a id="dlink"  style="display:none;"></a>
        <div class="download-btn">
            <button type="button" value="Export" onclick="tableToExcel('product-table', 'Manager Count Table', 'TotalSubmission.xls')" class="download-button"
            >
            <div class="docs">
                <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none"
                stroke-width="2"
                stroke="currentColor"
                height="20"
                width="20"
                viewBox="0 0 24 24"
                >
                <path
                    d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"
                ></path>
                <polyline points="14 2 14 8 20 8"></polyline>
                <line y2="13" x2="8" y1="13" x1="16"></line>
                <line y2="17" x2="8" y1="17" x1="16"></line>
                <polyline points="10 9 9 9 8 9"></polyline>
                </svg>
                Excel
            </div>
            <div class="download">
                <svg
                class="css-i6dzq1"
                stroke-linejoin="round"
                stroke-linecap="round"
                fill="none"
                stroke-width="2"
                stroke="currentColor"
                height="24"
                width="24"
                viewBox="0 0 24 24"
                >
                <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                <polyline points="7 10 12 15 17 10"></polyline>
                <line y2="3" x2="12" y1="15" x1="12"></line>
                </svg>
            </div>
            </button>
        </div>
        <?php } ?>
    </div>
    <?php echo form_close(); ?>
    <div class="table-div">
        <div class="table">
            <table id="product-table">
                <thead>
                    <tr>
                        <th class="table-header" style="border-top-left-radius: 20px">Date</th>
                        <th class="table-header">Consultant</th>
                        <th class="table-header">For Position</th>
                        <th class="table-header">Total IT</th>
                        <th class="table-header">End Client</th>
                        <th class="table-header">Loc</th>
                        <th class="table-header">Manager</th>
                        <th class="table-header">Submitted by</th>
                        <th class="table-header">Consultant Rate</th>
                        <th class="table-header">Visa</th>
                        <th class="table-header">Relocate</th>
                        <th class="table-header">Employer Details</th>
                        <th class="table-header">Status</th>
                        <th class="table-header" style="border-top-right-radius: 20px">Comments</th>
                        <?php if($session_data['usertype'] == 'SUPERADMIN'){ ?>
                        <th class="table-header" style="border-top-right-radius: 20px">Action</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                <?php
				
				//rint_r($rows->result());exit;
				$i=1;
				$date='';
				$classInactive = "userinactive";
				foreach($rows->result() as $res1){
				//print_r($rows->result());exit;
				if($i%2==0){
				$class="alternate-row";
				
				}else{
				$class='';
				}
					if($date == ''){
						$date=$res1->sdate; 
					
					}
					
					if($date != $res1->sdate){
						$date=$res1->sdate; 
					echo '<tr><td colspan="25" class="gerua"></td></tr>';
					}
				?>
				<tr class='<?php echo $class;?> <?php if ($res1->view == 1) { echo $classInactive; } ?>'>
					<td>
					<?php //echo $res1['userid']; ?>
					<?php echo date("M",strtotime($res1->sdate)); ?>
					<?php echo date("d",strtotime($res1->sdate)); ?>
					 	 	 	 	 	 	 	 		 	 	 	

					</td>
					<td><a href="<?php echo base_url(); ?>/<?php echo $res1->resume; ?>" target="_blank" title="<?php echo $res1->cname ?>"><?php echo substr($res1->cname, 0, 10)?></a> </td>
					<td><a href="#" target="_blank" title="<?php echo $res1->psub ?>"><?php echo substr($res1->psub, 0, 12)?></a></td>
					<td><?php echo $res1->totalit; ?></td>
					<td><?php echo $res1->company; ?></td>
					<td><?php echo $res1->st; ?></td>
					<td><a href="#" target="_blank" title="<?php echo $res1->manager ?>"><?php echo substr($res1->manager, 0, 6)?></a></td>
					<td><a href="#" target="_blank" title="<?php echo $res1->recruiter ?>"><?php echo substr($res1->recruiter, 0, 12)?></a></td>
										
					<?php 
					if($_SESSION["utype"] == "ADMIN"){
						echo "<td>".$res1->vcompany."</td>"; 
						}
				
					if($_SESSION['utype'] == 'ADMIN'){
						echo "<td>".$res1->vcontact."</td>"; 
						}
					?>
					<td><a href="#" title="<?php echo $res1->crate; ?>"><?php echo substr($res1->crate,0,8); ?></a></td>
					<td><?php echo $res1->visatype; ?></td>
					<td><?php echo ($res1->relocate)? $res1->relocate : 'NA'; ?></td>
					<?php 
					if($_SESSION['utype'] == 'ADMIN'){
						echo "<td>".$res1->srate."</td>"; 
						}
					$view = $res1->view;
					if($view == 0){
						$view1 = 1;	
					} else {
						$view1 = 0;		
					}
					?>
					
					<td onmouseover="document.getElementById('div<?php echo $i;?>').style.display = 'block';document.getElementById('div-<?php echo $i;?>').style.display = 'none';" onmouseout="document.getElementById('div<?php echo $i;?>').style.display = 'none';document.getElementById('div-<?php echo $i;?>').style.display = 'block';"><div id="div-<?php echo $i;?>" style="display: block;">View</div><div id="div<?php echo $i;?>" style="display: none;"><?php echo $res1->employerdetails; ?></div></td>


					<td><a href="#" title="<?php echo $res1->status ?>"><?php echo substr($res1->status, 0, 6)?></a></td>

					<td>
						<a class="comment-section" data-id="<?=$res1->id?>" href="javascript:void(0);" 
						   onclick="PopupCenter('<?php echo base_url(); ?>index.php/totalsubmissions/comments/<?php echo $res1->id; ?>', 'myPop1',600,600);">Read / Add</a>
						<div class="comment-view-section" id="comment-view-section-<?=$res1->id?>">Loading...</div>
					</td>


					<?php if($session_data['usertype'] == 'SUPERADMIN'){ ?>
					<td class="options-width">

					<!-- <a title="Status" class="icon-5 info-tooltip inactiveuser"></a> -->

					<a href="<?php echo base_url()?>index.php/totalsubmissions/view/<?php echo $view1;?>/<?php echo $res1->id; ?>" title="Status" class="cds_svg info-tooltip inactiveuser">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" class="iconify iconify--charm" width="20" height="20" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16">
                            <g fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
                                <path d="M10.25 2.75h-7.5v10.5h10.5v-3.5"></path>
                                <path d="m5.75 7.75l2.5 2.5l6-6.5"></path>
                            </g>
                        </svg>
                    </a>

					<?php if($session_data['id'] != 23): ?>
					<a href="<?php echo base_url()?>index.php/totalsubmissions/deletesubmission/<?php echo $res1->id; ?>" title="Delete" class="cds_svg info-tooltip delete-submission">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" class="iconify iconify--charm" width="20" height="20" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16">
                            <path fill="none" stroke="currentColor" stroke-linecap="round"
                                stroke-linejoin="round" stroke-width="1.5"
                                d="m10.25 5.75l-4.5 4.5m0-4.5l4.5 4.5m-7.5-7.5h10.5v10.5H2.75z">
                            </path>
                        </svg>
                    </a>
					<?php endif; ?>

					<?php if($session_data['id'] == 57){ ?>
					    <a href="<?php echo base_url()?>index.php/totalsubmissions/editmysubmissions/<?php echo $res1->id; ?>" title="Edit" class="icon-1 info-tooltip"></a>

					</td>
					<?php } ?>

					<?php } ?>
				</tr>
			
				<?php 
				$i++;
				} ?>
                </tbody>
            </table>
        </div>
        <?php if($links){ echo "<ul class='pagination:container'>".$links."</ul>"; } ?>
    </div>
    <!--End of JavaScript for the search block -->
    
    <!-- End of script of export to excel -->
    <script type="text/javascript">
    $('.delete-submission').click(function(e){
    e.preventDefault();
    var retVal = confirm("Are you sure, you want to delete this submission?");
    if(retVal == true){
        window.location.href=$(this).attr('href');
        return true;
    }else{
        return false;
    }
    });
    $('.inactiveuser').click(function() {
        $(this).closest('tr').css("background-color", "red");
        $(this).toggleClass('highlighted');
    });
</script>	
<div id="status-options" style="display: none;">
	<select name="status-options" id="status-options">
		<option value="">Select Option</option>
		<option value="Submitted to Client">Submitted to Client</option>
		<option value="Submitted to Vendor">Submitted to Vendor</option>
		<option value="Rejected (Reason: Comm Skills)">Rejected (Reason: Comm Skills)</option>
		<option value="Rejected (Reason: Position on Hold/Closed/Filled)">Rejected (Reason: Position on Hold/Closed/Filled)</option>
		<option value="Rejected (Reason: Technical Skills)">Rejected (Reason: Technical Skills)</option>
		<option value="Rejected (Reason: Rate Concern)">Rejected (Reason: Rate Concern)</option>
		<option value="Rejected (Reason: Location)">Rejected (Reason: Location)</option>
		<option value="Interview">Interview</option>
		<option value="Placement">Placement</option>
		<option value="Backout">Backout</option>
	</select>
	<span class="submit-option">SUBMIT</span>
	<span class="close-options">CLOSE</span>
</div>	
<style>
    .input-div{
        background-color: transparent;
    }
    .download-btn{
    margin-left:15px;
    }
    .table-search{
    Justify-content:flex-start !important;
    }
    .input-div{
    background-color: transparent;
    }
    .selectpicker {
    border-radius: 10px !important;
    width: auto;
    height:36px;
    }
    td, th {
    border-bottom: 1px solid #000 !important;
    border: none !important;
    text-align:left;
    }
</style>
<script type="text/javascript" src="<?=base_url()?>js/mysubmissions.js"></script>
<?php $this->load->view('includes/footer'); ?>
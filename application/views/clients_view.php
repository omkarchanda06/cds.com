<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/resume_db.css" />
<div class="search-resume-div" style="width: fit-content; padding-right: 60px;margin-left: 80px;" >
    <div class="search-inputs">
    <div class="search-text">Client Details</div>
    </div>
</div>
<!-- table -->
<div class="search-in-table" style="margin-left:45%;">
    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="&#128269; Enter keyword to search in the following table" />
</div>
<div class="table-div">
    <div class="table">
        <table id="myTable" style="width: 80% !important;">
            <thead>
                <tr>
                    <th class="table-header" style="border-top-left-radius: 20px"> ID </th>
                    <th class="table-header">Name</th>
                    <th class="table-header">Company</th>
                    <th class="table-header">Contact</th>
                    <th class="table-header">Email</th>
                    <th class="table-header">Status</th>
                    <th class="table-header">Comments</th>
                    <th class="table-header" style="border-top-right-radius: 20px">Edit</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($clientdetails as $details) { ?>
                <tr class="client-row-<?php echo $details->id; ?>">
                    <td class="table-data"style="text-align:center;"><?=$details->id?></td>
                    <td class="table-data"><?=$details->client_name?></td>
                    <td class="table-data"><?=$details->client_company?></td>
                    <td class="table-data"><?=$details->client_contact?></td>
                    <td class="table-data"><?=$details->client_email?></td>
                    <td class="table-data"><?=$details->client_status?></td>
                    <td class="table-data"><?=$details->comments?></td>
                    <td class="table-data options-width">
                        <a href="<?php echo base_url() ?>index.php/client/edit/<?=$details->id?>" id="<?=$details->id?>">Edit</a>
                    </td>
                    <!-- <td style="text-align:center;" class="options-width"><a class="delete-jobtitle" href="<?php echo base_url() ?>index.php/jobtitle/delete/<?=$details->id?>" id="<?=$details->id?>">Delete</a></td> -->
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<!--Start of JavaScript for the search block -->
<script>
    function myFunction() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>
<?php $this->load->view('includes/footer'); ?>
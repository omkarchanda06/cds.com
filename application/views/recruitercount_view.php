<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/profile.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>css/add-edit-user.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/resume_db.css" />
<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script>
    function get_requiter(val) {
        $.post("<?php echo base_url(); ?>index.php/recruitercount/requiterlist", {
            mid: val
        }, function(result) {
            $("#reqruiterid").html(result);
        });
    }

    function get_manager(val) {
        $.post("<?php echo base_url(); ?>index.php/managercount/managerlist", {
            Dirid: val
        }, function(result) {
            //alert(result);
            $("#managerid").html(result);
        });
    }

    function generatePDF() {
        var managerid = document.getElementById("managerid").value;
        var reqruiterid = document.getElementById("reqruiterid").value;
        var date1 = document.getElementById("date1").value;
        var date2 = document.getElementById("date2").value;
        if (date1 == "" || date2 == "") {
            alert("Please Select Date");
        } else {
            window.open('<?php echo base_url() ?>index.php/pdfrecruitercount/index/' + date1 + '/' + date2 + '/' + managerid + '/' + reqruiterid, '_blank', true)
        }
    }

    var tableToExcel = (function() {
        var uri = 'data:application/vnd.ms-excel;base64,',
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            },
            format = function(s, c) {
                return s.replace(/{(\w+)}/g, function(m, p) {
                    return c[p];
                })
            }
        return function(table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = {
                worksheet: name || 'Worksheet',
                table: table.innerHTML
            }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()
</script>
<div class="search-resume-div">
    <div class="search-text">Recruiters Count</div>
    <div class="search-inputs" style="display: flex;">
        <div class="container-db" style="display: flex;margin-left: 10px;">
            <div style="display: flex;">
                <form action="<?php echo base_url() ?>index.php/recruitercount/" onsubmit="return v.exec()" name="registration" method="POST">
                    <?php if ($usertype == "SUPERADMIN" || $usertype == "HR" || $usertype == "MIS") { ?>
                        <div class="col-md-2">
                            <!-- <label for="inputState">Directors</label> -->
                            <label>Select Director: </label>
                            <select name="directorid" id="directorid" class="inp-form" onchange="get_manager(this.value);" style="padding:5px; width:150px;">
                                <?php
                                if (!empty($directors)) {
                                    echo '<option value="All">All</option>';
                                    foreach ($directors as $dir) {
                                        $directorid = $dir->id;
                                        $directorname = $dir->username;
                                        echo '<option value="' . $directorid . '">' . $directorname . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    <?php } ?>
                    <?php if ($usertype == "SUPERADMIN" || $usertype == "DIRECTOR" || $usertype == "HR" || $usertype == "MIS") { ?>
                        <div class="col-md-2">
                            <label>Select Manager: </label>
                            <select name="managerid" id="managerid" class="inp-form" onchange="get_requiter(this.value);" style="padding:5px; width:150px;">
                                <!-- <option value="">Select Manager</option> -->
                                <option value="All">All</option>
                                <?php
                                if (!empty($managerslist)) {
                                    foreach ($managerslist as $mgr) {
                                        $managerid = $mgr->manager_id;
                                        $managername = $this->user->get_manager_from_id($mgr->manager_id);
                                        echo '<option value="' . $managerid . '">' . $managername . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    <?php } ?>
                    <?php if ($usertype == "SUPERADMIN" || $usertype == "DIRECTOR" || $usertype == "HR" || $usertype == "MIS") { ?>
                        <div class="col-md-2">
                            <label>Select Recruiter: </label>
                            <select name="reqruiterid" id="reqruiterid" class="inp-form" style="padding:5px; width:150px;">
                                <!-- <option value="">Select Recruiter</option> -->
                                <option value="All">All</option>
                                <?php
                                if (!empty($recruiterlist)) {
                                    foreach ($recruiterlist as $rcr) {
                                        // $recruiterid = $rcr->recruiter_id;
                                        $recruitername = $this->user->get_recruiter_from_id($rcr->recruiter_id);
                                        echo '<option value="' . $rcr->recruiter_id . '">' . $recruitername . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    <?php } ?>
                    <div class="col-md-2">
						<label for="inputState">From : </label>
						<input type="date" id="date1" name="date1">
					</div>
					<div class="col-md-2">
						<label for="inputState">To : </label>
						<input type="date" id="date2" name="date2">
					</div>
                    <!-- submit btn -->
                    <div class="animate-btn">
                        <div class="search-btn">
                            <button type="submit" class="cssbuttons-io-button" name="submit">
                                Submit
                                <div class="icon">
                                    <svg height="24" width="24" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M0 0h24v24H0z" fill="none"></path>
                                        <path d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z" fill="currentColor"></path>
                                    </svg>
                                </div>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <?php if ($usertype == "SUPERADMIN" || $usertype == "HR" || $usertype == "MIS") { ?>
                <div style="display: flex;">
                    <!-- Download excel btn -->
                    <div class="download-btn" style="margin-left: 10px;">
                        <a id="dlink"  style="display:none;"></a>
                        <button type="button" id="btnExport" value="Export" onclick="tableToExcel('product-table', 'Manager Count Table', 'RecruitersCount.xls')" class="download-button">
                            <div class="docs">
                                <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2" stroke="currentColor" height="20" width="20" viewBox="0 0 24 24">
                                    <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                    <polyline points="14 2 14 8 20 8"></polyline>
                                    <line y2="13" x2="8" y1="13" x1="16"></line>
                                    <line y2="17" x2="8" y1="17" x1="16"></line>
                                    <polyline points="10 9 9 9 8 9"></polyline>
                                </svg>
                                Excel
                            </div>
                            <div class="download">
                                <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2" stroke="currentColor" height="24" width="24" viewBox="0 0 24 24">
                                    <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                    <polyline points="7 10 12 15 17 10"></polyline>
                                    <line y2="3" x2="12" y1="15" x1="12"></line>
                                </svg>
                            </div>
                        </button>
                    </div>
                    <!-- Download pdf btn -->
                    <!-- <div class="download-btn" style="margin: 10px;">
                        <button type="button" id="btnExport" value="Export" onclick="generatePDF()" class="download-button">
                            <div class="docs">
                                <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2" stroke="currentColor" height="20" width="20" viewBox="0 0 24 24">
                                    <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                    <polyline points="14 2 14 8 20 8"></polyline>
                                    <line y2="13" x2="8" y1="13" x1="16"></line>
                                    <line y2="17" x2="8" y1="17" x1="16"></line>
                                    <polyline points="10 9 9 9 8 9"></polyline>
                                </svg>
                                PDF
                            </div>
                            <div class="download">
                                <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2" stroke="currentColor" height="24" width="24" viewBox="0 0 24 24">
                                    <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                    <polyline points="7 10 12 15 17 10"></polyline>
                                    <line y2="3" x2="12" y1="15" x1="12"></line>
                                </svg>
                            </div>
                        </button>
                    </div> -->
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="table-div">
                <div class="table">
                <?php if (!$showBlank) : ?>
                    <table id="product-table">
                        <thead>
                            <tr>
                                <th class="table-header" style="border-top-left-radius: 20px"> Recruiters</th>
                                <th class="table-header">Submission Target</th>
                                <?php
                                $totalTargets				= 0;
                                $submissionsbyDate			= array();
                                $totalSubmissionsOfAllDates = 0;
                                $totalDates					= 0;
                                $totalAverageSubmissions 	= 0;

                                $totalSubmittedToClients 	= 0;
                                $totalInterviews 			= 0;
                                $totalRejected 				= 0;
                                $totalPlacements 			= 0;

                                foreach ($dates as $d) : $day1 = date("D", strtotime($d));
                                    $day2 = date("d M", strtotime($d));
                                    $totalDates++; ?>
                                    <th class="table-header"><?= $day2 ?><br><span><?= $day1 ?></span></th>
                                <?php endforeach; ?>
                                <th class="table-header">Total Subs</th>
                                <th class="table-header">Average</th>
                                <th class="table-header">Total Submission %</th>
                                <th class="table-header">Submitted to Client</th>
                                <th class="table-header">Interviews</th>
                                <th class="table-header">Rejections</th>
                                <th class="table-header">No Updates</th>
                                <th class="table-header">Placements</th>
                                <th class="table-header" style="border-top-right-radius: 20px">
                                    Quality %
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        // For Interview List
                        $managerName 	= '';
                        $recruiterArray = array();
                        ?>
                        <?php foreach ($managers as $manager) : ?>
                            <?php
                            // For Interview List
                            $managerName = $manager->username;
                            ?>
                            <tr>
                                <td colspan="<?= $totalDates + 11 ?>" style="background: #FFA500 !important;">
                                    <h3 style="margin:0;color: #000; text-align:center !important;"><strong><?= $manager->username ?></strong></h3>
                                </td>
                            </tr>
                                <?php $recruiters = $this->recruiter->getRecruitersByManagerID($manager->id); ?>
                                <?php foreach ($recruiters as $recruiter) : ?>
                                    <tr>

                                        <?php
                                        // For Interview List
                                        $recruiter_name = $this->assignrecruiterstomgr->get_recruitername($recruiter->recruiter_id);
                                        $recruiterArray[$recruiter->recruiter_id] = $recruiter_name->recruiter;
                                        ?>

                                        <!-- Recruiter Name -->
                                        <td><?php echo $recruiter_name->recruiter; ?></td>

                                        <!-- Recruiter Targets -->
                                        <?php
                                        $targetResult	= $this->recruiter->getRecruiterTotalTarget($recruiter->recruiter_id, $fromDate, $toDate, $totalDates);
                                        $targets 		= $targetResult['sumtarget'];
                                        $targets 		= ($targets) ? ($targets / 5) * $totalDates : 0;
                                        ?>

                                        <td class="table-data"><?= $targets ?></td>
                                        <?php $totalTargets = $totalTargets + $targets; ?>


                                        <!-- Recruiter Submissions by Date -->
                                        <?php
                                        $submissionsOfAllDates = 0;
                                        foreach ($dates as $d) :
                                        ?>

                                            <?php //if($this->recruiter->getRecruiterAttendance($recruiter->recruiter, $d) == 1): 
                                            if ($this->recruiter->getRecruiterAttendance($recruiter_name, $d) != '') :
                                            ?>
                                                <td class="table-data" style="color: white; background:#FF0000;"> <strong>A</strong> </td>
                                            <?php else : ?>
                                                <td class="table-data"><?= ($subcount[$recruiter->recruiter_id][$d]['submissioncount'] ? $subcount[$recruiter->recruiter_id][$d]['submissioncount'] : 0) ?></td>
                                            <?php endif; ?>
                                            <?php
                                            $submissionsbyDate[$d] 		= $submissionsbyDate[$d] + $subcount[$recruiter->recruiter_id][$d]['submissioncount'];
                                            $submissionsOfAllDates		= $submissionsOfAllDates + $subcount[$recruiter->recruiter_id][$d]['submissioncount'];
                                            ?>
                                        <?php endforeach; ?>


                                        <!-- Recruiter Total Submissions-->
                                        <td class="table-data"><?= $submissionsOfAllDates ?></td>


                                        <!-- Recruiter Average Submissions-->
                                        <?php $averageSubmissions 		= $submissionsOfAllDates / $totalDates; ?>
                                        <td class="table-data"><?= round($averageSubmissions, 1) ?></td>

                                        <!-- Recruiter Submissions Percentage -->
                                        <?php $submissionsPercentage = $this->user->calculatePercentage($submissionsOfAllDates, $targetResult['targets']); ?>
                                        <td class="table-data" <?php if (round($submissionsPercentage) >= 90) : ?> style="background:#FFFF00" <?php endif; ?>><?= round($submissionsPercentage) ?> %</td>


                                        <!-- Recruiter Submissions Status Area -->

                                        <!-- Recruiter Submitted to clients -->
                                        <td class="table-data">
                                            <?php
                                            if (!isset($subcount[$recruiter->id]['submitted_to_client'])) $subcount[$recruiter->id]['submitted_to_client'] = 0;
                                            echo $subcount[$recruiter->id]['submitted_to_client'];
                                            ?>
                                        </td>

                                        <!-- Recruiter Interviews -->
                                        <td class="table-data">
                                            <?php
                                            $interviewsCount = $this->submission->countInterviewOfRecruiter($fromDate, $toDate, $recruiter->recruiter_id);
                                            echo $interviewsCount;
                                            ?>
                                        </td>

                                        <!-- Recruiter Rejected -->
                                        <td class="table-data">
                                            <?php
                                            if (!isset($subcount[$recruiter->recruiter_id]['rejections'])) $subcount[$recruiter->recruiter_id]['rejections'] = 0;
                                            echo $subcount[$recruiter->recruiter_id]['rejections'];
                                            ?>
                                        </td>

                                        <!-- No updates pending functionality -->
                                        <td class="table-data">
                                            <?php
                                            if (!isset($subcount[$recruiter->recruiter_id]['rejections'])) $subcount[$recruiter->recruiter_id]['rejections'] = 0;
                                            echo $subcount[$recruiter->recruiter_id]['rejections'];
                                            ?>
                                        </td>

                                        <!-- Recruiter Placements -->
                                        <td class="table-data">
                                            <?php
                                            if (!isset($subcount[$recruiter->recruiter_id]['placements'])) $subcount[$recruiter->recruiter_id]['placements'] = 0;
                                            echo $subcount[$recruiter->recruiter_id]['placements'];
                                            ?>
                                        </td>

                                        <!-- Recruiter Quality Percentage -->
                                        <?php $qualityPercentage  = $this->user->calculatePercentage($subcount[$recruiter->recruiter_id]['submitted_to_client'], $submissionsOfAllDates); ?>
                                        <td class="table-data"><?= round($qualityPercentage) ?> %</td>
                                    </tr>
                                <?php endforeach; ?>
						<?php endforeach; ?>
                        </tbody>
                        <!-- <tfoot>
                            <tr>
                                <td class="table-data" style="border-bottom-left-radius: 20px;">Total</td>
                                <td class="table-data">120</td>
                                <td class="table-data">0</td>
                                <td class="table-data">0</td>
                                <td class="table-data">0</td>
                                <td class="table-data">0</td>
                                <td class="table-data">0</td>
                                <td class="table-data">0</td>
                                <td class="table-data">0</td>
                                <td class="table-data">0</td>
                                <td class="table-data">0 %</td>
                                <td class="table-data">0</td>
                                <td class="table-data">0</td>
                                <td class="table-data">0</td>
                                <td class="table-data">0</td>
                                <td class="table-data">0</td>
                                <td class="table-data" style="border-bottom-right-radius: 20px;">0%</td>
                            </tr>
                        </tfoot> -->
                    </table>
                    <?php endif; ?>
                </div>
            </div>
            <style>
                .animate-btn{
                    margin-top:0;
                    margin-right:0;
                }
                .table-header{
                    text-align: center;
                    height: auto !important;
                    font-weight: 600;
                    padding: 5px;
                }
                label{
                    font-size: medium;
                    color: #000;
                    font-weight: 500;
                }
                .col-md-2{
                    padding: 5px;
                }
                tfoot tr td {
                    background-color: #2d2d2d !important;
                    color: #fff !important;
                }
                tfoot tr {
                    border-bottom: none !important;
                }
                .search-text{
                    margin-left:5vh;
                    padding: 25px;
                    font-size: 2em;
                }
                .container-db{
                  margin-left:5vh;
                }
                .inp-form{
                    border-radius: 10px;
                    border: 2px solid #407195;
                }
                td{
                    border: none !important;
                    text-align: left !important;
                }
            </style>
<?php $this->load->view('includes/footer'); ?>
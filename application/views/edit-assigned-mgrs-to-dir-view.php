<?php $this->load->view('includes/header'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js" defer></script>
<?php
// print_r($managerslist);exit();
    $attributes = array('id' => 'assign-mgrs-to-director');
    echo form_open('assignmgrstodir/update', $attributes); ?>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
            <tr valign="top" height="30">
                <td><span style="color:blue; line-height: 23px;"><?php if($message!=''){ echo $message; }?></span>	
                    <table cellspacing="0" cellpadding="0" border="0" id="id-form" width="100%" >
                        <tbody>
                            <tr>
                                <th valign="top">Directors</th>
                                <td class="noheight">
                                    <select name="directorslist" id="directors-list" required>
                                            <option value=""> Select Director </option>
                                            <?php foreach ($directorslist as $director) {?>
                                            <option <?php if($directorID == $director->id){echo "selected=selected";} ?>value="<?php echo $director->id; ?>"><?php echo $director->username; ?></option>
                                            <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th valign="top">Managers :</th>
                                <td class="noheight">
                                    <select name="managerslist[]" id="managers-list" multiple required>
                                            <option value=""> Select Managers </option>
                                            <?php
                                                foreach ($managerslist as $manager) {?>
                                                    <option <?php 
                                                    $selected_mgr = $this->assignmanagerstodirector->get_selected_mgrs_by_dirID($directorID, $manager->id); 
                                                    if($selected_mgr->isActive == 1){echo "selected=selected";}?> value="<?php echo $manager->id; ?>"><?php echo $manager->username; ?></option>
                                        <?php }?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>&nbsp;</th>
                                <td valign="top" style="padding-top: 10px;">
                                    <input type="hidden" name="directorid" value="<?php echo $directorID;?>">
                                    <input type="submit" name="assignmgrstodir" class="form-submit" value="update managers to director"/>
                                    <input type="reset" class="form-reset" value="">
                                </td>
                                <td></td>
                            </tr>
                        
                        </tbody>
                    </table>
                    <!-- end id-form  -->

                </td>
            </tr>
            </tbody>
        </table>
    </form>

<?php $this->load->view('includes/footer'); ?>
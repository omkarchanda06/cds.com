<?php
//echo $basename;
$basename = $this->uri->segment(1);
$basename2 = $this->uri->segment(2);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Panzeradmin - Home</title>
	<link rel="stylesheet" href="<?php echo base_url() ?>css/screen.css" type="text/css" media="screen" title="default" />
	<link rel="stylesheet" href="<?php echo base_url() ?>css/menu.css" type="text/css" media="screen" title="default" />
	<link rel="stylesheet" href="<?php echo base_url() ?>css/profile.css" />
	<!--[if IE]>
<link rel="stylesheet" media="all" type="text/css" href="css/pro_dropline_ie.css" />
<![endif]-->

	<!--  Jquery core -->
	<!-- <script src="<?php echo base_url() ?>js/jquery/jquery-1.8.3.js" type="text/javascript"></script> -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="<?php echo base_url() ?>assets/js/libs/jquery.min.js"></script>

	<!-- Jquery UI -->
	<script src="<?php echo base_url() ?>js/jquery/jquery-ui.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo base_url() ?>css/jquery-ui.css" type="text/css" media="screen" title="default" />
	<link rel="stylesheet" href="<?php echo base_url() ?>css/style.css" type="text/css" media="screen" title="default" />

	<!-- Config JS -->
	<script type="text/javascript">
		var base_url = window.location.protocol + '//' + window.location.host + '/cds.com/index.php';
	</script>

	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			function exportTableToCSV($table, filename) {

				var $rows = $table.find('tr:has(td)'),

					// Temporary delimiter characters unlikely to be typed by keyboard
					// This is to avoid accidentally splitting the actual contents
					tmpColDelim = String.fromCharCode(11), // vertical tab character
					tmpRowDelim = String.fromCharCode(0), // null character

					// actual delimiter characters for CSV format
					colDelim = '","',
					rowDelim = '"\r\n"',

					// Grab text from table into CSV formatted string
					csv = '"' + $rows.map(function(i, row) {
						var $row = $(row),
							$cols = $row.find('td');

						return $cols.map(function(j, col) {
							var $col = $(col),
								text = $col.text();

							return text.replace('"', '""'); // escape double quotes

						}).get().join(tmpColDelim);

					}).get().join(tmpRowDelim)
					.split(tmpRowDelim).join(rowDelim)
					.split(tmpColDelim).join(colDelim) + '"',

					// Data URI
					csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

				$(this)
					.attr({
						'download': filename,
						'href': csvData,
						'target': '_blank'
					});
			}

			// This must be a hyperlink
			$(".export").on('click', function(event) {
				// CSV
				exportTableToCSV.apply(this, [$('#product-table'), 'export.csv']);

				// IF CSV, don't do event.preventDefault() or return false
				// We actually need this to be a typical hyperlink
			});

			/**profile js */
			let menuToggle = document.querySelector('.menu-toggle');
			let navigation = document.querySelector('.navigation');

			menuToggle.onclick = function() {
				navigation.classList.toggle('active');
			}
		});
	</script>


</head>

<body>
	<?php $get_session_data = $this->session->userdata('logged_in'); ?>
	<!-- Start: page-top-outer -->
	<div id="page-top-outer">

		<!-- Start: page-top -->
		<div id="page-top">

			<!-- start logo -->
			<div id="logo">
				<a href="<?php echo base_url() ?>index.php/dashboard"><img src="<?php echo base_url() ?>assets/images/logo.png" alt="Panzer" /></a>
			</div>
			<!-- end logo -->
			<!-- Profile (this should be in all pages) -->
			<?php
			$session_data = $this->session->userdata('logged_in');
			$usertype = $session_data['username'];
			?>
			<div class="navigation">
				<div class="user-box">
					<div class="image-box">
						<img src="<?php echo base_url() ?>assets/images/profile.png" alt="avatar" />
					</div>
					<p class="username"><?php echo $usertype; ?></p>
				</div>
				<div class="menu-toggle"></div>
				<ul class="menu">
					<li>
						<a href="#">Profile</a>
					</li>
					<li>
						<a href="<?php echo base_url() ?>index.php/dashboard/logout">Logout</a>
					</li>
				</ul>
			</div>
			<!--  end top-search -->
			<div class="clear"></div>
		</div>
		<!-- End: page-top -->

		<!-- <div id='cssmenu'> -->
		<nav>
			<ul class="new-menu">
				<?php
				$session_data = $this->session->userdata('logged_in');
				$usertype = $session_data['usertype'];
				$username = $session_data['username'];
				$menu_data = $this->session->userdata('menudata');
				// echo "<pre>";
				// print_r($menu_data);
				// echo "</pre>";exit;

				foreach ($menu_data as $rmenu) {
					//echo $rmenu->category; die();
					if ($rmenu->parent_id == 0 && $rmenu->category == 1) {
						$parentID = $rmenu->id;
						if ($rmenu->menu_url == "") {
							$menu_url = "#";
							$class = "has-sub";
						} else {
							$menu_url = $rmenu->menu_url;
							$class = "";
						}?>
						<li class='drop'><?php echo ucwords(strtolower($rmenu->manu_name)) ?>
							<div class="dropdownContain">
            					<div class="dropOut">
              						<div class="triangle"></div>
									<ul class="dropdownli" id="dropdownli">
										<?php foreach ($menu_data as $rmenu1) {
											$parentID1 = $rmenu1->id;
											//echo "<br>";
											if ($rmenu1->category == 2 && $rmenu1->parent_id == $parentID) {
												if ($rmenu1->menu_url == "") {
													$menu_url1 = "#";
													$class1 = "has-sub";
												} else {
													$menu_url1 = $rmenu1->menu_url;
													$class1 = "";
												}?>
												<li><a class="sub_menu" href='<?php echo base_url() ?>index.php/<?php echo $menu_url1; ?>'><span><?php echo ucwords(strtolower($rmenu1->manu_name)) ?></span></a>
													<ul>
														<?php
														$categoryID = $rmenu1->parent_id;
														//echo "<br>";
														$parentID1 = $rmenu1->id;
														//echo "<br>";
														foreach ($menu_data as $rmenu2) {

															if ($rmenu2->category == 3 && $rmenu2->parent_id == $parentID1 &&  $parentID == $categoryID) {
														?>
																<li><a href='<?php echo base_url() ?>index.php/<?php echo $rmenu2->menu_url; ?>'><span><?php echo ucwords(strtolower($rmenu2->manu_name)); ?></span></a></li>
														<?php 					}
														}
														?>
													</ul>
												</li>
										<?php }
										} ?>
									</ul>
								</div>
							</div>
						<li>
					<?php
					}
				} //die();
				?>

				<!-- <?php if (isset($menuname)  && $menuname == 'profile') { ?>
				<li class='active last has-sub' id="float-right">
				<?php } else { ?>
				<li class='last has-sub' id="float-right">
				<?php } ?>
				<a href='#'><span><?php echo trim(explode(' ', $get_session_data['username'])[0]); ?></span></a>
					<ul>
						<?php if ($get_session_data['username'] != 'Demo') { ?>
						<li><a href='<?php echo base_url() ?>index.php/dashboard/changepwd'><span>View Profile</span></a></li>
						<?php } ?>
						<?php if ($usertype ==  'SUPERADMIN') { ?>
						<li><a href='<?php echo base_url() ?>index.php/rolemanagement'><span>Role Management</span></a></li>
						<?php } ?>
						<li><a href='<?php echo base_url() ?>index.php/dashboard/logout'><span>Log out</span></a></li>
					</ul>
				</li> -->
			</ul>
		</nav>
	</div>
	<!-- End: page-top-outer -->

	<!-- start content-outer ........................................................................................................................START -->
	<div id="content-outer">
		<!-- start content -->
		<div id="content">
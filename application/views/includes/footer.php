	<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->
<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<div id="footer">
	<!--  start footer-left -->
	<!-- <div id="footer-left">
	&copy; Copyright <?php echo date("Y");?> <a href="http://panzertechnologies.com/" target="_blank">PANZER Technologies</a>. All rights reserved.</div> -->
  <div class="copyright" style="padding: 10px;">
    <div class="copyright-text" style="display:table; margin:auto;">
      &copy;
      <span id="copyright">
        <script>
          document
            .getElementById("copyright")
            .appendChild(document.createTextNode(new Date().getFullYear()));
        </script>
      </span>
      Copyright Panzer Technologies. All rights reserved.
    </div>
  </div>
	<!--  end footer-left -->
	<div class="clear">&nbsp;</div>
</div>
<!-- end footer -->
<script type="text/javascript">
$(function() {
	$("#psub").autocomplete({
		source:base_url+'/mysubmissionsreport/positions', minLength:2,
		change: function (event, ui) {
        if (ui.item == null || ui.item == undefined) {
            $(this).val("");
            //$(this).attr("disabled", false);
			$("#job-title-pg").show();
        } else {
            //$(this).attr("disabled", true);
			$("#job-title-pg").hide();
        }
    }
	});
});

/*for fetching client names*/

$(function() {
	$("#client_name").autocomplete({
		source:base_url+'/mysubmissionsreport/getclientsnames', minLength:2,
		change: function (event, ui) {
        if (ui.item == null || ui.item == undefined) {
            $(this).val("");
			$("#job-pg-add-client").show();
            //$(this).attr("disabled", false);
        } else {
            //$(this).attr("disabled", true);
			$("#job-pg-add-client").hide();
        }
    }
	});
});

/*for fetching cities*/

$(function() {
	$("#city").autocomplete({
		source:base_url+'/mysubmissionsreport/getcitiesnames', minLength:2,
		change: function (event, ui) {
        if (ui.item == null || ui.item == undefined) {
            $(this).val("");
            //$(this).attr("disabled", false);
        } else {
            //$(this).attr("disabled", true);
        }
    }
	});
});

$(function(){
	$("#city").on('change', function(){
        var selected_city = $(this).val();
		//console.log(selected_city);
		$.ajax({
			type: "POST",
			url: base_url+'/mysubmissionsreport/getselectedcity',
			data: 'selected_city='+selected_city,
			success:function(data){
				var result = JSON.parse(data);
				if (result.success == "success") {
					var state_code_id = result.state_code_id;
					$(".states > [value=" + state_code_id + "]").attr("selected", "true");
				}
			},
			error: function(error) {
				//console.log("error data");
				console.log(error);
			} 
		});

	});
});

/*add job requirement page rate field validation*/

$(function() {
	$("#jobrate").autocomplete({
		source:base_url+'/mysubmissionsreport/showingjobrates', minLength:2,
		change: function (event, ui) {
        if (ui.item == null || ui.item == undefined) {
            $(this).val("");
			$("#job-rate-error").text(" Error: Use values between 10 to 150 range");
			$("#job-salary-rate-error").hide();
            //$(this).attr("disabled", false);
        } else {
            //$(this).attr("disabled", true);
			$("#job-rate-error").hide();
			$("#job-salary-rate-error").hide();
        }
    }
	});
});

/*add job requirement page salary rate field validation*/

$(function() {
	$("#salaryrate").autocomplete({
		source:base_url+'/mysubmissionsreport/showingsalary', minLength:1,
		change: function (event, ui) {
        if (ui.item == null || ui.item == undefined) {
            $(this).val("");
			$("#job-salary-rate-error").text(" Error: Use values between 1 to 300 range");
			$("#job-rate-error").hide();
            //$(this).attr("disabled", false);
        } else {
            //$(this).attr("disabled", true);
			$("#job-salary-rate-error").hide();
			$("#job-rate-error").hide();
        }
    }
	});
});

/*replace rate field label with salary when admin select full time from duration*/

$(function() {
	$("#job-duration").on('change', function(){
        var selected_duration = $(this).val();
		//console.log(selected_duration);
		if (selected_duration == "Full Time") {
			$("#rate-label").text("Salary");
			$("#jobrate").hide();
			$("#job-rate-error").hide();
			$("#salaryrate").show();
			$("#jobrate").removeAttr("required");
			$("#salaryrate").attr("required","required");
		}else{
			$("#rate-label").text("Rate");
			$("#jobrate").show();
			$("#salaryrate").hide();
			$("#job-rate-error").show();
			$("#salaryrate").removeAttr("required");
			$("#jobrate").attr("required","required");
			$("#job-salary-rate-error").hide();
		}
	});
});

/** Phone number validation for client form*/

$(function () {
	$("#client_contact").on("blur", function(e){
        var mobNum = $(this).val();
        var filter = /^\d*(?:\.\d{1,2})?$/;
		$("#lblError").html("");
          if (filter.test(mobNum)) {
            if(mobNum.length==10){
             } else {
                alert('Please put 10  digit mobile number');
                return false;
              }
            }
            else {
              //alert('Not a valid number');
			  $("#lblError").html(" Only Numbers allowed.");
			  $(this).val("");
              return false;
           }
    
  });
});

$(function(){
	$("#search-submittedby").autocomplete({
			source:base_url+'/mysubmissionsreport/sugggestrecruiter', minLength:2,
			change: function (event, ui) {
				console.log(ui.item);
	        if (ui.item == null || ui.item == undefined) {
	            $(this).val('');
	            $('#submittedby').val('');
	        } else {
	        	$('#submittedby').val(parseInt(ui.item.index));
	        }
    	}
	});
});

/* fetch  client name and job title */
$(function() {
	$("#jsub").autocomplete({
		source:base_url+'/mysubmissionsreport/getClientWithJobTitle', minLength:2,
		change: function (event, ui) {
        if (ui.item == null || ui.item == undefined) {
            $(this).val("");
			$("#job-pg-add-client-jobtitle").show();
            //$(this).attr("disabled", false);
        } else {
            //$(this).attr("disabled", true);
			$("#job-pg-add-client-jobtitle").hide();
        }
    }
	});
});

/**get values of managers from the dropdown */
// $(function() {
// 	$("#directors-list").on('change', function(){
// 		var director_ids = $('#directors-list').val();
// 		console.log(director_ids);
// 		$.ajax({
// 			type: "POST",
// 			url: base_url+'/assignmgrstodir/get_selected_mgrs_dir_ids',
// 			data: 'director_ids='+director_ids,
// 			success:function(data){
// 				console.log(data);
// 				//var result = JSON.parse(data);
// 				// if (result.success == "success") {
// 				// 	var state_code_id = result.state_code_id;
// 				// 	$(".styledselect_form_1 > [value=" + state_code_id + "]").attr("selected", "true");
// 				// }
// 			},
// 			error: function(error) {
// 				//console.log("error data");
// 				console.log(error);
// 			} 
// 		});
// 	});
// });

/**get values of managers from the dropdown */
// $(function() {
// 	$("#managers-list").on('change', function(){
// 		var manager_ids = $('#managers-list').val();
// 		console.log(manager_ids);
// 		$.ajax({
// 			type: "POST",
// 			url: base_url+'/assignmgrstodir/get_selected_mgrs_dir_ids',
// 			data: 'manager_ids='+manager_ids,
// 			success:function(data){
// 				console.log(data);
// 				//var result = JSON.parse(data);
// 				// if (result.success == "success") {
// 				// 	var state_code_id = result.state_code_id;
// 				// 	$(".styledselect_form_1 > [value=" + state_code_id + "]").attr("selected", "true");
// 				// }
// 			},
// 			error: function(error) {
// 				//console.log("error data");
// 				console.log(error);
// 			} 
// 		});
// 	});
// });
</script>
<script type="text/javascript">
(function( $ ) {
	$( document ).ready(function() {
	$('#cssmenu').prepend('<div id="menu-button">Menu</div>');
		$('#cssmenu #menu-button').on('click', function(){
			var menu = $(this).next('ul');
			if (menu.hasClass('open')) {
				menu.removeClass('open');
			}
			else {
				menu.addClass('open');
			}
		});

		var num = $("#dropdownli").find("li").length;
		if (num > 1) {
		console.log(num);
		}
	});
} )( jQuery );

</script>
</body>
</html>

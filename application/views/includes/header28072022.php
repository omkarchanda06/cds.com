<?php 
//echo $basename;
$basename=$this->uri->segment(1);
$basename2=$this->uri->segment(2);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panzeradmin - Home</title>
<link rel="stylesheet" href="<?php echo base_url()?>css/screen.css" type="text/css" media="screen" title="default" />
<link rel="stylesheet" href="<?php echo base_url()?>css/menu.css" type="text/css" media="screen" title="default" />
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"> -->
    <!-- Select2 CSS -->

<script src="<?php echo base_url()?>assets/js/libs/jquery.min.js"></script>

<!--[if IE]>
<link rel="stylesheet" media="all" type="text/css" href="css/pro_dropline_ie.css" />
<![endif]-->

<!--  Jquery core -->
<script src="<?php echo base_url()?>js/jquery/jquery-1.8.3.js" type="text/javascript"></script>

<!-- Jquery UI -->
<script src="<?php echo base_url()?>js/jquery/jquery-ui.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.css" type="text/css" media="screen" title="default"/>
<link rel="stylesheet" href="<?php echo base_url()?>css/style.css" type="text/css" media="screen" title="default"/>

<!-- Config JS -->
<script type="text/javascript">var base_url = window.location.protocol+'//'+window.location.host+'/cds.com/index.php';</script>

<script type="text/javascript" charset="utf-8">
$(document).ready(function ()  {
	 function exportTableToCSV($table, filename) {

        var $rows = $table.find('tr:has(td)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
            colDelim = '","',
            rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
                var $row = $(row),
                    $cols = $row.find('td');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                        text = $col.text();
						
                    	return text.replace('"', '""'); // escape double quotes

                }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim) + '"',

            // Data URI
            csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

        $(this)
            .attr({
            'download': filename,
            'href': csvData,
            'target': '_blank'
        });
    }

    // This must be a hyperlink
    $(".export").on('click', function (event) {
        // CSV
		
        exportTableToCSV.apply(this, [$('#product-table'), 'export.csv']);

        // IF CSV, don't do event.preventDefault() or return false
        // We actually need this to be a typical hyperlink
    });
});

</script>


</head>
<body> 
<?php $get_session_data = $this->session->userdata('logged_in'); ?>
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">

	<!-- start logo -->
	<div id="logo">
	<a href="<?php echo base_url()?>index.php/totalsubmissions"><img src="<?php echo base_url()?>images/shared/logo.png" /></a>	
	</div>
	<!-- end logo -->
	
	<!--  start top-search -->
	<?php if($basename == 'mysubmissions' || $basename == 'totalsubmissions' || $basename == 'search'){?>
	<div id="top-search">
	<form action="<?php echo base_url()?>index.php/search" method="get">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td style="font:bold 14px Cambria; color:#FFFFFF;">Search Resume:</td>
		</tr>
		<tr>
		<td width="200"><input type="text" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['tsearch'])):'');?>" name="tsearch" placeholder="Job Title" class="top-search-inp" /></td>
		<td><input type="text" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['ksearch1'])):'');?>" name="ksearch1" placeholder="Keyword 1" class="top-search-inp" /></td>
		<td><input type="text" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['ksearch2'])):'');?>" name="ksearch2" placeholder="Keyword 2" class="top-search-inp" /></td>
		<td><input type="text" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['ksearch3'])):'');?>" name="ksearch3" placeholder="Keyword 3" class="top-search-inp" /></td>
		<td><input type="text" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['ksearch4'])):'');?>" name="ksearch4" placeholder="Keyword 4" class="top-search-inp" /></td>

		<!-- <td width="150" align="right">
    <a href="<?php //echo base_url()?>index.php/cdsdashboard/changepwd" 
       style="color:#FFF;"><b>Welcome, User</b></a>
    </td> -->

        <?php 
        $userType = $this->session->userdata('logged_in')['usertype'];
        if($userType == 'SUPERADMIN' || $userType == 'ADMIN' || $userType == 'MANAGER'): ?>
        <td>
        <input type="text" 
               value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['candid'])):'');?>" 
               name="candid" placeholder="ID" class="top-search-inp"
               style="width:95px;"/>    
        </td>
        <?php endif; ?>
		
		</tr>
		<tr>
		<td width="200"><input type="text" value="<?=(isset($_REQUEST['cname'])?(htmlentities($_REQUEST['cname'])):'');?>" name="cname" placeholder="Consultant Name" class="top-search-inp" /></td>
		<td style="padding:5px 0;">
		<select name="datebtw" style="width:140px; height: 27px; padding: 3px 0;">
    <option value="">-- Uploaded Duration --</option>
    <option value="1 Week">1 Week</option>
    <option value="2 Week">2 Week</option>
    <option value="3 Week">3 Week</option>
    <option value="1 Month">1 Month</option>
    <option value="3 Month">3 Month</option>
    <option value="6 Month">6 Month</option>
    <option value="1 Year">1 Year</option>
    <option value="All">All</option>
    </select></td>
		<td style="padding:5px 0;">
    <!-- <select  name="location" style="width:140px; height: 27px; padding: 3px 0;">
			<option  value=""> Select Location </option>
			<?php //foreach($key1->result() as $result) { echo '<option value='.$result->lid.'>'.$result->location.'</option>'; }?>
			</select> -->
      <div class="multiselect">
      <div class="selectBox" onclick="showCheckboxes('loc')">
      <select><option style="display:none;">Select Location</option></select>
      <div class=""></div></div>
      <div id="checkboxes_loc">
        <?php foreach($key1->result() as $result) { 
              echo '<span class="check-loc"><input type="checkbox" name="location[]" value="'.$result->lid.'"/>&nbsp; '.$result->location.'</span>';
        }?>
      </div>
      </div>
		</td>
		<td style="padding:5px 0;">
    <select name="yrsexp" style="width:140px; height: 27px; padding: 3px 0;">
    <option value="">-- Years of exp --</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
    <option value="24">24</option>
    <option value="25">25</option>
    <option value="26">26</option>
    <option value="27">27</option>
    <option value="28">28</option>
    <option value="29">29</option>
    <option value="30">30</option>
    </select></td>
		<td align="left">
		<!-- <select name="visa" style="width:74px; height: 28px; position: relative; top: -4px;">
    <option value=""> VISA TYPE</option> -->
    <!-- <option value="1">H1</option>
    <option value="2">GC</option>
    <option value="3">CITIZEN</option>
    <option value="4">F1</option>
    <option value="5">EAD GC</option>
    <option value="6">TN Visa</option>
    <option value="7">EAD OPT</option> -->
    <?php //foreach($visatypes->result() as $visa) { echo '<option value='.$visa->vid.'>'.$visa->visatype.'</option>';}?>
    <!-- </select> -->
     <div class="multiselect">
        <div class="selectBox" onclick="showCheckboxes('visa')">
            <select><option style="display:none;">Select Visa Type</option></select>
            <div class=""></div>
        </div>
        <div id="checkboxes_visa">
       <?php foreach($visatypes->result() as $visa) { 
             echo '<span class="check-visa"><input type="checkbox" name="visa[]" value="'.$visa->vid.'"/>&nbsp; '.$visa->visatype.'</span>';
       }?>
        </div>
    </div>
    <style type="text/css">
    /* .multiselect {
        width: 140px;
    }
    .selectBox {
        position: relative;
    }
    .selectBox select {
        width: 140px;
        height: 27px;
    }
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    #checkboxes,
    #checkboxes_loc,
    #checkboxes_visa {
        width: 138px;
        display: none;
        border: 1px #dadada solid;
        position:absolute;
        background: white;
        max-height:250px; 
        overflow-y:scroll
    }
    #checkboxes label,
    #checkboxes_loc label,
    #checkboxes_visa label {
        display: block;
        padding: 0px 0px 0px 2px;
        color:black;
    }
    #checkboxes label:hover,
    #checkboxes_loc label:hover,
    #checkboxes_visa label:hover {
        background-color: #366289;
        color:white;
    }*/
    </style>
    <script type="text/javascript">
    var expanded = false;
    function showCheckboxes(type) {
      var type2;
        if(type == 'loc') {
           var type2 = 'visa';
        } else {
           var type2 = 'loc';
        }
        var checkboxes = document.getElementById("checkboxes_"+type);
        var checkboxes2 = document.getElementById("checkboxes_"+type2);
        if (!expanded) {
            checkboxes.style.display = "block";
            checkboxes2.style.display = "none";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }

    $('body').on('click', function(e){
    var $el = $(e.target);
    if($el.not(".multiselect") && $el.not(".selectBox") && $el.not("#checkboxes_loc")) {
       $('#checkboxes_loc').hide();
    }
    if($el.not(".multiselect") && $el.not(".selectBox") && $el.not("#checkboxes_visa")) {
       $('#checkboxes_visa').hide();
    }
    });

    $('.selectBox').click(function(e){
      e.stopPropagation(); 
    });

    $('.check-loc').click(function(e){
      e.stopPropagation(); 
    });

    $('.check-visa').click(function(e){
      e.stopPropagation(); 
    });

    </script>
		</td>
		<td width="150">
          <select name="relocate" style="width: 95px; height: 27px;">
              <option value="">--Relocate--</option>
              <option value="YES">YES</option>
              <option value="NO">NO</option>
          </select>
       </td>
		</tr>
    <tr>
      <td colspan="9" style="text-align: right; padding: 0px 22px;">
        <!-- <a href="<?php //echo base_url()?>index.php/totalsubmissions/logout" 
         style="color:#FFF; padding-right:10px;"><b>Logout</b></a> -->
        <input type="hidden" name="searchtype" value="<?=$searchtype;?>" /> 
        <input type="hidden" name="categorytype" value="<?php if(isset($categorytype)) echo $categorytype;?>" />
        <input type="image" src="<?php echo base_url()?>images/shared/search-button.png" 
           onMouseOver="this.src=window.location.origin+'/cds/images/shared/search-button-hover.png'" 
           onMouseOut="this.src=window.location.origin+'/cds/images/shared/search-button.png'" 
           onClick="this.src=window.location.origin+'/cds/images/shared/search-button-click.png'"/>
      </td>
    </tr>
		</table>
	</form>
	</div>
	<?php } else {?>
		<!-- <div style="position: relative; float: right; right: 17px; top: 18px; line-height: 30px; width: 100px; text-align: right;">
		<a href="<?php //echo base_url()?>index.php/cdsdashboard/changepwd" style="color:#FFF;"><b>My Profile</b></a> <br />
		
		<span style="position: relative; top:1px; left:-3px;"><a href="<?php //echo base_url()?>index.php/totalsubmissions/logout" style="color:#FFF; padding-right:10px;"><b>Logout</b></a></span>
		</div> -->
		
	<?php } ?>
 	<!--  end top-search -->
 	<div class="clear"></div>
  
</div>
<!-- End: page-top -->






<div id='cssmenu'>
<ul>
   <?php
    $session_data = $this->session->userdata('logged_in');
    $usertype = $session_data['usertype']; 
    $username = $session_data['username']; 
    if($usertype == "SUPERADMIN")
    {
    if($username != 'Report'){

   if(isset($menuname) && $menuname == 'users') { ?>
   <li class='active has-sub'>
   <?php } else { ?>
   <li class='has-sub'>
   <?php } ?>
   <a href='#'><span>User Management</span></a>
      <ul>
         <li class='has-sub'><a href='#'><span>Panzer</span></a>
            <ul>
               <li><a href='<?php echo base_url()?>index.php/cdsdashboard/panzer_userslist'><span>Active Users</span></a></li>
               <li class='last'><a href='<?php echo base_url()?>index.php/cdsdashboard/panzer_inactive_userslist'><span>Inactive Users</span></a></li>
            </ul>
         </li>
         <!-- <li class='has-sub'><a href='#'><span>RPO</span></a>
            <ul>
               <li><a href='<?php echo base_url()?>index.php/cdsdashboard/rpo_userslist'><span>Active Users</span></a></li>
               <li class='last'><a href='<?php echo base_url()?>index.php/cdsdashboard/rpo_inactive_userslist'><span>Inactive Users</span></a></li>
            </ul>
         </li> -->
         <!-- <li class='has-sub'><a href='#'><span>Canada</span></a>
            <ul>
               <li><a href='<?php echo base_url()?>index.php/cdsdashboard/canada_userslist'><span>Active Users</span></a></li>
               <li class="last"><a href='<?php echo base_url()?>index.php/cdsdashboard/canada_inactive_userslist'><span>Inactive Users</span></a></li>
            </ul>
         </li> -->
         <li><a href='<?php echo base_url()?>index.php/add_user/users_list'><span>All Active Users</span></a></li>
         <li><a href='<?php echo base_url()?>index.php/add_user/inactive_users_list'><span>All Inactive Users</span></a></li>
         <li class='last'><a href='<?php echo base_url()?>index.php/add_user'><span>Add New User</span></a></li>
      </ul>
   </li>
   <?php } } ?>

   <?php if(isset($menuname) && $menuname == 'submissions') { ?>
   <li class='active has-sub'>
   <?php } else { ?>
   <li class='has-sub'>
   <?php } ?>
   <a href='#'><span>Submissions</span></a>
      <ul>
         <?php if($session_data['usertype'] == 'SUPERADMIN'){ ?>
         <li class='has-sub'><a href='#'><span>Panzer</span></a>
            <ul>
               <li><a href='<?php echo base_url()?>index.php/totalsubmissions/total_panzer_submissions'><span>Total Submissions</span></a></li>
               <li class='last'><a href='<?php echo base_url()?>index.php/totalsubmissions/panzer_usumission'><span>Inactive Submissions</span></a></li>
            </ul>
         </li>
         <!-- <li class='has-sub'><a href='#'><span>RPO</span></a>
            <ul>
               <li><a href='<?php echo base_url()?>index.php/totalsubmissions/total_rpo_submissions'><span>Total Submissions</span></a></li>
               <li class='last'><a href='<?php echo base_url()?>index.php/totalsubmissions/rpo_usumission'><span>Inactive Submissions</span></a></li>
            </ul>
         </li>
         <li class='has-sub'><a href='#'><span>Canada</span></a>
            <ul>
               <li><a href='<?php echo base_url()?>index.php/totalsubmissions/total_canada_submissions'><span>Total Submissions</span></a></li>
               <li class='last'><a href='<?php echo base_url()?>index.php/totalsubmissions/canada_usumission'><span>Inactive Submissions</span></a></li>
            </ul>
         </li> -->
         <?php } ?>
         <li><a href='<?php echo base_url()?>index.php/totalsubmissions/index'>
         <span>
         <?php if($usertype == "SUPERADMIN") {
                echo  'All Total Submissions';
                } else {
                echo  'Total Submissions';  
                  }?>
         </span></a></li>
         <?php if($session_data['usertype'] == 'SUPERADMIN') { ?>
         <li><a href='<?php echo base_url()?>index.php/totalsubmissions/usumission'><span>All Inactive Submissions</span></a></li>
         <?php } ?>
         <?php if($get_session_data['username'] != 'Demo' && $session_data['usertype'] != 'SUPERADMIN' && $session_data['category'] != 4 ) { ?>
         <li><a href='<?php echo base_url()?>index.php/totalsubmissions/mysubmissions'><span>My Submissions</span></a></li>
         <li><a href='<?php echo base_url()?>index.php/cdsdashboard/add_submissions'><span>Add Submission</span></a></li>
         <?php } ?>
      </ul>
   </li>

   <?php if($get_session_data['username'] != 'Demo') { ?>
   <?php if(isset($menuname) && $menuname == 'recruiters') { ?>
   <li class='active has-sub'>
   <?php } else { ?>
   <li class='has-sub'>
   <?php } ?>
   <a href='#'><span>Recruiters</span></a>
      <ul>
         <?php if($session_data['usertype'] == 'SUPERADMIN'){ ?>
         <li class='has-sub'><a href='#'><span>Panzer</span></a>
            <ul>
               <li><a href='<?php echo base_url()?>index.php/cdsdashboard/panzer_recruiters'><span>Recruiters</span></a></li>
               <li><a href='<?php echo base_url()?>index.php/cdsdashboard/panzer_attendance'><span>Attendance</span></a></li>
            </ul>
         </li>
         <!-- <li class='has-sub'><a href='#'><span>RPO</span></a>
            <ul>
               <li><a href='<?php echo base_url()?>index.php/cdsdashboard/rpo_recruiters'><span>Recruiters</span></a></li>
               <li><a href='<?php echo base_url()?>index.php/cdsdashboard/rpo_attendance'><span>Attendance</span></a></li>
            </ul>
         </li>
         <li class='has-sub'><a href='#'><span>Canada</span></a>
            <ul>
               <li><a href='<?php echo base_url()?>index.php/cdsdashboard/canada_recruiters'><span>Recruiters</span></a></li>
               <li><a href='<?php echo base_url()?>index.php/cdsdashboard/canada_attendance'><span>Attendance</span></a></li>
            </ul>
         </li> -->
         <?php } ?>
         <?php if($session_data['usertype'] != 'SUPERADMIN' && $session_data['category'] != 4){ ?>
         <li><a href='<?php echo base_url()?>index.php/cdsdashboard/recruiters_attendance'><span>Team Attendance</span></a></li>
         <li><a href='<?php echo base_url()?>index.php/cdsdashboard/recruiters'><span>My Recruiters</span></a></li>
         <?php } ?>
         <?php if($session_data['usertype'] == 'SUPERADMIN' && $session_data['category'] != 4){ ?>
         <li><a href='<?php echo base_url()?>index.php/cdsdashboard/add_recruiter'><span>Add My Recruiter</span></a></li>
         <?php } ?>
         <?php  $session_data = $this->session->userdata('logged_in'); if($session_data['usertype'] == 'SUPERADMIN'){ ?>
         <li><a href='<?php echo base_url()?>index.php/cdsdashboard/all_recruiters_attendance'><span>All Attendance</span></a></li>
         <li><a href='<?php echo base_url()?>index.php/cdsdashboard/all_recruiters'><span>All Recruiters</span></a></li>
         <li class='last'><a href='<?php echo base_url()?>index.php/cdsdashboard/add_recruiters'><span>Add New Recruiter</span></a></li>
         <?php } ?>
      </ul>
   </li>
   <?php } ?>

   <?php if($get_session_data['username'] != 'Demo') { ?>
   <?php if(isset($menuname) && $menuname == 'submissionscount') { ?>
   <li class='active has-sub'>
   <?php } else { ?>
   <li class='has-sub'>
   <?php } ?>
   <a href='#'><span>Submissions Count</span></a>
      <ul>
         <?php if($usertype == "SUPERADMIN" || ($session_data['usertype'] == "ADMIN" && $session_data['category'] == 1)) {?>
         <li class='has-sub'><a href='#'><span>Panzer</span></a>
            <ul>
               <li><a href='<?php echo base_url()?>index.php/managercount/category/panzer'><span>Managers Count</span></a></li>
               <li class='last'><a href='<?php echo base_url()?>index.php/recruitercount/category/panzer'><span>Recruiters Count</span></a></li>
            </ul>
         </li>
         <?php } if($usertype == "SUPERADMIN" || ($session_data['usertype'] == "ADMIN" && $session_data['category'] == 2)) {?>
         <!-- <li class='has-sub'><a href='#'><span>RPO</span></a>
            <ul>
               <li><a href='<?php echo base_url()?>index.php/managercount/category/rpo'><span>Managers Count</span></a></li>
               <li class='last'><a href='<?php echo base_url()?>index.php/recruitercount/category/rpo'><span>Recruiters Count</span></a></li>
            </ul>
         </li>
         <?php } if($usertype == "SUPERADMIN" || ($session_data['usertype'] == "ADMIN" && $session_data['category'] == 3)) {?>
         <li class='has-sub'><a href='#'><span>Canada</span></a>
            <ul>
               <li><a href='<?php echo base_url()?>index.php/managercount/category/canada'><span>Managers Count</span></a></li>
               <li class='last'><a href='<?php echo base_url()?>index.php/recruitercount/category/canada'><span>Recruiters Count</span></a></li>
            </ul>
         </li> -->
         <?php } ?>
         <li><a href='<?php echo base_url()?>index.php/managercount'><span><?php if($usertype == "SUPERADMIN") { echo  'All'; }?> Managers Count</span></a></li>
         <li class='last'><a href='<?php echo base_url()?>index.php/recruitercount'><span><?php if($usertype == "SUPERADMIN") { echo  'All'; }?> Recruiters Count</span></a></li>
      </ul>
   </li>
   <?php } ?>
   
   <!-- requirement section -->
   <!-- <?php if($get_session_data['username'] != 'Demo') { if($usertype == "MANAGER" || $usertype == "ADMIN" || $usertype == "SUPERADMIN") { ?>
   <?php if(isset($menuname) && $menuname == 'requirement') { ?>
   <li class='active has-sub'>
   <?php } else { ?>
   <li class='has-sub'>
   <?php } ?>
   <a href='#'><span>Requirements</span></a>
   <ul>
     <li><a href='<?php echo base_url()?>index.php/requirement/addrequirement'><span>Add Requirement</span></a>
     <li><a href='<?php echo base_url()?>index.php/requirement'><span>View Requirements</span></a>
     <?php if($usertype == "SUPERADMIN") { ?>
     <li class='last'><a href='<?php echo base_url()?>index.php/requirement/all'><span>All Requirements</span></a>
     <?php } ?>
   </ul>
   </li>
   <?php } } ?> -->
   <!-- end of requirement section -->


   <!-- Job Title section -->
   <?php if($get_session_data['username'] != 'Demo') { 
            if($usertype == "SUPERADMIN") { ?>
   <?php if(isset($menuname) && $menuname == 'jobtitle') { ?>
   <li class='active has-sub'>
   <?php } else { ?>
   <li class='has-sub'>
   <?php } ?>
   <a href='#'><span>Job Title</span></a>
   <ul>
     <li><a href='<?php echo base_url()?>index.php/jobtitle/addjobtitle'><span>Add Job Title</span></a></li>
     <li><a href='<?php echo base_url()?>index.php/jobtitle'><span>View Job Titles</span></a></li>
   </ul>
   </li>
   <?php } } ?>
   <!-- end of requirement section -->



   <!-- Blacklisted Section -->
   <?php if(isset($menuname) && $menuname == 'blacklist') { ?>
   <li class='active has-sub'>
   <?php } else { ?>
   <li class='has-sub'>
   <?php } ?>
   <a href='#'><span>Blacklisted</span></a>
   <ul>

     <?php if($usertype == "SUPERADMIN" || $username == "HR"): ?>
     <li class='has-sub'><a href='<?php echo base_url()?>index.php/blacklist/candidate'><span>Candidate</span></a>
          <ul>
             <li><a href='<?php echo base_url()?>index.php/blacklist/candidate'><span>View Candidates</span></a></li>
             <li><a href='<?php echo base_url()?>index.php/blacklist/addcandidate'><span>Add Candidate</span></a></li>
          </ul>
      </li>
     <?php else: ?>
      <li><a href='<?php echo base_url()?>index.php/blacklist/candidate'><span>Candidate</span></a></li>
     <?php endif; ?>


     <?php if($usertype == "SUPERADMIN" || $username == "HR"): ?>
     <li class='has-sub'><a href='<?php echo base_url()?>index.php/blacklist/employer'><span>Employer</span></a>
          <ul>
             <li><a href='<?php echo base_url()?>index.php/blacklist/employer'><span>View Employer</span></a></li>
             <li><a href='<?php echo base_url()?>index.php/blacklist/addemployer'><span>Add Employer</span></a></li>
          </ul>
      </li>
     <?php else: ?>
     <li><a href='<?php echo base_url()?>index.php/blacklist/employer'><span>Employer</span></a></li>
     <?php endif; ?>

     <li class='has-sub'><a href='<?php echo base_url()?>index.php/blacklist/vendors'><span>Vendor</span></a>
          <ul>
             <li><a href='<?php echo base_url()?>index.php/blacklist/vendors'><span>View Vendors</span></a></li>
             <li><a href='<?php echo base_url()?>index.php/blacklist/addvendor'><span>Add Vendor</span></a></li>
          </ul>
      </li>

   </ul>
   </li>
   <!-- End of Blacklisted Section -->



   <!-- Job Title section -->
   <?php if(isset($menuname) && $menuname == 'visacopy') { ?>
   <li class='active'>
   <?php } else { ?>
   <li>
   <?php } ?>
   <a href='<?php echo base_url()?>index.php/visacopy'><span>Visa Copy</span></a></li>
   <!-- end of requirement section -->

   <!-- Clients section -->
   <?php if($get_session_data['username'] != 'Demo') { 
            if($usertype == "SUPERADMIN" || $usertype == "DIRECTOR") { ?>
   <?php if(isset($menuname) && $menuname == 'client') { ?>
   <li class='active has-sub'>
   <?php } else { ?>
   <li class='has-sub'>
   <?php } ?>
   <a href='#'><span>Clients</span></a>
   <ul>
     <li><a href='<?php echo base_url()?>index.php/client/clientform'><span>Add Client</span></a></li>
     <li><a href='<?php echo base_url()?>index.php/client'><span>View Clients</span></a></li>
   </ul>
   </li>
   <?php } } ?>
   <!-- end of Clients section -->

   <!-- Clients section -->
   <?php if($get_session_data['username'] != 'Demo') { 
            if($usertype == "SUPERADMIN" || $usertype == "DIRECTOR") { ?>
   <?php if(isset($menuname) && $menuname == 'requirements') { ?>
   <li class='active has-sub'>
   <?php } else { ?>
   <li class='has-sub'>
   <?php } ?>
   <a href='#'><span>Requirements</span></a>
   <ul>
     <li><a href='<?php echo base_url()?>index.php/requirements/add_requirement'><span>Add Job</span></a></li>
     <li><a href='<?php echo base_url()?>index.php/requirements/'><span>View Job Requirements</span></a></li>
   </ul>
   </li>
   <?php } } ?>
   <!-- end of Clients section -->





   <?php  //if($usertype == "ADMIN" || $usertype == "SUPERADMIN") { if($username != 'Report') {
          //if(isset($menuname) && $menuname == 'intreport') { ?>
   <!-- <li class='active'> -->
   <?php //} else { ?>
   <!-- <li> -->
   <?php //} ?>
   <!-- <a href='<?php //echo base_url()?>index.php/intreport'><span>Interview Report</span></a></li> -->
   <?php //if(isset($menuname) && $menuname == 'log') { ?>
   <!-- <li class='active'> -->
   <?php //} else { ?>
   <!-- <li class=''> -->
   <?php //} ?>
   <!-- <a href='<?php //echo base_url()?>index.php/log'><span>Log</span></a></li> -->
   <?php //} } ?> 
 
   <?php if(isset($menuname)  && $menuname == 'profile') { ?>
   <li class='active last has-sub' id="float-right">
   <?php } else { ?>
   <li class='last has-sub' id="float-right">
   <?php } ?>
   <a href='#'><span><?php echo trim(explode(' ', $get_session_data['username'])[0]); ?></span></a>
      <ul>
         <?php if($get_session_data['username'] != 'Demo') { ?>
         <li><a href='<?php echo base_url()?>index.php/dashboard/changepwd'><span>View Profile</span></a></li>
         <?php } ?>
         <li><a href='<?php echo base_url()?>index.php/dashboard/logout'><span>Log out</span></a></li>
      </ul>
   </li>
</ul>
</div>














</div>
<!-- End: page-top-outer -->
	
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat................................................................................................. START -->
<!-- <div class="nav-outer-repeat">  -->
<!--  start nav-outer -->
<!-- <div class="nav-outer">  -->
<!-- start nav-right -->

<!-- </div> -->
<!--  start nav-outer -->
<!-- </div> -->
<!--  start nav-outer-repeat................................................... END -->

 <div class="clear"></div>
 
<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer">
<!-- start content -->
<div id="content">
 

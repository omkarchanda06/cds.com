
<?php 
//$basename = strstr(basename($_SERVER['REQUEST_URI']), ".php", true);
//if(!isset($_SESSION) || $_SESSION['uid'] ==''){
//header("Location:index.php");
//exit;
//}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panzeradmin - Home</title>
<link rel="stylesheet" href="<?php echo base_url()?>css/screen.css" type="text/css" media="screen" title="default" />
<!--[if IE]>
<link rel="stylesheet" media="all" type="text/css" href="css/pro_dropline_ie.css" />
<![endif]-->

<!--  jquery core -->
<script src="<?php echo base_url()?>js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>
 
  


<![if !IE 7]>

<!--  styled select box script version 1 -->
<script src="<?php echo base_url()?>js/jquery/jquery.selectbox-0.5.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.styledselect').selectbox({ inputClass: "selectbox_styled" });
});
$(document).ready(function () {

    function exportTableToCSV($table, filename) {

        var $rows = $table.find('tr:has(td)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
            colDelim = '","',
            rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
                var $row = $(row),
                    $cols = $row.find('td');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                        text = $col.text();

                    return text.replace('"', '""'); // escape double quotes

                }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim) + '"',

            // Data URI
            csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

        $(this)
            .attr({
            'download': filename,
                'href': csvData,
                'target': '_blank'
        });
    }

    // This must be a hyperlink
    $(".export").on('click', function (event) {
        // CSV
		alert('helllllllllllll');
        exportTableToCSV.apply(this, [$('#product-table'), 'export.csv']);

        // IF CSV, don't do event.preventDefault() or return false
        // We actually need this to be a typical hyperlink
    });
});
</script>
 

<![endif]>


<!--  styled select box script version 2 --> 
<script src="<?php echo base_url()?>js/jquery/jquery.selectbox-0.5_style_2.js" type="text/javascript"></script>
<!--<script type="text/javascript">
$(document).ready(function() {
	$('.styledselect_form_1').selectbox({ inputClass: "styledselect_form_1" });
	$('.styledselect_form_2').selectbox({ inputClass: "styledselect_form_2" });
});
</script>-->



<!--  styled file upload script --> 
<script src="<?php echo base_url()?>js/jquery/jquery.filestyle.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(function() {
	$("input.file_1").filestyle({ 
	image: "<?php echo base_url()?>images/forms/upload_file.gif",
	imageheight : 29,
	imagewidth : 78,
	width : 300
	});
});
</script>


</head>
<body> 
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">

	<!-- start logo -->
	<div id="logo">
	<a href="<?php echo base_url()?>index.php"><img src="<?php echo base_url()?>images/shared/logo.png" /></a>	</div>
	<!-- end logo -->
	
	<!--  start top-search -->
	<div id="top-search">
	<form action="search.php">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><input type="text" value="Search" name="search" onblur="if (this.value=='') { this.value='Search'; }" onfocus="if (this.value=='Search') { this.value=''; }" class="top-search-inp" /></td>
<?php
//if($basename == 'mysubmissions'){
//echo "<input type='hidden' name='mysubmissions' value='1'>";
//}
?>
		<td>
		<input type="image" src="<?php echo base_url()?>images/shared/top_search_btn.gif"  />
		</td>
		</tr>
		</table>
	</form>
	</div>
 	<!--  end top-search -->
 	<div class="clear"></div>

</div>
<!-- End: page-top -->

</div>
<!-- End: page-top-outer -->
	
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat................................................................................................. START -->
<div class="nav-outer-repeat"> 
<!--  start nav-outer -->
<div class="nav-outer"> 
<?php 
//echo $basename;
$basename = basename($_SERVER["SCRIPT_FILENAME"]); 
if($basename == 'users' || $basename == 'inactiveusers' || $basename == 'adduser'){
$user_active='current';
$user_active_1='select_sub show';
$sub_active='select';
$sub_active_1='select_sub';
$sub_count='select';
$sub_r='select';
$sub_r_1='select_sub';
$sub_count_1='select_sub';
$sub_inter='select';
$sub_inter_1='select_sub';
$sub_tar='select';
$sub_tar_1='select_sub';
}else if($basename == 'mysubmissions' || $basename == 'totalsubmissions' || $basename == 'addsubmission'){
$user_active='select';
$user_active_1='select_sub';
$sub_active='current';
$sub_active_1='select_sub show';
$sub_count='select';
$sub_r='select';
$sub_r_1='select_sub';
$sub_count_1='select_sub';
$sub_inter='select';
$sub_inter_1='select_sub';
$sub_tar='select';
$sub_tar_1='select_sub';

}else if($basename == 'addrecruiter' || $basename == 'recruiters' || $basename == 'recruiters_attendance'){
$user_active='select';
$user_active_1='select_sub';
$sub_active='select';
$sub_active_1='select_sub';
$sub_r='current';
$sub_r_1='select_sub show';
$sub_count='select';
$sub_count_1='select_sub';
$sub_inter='select';
$sub_inter_1='select_sub';
$sub_tar='select';
$sub_tar_1='select_sub';

}else if($basename == 'submissionscount' || $basename == 'rsubmissionscount'){
$user_active='select';
$user_active_1='select_sub';
$sub_active='select';
$sub_active_1='select_sub';
$sub_count='current';
$sub_count_1='select_sub show';
$sub_r='select';
$sub_r_1='select_sub';
$sub_inter='select';
$sub_inter_1='select_sub';
$sub_tar='select';
$sub_tar_1='select_sub';

}else if($basename == 'mtargets' || $basename == 'rtargets'){
$user_active='select';
$user_active_1='select_sub';
$sub_active='select';
$sub_active_1='select_sub';
$sub_count='select';
$sub_count_1='select_sub';
$sub_r='select';
$sub_r_1='select_sub';
$sub_inter='select';
$sub_inter_1='select_sub';
$sub_tar='current';
$sub_tar_1='select_sub show';

}else if($basename == 'minterviews' || $basename == 'rinterviews'){
$user_active='select';
$user_active_1='select_sub';
$sub_active='select';
$sub_active_1='select_sub';
$sub_count='select';
$sub_count_1='select_sub';
$sub_r='select';
$sub_r_1='select_sub';
$sub_tar='select';
$sub_tar_1='select_sub';
$sub_inter='current';
$sub_inter_1='select_sub show';

}else{
$user_active='select';
$user_active_1='select_sub';
$sub_active='select';
$sub_active_1='select_sub';
$sub_count='select';
$sub_count_1='select_sub';
$sub_r='select';
$sub_r_1='select_sub';
$sub_tar='select';
$sub_tar_1='select_sub';
$sub_inter='select';
$sub_inter_1='select_sub';
}
?>
		<!-- start nav-right -->
		<div id="nav-right">
		
			<div class="nav-divider">&nbsp;</div>
			<a id="logout" href='<?php echo base_url()?>index.php/cdsdashboard/changepwd'>Change Password</a>
			<div class="nav-divider">&nbsp;</div>
			<a href="<?php echo base_url()?>index.php/totalsubmissions/logout" id="logout">Logout</a>
			<div class="clear">&nbsp;</div>
	
		</div>
		<!-- end nav-right -->


		<!--  start nav -->
		<div class="nav">
		<div class="table">
<?php 
//if($_SESSION['utype'] == 'ADMIN'){
?>
		<ul class="<?php echo $user_active;?>"><li><a href="<?php echo base_url()?>index.php/cdsdashboard/users_list"><b>User Management</b>
		<div class="<?php echo $user_active_1;?>">
			<ul class="sub">
				<li><a href="<?php echo base_url()?>index.php/cdsdashboard/users_list">Active Users</a></li>
				<li><a href="<?php echo base_url()?>index.php/cdsdashboard/inactive_users_list">Inactive Users</a></li>
				<li><a href="<?php echo base_url()?>index.php/cdsdashboard/add_user">Add New User</a></li>
			</ul>
		</div>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
		</li>
		</ul>
<?php 
//}
?>		
		<div class="nav-divider">&nbsp;</div>
		                    
		<ul class="<?php echo $sub_active;?>"><li><a href="<?php echo base_url()?>index.php/totalsubmissions/index"><b>Submissions</b>
		<div class="<?php echo $sub_active_1;?>">
			<ul class="sub">
				<li><a href="<?php echo base_url()?>index.php/totalsubmissions/mysubmissions">My Submissions</a></li>
				
				<li><a href="<?php echo base_url()?>index.php/totalsubmissions/index">Total Submissions</a></li>
				<li><a href="<?php echo base_url()?>index.php/cdsdashboard/add_submissions">Add Submission</a></li>
				<!--<li><a href="<?php echo base_url()?>interview_scheduling.php">Interview Scheduling</a></li>
				<li><a href="<?php echo base_url()?>report.php">Interviews Report</a></li>-->
			 
			</ul>
		</div>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->

		</li>
		</ul>
		
		<div class="nav-divider">&nbsp;</div>		
		
		<ul class="<?php echo $sub_r;?>"><li><a href="<?php echo base_url()?>index.php/cdsdashboard/recruiters"><b>Recruiters</b>
		<div class="<?php echo $sub_r_1;?>">
			<ul class="sub">
				<li><a href="<?php echo base_url()?>index.php/cdsdashboard/add_recruiter">Add Recruiter</a></li>
				<li><a href="<?php echo base_url()?>index.php/cdsdashboard/recruiters">Recruiters</a></li>	
			 	<li><a href="<?php echo base_url()?>recruiters_attendance.php">Attendance</a></li>
			</ul>
		</div>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->

		</li>
		</ul>
				
		<div class="nav-divider">&nbsp;</div>
		
		<ul class="<?php echo $sub_count;?>"><li><a href="<?php echo base_url()?>submissionscount.php"><b>Submissions Count</b>
		<div class="<?php echo $sub_count_1;?>">
			<ul class="sub">
				<li><a href="<?php echo base_url()?>submissionscount.php">Managers Count</a></li>
				<li><a href="<?php echo base_url()?>rsubmissionscount.php">Recruiters Count</a></li>	
			 
			</ul>
		</div>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->

		</li>
		</ul>
				
		<div class="nav-divider">&nbsp;</div>
<?php //if($_SESSION['utype'] == 'ADMIN' ){ ?>
		<ul class="<?php echo $sub_inter;?>"><li><a href="<?php echo base_url()?>minterviews.php"><b>Interviews</b>
		<div class="<?php echo $sub_inter_1;?>">
			<ul class="sub">
				<li><a href="<?php echo base_url()?>minterviews.php">Managers Interviews</a></li>
				<li><a href="<?php echo base_url()?>rinterviews.php">Recruiters Interviews</a></li>	
			 
			</ul>
		</div>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->

		</li>
		</ul>
				
		<div class="nav-divider">&nbsp;</div>


		<ul class="<?php echo $sub_tar;?>"><li><a href="<?php echo base_url()?>mtargets.php"><b>Targets</b>
		<div class="<?php echo $sub_tar_1;?>">
			<ul class="sub">
				<li><a href="<?php echo base_url()?>mtargets.php">Managers Targets</a></li>
				<li><a href="<?php echo base_url()?>rtargets.php">Recruiters Targets</a></li>	
			 
			</ul>
		</div>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->

		</li>
		</ul>
				
		<div class="nav-divider">&nbsp;</div>
	<?php
//}
?>
		
		

		
		
		<div class="clear"></div>
		</div>
		<div class="clear"></div>
		</div>
		<!--  start nav -->

</div>
<div class="clear"></div>
<!--  start nav-outer -->
</div>
<!--  start nav-outer-repeat................................................... END -->

 <div class="clear"></div>
 
<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer">
<!-- start content -->
<div id="content">
 

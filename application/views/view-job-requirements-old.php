<?php $this->load->view('includes/header'); ?>

<div id="page-heading"><h1>Job Requirements</h1></div>

<table width="100%" cellspacing="0" cellpadding="0" border="0" id="content-table">
<tbody>
<tr>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowleft.jpg"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowright.jpg"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
   
 <!-- start product-table-->
	<!-- <form id="mainform"> -->

				<!-- <div class="search_area">
                <input type="search" class="light-table-filter" data-table="target-table" placeholder="Type here to search the requirement" />
                <img class="reset-search-bar" src="<?php echo base_url() ?>img/close-modal.png"/>
                </div> -->
                <!-- <div class="reset-search-bar"></div> -->

				<table border="0" width="98%" cellpadding="0" cellspacing="0" 
				       id="product-table" class="target-table">
				<thead>
					<th class="table-header-repeat line-left minwidth-1">ID</th>
					<th class="table-header-repeat line-left minwidth-2">Job Title</th>
					<th class="table-header-repeat line-left minwidth-2">City</th>
					<th class="table-header-repeat line-left minwidth-2">State</th>
					<th class="table-header-repeat line-left minwidth-2">Duration</th>
					<th class="table-header-repeat line-left minwidth-2">Bill Rate</th>
					<th class="table-header-repeat line-left minwidth-2">Client Company</th>
					<th class="table-header-repeat line-left minwidth-2">Job Description</th>
					<th class="table-header-repeat line-left minwidth-2">Date</th>
					<th class="table-header-repeat line-left minwidth-2">Edit</th>
					<th class="table-header-repeat line-left minwidth-2">Submit</th>
				</thead>
				<tbody>
				<?php foreach($jobrequirementsdetails as $index => $req_details) { ?>
					<tr class="req-row-<?php echo $req_details->id; ?>">

						<td style="text-align:center;"><?=$req_details->id?></td>
						<td><?=$req_details->job_title?></td>
						<td><?=$req_details->city?></td>
						<td><?=$states[$index]->statename?></td>
						<td><?=$req_details->duration?></td>
						<td><?=$req_details->bill_rate?></td>
						<td><?=$req_details->client_company?></td>
						<td><?=$req_details->job_description?></td>
						<td><?=$req_details->date?></td>
						<td style="text-align:center;" class="options-width"><a class="edit-job" href="<?php echo base_url() ?>index.php/requirements/edit/<?=$req_details->id?>" id="<?=$req_details->id?>">Edit</a></td><td><a href="<?php echo base_url() ?>index.php/requirements/submit/<?=$req_details->id?>">Submit</a></td>
					</tr>
                <?php }?>	
                </tbody>			
				</table>
                <div style="padding:0px 0px 420px 0px;"></div>				
				<!--  end product-table................................... --> 
				<!-- </form> -->

	<div class="clear"></div>
	 
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</tbody>
</table>

<script type="text/javascript" src="<?php echo base_url(); ?>js/custom.js"></script>
<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url()?>css/profile.css" />
<link rel="stylesheet" href="<?php echo base_url()?>css/add-edit-user.css" />
<link rel="stylesheet" href="<?php echo base_url()?>css/resume_db.css" />

 <div class="search-resume-div" style="width: fit-content;padding-right: 60px;margin-left: 15%;">
    <div class="search-inputs">
      <div class="search-text">Inactive Users [ <?php if(isset($category)) echo $category; else echo 'All'; ?> ]</div>
    </div>
  </div>
  <!-- table -->
  <div class="table-div">
  <?php
			//$error='';
			if($error!=''){
			echo $error;	
			}
			?>
    <div class="table">
      <table id="myTable" style="width: 80% !important;">
        <thead>
          <tr>
            <th class="table-header" style="border-top-left-radius: 20px"> S.NO </th>
            <th class="table-header">User Name</th>
            <th class="table-header">User Type</th>
            <th class="table-header">Status</th>
            <th class="table-header" style="border-top-right-radius: 20px">Action</th>
          </tr>
        </thead>
        <tbody>
        <?php $i=1;
        foreach($keys->result() as $rows) {
        $_SESSION['utype']=$rows->usertype;?>
          <tr>
            <td class="table-data"><?php echo $i;?> </td>
            <td class="table-data"><?php echo $rows->username?></td>
            <td class="table-data"><?php echo $rows->usertype ;?></td>
            <td class="table-data"><?php echo $rows->status ?> </td>
            <td class="table-data">
              <a href="<?php echo base_url(); ?>index.php/add_user/users_edit/<?php echo $rows->id;?>">
                <svg xmlns="http://www.w3.org/2000/svg"
                  xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img"
                  class="iconify iconify--material-symbols" width="20" height="20" preserveAspectRatio="xMidYMid meet"
                  viewBox="0 0 24 24">
                  <path fill="currentColor"
                    d="M5 19h1.4l8.625-8.625l-1.4-1.4L5 17.6ZM19.3 8.925l-4.25-4.2l1.4-1.4q.575-.575 1.413-.575q.837 0 1.412.575l1.4 1.4q.575.575.6 1.388q.025.812-.55 1.387ZM17.85 10.4L7.25 21H3v-4.25l10.6-10.6Zm-3.525-.725l-.7-.7l1.4 1.4Z">
                  </path>
                </svg>
              </a>
              <a href="<?php echo base_url(); ?>index.php/add_user/users_delete/<?php echo $rows->id;?>">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true"
                  role="img" class="iconify iconify--charm" width="20" height="20" preserveAspectRatio="xMidYMid meet"
                  viewBox="0 0 16 16">
                  <path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                    stroke-width="1.5" d="m10.25 5.75l-4.5 4.5m0-4.5l4.5 4.5m-7.5-7.5h10.5v10.5H2.75z">
                  </path>
                </svg>
              </a>
            </td>
          </tr>
          <?php $i++; } ?>
        </tbody>
    </table>
<?php $this->load->view('includes/footer'); ?>
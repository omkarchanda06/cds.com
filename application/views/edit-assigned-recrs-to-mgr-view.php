<?php $this->load->view('includes/header'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js" defer></script>
<?php
// print_r($managerslist);exit();
    $attributes = array('id' => 'assign-recrs-to-mgr');
    echo form_open('assignrecrstomgr/update', $attributes); ?>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
            <tr valign="top" height="30">
                <td><span style="color:blue; line-height: 23px;"><?php if($message!=''){ echo $message; }?></span>	
                    <table cellspacing="0" cellpadding="0" border="0" id="id-form" width="100%" >
                        <tbody>
                            <tr>
                                <th valign="top">Managers</th>
                                <td class="noheight">
                                    <select name="managerslist" id="managers-list" required>
                                            <option value=""> Select Manager </option>
                                            <?php foreach ($managerslist as $manager) {?>
                                            <option <?php if($managerID == $manager->id){echo "selected=selected";} ?>value="<?php echo $manager->id; ?>"><?php echo $manager->username; ?></option>
                                            <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th valign="top">Recruiters :</th>
                                <td class="noheight">
                                    <select name="recruiterslist[]" id="recruiter-list" multiple required>
                                            <option value=""> Select Recruiters </option>
                                            <?php
                                                foreach ($recruiterslist as $recruiter) {?>
                                                    <option <?php 
                                                    $selected_recruiter = $this->assignrecruiterstomgr->get_selected_recrs_by_MgrID($managerID, $recruiter->id); 
                                                    if($selected_recruiter->isActive == 1){echo "selected=selected";}?> value="<?php echo $recruiter->id; ?>"><?php echo $recruiter->recruiter; ?></option>
                                        <?php }?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>&nbsp;</th>
                                <td valign="top" style="padding-top: 10px;">
                                    <input type="hidden" name="managerid" value="<?php echo $managerID;?>">
                                    <input type="submit" name="assignrecrtomgr" class="form-submit" value="update recruiters to manager"/>
                                    <input type="reset" class="form-reset" value="">
                                </td>
                                <td></td>
                            </tr>
                        
                        </tbody>
                    </table>
                    <!-- end id-form  -->

                </td>
            </tr>
            </tbody>
        </table>
    </form>

<?php $this->load->view('includes/footer'); ?>
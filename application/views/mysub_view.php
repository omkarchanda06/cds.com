<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/profile.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>css/add-edit-user.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url() ?>css/jquery-ui.css" />
  <script src="<?php echo base_url() ?>js/jquery/jquery-1.8.3.js"></script>
  <script language="JavaScript" src="<?php echo base_url() ?>js/jquery/jquery.validate.js"></script>
  <script src="<?php echo base_url() ?>js/jquery/jquery-ui.js"></script>
<script>
    $(function(){
        $("#submissionForm").validate({
            rules: {
                copypaste: {
                required: true,
                minlength: 500
                }
            }
        });
    });
</script>
<div id="page-heading"><h1>Submission Edit Form</h1></div>
<?php
foreach($keys->result() as $result){
	
}
//print_r($sub_attachments);
?>

<form name="registration" id="submissionForm" method="post" onsubmit="return v.exec()" enctype="multipart/form-data">
<input type="hidden" name="sid" value="<?php echo $result->id;?>" />

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"><img src="<?php echo base_url()?>images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="<?php echo base_url()?>images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td >
		<!-- start id-form -->
		<table border="0"  width="100%"  cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
		<th valign="top">Select a date:</th>
		<td class="noheight">
		<?php if($this->session->userdata('logged_in')['id'] != 57) { ?>
		<input type="text" class="inp-form tcal required" value="<?php echo $result->sdate;?>" readonly disabled="disabled"/>
		<input type="hidden" name="sdate" id="sdate" value="<?php echo $result->sdate;?>"/>
		<?php } else { ?>
		<input type="text" class="inp-form tcal required" name="sdate" id="sdate" value="<?php echo $result->sdate;?>"/>	
		<?php }?>
		</td>
		<td></td>
	</tr>
		<tr>
			<th valign="top">Consultant Name :</th>
			<td><input type="text" name='cname' class="inp-form required" value="<?php echo $result->cname;?>" /></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Position Submitted to :</th>
			<td><input type="text" class="search inp-form required" name="psub"  value="<?php echo $result->psub;?>" /></td>
			<td>
			</td>
		</tr>
		<tr>
		<th valign="top">Total IT Exp:</th>
		<td>
			<select class="styledselect_form_1" name="totalit" required>
			<?php
			for($i=0;$i<41;$i++){
				if($i==$result->totalit){
					echo "<option selected value=".$i.">".$i." Years</option>";
				}else{
					echo "<option value=".$i.">".$i." Years</option>";
				}
			}		
			?>	
			</select>

</td>
		<td></td>
		</tr>
		<tr>
		<th valign="top">End Client :</th>
		<td><input type="text" class="inp-form required" name="company"  value="<?php echo $result->company;?>" /></td>
		<td></td>
		</tr> 
		<tr>
			<th valign="top">Location :</th>
			<td>	
			<select  name="location"  class="styledselect_form_1 required">
			<?php
					
			foreach($result1 as $res){
			//print_r('123');exit;
				if($res->lid == $result->locationid){
					echo '<option selected value='.$res->lid.'>'.$res->location.'</option>';
				}else{
					echo '<option value='.$res->lid.'>'.$res->location.'</option>';
				}
			}
			?>

			</select>
		</td>
			<td></td>
		</tr>
		
	<tr>
		<th>Submitted by :</th>
		<td>
			<!-- <select  name="submittedby" class="styledselect_form_1 required"> -->
			<?php
			// $recruiterName = '';
			// foreach($result2 as $res_r) {
			// 	if($result->submittedby == $res_r->id){
			// 		//echo '<option selected value='.$res_r->id.'>'.$res_r->recruiter.'</option>';
			// 		$recruiterName = $res_r->recruiter;
			// 	}
			// 	// else{
			// 	// 	echo '<option value='.$res_r->id.'>'.$res_r->recruiter.'</option>';
			// 	// }
			// }
			?>
		<!-- </select> -->
<input type="text" class="inp-form required" name="search-submittedby" id="search-submittedby" 
	   value="<?=$this->recruiter->getRecruiter($result->submittedby)?>" 
	   <?php if($this->session->userdata('logged_in')['id'] != 57) { ?>
	   disabled="disabled"
	   <?php } ?>
	   />
<input type="hidden" name="submittedby" id="submittedby" value="<?=$result->submittedby?>"/>
</td>

	</tr>
	<!-- <tr>
		<th>Vendor Company :</th>
		<td><input type="text" name="vcompany" class="inp-form required"  value="<?php echo $result->vcompany;?>" /></td>
	</tr>
	<tr>
		<th>Vendor Contact Name :</th>
		<td><input type="text" class="inp-form required" name="vcontact"  value="<?php echo $result->vcontact;?>" /></td>
	</tr>
	<tr>
		<th>Vendor Email :</th>
		<td><input type="text" class="inp-form required email" name="vemail"   value="<?php echo $result->vemail;?>" /></td>
	</tr>
	<tr> -->
		<th>Consultant Rate :</th>
		<td><input type="text" class="inp-form required" name="crate"  value="<?php echo $result->crate;?>" /></td>
	</tr>
	
	<tr>
		<th>Submission Rate :</th>
		 <td><input type="text" name="srate" class="inp-form required"  value="<?php echo $result->srate;?>" /></td>
	</tr>
	<tr>
		<th>Visa :</th>
		<td>
		<select  name="visa" class="styledselect_form_1" required>
			<?php
			foreach($result3 as $res3){
				if($res3->vid==$result->visa){
					echo '<option selected value='.$res3->vid.'>'.$res3->visatype.'</option>';
				}else{
					echo '<option value='.$res3->vid.'>'.$res3->visatype.'</option>';
				}
			}
			?>
			
		</select>
		</td>
	</tr>

	<tr>
		<th>Relocate :</th>
		<td>
		<select  name="relocate" class="styledselect_form_1 required">
		<?php $relocateOptions = array('YES', 'NO'); ?>
    	<option value="">Select</option>
    	<?php foreach($relocateOptions as $relocateOption): ?>
		<option value="<?=$relocateOption?>"
		<?php if($result->relocate == $relocateOption): ?>
		selected="selected"
		<?php endif; ?>
		><?=$relocateOption?></option>
		<?php endforeach; ?>
		</select>
		</td>
	</tr>

	<th>Employer Details :</th>
	<td><textarea type="text" name="employerdetails"  style="width: 308px; height: 70px;" required /><?php echo $result->employerdetails;?></textarea></td>

		<tr>
		<th>Status :</th>
		 <td>
			<select name="status" id="status" class="styledselect_form_1 required">
            <?php 
            	$rejectedString = '';
            	foreach ($rejectedArray as $value) {
            			$rejectedString .= '<option value = "'.$value.'">'.$value.'</option>';
            	}

				if($result->status == 'Submitted to Vendor'){
					echo '<option value = "Submitted to Vendor">Submitted to Vendor</option>
						  <option value = "Submitted to Client">Submitted to Client</option>';
					echo $rejectedString;	  
						 	 
				}elseif(in_array($result->status, $rejectedArray)){
					echo '<option value = "'.$result->status.'">'.$result->status.'</option>';

				}elseif($result->status == 'Submitted to Client'){
					echo '<option value = "Submitted to Client">Submitted to Client</option>
						  <option value = "Interview">Interview</option>
						  <option value = "Rejected">Rejected</option>';

				}elseif($result->status == 'Interview'){
					echo '<option value = "Interview">Interview</option>
						  <option value = "Backout">Backout</option>
						  <option value = "Placement">Placement</option>
						  <option value = "Rejected After Interview">Rejected After Interview</option>
						  <option value = "No Feedback">No Feedback</option>
						  <option value = "Onhold">Onhold</option>';

				}elseif($result->status == 'Placement'){
					echo '<option value = "Placement">Placement</option>
						  <option value = "Backout">Backout</option>';

				}elseif($result->status == 'Rejected After Interview'){
					echo '<option value = "Rejected After Interview">Rejected After Interview</option>';
				}elseif($result->status == 'No Feedback'){
					echo '<option value = "No Feedback">No Feedback</option>';
				}elseif($result->status == 'Onhold'){
					echo '<option value = "Onhold">Onhold</option>';
				}elseif($result->status == 'Rejected'){
					echo '<option value = "Rejected">Rejected</option>';
				}
			?>
			<?php /*?><option <?php echo ($result->status == 'Submitted to Vendor') ? "Selected": "";?>  value="Submitted to Vendor">Submitted to Vendor</option>
            <option <?php echo ($result->status == 'Rejected') ? "Selected": "";?>  value="Rejected">Rejected</option>
			<option <?php echo ($result->status == 'Submitted to Client') ? "Selected": "";?> value="Submitted to Client">Submitted to Client</option>
            <option <?php echo ($result->status == 'Interview') ? "Selected": "";?>  value="Rejected">Interview</option>
            <option <?php echo ($result->status == 'Rejected after Interview') ? "Selected": "";?>  value="Rejected">Rejected after Interview</option>
            <option <?php echo ($result->status == 'Placement') ? "Selected": "";?>  value="Rejected">Placement</option><?php */?>
			<?php /*?><option <?php echo ($result->status == 'Interview in Progress') ? "Selected": "";?> value="Interview in Progress">Interview in Progress</option>
			<option <?php echo ($result->status == 'Waiting for Final Confirmation') ? "Selected": "";?> value="Waiting for Final Confirmation">Waiting for Final Confirmation</option>
			<option <?php echo ($result->status == 'Placed Should be starting soon') ? "Selected": "";?> value="Placed Should be starting soon">Placed Should be starting soon</option>
			<option <?php echo ($result->status == 'Blacklist him/her fake candidate') ? "Selected": "";?> value="Blacklist him/her fake candidate">Blacklist him/her fake candidate</option>
			<option <?php echo ($result->status == 'Didnt show up to start the project') ? "Selected": "";?> value="Didnt show up to start the project">Didn't show up to start the project</option>
			<option <?php echo ($result->status == 'Didnt took the interview not reliable') ? "Selected": "";?> value="Didnt took the interview not reliable">Didn't took the interview not reliable</option>
			<option <?php echo ($result->status == 'Dont work with him/her') ? "Selected": "";?> value="Dont work with him/her">Don't work with him/her</option>
			<option <?php echo ($result->status == 'Position Filled') ? "Selected": "";?> value="Position Filled">Position Filled</option>
			<option <?php echo ($result->status == 'Backout') ? "Selected": "";?> value="Backout">Backout</option><?php */?>
			</select>	 
		 </td>
	</tr>
	<tr>


	<tr class="interview-date-section">
		<th valign="top">Interview date:</th>
		<td class="noheight">
		<?php if($result->status == 'Submitted to Client'): ?>
		<input type="text" name="interview_date" id="interview_date" readonly class="inp-form tcal"/>
		<?php else :  ?>
		<input type="text"  name="interview_date" id="interview_date" class="inp-form tcal" 
				value="<?=$result->interview_date?>" />	
		<!-- <input type="text" class="inp-form tcal" value="<?=$result->interview_date?>" disabled="disabled" readonly/>	
		<input type="hidden" name="interview_date" id="interview_date" 
			   class="inp-form tcal" value="<?=$result->interview_date?>"/>	 -->
		<?php endif; ?>
		</td>
		<td></td>
	</tr>

	<tr class="interview-time-section">
		<th valign="top">Interview Time:</th>
		<td class="noheight">
		<?php if($result->status == 'Submitted to Client'): ?>
		<input type="time" name="interview_time" id="interview_time" readonly class="inp-form tcal"/>
		<?php else :  ?>
		<input type="time"  name="interview_time" id="interview_time" class="inp-form tcal" 
				value="<?=$result->interview_time?>" />
		<?php endif; ?>
		</td>
		<td></td>
	</tr>

	<tr class="modeof-interview-section">
		<th valign="top">Mode of Interview :</th>
		<td class="noheight">
		<select name="modeofinterview" id="modeofinterview">
		
		<?php if($result->status == 'Submitted to Client'): ?>
			echo '<option value = "Phone">Phone</option>
				<option value = "Video">Video</option>
				<option value = "In-Person">In-Person</option>';
		<?php else :  ?>
			<?php
			if ($result->mode_of_interview == 'Phone') {
				echo "<option value = ".$result->mode_of_interview." selected>".$result->mode_of_interview."</option>
				<option value = 'Video'>Video</option>
				<option value = 'In-Person'>In-Person</option>";
			} elseif ($result->mode_of_interview == 'Video') {
				echo "<option value = ".$result->mode_of_interview." selected>".$result->mode_of_interview."</option>
				<option value = 'Phone'>Phone</option>
				<option value = 'In-Person'>In-Person</option>";
			} else {
				echo "<option value = ".$result->mode_of_interview." selected>".$result->mode_of_interview."</option>
				<option value = 'Phone'>Phone</option>
				<option value = 'Video'>Video</option>";
			}
			
				?>
		
		<?php endif; ?>
		</select>
		</td>
		<td></td>
	</tr>

	<!-- <tr>
		<th valign="top">Status updated date:</th>
		<td class="noheight">
		<input type="text" name="mdate" id="mdate" readonly class="inp-form tcal" value="<?php //echo $result->mdate;?>" required />
		</td>
		<td></td>
	</tr> -->
	<th>Resume Upload :</th>
	<td><input type="file" name="userfile" size="20" /></td>
	</tr>
		
	<tr>
	<th>Check the Resume :</th>
	<td><input type="hidden" name="file" value="<?php echo $sub_attachments->resume;?>"/> <a href="<?php echo base_url() ?><?php echo $sub_attachments->resume;?>">View Resume</a></td>
	<tr> 
	<th valign="top">Copy Paste Resume :</th>
	<td><textarea name="copypaste" style="width: 308px; height: 70px;" required><?php echo $sub_attachments->resumecopy;?></textarea></td>
	</tr>
	</tr>
	<tr>
		<th>&nbsp;</th>
		<td valign="top">
			<input type="submit" name="submit" value="submit" class="form-submit" />
			<input type="reset" value="" class="form-reset"  />
		</td>
		<td></td>
	</tr>
	</table>
	<!-- end id-form  -->

	</td>
</tr>
<tr>
<td><img src="<?php echo base_url() ?>images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
 
<div class="clear"></div>
 
<!-- default menu -->
<div class="default_nav">
    <div class="logo">
        <img src="<?php echo base_url() ?>images/logo.png" alt="Panzer" />
    </div>
</div>
<?php
   // print_r($keys->result());
    foreach($keys->result() as $result){
	
    }
?>
<?php //print_r($result); ?>
<?php if($result->status != 'Interview' && $result->status != 'Placement' && $result->status != 'Rejected After Interview'): ?>
<script type="text/javascript">$('.interview-date-section').hide();$('.interview-time-section').hide();$('.modeof-interview-section').hide();</script>
<?php endif; ?>

<script type="text/javascript">
$(document).on('change', '#status', function(){
	var txt = $(this).val();
	if(txt == 'Interview' || txt == 'Placement' || txt == 'Rejected After Interview' || txt == 'Backout') {
		$('.interview-date-section').show();
		$('.interview-time-section').show();
		$('.modeof-interview-section').show();
		$('#interview_date').prop('required', 1);
		$('#interview_time').prop('required', 1);
	} else {
		$('.interview-date-section').hide();
		$('.interview-time-section').hide();
		$('.modeof-interview-section').hide();
		$('#interview_date').prop('required', 0);
		$('#interview_time').prop('required', 0);
	}
});
</script>
<!-- Start of body -->
<div class="background">
    <div class="title-div">
        <p class="title">Submission Edit Form</p>
    </div>
    <?php
    $session_data = $this->session->userdata('logged_in');
    $userId = $session_data['id'];
    ?>
    <?php $attributes = array('name' => 'registration', 'id' => 'submissionForm', 'onsubmit' => 'return v.exec()', 'enctype' => 'multipart/form-data');
    echo form_open('', $attributes); ?>
        <input type="hidden" name="sid" value="<?php echo $result->id;?>" />
        <div class="input-container">
            <div class="input-div">
                <div class="input-name">Select a Date :</div>
                <?php if($this->session->userdata('logged_in')['id'] != 57) { ?>
                        <input type="text" class="selectpicker" value="<?php echo $result->sdate;?>" readonly disabled="disabled"/>
                        <input type="hidden" name="sdate" id="sdate" value="<?php echo $result->sdate;?>"/>
                <?php } else { ?>
                        <input type="text" class="selectpicker" name="sdate" id="sdate" value="<?php echo $result->sdate;?>"/>	
                <?php }?>
            </div>
            <div class="input-div">
                <div class="input-name">Consultant Name :</div>
                <input type="text" name='cname' class="selectpicker" placeholder="Consultant Name" value="<?php echo $result->cname;?>" required/>
            </div>
            <div class="input-div">
                <div class="input-name">Position Submitted to :</div>
                <input type="text" class="selectpicker" value="<?=(isset($result->psub)?(htmlentities($result->psub)):'');?>" readonly disabled/>
				<input type="hidden" class="selectpicker" name="psub" id="psub" value="<?=(isset($result->psub)?(htmlentities($result->psub)):'');?>"/>
            </div>
            <div class="input-div">
                <div class="input-name">Candidate job Title :</div>
                <input type="text" class="selectpicker" value="<?=(isset($result->job_submitted_to)?(htmlentities($result->job_submitted_to)):'');?>" readonly disabled/>
				<input type="hidden" class="selectpicker" name="jsub" id="jsub" value="<?=(isset($result->job_submitted_to)?(htmlentities($result->job_submitted_to)):'');?>"/>
                <span id="job-pg-add-client-jobtitle" style="display: none;"></span>
            </div>
            <div class="input-div">
                <div class="input-name">Total IT Exp :</div>
                <select class="styledselect_form_1 selectpicker" name="totalit" required>
                    <option value="">Select Years of Exp</option>
                    <?php 
                    for($i=0;$i<41;$i++){
                        if($i==$result->totalit){
                            echo "<option selected value=".$i.">".$i." Years</option>";
                        }else{
                            echo "<option value=".$i.">".$i." Years</option>";
                        }
			        }?>	
                </select>
            </div>
            <div class="input-div">
                <div class="input-name">End Client :</div>
                <input type="text" class="selectpicker" value="<?=(isset($result->company)?(htmlentities($result->company)):'');?>" readonly disabled/>
			    <input type="hidden" class="selectpicker" name="company" value="<?=(isset($result->company)?(htmlentities($result->company)):'');?>"/>
            </div>
            <div class="input-div">
                <div class="input-name">Location :</div>
                <select  name="location" class="selectpicker styledselect_form_1" required>
                    <option value="">Select Location</option>
                    <?php 
                    foreach($result1 as $res){
                        if($res->lid == $result->locationid){
                            echo '<option selected value='.$res->lid.'>'.$res->location.'</option>';
                        }else{
                            echo '<option value='.$res->lid.'>'.$res->location.'</option>';
                        }
                    }?>
			    </select>
            </div>
            <div class="input-div">
                <div class="input-name">Submitted By :</div>
                <input type="text" class="selectpicker" name="search-submittedby" id="search-submittedby" value="<?=$this->recruiter->getRecruiter($result->submittedby)?>" required <?php if($this->session->userdata('logged_in')['id'] != 57) { ?> disabled="disabled" <?php } ?>/>
		        <input type="hidden" name="submittedby" id="submittedby" value="<?=$result->submittedby?>"/>
            </div>
            <div class="input-div">
                <div class="input-name">Client Rate :</div>
                <input class="selectpicker" name="crate" value="<?=(isset($result->crate)?(htmlentities($result->crate)):'');?>" placeholder="Client Rate">
            </div>
            <div class="input-div">
                <div class="input-name">Consultant Rate :</div>
                <input class="selectpicker" value="<?=(isset($result->srate)?(htmlentities($result->srate)):'');?>" name="srate" placeholder="Consultant Rate" required/>
            </div>
            <div class="input-div">
                <div class="input-name">Visa :</div>
                <select  name="visa" id="select_visa" class="selectpicker styledselect_form_1" required>
                    <option value="">Select Visa</option>
                    <?php 
                    foreach($result3 as $res3){
                        if($res3->vid==$result->visa){
                            echo '<option selected value='.$res3->vid.'>'.$res3->visatype.'</option>';
                        }else{
                            echo '<option value='.$res3->vid.'>'.$res3->visatype.'</option>';
                        }
			        }?>
                </select>
            </div>
            <div class="input-div">
                <div class="input-name">Status :</div>
                <select name="status" class="selectpicker" placeholder="Status" required>
                    <?php 
                    $rejectedString = '';
                    foreach ($rejectedArray as $value) {
                            $rejectedString .= '<option value = "'.$value.'">'.$value.'</option>';
                    }

                    if($result->status == 'Submitted to Vendor'){
                        echo '<option value = "Submitted to Vendor">Submitted to Vendor</option>
                            <option value = "Submitted to Client">Submitted to Client</option>';
                        echo $rejectedString;	  
                                
                    }elseif(in_array($result->status, $rejectedArray)){
                        echo '<option value = "'.$result->status.'">'.$result->status.'</option>';

                    }elseif($result->status == 'Submitted to Client'){
                        echo '<option value = "Submitted to Client">Submitted to Client</option>
                            <option value = "Interview">Interview</option>
                            <option value = "Rejected">Rejected</option>';

                    }elseif($result->status == 'Interview'){
                        echo '<option value = "Interview">Interview</option>
                            <option value = "Placement">Placement</option>
                            <option value = "Rejected After Interview">Rejected After Interview</option>
                            <option value = "No Feedback">No Feedback</option>
                            <option value = "Onhold">Onhold</option>';

                    }elseif($result->status == 'Placement'){
                        echo '<option value = "Placement">Placement</option>
                            <option value = "Backout">Backout</option>';

                    }elseif($result->status == 'Rejected After Interview'){
                        echo '<option value = "Rejected After Interview">Rejected After Interview</option>';
                    }elseif($result->status == 'No Feedback'){
                        echo '<option value = "No Feedback">No Feedback</option>';
                    }elseif($result->status == 'Onhold'){
                        echo '<option value = "Onhold">Onhold</option>';
                    }elseif($result->status == 'Rejected'){
                        echo '<option value = "Rejected">Rejected</option>';
                    } ?>
                </select>
            </div>
            <div class="input-div interview-date-section">
                <div class="input-name">Interview date:</div>
                <?php if($result->status == 'Submitted to Client'): ?>
                <input type="text" name="interview_date" id="interview_date" readonly class="inp-form tcal"/>
                <?php else :  ?>
                <input type="text"  name="interview_date" id="interview_date" class="inp-form tcal" 
                        value="<?=$result->interview_date?>" />
                <?php endif; ?>
            </div>
            <div class="input-div interview-time-section">
                <div class="input-name">Interview Time:</div>
                <?php if($result->status == 'Submitted to Client'): ?>
                <input type="time" name="interview_time" id="interview_time" readonly class="inp-form tcal"/>
                <?php else :  ?>
                <input type="time"  name="interview_time" id="interview_time" class="inp-form tcal" 
                        value="<?=$result->interview_time?>" />
                <?php endif; ?>
            </div>
            <div class="input-div interview-timezone">
                <div class="input-name">Time Zone of Interview :</div>
                <select name="interview_time_zone" id="interview_time_zone">
		
                <?php if($result->status == 'Submitted to Client'):
                    echo '<option value="EST">EST</option>
					<option value="CST">CST</option>
					<option value="MST">MST</option>
					<option value="PST">PST</option>';
                    else :
                    if ($result->time_zone == 'EST') {
                        echo "<option value = ".$result->time_zone." selected>".$result->time_zone."</option>
                        <option value = 'CST'>CST</option>
                        <option value = 'MST'>MST</option>
                        <option value = 'PST'>PST</option>";
                    } elseif ($result->time_zone == 'CST') {
                        echo "<option value = ".$result->time_zone." selected>".$result->time_zone."</option>
                        <option value = 'EST'>EST</option>
                        <option value = 'MST'>MST</option>
                        <option value = 'PST'>PST</option>";
                    } elseif ($result->time_zone == 'MST') {
                        echo "<option value = ".$result->time_zone." selected>".$result->time_zone."</option>
                        <option value = 'EST'>EST</option>
                        <option value = 'MST'>CST</option>
                        <option value = 'PST'>PST</option>";
                    }else{
                        echo "<option value = ".$result->time_zone." selected>".$result->time_zone."</option>
                        <option value = 'EST'>EST</option>
                        <option value = 'MST'>CST</option>
                        <option value = 'MST'>MST</option>";
                    }
                    ?>
		
		        <?php endif; ?></select>
            </div>
            <div class="input-div modeof-interview-section">
                <div class="input-name">Mode of Interview :</div>
                <select name="modeofinterview" id="modeofinterview">
		
		<?php if($result->status == 'Submitted to Client'):
			echo '<option value = "Phone">Phone</option>
				<option value = "Video">Video</option>
				<option value = "In-Person">In-Person</option>';
            else :
            if ($result->mode_of_interview == 'Phone') {
				echo "<option value = ".$result->mode_of_interview." selected>".$result->mode_of_interview."</option>
				<option value = 'Video'>Video</option>
				<option value = 'In-Person'>In-Person</option>";
			} elseif ($result->mode_of_interview == 'Video') {
				echo "<option value = ".$result->mode_of_interview." selected>".$result->mode_of_interview."</option>
				<option value = 'Phone'>Phone</option>
				<option value = 'In-Person'>In-Person</option>";
			} else {
				echo "<option value = ".$result->mode_of_interview." selected>".$result->mode_of_interview."</option>
				<option value = 'Phone'>Phone</option>
				<option value = 'Video'>Video</option>";
			}
			?>
		
		<?php endif; ?></select>
            </div>
            <div class="input-div">
                <div class="input-name">Relocate :</div>
                <select name="relocate" class="selectpicker" placeholder="Relocate">
                    <?php $relocateOptions = array('YES', 'NO'); ?>
                    <option value="">Select</option>
                    <?php foreach($relocateOptions as $relocateOption): ?>
                    <option value="<?=$relocateOption?>"
                    <?php if($result->relocate == $relocateOption): ?>
                    selected="selected"
                    <?php endif; ?>
                    ><?=$relocateOption?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="input-div">
                <div class="input-name">Upload Resume :</div>
                <input class="selectpicker" type="file" name="userfile" size="20" style="font-size: 13px; width: 200px;"/>
            </div>
            <div class="input-div">
                <div class="input-name">Check the Resume :</div>
                <input type="hidden" name="file" value="<?php echo $sub_attachments->resume;?>"/> <a href="<?php echo base_url() ?><?php echo $sub_attachments->resume;?>">View Resume</a>
            </div>
            <div class="input-div" id="upload-visa-area" style="display:none;">
                <div class="input-name">Upload Visa :</div>
                <input class="selectpicker" id="upload-visa" name="uploadvisa" size="20" type="file" style="font-size: 13px; width: 200px;">
            </div>
            <div class="input-div" style="margin: auto; margin-bottom: 20px; width: 410px">
                <div class="input-name">Employer Details :</div>
                <textarea class="selectpicker" name="employerdetails" placeholder="Employer Details" style="
                width: 200px;
                height: 150px;
                border: 2px solid #0087c4;
                border-radius: 10px;" required><?php echo $result->employerdetails;?></textarea>
            </div>
            <!-- chose file -->
            <div class="input-div" style="margin: auto; margin-bottom: 20px; width: 410px">
                <div class="input-name">Copy Paste Resume :</div>
                <textarea class="selectpicker" name="copypaste" id="copypaste" placeholder="Copy Paste Resume" style="
                width: 200px;
                height: 150px;
                border: 2px solid #0087c4;
                border-radius: 10px;"><?php echo $sub_attachments->resumecopy;?></textarea>
            </div>
        </div>
        <div class="form-btn">
            <input type="hidden" name="submit"/>
            <button type="submit" class="cssbuttons-io-button add-submission" name="submit" value="submit">Submit
                <div class="icon">
                    <svg height="24" width="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 0h24v24H0z" fill="none"></path>
                        <path d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z" fill="currentColor"></path>
                    </svg>
                </div>
            </button>
        </div>
    <?php echo form_close(); ?>
</div>
<!-- submission update result showing with the alert command -->
<?php if($error !=''){ ?>
<script type="text/javascript">
$(document).ready(function(){
	alert( "<?php echo $error;?>");
});
</script>
<?php } ?>
<!-- aditional css -->
<style>
    .input-div {
        width: 410px;
    }

    input {
        width: 200px;
    }
</style>
<?php if($session_data['category'] == 1): ?>
<script type="text/javascript">
$(document).on('change', '#select_visa', function(){
	var val 	= parseInt($(this).val());
	var myarray = [ 1, 5, 7 ];

	if($.inArray(val, myarray) !== -1) {
	$('#upload-visa-area').show();
	$('#upload-visa').prop('required',false);	
	} else {
	$('#upload-visa-area').hide();
	$('#upload-visa').prop('required',false);		
	}

});
</script>
<?php endif; ?>
<?php $this->load->view('includes/footer'); ?>
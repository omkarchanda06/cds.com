<?php $this->load->view('includes/header'); ?>

<div id="page-heading"><h1>My Profile</h1></div>

<table width="100%" cellspacing="0" cellpadding="0" border="0" id="content-table">
<tbody>
<tr>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowleft.jpg"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowright.jpg"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	
	<?php echo form_open('dashboard/changepwd'); ?>
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
	<tr valign="top" height="30">
		<td><span style="color:#ff0000; line-height: 23px;">
			<!-- start id-form -->
			<?php
			//$error='';
			if($error!=''){
				echo $error;	
			}
			?>	</span>	
			<table cellspacing="0" cellpadding="0" border="0" id="id-form" width="100%" >
			<tbody>
				<tr>
					<th valign="top">User Name :</th>
					<td><input type="text" class="inp-form" value="<?php echo $username;?>" name="username" readonly="readonly"> 
					</td>
					<td><?php echo form_error('username') ?></td>
				</tr>
				<tr>
					<th valign="top">Email Address :</th>
					<td><input type="text" class="inp-form" name="emailaddress" value="<?php echo $emailaddress;?>">
					</td>
					<td><?php echo form_error('emailaddress') ?></td>
				</tr>
				<tr>
					<th valign="top">PT No :</th>
					<td><input type="text" class="inp-form" name="ptno" value="<?php echo $ptno;?>">
					</td>
					<td><?php echo form_error('ptno') ?></td>
				</tr>
				<tr>
					<th valign="top">Old Password :</th>
					<td><input type="password" class="inp-form" name="oldpwd">
					</td>
					<td><?php echo form_error('oldpwd') ?></td>
				</tr>
				<tr>
					<th>New Password :</th>
					 <td><input type="password" class="inp-form" name="newpwd"></td>
					 <td><?php echo form_error('newpwd') ?></td>
				</tr>
				<tr>
					<th>Confirm Password :</th>
					 <td><input type="password" class="inp-form" name="cpwd"></td>
					 <td><?php echo form_error('cpwd') ?></td>
				</tr>
			
			
				<tr>
					<th>&nbsp;</th>
					<td valign="top">
						<input type="submit" name="updatepwd" class="form-submit" value="updatepwd">
						<input type="reset" class="form-reset" value="">
					</td>
					<td></td>
				</tr>
			
			</tbody>
			</table>
			<!-- end id-form  -->

		</td>
	</tr>
<!--<tr>
<td><img width="695" height="1" alt="blank" src="images/shared/blank.gif"></td>

</tr> -->
	</tbody>
	</table>
	</form>
	
	<div class="clear"></div>
	 
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</tbody>
</table>

<?php $this->load->view('includes/footer'); ?>

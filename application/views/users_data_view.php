<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url()?>css/profile.css" />
<link rel="stylesheet" href="<?php echo base_url()?>css/add-edit-user.css" />
    <!-- default menu -->
    <div class="default_nav">
      <div class="logo">
        <img src="<?php echo base_url()?>images/logo.png" alt="Panzer" />
      </div>
    </div>
    <!-- End of profile -->
    <?php //var_dump($keys);
				foreach($keys as $result) {
				//print_r($result);
				?>	
    <!-- Start of body -->
    <div class="background">
      <div class="title-div">
        <p class="title"> Edit User Form </p>
      </div>
      <?php $attributes = array('name' => 'edit-user-form', 'id' => 'edit-user-form'); echo form_open('', $attributes); ?>
      <div class="input-container">
        <div class="input-div">
          <div class="input-name">User Name : <?php echo $result->username;?></div>
          
			    <input type="hidden" class="inp-form" name="uid" value="<?php echo $result->id;?>">
            <input type="hidden" class="inp-form" name="username" value="<?php
                    $username = str_replace(' Panzer', '', $result->username);
                    // $username = str_replace(' RPO', '', $username);
                    // $username = str_replace(' Canada', '', $username);
                    echo $username;?>">
        </div>
        <div class="input-div">
          <div class="input-name">PT Number :</div>
          <input class="selectpicker" placeholder="PT Number" name="ptno" value="<?php echo $result->ptno;?>" style="text-transform:uppercase"/><span><?php echo form_error('ptno'); ?></span>
        </div>
        <div class="input-div">
          <div class="input-name">Email :</div>
          <input class="selectpicker" placeholder="Email" name="email" value="<?php echo $result->email;?>"/><span><?php echo form_error('email'); ?></span>
        </div>
        <div class="input-div">
          <div class="input-name">Status :</div>
          <select class="selectpicker" placeholder="Status" name="status">
              <option value="">Select</option>
              <option <?php if($result->status =='ACTIVE'){ echo 'selected="selected"';} ?> value="1">Active</option>
							<option <?php if($result->status=='INACTIVE'){ echo 'selected="selected"';} ?> value="2">Inactive</option>
          </select>
        </div>
        <div class="input-div">
          <div class="input-name">Password :</div>
          <input class="selectpicker" type="password" placeholder="Password" name="pwd"/><span><?php echo form_error('pwd'); ?></span>
        </div>
        <div class="input-div">
          <div class="input-name">Confirm Password :</div>
          <input class="selectpicker" type="password" placeholder="Confirm Password" name="cpwd"/><span><?php echo form_error('cpwd'); ?></span>
        </div>
        <div class="input-div">
          <div class="input-name">User Category :</div>
          <select class="selectpicker" placeholder="User Category" name="ucat">
              <option <?php if($result->user_category == 1){ echo 'selected="selected"';} ?> value="1">Panzer</option>							
							<!-- <option <?php if($result->user_category == 2){ echo 'selected="selected"';} ?> value="2">RPO</option>
							<option <?php if($result->user_category == 3){ echo 'selected="selected"';} ?> value="3">Canada</option>							 -->
							<option <?php if($result->user_category == 4){ echo 'selected="selected"';} ?> value="4">All (Super Admin)</option>		
          </select>
        </div>
        <div class="input-div">
          <div class="input-name">User Department :</div>
          <select class="selectpicker" placeholder="User Category" name="udepartment">
              <option value="">Select</option>
              <option value="ITSTAFFING" <?php if ($result->department == "ITSTAFFING"){ echo 'selected="selected"';} ?> >IT Staffing</option>
							<option value="MARKETING" <?php if ($result->department == "MARKETING"){ echo 'selected="selected"';} ?> >Marketing</option>
							<option value="SOFTWARE" <?php if ($result->department == "SOFTWARE"){ echo 'selected="selected"';} ?> >Software</option>																		
							<option value="HR" <?php if ($result->department == "HR"){ echo 'selected="selected"';} ?> >Human Resource</option>
							<option value="ACCOUNTS" <?php if ($result->department == "ACCOUNTS"){ echo 'selected="selected"';} ?> >Accounts</option>
							<option value="NETWORK" <?php if ($result->department == "NETWORK"){ echo 'selected="selected"';} ?> >Network</option>
							<option value="OFFICEADMINISTRATION" <?php if ($result->department == "OFFICEADMINISTRATION"){ echo 'selected="selected"';} ?> >Office Administration</option>
          </select>
        </div>
        <div class="input-div">
          <div class="input-name">User Type :</div>
          <select class="selectpicker" name="utype" placeholder="User Type" >
            <option value="">Select</option>
            <?php foreach ($usertypes_data as $usertype) {
              if ($usertype->usertype !='' && $usertype->usertype != '0') {
                if ($result->usertype === $usertype->usertype) {
                  echo "<option value= $usertype->usertype  selected = 'selected'> $usertype->usertype </option>";
                } else {
                  echo "<option value= $usertype->usertype> $usertype->usertype </option>";
                }
              }
            } ?>
          </select>
        </div>
      </div>
      <div class="form-btn">
      <input type="hidden" class="inp-form" name="uid" value="<?php echo $result->id;?>">
      <input type="hidden" name="updateuser" value="updateuser">
        <button class="cssbuttons-io-button">
          Submit
          <div class="icon">
            <svg
              height="24"
              width="24"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M0 0h24v24H0z" fill="none"></path>
              <path
                d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z"
                fill="currentColor"
              ></path>
            </svg>
          </div>
        </button>
        <!-- clear form btn -->
        <button class="button">
          <div class="trash">
            <div class="top">
              <div class="paper"></div>
            </div>
            <div class="box"></div>
            <div class="check">
              <svg viewBox="0 0 8 6">
                <polyline points="1 3.4 2.71428571 5 7 1"></polyline>
              </svg>
            </div>
          </div>
          <span>Clear Form</span>
        </button>
        <!-- js for btn -->
        <script>
          document.querySelectorAll(".button").forEach((button) =>
            button.addEventListener("click", (e) => {
              if (!button.classList.contains("delete")) {
                button.classList.add("delete");
                document.getElementById("edit-user-form").reset();
                setTimeout(() => button.classList.remove("delete"), 3200);
              }
              e.preventDefault();
            })
          );
        </script>
      </div>
      <?php } ?>
    </div>
  <!-- css for input fields -->
  <link rel="stylesheet" href="assets/js/test_js/1.css" />
  <link rel="stylesheet" href="assets/js/test_js/2.css" />
  <!-- script for input fields -->
  <script src="assets/js/test_js/1.js"></script>
  <script src="assets/js/test_js/2.js"></script>
  <script src="assets/js/test_js/3.js"></script>
  <?php $this->load->view('includes/footer'); ?>

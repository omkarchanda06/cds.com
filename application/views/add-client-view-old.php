<?php $this->load->view('includes/header'); ?>

<div id="page-heading">
    <h1><?php
	$updatemessage = $this->session->flashdata('updatemessage');
	if(!empty($result)){
        echo $result;
    }elseif(!empty($updatemessage)){
        echo $updatemessage;
    }else{
		echo "Add Client Details";
	} ?>
    </h1>
</div>

<table width="100%" cellspacing="0" cellpadding="0" border="0" id="content-table">
<tbody>
<tr>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowleft.jpg"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowright.jpg"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php $attributes = array('name' => 'client-form'); echo form_open('client/addclient', $attributes); ?>
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
	<tr valign="top" height="30">
		<td><span style="color:blue; line-height: 23px;"><?php if($message!=''){ echo $message; }?></span>	
			<table cellspacing="0" cellpadding="0" border="0" id="id-form" width="100%" >
			<tbody>
				<tr>
					<th valign="top">Contact Name :</th>
					<td><input type="text" class="inp-form"  name="client_name" value="<?php echo $client_name;?>"
					     style="width:500px;" placeholder="Client Name" required> 
					</td>
					<!-- <td style="color:red;"><?php echo form_error('job_title') ?></td> -->
				</tr>
                <tr>
					<th valign="top">Company Name:</th>
					<td><input type="text" class="inp-form"  name="client_company" value="<?php echo $client_company;?>"
					     style="width:500px;" placeholder="Client Company" required> 
					</td>
				</tr>
                <tr>
					<th valign="top">Contact Number :</th>
					<td><input type="text" class="inp-form"  name="client_contact" id="client_contact" value="<?php echo $client_contact;?>"
					     style="width:500px;" placeholder="Client Contact" required> <span id="lblError" style="color: red;"></span>
					</td>
				</tr>
                <tr>
					<th valign="top">Email ID:</th>
					<td><input type="text" class="inp-form"  name="client_email" value="<?php echo $client_email;?>"
					     style="width:500px;" placeholder="Client Email" required> 
					</td>
				</tr>
                <tr>
					<th>Comments/signature :</th>
					 <td><textarea class="inp-form" name="comments" placeholder="Comments" style=" width: 500px; min-height: 150px;" required></textarea>
					 <div class="field-error"></div>
					 </td><td></td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td valign="top" style="padding-top: 10px;">
						<input type="submit" name="addclientdetails" class="form-submit" value="Add client Details"/>
						<input type="reset" class="form-reset" value="">
					</td>
					<td></td>
				</tr>
			
			</tbody>
			</table>
			<!-- end id-form  -->

		</td>
	</tr>
<!--<tr>
<td><img width="695" height="1" alt="blank" src="images/shared/blank.gif"></td>

</tr> -->
	</tbody>
	</table>
	</form>
	
	<div class="clear"></div>
	 
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</tbody>
</table>

<?php $this->load->view('includes/footer'); ?>

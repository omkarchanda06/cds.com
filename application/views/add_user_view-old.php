<?php $this->load->view('includes/header'); ?>
<div id="page-heading"><h1>Add / Edit User Form</h1></div>

<table width="100%" cellspacing="0" cellpadding="0" border="0" id="content-table">
<tbody>
<!-- <tr>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowleft.jpg"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowright.jpg"></th>
</tr> -->
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	
	<form method="POST">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
	<tr valign="top">
		<td>
		<?php
			//$error='';
			if($error!=''){
			echo $error;	
			}
			?>
			<!-- start id-form -->
			<table cellspacing="0" cellpadding="0" border="0" id="id-form" width="100%" >
			<tbody>
				<tr>
					<th valign="top">User Name :</th>
					<td><input type="text" class="inp-form" name="uname"></td>
					<td><?php echo form_error('uname'); ?></td>
				</tr>
				<tr>
					<th valign="top">PT No :</th>
					<td><input type="text" class="inp-form" name="ptno"></td>
					<td><?php echo form_error('ptno'); ?></td>
				</tr>
				<tr>
					<th valign="top">Email :</th>
					<td><input type="email" class="inp-form" name="email"></td>
					<td><?php echo form_error('email'); ?></td>
				</tr>
				<tr>
					<th>Password :</th>
					 <td><input type="password" class="inp-form" name="pwd"></td>
					 <td><?php echo form_error('pwd'); ?></td>
				</tr>
				<tr>
					<th>Confirm Password :</th>
					 <td><input type="password" class="inp-form" name="cpwd"></td>
					 <td><?php echo form_error('cpwd'); ?></td>
				</tr>
				<tr>
					<th>User Category :</th>
					 <td>
						<select name="ucat" class="styledselect_user">
						    <option value="1">Panzer</option>
							<option value="2">RPO</option>
							<option value="3">Canada</option>																		
							<option value="4">All (Super Admin)</option>
						</select>
					 </td>
				</tr>				
				<tr>
					<th>User Type :</th>
					 <td>
						<select name="utype" class="styledselect_user">
						    <option value="1">Super Admin</option>
							<option value="2">DIRECTOR</option>							
							<option value="4">TA MANAGER</option>							
												
						</select>
					 </td>
				</tr>
				<tr>	
					<th>Status :</th>
					 <td>
						<select name="status" class="styledselect_user">
							<option value="1">Active</option>
							<option value="2">Inactive</option>							
						</select>
					 </td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td valign="top">
						<input type="submit" name="adduser" class="form-submit" value="adduser">
						<input type="reset" class="form-reset" value="">
					</td>
					<td></td>
				</tr>
				
			</tbody>
			</table>
			<!-- end id-form  -->

		</td>
	</tr>
<!--<tr>
<td><img width="695" height="1" alt="blank" src="images/shared/blank.gif"></td>

</tr> -->
	</tbody>
	</table>
	</form>

	<div class="clear"></div>
	 
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</tbody>
</table>
<script>
    
</script>
<?php //$this->load->view('includes/footer'); ?>
<?php 
$this->load->view('includes/header'); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!----======== CSS ======== -->
    <link rel="stylesheet" href="<?php echo base_url()?>css/style.css">
    
    <!----===== Icons CSS ===== -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url() ?>css/jquery-ui.css" />
  <script src="<?php echo base_url() ?>js/jquery/jquery-1.8.3.js"></script>
  <script language="JavaScript" src="<?php echo base_url() ?>js/jquery/jquery.validate.js"></script>
  <script src="<?php echo base_url() ?>js/jquery/jquery-ui.js"></script>
    <title>Panzer</title> 
    <style type="text/css">
        .form-group {
            
          padding-top: 0;
          padding-bottom: 0;
        }
        .sidebar {
        	position: relative;
			width: 100%;
        }

    </style>
    <script>
  $(function(){
	var pickerOpts = { dateFormat:"yy-mm-dd" };	
	$("#sdate").datepicker(pickerOpts);
	// validate the comment form when it is submitted
	$("#submissionForm").validate({
		rules: {
		    copypaste: {
		      required: true,
		      minlength: 500
		    }
		}
	});

});
  </script>
    

</head>
<body>
    <nav class="sidebar">
        <header>
            <!-- <div class="image-text">
                <span class="image">
                    <img src="<?php echo base_url()?>images/logo.png" alt="Panzer">
                </span>
            </div> -->
            
        </header>
        <section>
			<div id="page-heading" style="width:560px"><h1 style="font-size: 16px">Add Submission Form</h1>
			</div>
				<?php 
				$session_data = $this->session->userdata('logged_in');
				$userId = $session_data['id'];
				?>
		</section>
		<section>
			<form name="registration" id="submissionForm" action="<?php echo base_url()?>index.php/submissions/add_submissions" method="post" onsubmit="return v.exec()" enctype="multipart/form-data">
				<input type="hidden" name="userid" value="<?php echo $userId;?>" />
				<table border="0"  width="100%"  cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Select a date:</th>
			<td class="noheight">
			<input type="text" name="sdate" id="sdate" class="inp-form tcal required"/>
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Consultant Name :</th>
			<td><input type="text" name='cname' class="inp-form" /></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Position Submitted to :</th>
			<td>
				<input type="text" class="inp-form required" value="<?=(isset($clientdetails->job_title)?(htmlentities($clientdetails->job_title)):'');?>" readonly disabled/>
				<input type="hidden" class="inp-form required" name="psub" id="psub" value="<?=(isset($clientdetails->job_title)?(htmlentities($clientdetails->job_title)):'');?>"/>
			</td>
			<td>
	<!--		<div class="error-left"></div>
			<div class="error-inner">This field is required.</div>
-->			</td>
		</tr>
		<tr>
			<th valign="top">Candidate job Title :</th>
			<td>
				<input type="text" class="inp-form required" value="<?=(isset($clientdetails->job_title)?(htmlentities($clientdetails->job_title.', '.$clientdetails->client_company)):'');?>" readonly disabled/>
				<input type="hidden" class="inp-form required" name="jsub" id="jsub" value="<?=(isset($clientdetails->job_title)?(htmlentities($clientdetails->job_title.', '.$clientdetails->client_company)):'');?>"/>
			</td>
			<span id="job-pg-add-client-jobtitle" style="display: none;"></span>
		</tr>
		<tr>
		<th valign="top">Total IT Exp:</th>
		<td>
		<select class="styledselect_form_1" name="totalit" required>
        <option value="">Select Years of Exp</option>
		<?php for($i=0;$i<41;$i++){ echo "<option value=".$i.">".$i." Years</option>";}?>	
		</select>
		</td>
		<td></td>
		</tr>
		<tr>
		<th valign="top">End Client :</th>
		<td>
			<input type="text" class="inp-form required" value="<?=(isset($clientdetails->client_company)?(htmlentities($clientdetails->client_company)):'');?>" readonly disabled/>
			<input type="hidden" class="inp-form required" name="company" value="<?=(isset($clientdetails->client_company)?(htmlentities($clientdetails->client_company)):'');?>"/>
		</td>
		<td></td>
		</tr> 
		<tr>
			<th valign="top">Location :</th>
		<td>	
			<select  name="location" class="styledselect_form_1 required">
             <option value="">Select Location</option>
				
			<?php 
			foreach($key1->result() as $result){
				echo '<option value='.$result->lid.'>'.$result->location.'</option>'; 
			} ?>
			</select>
		</td>
		<td></td>
		</tr>
		<tr>
		<th>Submitted by :</th>
		<td>
		
		<input type="text" class="inp-form required" name="search-submittedby" id="search-submittedby"/>
		<input type="hidden" name="submittedby" id="submittedby"/>
		</td>
	</tr>
	<tr>
		<th>Client Rate :</th>
		<td><input type="text" class="inp-form required" name="crate" value="<?=(isset($clientdetails->bill_rate)?(htmlentities($clientdetails->bill_rate)):'');?>" /></td>
	</tr>
	
	<tr>
		<th>Consultant Rate :</th>
		 <td><input type="text" name="srate" class="inp-form required"/></td>
	</tr>
	<tr>
		<th>Visa :</th>
		<td>
		<select  name="visa" id="select_visa" class="styledselect_form_1 required">
    	<option value="">Select Visa</option>
		<?php foreach($key3->result() as $result1){ echo '<option value='.$result1->vid.'>'.$result1->visatype.'</option>'; } ?>
		</select>
		</td>
	</tr>
	<tr>
		<th>Relocate :</th>
		<td>
		<select  name="relocate" class="styledselect_form_1 required">
		<?php $relocateOptions = array('YES', 'NO'); ?>
    	<option value="">Select</option>
    	<?php foreach($relocateOptions as $relocateOption): ?>
		<option value="<?=$relocateOption?>"><?=$relocateOption?></option>
		<?php endforeach; ?>
		</select>
		</td>
	</tr>
	
	
	<tr>
	<th valign="top">Employer Details :</th>
	<td><textarea style="width: 308px; height: 70px;" name="employerdetails" required></textarea></td>

	</tr>
	<tr>
		<th>Status :</th>
		 <td>		 
			<select name="status" class="styledselect_form_1" required>
			<option value="Submitted to Vendor">Submitted to Vendor</option>
            
			</select>
		</td>
	</tr>

	<tr>
	<th>Resume Upload :</th>
	<td><!--<input type="file" class="file_1" name="file"/>-->
	<input type="file" name="userfile" size="20" required/>
	</td>
	</tr>

	<tr style="display:none;" id="upload-visa-area">
	<th>Upload Visa:</th>
	<td>
	<input type="file" id="upload-visa" name="uploadvisa" size="20" />
	</td>
	</tr>

	<tr>
	<th valign="top">Copy Paste Resume :</th>
	<td><textarea name="copypaste" id="copypaste" style="width: 308px; height: 70px;"></textarea></td>
	</tr>

	<tr>
		<th>&nbsp;</th>
		<td valign="top">
			<input type="submit" name="submit" value="submit" class="form-submit add-submission" />
			<input type="reset" value="" class="form-reset"  />
		</td>
		<td></td>
	</tr>
	</table>
			</form>
		</section>
</body>
<?php if($session_data['category'] == 1): ?>
<script type="text/javascript">
$(document).on('change', '#select_visa', function(){
	var val 	= parseInt($(this).val());
	var myarray = [ 1, 5, 7 ];

	if($.inArray(val, myarray) !== -1) {
	$('#upload-visa-area').show();
	$('#upload-visa').prop('required',false);	
	} else {
	$('#upload-visa-area').hide();
	$('#upload-visa').prop('required',false);		
	}

});
</script>
<?php endif; ?>

<?php $this->load->view('includes/footer'); ?>
<?php if($error !='') { ?>
<script type="text/javascript">
$(document).ready(function(){  alert("<?php echo $error;?>"); });
</script>
<?php } ?>
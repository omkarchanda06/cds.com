<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/resume_db.css" />
    <!-- table -->
    <div class="search-resume-div" style="width: fit-content;">
      <div class="search-inputs">
        <div class="search-text">Job Requirements</div>
      </div>
    </div>
    <!-- table -->
    <!-- <div class="search-in-table" style="margin-left:15%;margin-top: 20px;">
      <input
        type="text"
        id="myInput"
        onkeyup="myFunction()"
        placeholder="&#128269; Enter keyword to search in the following table"
      />
    </div> -->
    <div class="table-div">
        <div class="table">
            <table id="myTable">
                <thead>
                    <tr>
                        <th class="table-header" style="border-top-left-radius: 20px"> ID </th>
                        <th class="table-header">Job Title</th>
                        <th class="table-header">City</th>
                        <th class="table-header">State</th>
                        <th class="table-header">Duration</th>
                        <th class="table-header">Bill Rate</th>
                        <th class="table-header">Client Company</th>
                        <th class="table-header">Job Description</th>
                        <th class="table-header">Date</th>
                        <th class="table-header">Edit</th>
                        <th class="table-header" style="border-top-right-radius: 20px">Submit</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($jobrequirementsdetails as $index => $req_details) { ?>
                    <tr class="req-row-<?php echo $req_details->id; ?>">
                        <td class="table-data"><?=$req_details->id?></td>
                        <td class="table-data"><?=$req_details->job_title?></td>
                        <td class="table-data"><?=$req_details->city?></td>
                        <td class="table-data"><?=$states[$index]->statename?></td>
                        <td class="table-data"><?=$req_details->duration?></td>
                        <td class="table-data"><?=$req_details->bill_rate?></td>
                        <td class="table-data"><?=$req_details->client_company?></td>
                        <td class="table-data"><?=$req_details->job_description?></td>
                        <td class="table-data"><?=$req_details->date?></td>
                        <td class="table-data"><a class="edit-job" href="<?php echo base_url() ?>index.php/requirements/edit/<?=$req_details->id?>" id="<?=$req_details->id?>">Edit</a></td>
                        <td class="table-data"><a href="<?php echo base_url() ?>index.php/requirements/submit/<?=$req_details->id?>">Submit</a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!--Start of JavaScript for the search block -->
    <script>
        function myFunction() {
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
    <!--End of JavaScript for the search block -->
    <?php $this->load->view('includes/footer'); ?>
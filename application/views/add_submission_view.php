<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/profile.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>css/add-edit-user.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url() ?>css/jquery-ui.css" />
  <script src="<?php echo base_url() ?>js/jquery/jquery-1.8.3.js"></script>
  <script language="JavaScript" src="<?php echo base_url() ?>js/jquery/jquery.validate.js"></script>
  <script src="<?php echo base_url() ?>js/jquery/jquery-ui.js"></script>
<script>
    $(function(){
        $("#submissionForm").validate({
            rules: {
                copypaste: {
                required: true,
                minlength: 500
                }
            }
        });
    });
</script>
<!-- default menu -->
<div class="default_nav">
    <div class="logo">
        <img src="<?php echo base_url() ?>images/logo.png" alt="Panzer" />
    </div>
</div>
<!-- Start of body -->
<div class="background">
    <div class="title-div">
        <p class="title">Add Submission Forms</p>
    </div>
    <?php
    $session_data = $this->session->userdata('logged_in');
    $userId = $session_data['id'];
    ?>
    <?php $attributes = array('name' => 'registration', 'id' => 'submissionForm', 'onsubmit' => 'return v.exec()', 'enctype' => 'multipart/form-data');
    echo form_open('submissions/add_submissions', $attributes); ?>
        <input type="hidden" name="userid" value="<?php echo $userId;?>" />
        <div class="input-container">
            <div class="input-div">
                <div class="input-name">Select a Date :</div>
                <input class="selectpicker" aria-label="Default select example" name="sdate" id="sdate" type="date" style="width: 53%" required/>
            </div>
            <div class="input-div">
                <div class="input-name">Consultant Name :</div>
                <input type="text" name='cname' class="selectpicker" placeholder="Consultant Name" required/>
            </div>
            <div class="input-div">
                <div class="input-name">Position Submitted to :</div>
                <input type="text" class="selectpicker" value="<?=(isset($clientdetails->job_title)?(htmlentities($clientdetails->job_title)):'');?>" readonly disabled/>
				<input type="hidden" class="selectpicker" name="psub" id="psub" value="<?=(isset($clientdetails->job_title)?(htmlentities($clientdetails->job_title)):'');?>"/>
            </div>
            <div class="input-div">
                <div class="input-name">Candidate job Title :</div>
                <input type="text" class="selectpicker" value="<?=(isset($clientdetails->job_title)?(htmlentities($clientdetails->job_title.', '.$clientdetails->client_company)):'');?>" readonly disabled/>
				<input type="hidden" class="selectpicker" name="jsub" id="jsub" value="<?=(isset($clientdetails->job_title)?(htmlentities($clientdetails->job_title.', '.$clientdetails->client_company)):'');?>"/>
                <span id="job-pg-add-client-jobtitle" style="display: none;"></span>
            </div>
            <div class="input-div">
                <div class="input-name">Total IT Exp :</div>
                <select class="styledselect_form_1 selectpicker" name="totalit" required>
                    <option value="">Select Years of Exp</option>
                    <?php for($i=0;$i<41;$i++){ echo "<option value=".$i.">".$i." Years</option>";}?>	
                </select>
            </div>
            <div class="input-div">
                <div class="input-name">Client :</div>
                <input type="text" class="selectpicker" value="<?=(isset($clientdetails->client_company)?(htmlentities($clientdetails->client_company)):'');?>" readonly disabled/>
			    <input type="hidden" class="selectpicker" name="company" value="<?=(isset($clientdetails->client_company)?(htmlentities($clientdetails->client_company)):'');?>"/>
            </div>
            <div class="input-div">
                <div class="input-name">Location :</div>
                <select  name="location" class="selectpicker styledselect_form_1" required>
                    <option value="">Select Location</option>
                    <?php 
                    foreach($key1->result() as $result){
                        echo '<option value='.$result->lid.'>'.$result->location.'</option>'; 
                    } ?>
			    </select>
            </div>
            <div class="input-div">
                <div class="input-name">Submitted By :</div>
                <input type="text" class="selectpicker" name="search-submittedby" id="search-submittedby" required/>
		        <input type="hidden" name="submittedby" id="submittedby"/>
            </div>
            <div class="input-div">
                <div class="input-name">Client Rate :</div>
                <input class="selectpicker" name="crate" value="<?=(isset($clientdetails->bill_rate)?(htmlentities($clientdetails->bill_rate)):'');?>" placeholder="Client Rate">
            </div>
            <div class="input-div">
                <div class="input-name">Consultant Rate :</div>
                <input class="selectpicker" aria-label="Default select example" name="srate" placeholder="Consultant Rate" required/>
            </div>
            <div class="input-div">
                <div class="input-name">Visa :</div>
                <select  name="visa" id="select_visa" class="selectpicker styledselect_form_1" required>
                    <option value="">Select Visa</option>
                    <?php foreach($key3->result() as $result1){ echo '<option value='.$result1->vid.'>'.$result1->visatype.'</option>'; } ?>
                </select>
            </div>
            <div class="input-div">
                <div class="input-name">Status :</div>
                <select name="status" class="selectpicker" placeholder="Status" required>
                    <option value="Submitted to Vendor">Submitted to Vendor</option>
                </select>
            </div>
            <div class="input-div">
                <div class="input-name">Relocate :</div>
                <select name="relocate" class="selectpicker" placeholder="Relocate">
                <?php $relocateOptions = array('YES', 'NO'); ?>
                    <?php foreach($relocateOptions as $relocateOption): ?>
		            <option value="<?=$relocateOption?>"><?=$relocateOption?></option>
		            <?php endforeach; ?>
                </select>
            </div>
            <div class="input-div">
                <div class="input-name">Upload Resume :</div>
                <input class="selectpicker" type="file" name="userfile" size="20" style="font-size: 13px; width: 200px;" required/>
            </div>
            <div class="input-div" id="upload-visa-area" style="display:none;">
                <div class="input-name">Upload Visa :</div>
                <input class="selectpicker" id="upload-visa" name="uploadvisa" size="20" type="file" style="font-size: 13px; width: 200px;">
            </div>
            <div class="input-div" style="margin: auto; margin-bottom: 20px; width: 410px">
                <div class="input-name">Employer Details :</div>
                <textarea class="selectpicker" name="employerdetails" placeholder="Employer Details" style="
                width: 200px;
                height: 150px;
                border: 2px solid #0087c4;
                border-radius: 10px;" required></textarea>
            </div>
            <!-- chose file -->
            <div class="input-div" style="margin: auto; margin-bottom: 20px; width: 410px">
                <div class="input-name">Copy Paste Resume :</div>
                <textarea class="selectpicker" name="copypaste" id="copypaste" placeholder="Copy Paste Resume" style="
                width: 200px;
                height: 150px;
                border: 2px solid #0087c4;
                border-radius: 10px;"></textarea>
            </div>
        </div>
        <div class="form-btn">
            <input type="hidden" name="jobid" value="<?=(isset($job_id) ? $job_id :'');?>" >
            <input type="hidden" name="submit"/>
            <input type="hidden" name="jobid" value="<?=(isset($job_id) ? $job_id :'');?>" >
            <button type="submit" class="cssbuttons-io-button add-submission" name="submit" value="submit">Submit
                <div class="icon">
                    <svg height="24" width="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 0h24v24H0z" fill="none"></path>
                        <path d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z" fill="currentColor"></path>
                    </svg>
                </div>
            </button>
        </div>
    <?php echo form_close(); ?>
</div>
<!-- submission result showing with the alert command -->
<?php if($error !=''){ ?>
<script type="text/javascript">
$(document).ready(function(){
	alert( "<?php echo $error;?>");
});
</script>
<?php } ?>
<!-- aditional css -->
<style>
    .input-div {
        width: 410px;
    }

    input {
        width: 200px;
    }
</style>
<?php if($session_data['category'] == 1): ?>
<script type="text/javascript">
$(document).on('change', '#select_visa', function(){
	var val 	= parseInt($(this).val());
	var myarray = [ 1, 5, 7 ];

	if($.inArray(val, myarray) !== -1) {
	$('#upload-visa-area').show();
	$('#upload-visa').prop('required',false);	
	} else {
	$('#upload-visa-area').hide();
	$('#upload-visa').prop('required',false);		
	}

});
</script>
<?php endif; ?>
<?php $this->load->view('includes/footer'); ?>
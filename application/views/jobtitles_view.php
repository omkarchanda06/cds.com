<?php $this->load->view('includes/header'); ?>

<div id="page-heading"><h1>Job Titles</h1></div>

<table width="100%" cellspacing="0" cellpadding="0" border="0" id="content-table">
<tbody>
<tr>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowleft.jpg"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowright.jpg"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
    	

   
 <!-- start product-table-->
	<!-- <form id="mainform"> -->

				<div class="search_area">
                <input type="search" class="light-table-filter" data-table="target-table" placeholder="Type here to search the requirement" />
                <img class="reset-search-bar" src="<?php echo base_url() ?>img/close-modal.png"/>
                </div>
                <!-- <div class="reset-search-bar"></div> -->

				<table border="0" width="98%" cellpadding="0" cellspacing="0" 
				       id="product-table" class="target-table">
				<thead>
					<th class="table-header-repeat line-left minwidth-1">ID</th>
					<th class="table-header-repeat line-left minwidth-2">Job Title</th>
					<th class="table-header-repeat line-left minwidth-2">Edit</th>
					<th class="table-header-repeat line-left minwidth-2">Delete</th>
				</thead>
				<tbody>
				<?php foreach($jobtitles as $job) { ?>
				<tr class="jobtitle-<?php echo $require->id; ?>">

					<td style="text-align:center;"><?=$job->id?></td>
					<td><?=$job->title?></td>
					<td style="text-align:center;" class="options-width"><a href="<?php echo base_url() ?>index.php/jobtitle/edit/<?=$job->id?>" id="<?=$job->id?>">Edit</a></td>
					<td style="text-align:center;" class="options-width"><a class="delete-jobtitle" href="<?php echo base_url() ?>index.php/jobtitle/delete/<?=$job->id?>" id="<?=$job->id?>">Delete</a></td>
				</tr>
                <?php  }?>	
                </tbody>			
				</table>
                <div style="padding:0px 0px 420px 0px;"></div>				
				<!--  end product-table................................... --> 
				<!-- </form> -->

	<div class="clear"></div>
	 
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</tbody>
</table>

<script type="text/javascript">
$('.delete-jobtitle').click(function(e){
   e.preventDefault();
   var retVal = confirm("Are you sure, you want to delete this Job Title?");
   if(retVal == true){
      window.location.href=$(this).attr('href');
      return true;
   }else{
      return false;
   }
});
</script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/custom.js"></script>
<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('includes/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>css/jquery-ui.css" />
<script src="<?php echo base_url() ?>js/jquery/jquery-1.8.3.js"></script>
<script language="JavaScript" src="<?php echo base_url() ?>js/jquery/jquery.validate.js"></script>
<script src="<?php echo base_url() ?>js/jquery/jquery-ui.js"></script>
<script>
    $(function() {
        var pickerOpts = {
            dateFormat: "yy-mm-dd"
        };
        $("#jdate").datepicker(pickerOpts);
        // validate the comment form when it is submitted
        $("#job-requirement-form").validate({
            rules: {
                copypaste: {
                    required: true,
                    minlength: 500
                }
            }
        });

    });
</script>

<div id="page-heading">
    <h1><?php
        $result = $this->session->flashdata('result');
        if (!empty($result)) {
            echo $result;
        } else {
            echo "Edit Job Requirement";
        } ?>
    </h1>
</div>

<table width="100%" cellspacing="0" cellpadding="0" border="0" id="content-table">
    <tbody>
        <tr>
            <th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowleft.jpg"></th>
            <th class="topleft"></th>
            <td id="tbl-border-top">&nbsp;</td>
            <th class="topright"></th>
            <th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowright.jpg"></th>
        </tr>
        <tr>
            <td id="tbl-border-left"></td>
            <td>
                <!--  start content-table-inner -->
                <div id="content-table-inner">
                    <?php
                    if (sizeof($jobdetails) > 0) {
                        // var_dump($jobdetails->duration);
                        // if($jobdetails->duration == "3 Months"){ 
                        //     echo 'selected="selected"';
                        // }else{
                        //     echo "not working";
                        // }
                        // die(); 
                        //foreach ($jobdetails as $detail) {
                        $attributes = array('id' => 'job-requirement-form');
                        echo form_open('requirements/update', $attributes); ?>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr valign="top" height="30">
                                    <td><span style="color:blue; line-height: 23px;"><?php if ($message != '') {
                                                                                            echo $message;
                                                                                        } ?></span>
                                        <table cellspacing="0" cellpadding="0" border="0" id="id-form" width="100%">
                                            <tbody>
                                                <tr>
                                                    <th valign="top">Select a date:</th>
                                                    <td class="noheight">
                                                        <input type="text" name="jdate" id="jdate" readonly class="inp-form tcal required" value="<?php echo $jobdetails->date; ?>" />
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <th valign="top">Job Title :</th>
                                                    <td><input type="text" value="<?php echo $jobdetails->job_title; ?>" class="inp-form required" name="psub" id="psub" placeholder="Job Title" required /><span id="job-title-pg" style="display: none;">
                                                            The Job title is not exists click <a href="<?php echo base_url() ?>index.php/jobtitle/addjobtitle">here</a> to add.</span></td>
                                                </tr>
                                                <tr>
                                                    <th valign="top">City :</th>
                                                    <td><input type="text" class="inp-form" name="city" id="city" placeholder="City" value="<?php echo $jobdetails->city; ?>" required />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top">State :</th>
                                                    <td>
                                                        <select name="location" class="styledselect_form_1" required>
                                                            <option value="">Select State</option>
                                                            <?php foreach ($key1->result() as $result) {
                                                                if ($jobdetails->location == $result->lid) {
                                                                    echo '<option value=' . $result->lid . ' selected>' . $result->location . '</option>';
                                                                } else {
                                                                    echo '<option value=' . $result->lid . '>' . $result->location . '</option>';
                                                                }
                                                            } ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top">Duration :</th>
                                                    <td>
                                                        <select name="datebtw" style="width:140px; height: 27px; padding: 3px 0;" id="job-duration" required>
                                                            <option value=""> Select Duration --</option>
                                                            <option <?php if ($jobdetails->duration == "1 Week") {
                                                                        echo 'selected="selected"';
                                                                    } ?> value="1 Week">1 Week</option>
                                                            <option <?php if ($jobdetails->duration == "2 Weeks") {
                                                                        echo 'selected="selected"';
                                                                    } ?> value="2 Weeks">2 Weeks</option>
                                                            <option <?php if ($jobdetails->duration == "3 Weeks") {
                                                                        echo 'selected="selected"';
                                                                    } ?> value="3 Weeks">3 Weeks</option>
                                                            <option <?php if ($jobdetails->duration == "1 Month") {
                                                                        echo 'selected="selected"';
                                                                    } ?> value="1 Month">1 Month</option>
                                                            <option <?php if ($jobdetails->duration == "3 Months") {
                                                                        echo 'selected="selected"';
                                                                    } ?> value="3 Months">3 Months</option>
                                                            <option <?php if ($jobdetails->duration == "6 Months") {
                                                                        echo 'selected="selected"';
                                                                    } ?> value="6 Months">6 Months</option>
                                                            <option <?php if ($jobdetails->duration == "1 Year") {
                                                                        echo 'selected="selected"';
                                                                    } ?> value="1 Year">1 Year</option>
                                                            <option <?php if ($jobdetails->duration == "18 Months") {
                                                                        echo 'selected="selected"';
                                                                    } ?> value="18 Months">18 Months</option>
                                                            <option <?php if ($jobdetails->duration == "2 Years") {
                                                                        echo 'selected="selected"';
                                                                    } ?> value="2 Years">2 Years</option>
                                                            <option <?php if ($jobdetails->duration == "Full Time") {
                                                                        echo 'selected="selected"';
                                                                    } ?> value="Full Time">Full Time</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <?php if ($jobdetails->duration != 'Full Time') { ?>
                                                        <th valign="top" id="rate-label">Rate :</th>
                                                        <td>
                                                            <input type="text" value="<?php echo $jobdetails->bill_rate; ?>" class="inp-form" name="jobrate" id="jobrate" placeholder="Rate" required /><span id="job-rate-error" style="color: red;"></span>
                                                            <input type="text" class="inp-form" name="salaryrate" id="salaryrate" placeholder="Salary" style="display: none;" required />
                                                        </td>
                                                    <?php } else { ?>
                                                        <th valign="top" id="rate-label">Salary :</th>
                                                        <td>
                                                            <input type="text" class="inp-form" name="jobrate" id="jobrate" placeholder="Rate" style="display: none;" required />
                                                            <input type="text" value="<?php echo $jobdetails->bill_rate; ?>" class="inp-form" name="salaryrate" id="salaryrate" placeholder="Salary" required /></span><span id="job-salary-rate-error" style="color: red;"></span>
                                                        </td>
                                                    <?php }

                                                    ?>
                                                </tr>
                                                <tr>
                                                    <th valign="top">Client Name :</th>
                                                    <td><input type="text" value="<?php echo $client_name; ?>" class="inp-form required" name="client_name" id="client_name" placeholder="Client Name" required /><span id="job-pg-add-client" style="display: none;">
                                                            The client is not exists click <a href="<?php echo base_url() ?>index.php/client/clientform">here</a> to add.</span></td>
                                                </tr>
                                                <tr>
                                                    <th>Job Description :</th>
                                                    <td><textarea class="inp-form" name="comments" placeholder="Job Description" style="width: 500px; min-height: 150px;"><?php echo $jobdetails->job_description; ?></textarea>
                                                        <div class="field-error"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <td valign="top" style="padding-top: 10px;">
                                                        <input type="hidden" name="id" value="<?= $jobdetails->id ?>">
                                                        <input type="submit" name="updatejobrequirement" class="form-submit" value="update Job Requirement" />
                                                        <input type="reset" class="form-reset" value="">
                                                    </td>
                                                    <td></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                        <!-- end id-form  -->

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </form>
                    <?php }
                    //} 
                    ?>

                    <div class="clear"></div>

                </div>
                <!--  end content-table-inner  -->
            </td>
            <td id="tbl-border-right"></td>
        </tr>
        <tr>
            <th class="sized bottomleft"></th>
            <td id="tbl-border-bottom">&nbsp;</td>
            <th class="sized bottomright"></th>
        </tr>
    </tbody>
</table>

<?php $this->load->view('includes/footer'); ?>
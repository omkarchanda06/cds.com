<link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
<style>
    .panel-group {
        width:70%; 
        //margin-left:15%; 
        margin-right:15%;
    }
    .innerHeader {
        background-color: #556b2f;
    }
    
    .tooltip {
  position: relative;
  /*display: inline-block;*/
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 120px;
  background-color: #555;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;
  position: absolute;
  z-index: 1;
  bottom: 125%;
  left: 50%;
  margin-left: -60px;
  opacity: 0;
  transition: opacity 0.3s;
}

.tooltip .tooltiptext::after {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #555 transparent transparent transparent;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
  opacity: 1;
}
.nav-tabs li.active {
    background-color: #0087C4 !important;
    border-radius: 10px;
    color: #fff !important;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus {
    background-color: #0087C4 !important;
    border-radius: 10px;
    color: #fff !important;
}

.nav-tabs {
    padding: 15px;
    justify-content: center;
    display: flex !important;
    background-color: #f5f5f5 !important;
    border-radius: 15px;
    align-content: center;
    width: max-content;
    margin-left: auto;
    margin-right: auto
}

.nav-tabs li {
    border: none !important;
    border-radius: 10px !important;
}

.nav-tabs li a:hover {
    border: 1px solid #0087C4 !important;
    border-radius: 10px !important;
    background-color: #EDE6D6;
}

.nav-tabs li a {
    position: relative;
    border: none !important;
    -webkit-transition: .5s;
    transition: .5s;
    max-height: 38px !important;
    margin-left: 2px;
    margin-right: 2px;
}

.nav-item:not(.active):hover:before {
    opacity: 1;
    bottom: 0;
}

.nav-item:not(.active):hover {
    color: #333;
}
.panel-group{
    margin-left:auto;
    margin-right:auto;
}

</style>

<div class="row" style="font-family: Calibri;font-size: 12px;">
                    <div class="col-lg-12">                       
                    <nav class="nav nav_copy js-nav2">
                          <ul id="myTab" class="nav nav-tabs">
                              <li>
                                  <a href="#home" class="nav-item active" data-toggle="tab">
                                      Client Tracker
                                  </a>
                              </li>
                              <li><a href="#a" class="nav-item" data-toggle="tab">Interview Tracker</a></li>
                              <li><a href="#b" class="nav-item" data-toggle="tab">STATS</a></li>
                              <!-- <li><a href="#c" data-toggle="tab"><b>Goals2022</b></a></li> -->
                              <li><a href="#d" class="nav-item" data-toggle="tab">Comparision Report</a></li>
                              <li><a href="#e" class="nav-item" data-toggle="tab">Recruiters Data</a></li>
                          </ul>
                      </nav>
                        <div id="myTabContent" class="tab-content">
                           <div class="tab-pane fade in active" id="home">
                              <div class="content_accordion">
                                <div class="panel-group" id="res">
                                    <div class="row">
                                        <div class="col-md-11">
                                            <table class="table table-bordered" id="clientTracker">
                                                <tr>
                                                    <th>Client Name</th><th>Requirement</th><th>Date</th><th>Submissions</th><th>Rejections</th><th>No Updates</th><th>Client Submission</th><th>Interview</th><th>Recjected after Interview</th><th>Placement</th><th>Backout</th>
                                                </tr>
                                                
                                                <?php foreach($monthyears as $monthyear) {
                                                    ?>
                                                    <tr style="background-color: #FFA500;color:#fff;"><td colspan="11"><?php echo date("F Y", strtotime($monthyear)); ?></td></tr>
                                                    
                                                    <?php 
                                                       // var_dump($allclientTrackers);exit;
                                                    foreach($allclientTrackers as $key => $allclientTracker) { 
                                                       $aid = explode(' ', $key);
                                                        ?>
                                                       
                                                    <tr style="background-color: green;color:#fff;" data-toggle="collapse" data-target="#<?php echo $aid[0].$monthyear ?>" class="accordion-toggle"><td colspan="11"><?php echo $key; ?><button class="btn btn-default btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button></td></tr>
                                                    
                                                    <tbody class="accordian-body collapse" id="<?php echo $aid[0].$monthyear ?>">
                                                        <?php
                                                         //echo '<div class="accordian-body collapse" id="demo1" style="display: table-row-group">';
                                                        foreach($allclientTracker as $clientTrackers) {
                                                           
                                                            if(sizeof($clientTrackers) > 0) {
                                                                foreach($clientTrackers as $clientTracker) {
                                                                
                                                        $jobdate_yyyymm = date("Y-m", strtotime($clientTracker->jobdate)); 
                                                        if ($monthyear == $jobdate_yyyymm) { 
                                                            $cnamesArray = explode(',',$clientTracker->cname);
                                                            $rejectsArray = explode(',',$clientTracker->rejnames);
                                                            $sub_client_names_Array = explode(',',$clientTracker->sub_client_names);
                                                            $interview_names_Array = explode(',',$clientTracker->interview_names);
                                                    ?>
                                                    
                                                <tr class="">
                                                    
                                                    <td><?php echo $clientTracker->clientname; ?></td><td><?php echo $clientTracker->job_title; ?></td><td><?php echo $clientTracker->jobdate; ?></td><td class="tooltip" style="opacity:1"><?php echo $clientTracker->subcount; ?> <span class="tooltiptext">
                                                        <?php 
                                                            //echo $clientTracker->cname;
                                                            foreach($cnamesArray as $cnameArray) {
                                                                echo $cnameArray,"<br>";
                                                            }
                                                        ?>
                                                    </span></td><td class="tooltip" style="opacity:1"><?php echo $clientTracker->rej_count; ?><span class="tooltiptext">
                                                        <?php 
                                                            //echo $clientTracker->cname;
                                                            foreach($rejectsArray as $rejectArray) {
                                                                echo $rejectArray,"<br>";
                                                            }
                                                        ?>
                                                    </span></td><td>0</td><td class="tooltip" style="opacity:1"><?php echo $clientTracker->client_sub_count; ?><span class="tooltiptext">
                                                        <?php 
                                                            //echo $clientTracker->cname;
                                                            foreach($sub_client_names_Array as $sub_client_name_Array) {
                                                                echo $sub_client_name_Array,"<br>";
                                                            }
                                                        ?>
                                                    </span></td><td class="tooltip" style="opacity:1"><?php echo $clientTracker->interview_count; ?><span class="tooltiptext">
                                                        <?php 
                                                            //echo $clientTracker->cname;
                                                            foreach($interview_names_Array as $interview_name_Array) {
                                                                echo $interview_name_Array,"<br>";
                                                            }
                                                        ?>
                                                    </span></td><td><?php echo $clientTracker->reject_after_interview_count; ?></td><td><?php echo $clientTracker->placed_count; ?></td><td><?php echo $clientTracker->backout_count; ?></td>
                                               
                                                </tr>
                                                <?php }}}} echo '</tbody>'; } ?>

                                                <?php
                                                } ?>
                                            </table>
                                        </div>
                                        <?php if($usertype == 'SUPERADMIN' || $usertype == 'HR' || $usertype == 'MIS') {  ?>
                                            <!-- <div class="col-md-1">    
                                                <table><tr style="border-bottom:none"><td><a id="dlink"  style="display:none;"></a><input type="button" name="submit" value="Generate Excel" class="export" id="excel-button" onclick="tableToExcel('clientTracker', 'clientTracker', 'clientTracker.xls')" /></td></tr>
                                                </table>
                                            </div> -->

                                            <div class="download-btn">
                                                <button type="button" id="excel-button" value="Generate Excel" onclick="tableToExcel('clientTracker', 'clientTracker', 'clientTracker.xls')" class="download-button export">
                                                    <div class="docs">
                                                    <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                        stroke="currentColor" height="20" width="20" viewBox="0 0 24 24">
                                                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                        <polyline points="14 2 14 8 20 8"></polyline>
                                                        <line y2="13" x2="8" y1="13" x1="16"></line>
                                                        <line y2="17" x2="8" y1="17" x1="16"></line>
                                                        <polyline points="10 9 9 9 8 9"></polyline>
                                                    </svg>
                                                    Excel
                                                    </div>
                                                    <div class="download">
                                                    <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                        stroke="currentColor" height="24" width="24" viewBox="0 0 24 24">
                                                        <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                        <polyline points="7 10 12 15 17 10"></polyline>
                                                        <line y2="3" x2="12" y1="15" x1="12"></line>
                                                    </svg>
                                                    </div>
                                                </button>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    </div>
                             </div>
<!--accordion end-->
                           </div>
                           <div class="tab-pane fade" id="a">
                             <div class="content_accordion">
                                    <div class="panel-group" id="ga">
                                        <div class="row">
                                            <div class="col-md-11">
                                                <table class="table table-bordered" id="interviewTracker">
                                                    <tr>
                                                        <th>Date</th><th>Time</th><th>Recruiter</th><th>Requirement</th><th>Client</th><th>Client Contact</th><th>Consultant</th><th>Feedback</th>
                                                    </tr>
                                                    <?php foreach($monthyears as $monthyear) {
                                                        ?>
                                                        <tr style="background-color: #FFA500;"><td colspan="11"><?php echo date("F Y", strtotime($monthyear)); ?></td></tr>
                                                    <?php  foreach($interviewTrackers as $interviewTracker) {
                                                            $jobdate_yyyymm = date("Y-m", strtotime($interviewTracker->interview_date)); 
                                                            if ($monthyear == $jobdate_yyyymm) { 
                                                                $getClientContact = $this->reportsmodel->getClientContact($interviewTracker->company);
                                                        ?>
                                                    <tr>
                                                        <td><?php echo $interviewTracker->interview_date; ?></td><td><?php echo $interviewTracker->interview_time; ?></td><td><?php echo $interviewTracker->recruiter; ?></td><td><?php echo $interviewTracker->psub; ?></td><td><?php echo $interviewTracker->company; ?></td><td><?php echo $getClientContact->client_name; ?></td><td><?php echo $interviewTracker->cname; ?></td><td><?php echo $interviewTracker->status; ?></td>
                                                    </tr>
                                                    <?php }}} ?>
                                                </table>
                                            </div>
                                            <?php if($usertype == 'SUPERADMIN' || $usertype == 'HR' || $usertype == 'MIS') {  ?>
                                                <!-- <div class="col-md-1">    
                                                    <table><tr style="border-bottom:none"><td><a id="dlink"  style="display:none;"></a><input type="button" name="submit" value="Generate Excel" class="export" id="excel-button" onclick="tableToExcel('interviewTracker', 'interviewTracker', 'interviewTracker.xls')" /></td></tr>
                                                    </table>
                                                </div> -->

                                                <div class="download-btn">
                                                    <button type="button" id="excel-button" value="Generate Excel" onclick="tableToExcel('interviewTracker', 'interviewTracker', 'interviewTracker.xls')" class="download-button export">
                                                        <div class="docs">
                                                        <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                            stroke="currentColor" height="20" width="20" viewBox="0 0 24 24">
                                                            <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                            <polyline points="14 2 14 8 20 8"></polyline>
                                                            <line y2="13" x2="8" y1="13" x1="16"></line>
                                                            <line y2="17" x2="8" y1="17" x1="16"></line>
                                                            <polyline points="10 9 9 9 8 9"></polyline>
                                                        </svg>
                                                        Excel
                                                        </div>
                                                        <div class="download">
                                                        <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                            stroke="currentColor" height="24" width="24" viewBox="0 0 24 24">
                                                            <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                            <polyline points="7 10 12 15 17 10"></polyline>
                                                            <line y2="3" x2="12" y1="15" x1="12"></line>
                                                        </svg>
                                                        </div>
                                                    </button>
                                                </div>
                                            <?php } ?>
                                        </div>
                                      
                                    </div>
                             </div>
<!--accordion end-->
                           </div>
                           <div class="tab-pane fade" id="b">
                              <div class="content_accordion">
                                    <div class="panel-group" id="gb">
                                    <div class="row">
                                        <div class="col-md-11">
                                            <table class="table table-bordered" id="stats">
                                                <tr>
                                                    <th>Submission Target</th><th>Submission</th><th>Rejection</th><th>Client Submission</th><th>Interview</th><th>Placement</th>
                                                </tr>
                                                <tr style="background-color: #FFA500;"><td colspan="11"><?php echo date("F Y", strtotime(date('Y-m'))); ?></td></tr>
                                                <?php 
                                                   
                                                foreach($allstats as $key1 => $allstatsTracker) { 
                                                    $get_username = $this->assignmanagerstodirector->get_username($key1);

                                                    // // get managers
                                                    // $getallmanagers = $this->assignmanagerstodirector->get_all_managersIdByDirectorID($key1);
                                                   
                                                    // foreach($getallmanagers as $getallmanager) {

                                                    //   // get Recruiters
                                                    //   $getRecruiters = $this->recruiter->getRecruitersByManagerID($getallmanager);
                                                      
                                                        
                                                    ?>
                                                    <tr style="background-color: green;color:#fff;"><td colspan="11"><?php echo $get_username->username; ?></td></tr>
                                                <?php

                                                    foreach($allstatsTracker as $allstat) {
                                                        if ($allstat->placements != null) {
                                                ?> 
                                                    
                                                <tr>
                                                <td><?php echo $targets; ?></td><td><?php echo $allstat->submissioncount ?></td><td><?php echo $allstat->rejections ?></td><td><?php echo $allstat->submitted_to_client ?></td><td><?php echo $allstat->interviews ?><td><?php echo $allstat->placements; ?></td>
                                                </tr>
                                                <?php }} }//}?>
                                            </table>
                                                <div id="chart-container" style="width:600px;height:600px">
                                                    <canvas id="mycanvas" ></canvas>
                                                </div>
                                        </div>
                                        <?php if($usertype == 'SUPERADMIN' || $usertype == 'HR' || $usertype == 'MIS') {  ?>
                                            <!-- <div class="col-md-1">    
                                                    <table><tr style="border-bottom:none"><td><a id="dlink"  style="display:none;"></a><input type="button" name="submit" value="Generate Excel" class="export" id="excel-button" onclick="tableToExcel('stats', 'STATS', 'stats.xls')" /></td></tr>
                                                    </table>
                                            </div> -->
                                            
                                            <div class="download-btn">
                                                <button type="button" id="excel-button" value="Generate Excel" onclick="tableToExcel('stats', 'STATS', 'stats.xls')" class="download-button export">
                                                    <div class="docs">
                                                    <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                        stroke="currentColor" height="20" width="20" viewBox="0 0 24 24">
                                                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                        <polyline points="14 2 14 8 20 8"></polyline>
                                                        <line y2="13" x2="8" y1="13" x1="16"></line>
                                                        <line y2="17" x2="8" y1="17" x1="16"></line>
                                                        <polyline points="10 9 9 9 8 9"></polyline>
                                                    </svg>
                                                    Excel
                                                    </div>
                                                    <div class="download">
                                                    <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                        stroke="currentColor" height="24" width="24" viewBox="0 0 24 24">
                                                        <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                        <polyline points="7 10 12 15 17 10"></polyline>
                                                        <line y2="3" x2="12" y1="15" x1="12"></line>
                                                    </svg>
                                                    </div>
                                                </button>
                                            </div>
                                        <?php } ?>

                                    </div>
                                    </div>
                             </div>
<!--accordion end-->
                           </div>
                           <!-- <div class="tab-pane fade" id="c">
                                <div class="content_accordion">
                                    <div class="panel-group" id="gc">
                                    <p>No data Available</p>
                                    </div>
                             </div>
                accordion end
                           </div> -->
                            <div class="tab-pane fade" id="d">
                                <div class="content_accordion">
                                    <div class="panel-group" id="gd">
                                        <div class="row">
                                            <div class="col-md-11">                                       
                                                <table class="table table-bordered" id="comparison">
                                                    <tr style="background-color:#FFA500"><td colspan="3">Comparision Report LLW(<?php echo date('jS', strtotime($comparision['last_last_week_sd'])).'-'. date('jS', strtotime($comparision['last_last_week_ed'])).' '.date('F', strtotime($comparision['last_last_week_sd'])); ?>) Vs LW(<?php echo date('jS', strtotime($comparision['last_week_sd'])).'-'. date('jS', strtotime($comparision['last_week_ed'])).' '.date('F', strtotime($comparision['last_week_sd'])); ?>) Submission</td></tr>
                                                    <tr>
                                                        <th>Manager Name</th><th>LLW SUBMISSIONS</th><th>LW SUBMISSIONS</th>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <?php 
                                                        if ($usertype != 'RECRUITER') {
                                                            foreach($managers as $manager) { echo $manager->username; }
                                                        } else {
                                                            echo $recruitername;
                                                        }
                                                            
                                                        ?>
                                                    </td>
                                                    <?php 
                                                    $llw_average = [];
                                                    $lw_average = [];
                                                    if (sizeof($comparision['last_last_week']) > 0) {
                                                    foreach($comparision['last_last_week'] as $compllw) {
                                                        $llw_average[] = $compllw->submissioncount/5;
                                                        ?>
                                                    
                                                    <td><table class="table table-bordered"><tr class="innerRow" style="background-color:#a52a2a;"><th class="innerHeader">Submission Target</th><th class="innerHeader">Total Subs</th><th class="innerHeader">Average</th><th class="innerHeader">Total Submission %</th></tr>
                                                    <tr><td><?php echo $targets; ?></td><td><?php echo $compllw->submissioncount; ?></td><td><?php echo $compllw->submissioncount/5 ?></td><td><?php echo  number_format(($compllw->submissioncount/$targets)*100, 2, '.', '')?></td></tr>
                                                    </table>
                                                </td>
                                                <?php }} else { echo '<td>No Data Available</td>'; } foreach($comparision['last_week'] as $complw) {
                                                    $lw_average[] = $complw->submissioncount/5;
                                                    ?>
                                                <td><table class="table table-bordered"><tr><th class="innerHeader">Submission Target</th><th class="innerHeader">Total Subs</th><th class="innerHeader">Average</th><th class="innerHeader">Total Submission %</th><th class="innerHeader">DIFF BTW LW,TW AVG</th></tr>
                                                    <tr><td><?php echo $targets; ?></td><td><?php echo $complw->submissioncount; ?></td><td><?php echo $complw->submissioncount/5 ?></td><td><?php echo  number_format(($complw->submissioncount/$targets)*100, 2, '.', '')?></td><td><?php echo $lw_average[0] - $llw_average[0]; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                </table></td>
                                            </div>
                                            <?php if($usertype == 'SUPERADMIN' || $usertype == 'HR' || $usertype == 'MIS') {  ?>
                                                <!-- <div class="col-md-1">    
                                                    <table><tr style="border-bottom:none"><td><a id="dlink"  style="display:none;"></a><input type="button" name="submit" value="Generate Excel" class="export" id="excel-button" onclick="tableToExcel('comparison', 'comparison', 'comparison.xls')" /></td></tr>
                                                    </table>
                                                </div> -->
                                                <div class="download-btn">
                                                    <button type="button" id="excel-button" value="Generate Excel" onclick="tableToExcel('comparison', 'comparison', 'comparison.xls')" class="download-button export">
                                                        <div class="docs">
                                                        <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                            stroke="currentColor" height="20" width="20" viewBox="0 0 24 24">
                                                            <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                            <polyline points="14 2 14 8 20 8"></polyline>
                                                            <line y2="13" x2="8" y1="13" x1="16"></line>
                                                            <line y2="17" x2="8" y1="17" x1="16"></line>
                                                            <polyline points="10 9 9 9 8 9"></polyline>
                                                        </svg>
                                                        Excel
                                                        </div>
                                                        <div class="download">
                                                        <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                            stroke="currentColor" height="24" width="24" viewBox="0 0 24 24">
                                                            <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                            <polyline points="7 10 12 15 17 10"></polyline>
                                                            <line y2="3" x2="12" y1="15" x1="12"></line>
                                                        </svg>
                                                        </div>
                                                    </button>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                             </div>
<!--accordion end-->
                           </div>
                            <div class="tab-pane fade" id="e">
                                <div class="content_accordion">
                                    <div class="panel-group" id="ge">
                                    <div class="row">
                                        <div class="col-md-11">   
                                        <table class="table table-bordered" id="recruiterdata">
                                            <tr>
                                            <th>Manager Name</th><th>Recruiter Name</th><th>Submission Target</th><th>Total Submissions</th><th>Submissions %</th><th>Interviews</th><th>Rejections</th><th>Placements</th>
                                            </tr>
                                            <?php foreach($monthyears as $monthyear) {
                                                ?>
                                                <tr style="background-color: #FFA500;"><td colspan="8"><?php echo date("F Y", strtotime($monthyear)); ?></td></tr>
                                            <?php  
                                           if ($usertype != 'RECRUITER') {
                                            foreach($allrecruitersdata as $key1 => $allrecruiterdata) {
                                               // var_dump($allrecruiterdata);
                                                ?>
                                                <tr style="background-color: green;"><td colspan="11"><?php echo $key1; ?></td></tr>
                                            <?php
                                               foreach($allrecruiterdata as $key2 => $allrecruiter) {
                                                
                                                if (sizeof($allrecruiter)>0) {
                                                foreach($allrecruiter as $recruiterdata) {
                                               //var_dump($recruiterdata);
                                                    $jobdate_yyyymm = date("Y-m", strtotime($recruiterdata->sdate)); 
                                                    if ($monthyear == $jobdate_yyyymm) { 
                                                        //$recruiters = $this->recruiter->getRecruitersByManagerID($managerId);
                                                   //foreach($recruiters as $key1 => $recruiter) {
                                                    //  if($recruiterdata->recruiterId == $recruiter->id) {
                                                        //var_dump($recruiters);exit;
                                                      // $submissionsPercentage = $recruitersdata[$key]->submissioncount;
                                                       
                                                    $targetResult	= $this->recruiter->getRecruiterTotalTarget($recruiterdata->recruiterId, $fromdate, $todate, count($dates)); 
                                                    
                                                    $submissionsPercentage = $this->user->calculatePercentage($recruiterdata->submissioncount, $targetResult['targets']); 
                                                    if ($recruiterdata->recruiterId != "") {
                                                       
                                                    $gerecruitername = $this->user->gerecruitername($recruiterdata->recruiterId);
                                                    $managername = $this->assignmanagerstodirector->get_username($gerecruitername->mid);
                                                    
                                            ?>
                                            <tr>
                                                <td><?php echo $managername->username; ?></td><td><?php echo $gerecruitername->recruiter; ?></td><td><?php echo $targetResult['targets']; ?></td><td><?php echo $recruiterdata->submissioncount; ?></td><td><?php echo $submissionsPercentage; ?>%</td><td><?php echo $recruiterdata->interviews; ?></td><td><?php echo $recruiterdata->rejections; ?></td><td><?php echo $recruiterdata->placements; ?></td>
                                            </tr>
                                            <?php 
                                            //} else { ?>
                                             <!-- <tr>
                                                <td><?php //echo $recruiter->recruiter; ?></td><td>0</td><td></td>
                                            </tr> -->
                                            <?php
                                           // }
                                        
                                        //}
                                    }}} }}}
                                    } else {
                                        foreach($recruitersdata as $key => $recruiterdata) {
                                               
                                            //var_dump($recruiterdata);
                                                 $jobdate_yyyymm = date("Y-m", strtotime($recruiterdata->sdate)); 
                                                 if ($monthyear == $jobdate_yyyymm) { 
                                                                                                       
                                                 //$targetResult	= $this->recruiter->getRecruiterTotalTarget($recruiter->id, $fromdate, $todate, count($dates)); 
                                                 
                                                 $submissionsPercentage = $this->user->calculatePercentage($recruiterdata->submissioncount, $targets); 
                                                 
                                         ?>
                                         <tr>
                                             <td><?php echo $recruitername; ?></td><td><?php echo $targets; ?></td><td><?php echo $recruiterdata->submissioncount; ?></td><td><?php echo $submissionsPercentage; ?>%</td><td><?php echo $recruiterdata->interviews; ?></td><td><?php echo $recruiterdata->rejections; ?></td><td><?php echo $recruiterdata->placements; ?></td>
                                         </tr>
                                         <?php 
                                         
                                     
                                     }}
                                 }
                                    
                                        }
                                            
                                            ?>
                                        </table>
                                    </div>
                                    <?php if($usertype == 'SUPERADMIN' || $usertype == 'HR' || $usertype == 'MIS') {  ?>
                                        <!-- <div class="col-md-1">    
                                            <table><tr style="border-bottom:none"><td><a id="dlink"  style="display:none;"></a><input type="button" name="submit" value="Generate Excel" class="export" id="excel-button" onclick="tableToExcel('recruiterdata', 'RecruiterData', 'RecruiterData.xls')" /></td></tr>
                                            </table>
                                        </div> -->

                                        <div class="download-btn">
                                                <button type="button" id="excel-button" value="Generate Excel" onclick="tableToExcel('recruiterdata', 'RecruiterData', 'RecruiterData.xls')" class="download-button export">
                                                    <div class="docs">
                                                    <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                        stroke="currentColor" height="20" width="20" viewBox="0 0 24 24">
                                                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                        <polyline points="14 2 14 8 20 8"></polyline>
                                                        <line y2="13" x2="8" y1="13" x1="16"></line>
                                                        <line y2="17" x2="8" y1="17" x1="16"></line>
                                                        <polyline points="10 9 9 9 8 9"></polyline>
                                                    </svg>
                                                    Excel
                                                    </div>
                                                    <div class="download">
                                                    <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                        stroke="currentColor" height="24" width="24" viewBox="0 0 24 24">
                                                        <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                        <polyline points="7 10 12 15 17 10"></polyline>
                                                        <line y2="3" x2="12" y1="15" x1="12"></line>
                                                    </svg>
                                                    </div>
                                                </button>
                                            </div>
                                    <?php } ?>
                                    </div>
                                    </div>
                             </div>
<!--accordion end-->
                           </div>
                    
                    </div>
                    
                </div>
                
                <!-- /.row -->
<script src="<?php echo base_url()?>js/Chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        
        var jsonObj = '{"Total_target":"<?php echo $targets ?>","submissions": "<?php echo $stats[0]->submissioncount ?>", "placements":"<?php echo $stats[0]->placements ?>", "submit_to_client":"<?php echo $stats[0]->submitted_to_client ?>", "interviews":"<?php echo $stats[0]->interviews ?>", "rejections":"<?php echo $stats[0]->rejections ?>"}';

        var jsonparse = JSON.parse(jsonObj);
        var jsonkey = [];
        var jsonvalue = [];
        for (var key in jsonparse) {
            jsonkey.push(key);
            jsonvalue.push(jsonparse[key]);
        }

        var chartdata = {
        labels: jsonkey,
        datasets : [
        {
            label: 'July 2022',
            backgroundColor: 'rgb(0,162,237)',
            borderColor: 'rgba(200, 200, 200, 0.75)',
            //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
            //hoverBorderColor: 'rgba(200, 200, 200, 1)',
            data: jsonvalue
        }
        ]
    };
        var ctx = $("#mycanvas");
        var barGraph = new Chart(ctx, {
            type: 'bar',
            data: chartdata
        });

    });
    var tableToExcel 	= (function(){
        var uri 	= 'data:application/vnd.ms-excel;base64,', 
        template 	= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>', 
        base64 		= function (s) { return window.btoa(unescape(encodeURIComponent(s))) }, 
        format 		= function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return 		  function (table, name, filename) {
	            if(!table.nodeType) table = document.getElementById(table)
	            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
	            document.getElementById("dlink").href 		= uri + base64(format(template, ctx));
	            document.getElementById("dlink").download 	= filename;
	            document.getElementById("dlink").click();
        }
})()
</script>

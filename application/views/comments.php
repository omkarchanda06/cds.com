<div class="comments-list">
<?php if(sizeof($getcomments) > 0): foreach($getcomments as $comment): ?>
	<div class="comment-item">
		<div class="comment-head">
		<span class="comment-user"><?=$comment->username?></span>
		<span class="comment-time"><?=$comment->ondate?></span>
		</div>
		<div class="comment-content">
		<?=$comment->comment?>
		</div>
	</div>
<?php endforeach; else: ?>
No Comments Found
<?php endif; ?>
</div>
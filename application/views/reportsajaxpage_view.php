<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script>


</script>
<style>
    .panel-group {
        width:70%;
        margin-right:15%;
    }
    .innerHeader {
        background-color: #556b2f;
    }
    
    .tooltip {
  position: relative;
  /*display: inline-block;*/
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 120px;
  background-color: #555;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;
  position: absolute;
  z-index: 1;
  bottom: 125%;
  left: 50%;
  margin-left: -60px;
  opacity: 0;
  transition: opacity 0.3s;
}

.tooltip .tooltiptext::after {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #555 transparent transparent transparent;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
  opacity: 1;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus {
    background-color: #0087C4 !important;
    border-radius: 10px;
    color: #fff !important;
}

.nav-tabs {
    padding: 15px;
    justify-content: center;
    display: flex !important;
    background-color: #f5f5f5 !important;
    border-radius: 15px;
    align-content: center;
    width: max-content;
    margin-left: auto;
    margin-right: auto
}

.nav-tabs li {
    border: none !important;
    border-radius: 10px !important;
}

.nav-tabs li a:hover {
    border: 1px solid #0087C4 !important;
    border-radius: 10px !important;
    background-color: #EDE6D6;
}

.nav-tabs li a {
    position: relative;
    border: none !important;
    -webkit-transition: .5s;
    transition: .5s;
    max-height: 38px !important;
    margin-left: 2px;
    margin-right: 2px;
}

.nav-item:not(.active):hover:before {
    opacity: 1;
    bottom: 0;
}

.nav-item:not(.active):hover {
    color: #333;
}
.panel-group{
    margin-left:auto;
    margin-right:auto;
}

</style>


<div class="row" style="font-family: Calibri;font-size: 12px;">
                    <div class="col-lg-12">                       
                    <nav class="nav nav_copy js-nav2">
                          <ul id="myTab" class="nav nav-tabs">
                              <li>
                                  <a href="#home" class="nav-item active" data-toggle="tab">
                                      Client Tracker
                                  </a>
                              </li>
                              <li><a href="#a" class="nav-item" data-toggle="tab">Interview Tracker</a></li>
                              <li><a href="#b" class="nav-item" data-toggle="tab">STATS</a></li>
                              <!-- <li><a href="#c" data-toggle="tab"><b>Goals2022</b></a></li> -->
                              <li><a href="#d" class="nav-item" data-toggle="tab">Comparision Report</a></li>
                              <li><a href="#e" class="nav-item" data-toggle="tab">Recruiters Data</a></li>
                          </ul>
                      </nav>
                        <div id="myTabContent" class="tab-content">
                           <div class="tab-pane fade in active" id="home">
                              <div class="content_accordion">
                                <div class="panel-group" id="res">
                                    <div class="row">
                                        <div class="col-md-11">
                                            <table class="table table-bordered" id="clientTracker">
                                                <tr>
                                                    <th>Client Name</th><th>Company</th><th>Requirement</th><th>Date</th><th>Submissions</th><th>Rejections</th><th>No Updates</th><th>Client Submission</th><th>Interview</th><th>Recjected after Interview</th><th>Placement</th><th>Backout</th>
                                                </tr>
                                                
                                                <?php
                                                $session_data = $this->session->userdata('logged_in');
                                                $usertype = $session_data['usertype'];
                                                //print_r($clientTrackers);
                                                foreach($clientTrackers as $clientTracker){
                                                    $company_names[] = $clientTracker->clientname;
                                                }
                                                $company_count = array_count_values($company_names);
                                                // print_r($company_names);echo "<br/>";print_r($company_count);
                                                $i = 1;$row_id = 0;$active_clients_sno = 0;
                                                foreach($monthyears as $monthyear) {
                                                    ?>
                                                    <tr style="background-color: #FFA500;"><td colspan="12"><?php echo date("F Y", strtotime($monthyear)); ?></td></tr>
                                                    <?php
                                                    foreach($clientTrackers as $key => $clientTracker) {
                                                        $row_id++;
                                                        $jobdate_yyyymm = date("Y-m", strtotime($clientTracker->jobdate)); 
                                                        if ($monthyear == $jobdate_yyyymm) { 
                                                            $cnamesArray = explode(',',$clientTracker->cname);
                                                            $rejectsArray = explode(',',$clientTracker->rejnames);
                                                            $sub_client_names_Array = explode(',',$clientTracker->sub_client_names);
                                                            $interview_names_Array = explode(',',$clientTracker->interview_names);

                                                            $client_details = $this->reportsmodel->get_clientDetailsByName_userid($clientTracker->clientname,$clientTracker->userID);
                                                    ?>
                                                <tr id="<?php echo $row_id; ?>">
                                                    <?php 
                                                    if ($company_count[$clientTracker->clientname] == 1) { ?>
                                                        <td>
                                                            <?php
                                                            $client_status = $client_details[0]->client_status;
                                                            switch ($client_status) {
                                                                case 'ACTIVE':
                                                                        $active_clients_sno++;
                                                                        echo $active_clients_sno.". <b class='active-client'>".$client_details[0]->client_name."</b>";
                                                                    break;

                                                                case 'RISING':
                                                                    echo "<b class='rising-client'>".$client_details[0]->client_name."</b>";
                                                                    break;
                                                                
                                                                default:
                                                                echo $client_details[0]->client_name;
                                                                    break; 
                                                            }?>
                                                        </td>
                                                        <td><?php echo $clientTracker->clientname; ?></td>
                                                        <td><?php echo $clientTracker->job_title; ?></td>
                                                        <td><?php echo $clientTracker->jobdate; ?></td>
                                                        <td class="tooltip" style="opacity:1"><?php echo $clientTracker->subcount; ?>
                                                            <span class="tooltiptext">
                                                            <?php
                                                                foreach($cnamesArray as $cnameArray) {
                                                                    echo $cnameArray,"<br>";
                                                                }
                                                            ?>
                                                            </span>
                                                        </td>
                                                        <td class="tooltip" style="opacity:1"><?php echo $clientTracker->rej_count; ?>
                                                            <span class="tooltiptext">
                                                            <?php
                                                                foreach($rejectsArray as $rejectArray) {
                                                                    echo $rejectArray,"<br>";
                                                                }
                                                            ?>
                                                            </span>
                                                        </td>
                                                        <td>0</td>
                                                        <td class="tooltip" style="opacity:1"><?php echo $clientTracker->client_sub_count; ?>
                                                            <span class="tooltiptext">
                                                            <?php
                                                                foreach($sub_client_names_Array as $sub_client_name_Array) {
                                                                    echo $sub_client_name_Array,"<br>";
                                                                }
                                                            ?>
                                                        </span>
                                                        </td>
                                                        <td class="tooltip" style="opacity:1"><?php echo $clientTracker->interview_count; ?>
                                                            <span class="tooltiptext">
                                                            <?php
                                                                foreach($interview_names_Array as $interview_name_Array) {
                                                                    echo $interview_name_Array,"<br>";
                                                                }
                                                            ?>
                                                        </span>
                                                        </td>
                                                        <td><?php echo $clientTracker->reject_after_interview_count; ?></td>
                                                        <td><?php echo $clientTracker->placed_count; ?></td>
                                                        <td><?php echo $clientTracker->backout_count; ?></td>
                                                        <?php
                                                    } else {
                                                        $i++;
                                                        if ($i > 1 && $client_company != $clientTracker->clientname) {
                                                            $rowspan = $company_count[$clientTracker->clientname];
                                                            $client_company = $clientTracker->clientname;
                                                            $active_clients_sno++;
                                                        }else{ ?>
                                                            <script type='text/javascript'>
                                                            $( document ).ready(function() {
                                                                var row_id = <?php echo $row_id;?>;
                                                                remove_first_td(row_id);
                                                                function remove_first_td(row_id) {
                                                                    // alert(row_id);
                                                                    var row = document.getElementById(row_id);
                                                                    row.deleteCell(0);
                                                                }
                                                            });</script><?php
                                                        }
                                                        ?>
                                                        <td rowspan="<?php echo $rowspan; ?>">
                                                            <?php
                                                            $client_status = $client_details[0]->client_status;
                                                            switch ($client_status) {
                                                                case 'ACTIVE':
                                                                        echo $active_clients_sno.". <b class='active-client'>".$client_details[0]->client_name."</b>";
                                                                    break;

                                                                case 'RISING':
                                                                    echo "<b class='rising-client'>".$client_details[0]->client_name."</b>";
                                                                    break;
                                                                
                                                                default:
                                                                echo $client_details[0]->client_name;
                                                                    break;
                                                            }?>
                                                        </td>
                                                        <td><?php echo $clientTracker->clientname; ?></td>
                                                        <td><?php echo $clientTracker->job_title; ?></td>
                                                        <td><?php echo $clientTracker->jobdate; ?></td>
                                                        <td class="tooltip" style="opacity:1"><?php echo $clientTracker->subcount; ?>
                                                            <span class="tooltiptext">
                                                            <?php 
                                                                //echo $clientTracker->cname;
                                                                foreach($cnamesArray as $cnameArray) {
                                                                    echo $cnameArray,"<br>";
                                                                }
                                                            ?>
                                                        </span>
                                                        </td>
                                                        <td class="tooltip" style="opacity:1"><?php echo $clientTracker->rej_count; ?>
                                                            <span class="tooltiptext">
                                                            <?php 
                                                                //echo $clientTracker->cname;
                                                                foreach($rejectsArray as $rejectArray) {
                                                                    echo $rejectArray,"<br>";
                                                                }
                                                            ?>
                                                            </span>
                                                        </td>
                                                        <td>0</td>
                                                        <td class="tooltip" style="opacity:1"><?php echo $clientTracker->client_sub_count; ?>
                                                            <span class="tooltiptext">
                                                            <?php 
                                                                //echo $clientTracker->cname;
                                                                foreach($sub_client_names_Array as $sub_client_name_Array) {
                                                                    echo $sub_client_name_Array,"<br>";
                                                                }
                                                            ?>
                                                            </span>
                                                        </td>
                                                        <td class="tooltip" style="opacity:1"><?php echo $clientTracker->interview_count; ?>
                                                            <span class="tooltiptext">
                                                            <?php 
                                                                //echo $clientTracker->cname;
                                                                foreach($interview_names_Array as $interview_name_Array) {
                                                                    echo $interview_name_Array,"<br>";
                                                                }
                                                            ?>  
                                                            </span>
                                                        </td>
                                                        <td><?php echo $clientTracker->reject_after_interview_count; ?></td>
                                                        <td><?php echo $clientTracker->placed_count; ?></td>
                                                        <td><?php echo $clientTracker->backout_count; ?></td> 
                                                        <?php
                                                        $i++;
                                                    }
                                                    ?>
                                                </tr>
                                                <?php }} ?>

                                                <?php
                                                } ?>
                                            </table>
                                        </div>
                                        <?php if($usertype == 'SUPERADMIN' || $usertype == 'HR' || $usertype == 'MIS') {  ?>
                                            <!-- <div class="col-md-1">    
                                                <table><tr style="border-bottom:none"><td><a id="dlink"  style="display:none;"></a><input type="button" name="submit" value="Generate Excel" class="export" id="excel-button" onclick="tableToExcel('clientTracker', 'clientTracker', 'clientTracker.xls')" /></td></tr>
                                                </table>
                                            </div> -->
                                            
                                            <div class="download-btn">
                                                <button type="button" id="excel-button" value="Generate Excel" onclick="tableToExcel('clientTracker', 'clientTracker', 'clientTracker.xls')" class="download-button export">
                                                    <div class="docs">
                                                    <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                        stroke="currentColor" height="20" width="20" viewBox="0 0 24 24">
                                                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                        <polyline points="14 2 14 8 20 8"></polyline>
                                                        <line y2="13" x2="8" y1="13" x1="16"></line>
                                                        <line y2="17" x2="8" y1="17" x1="16"></line>
                                                        <polyline points="10 9 9 9 8 9"></polyline>
                                                    </svg>
                                                    Excel
                                                    </div>
                                                    <div class="download">
                                                    <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                        stroke="currentColor" height="24" width="24" viewBox="0 0 24 24">
                                                        <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                        <polyline points="7 10 12 15 17 10"></polyline>
                                                        <line y2="3" x2="12" y1="15" x1="12"></line>
                                                    </svg>
                                                    </div>
                                                </button>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    </div>
                             </div>
<!--accordion end-->
                           </div>
                           <div class="tab-pane fade" id="a">
                             <div class="content_accordion">
                                    <div class="panel-group" id="ga">
                                        <div class="row">
                                            <div class="col-md-11">
                                                <table class="table table-bordered" id="interviewTracker">
                                                    <tr>
                                                        <th>Date</th><th>Time</th><th>Recruiter</th><th>Requirement</th><th>Client</th><th>Client Contact</th><th>Consultant</th><th>Feedback</th>
                                                    </tr>
                                                    <?php foreach($monthyears as $monthyear) {
                                                        ?>
                                                        <tr style="background-color: #FFA500;"><td colspan="11"><?php echo date("F Y", strtotime($monthyear)); ?></td></tr>
                                                    <?php  foreach($interviewTrackers as $interviewTracker) {
                                                            $jobdate_yyyymm = date("Y-m", strtotime($interviewTracker->interview_date)); 
                                                            if ($monthyear == $jobdate_yyyymm) { 
                                                                $getClientContact = $this->reportsmodel->getClientContact($interviewTracker->company);
                                                        ?>
                                                    <tr>
                                                        <td><?php echo $interviewTracker->interview_date; ?></td><td><?php echo $interviewTracker->interview_time; ?></td><td><?php echo $interviewTracker->recruiter; ?></td><td><?php echo $interviewTracker->psub; ?></td><td><?php echo $interviewTracker->company; ?></td><td><?php echo $getClientContact->client_name; ?></td><td><?php echo $interviewTracker->cname; ?></td><td><?php echo $interviewTracker->status; ?></td>
                                                    </tr>
                                                    <?php }}} ?>
                                                </table>
                                            </div>
                                            <?php if($usertype == 'SUPERADMIN' || $usertype == 'HR' || $usertype == 'MIS') {  ?>
                                                <!-- <div class="col-md-1">    
                                                    <table><tr style="border-bottom:none"><td><a id="dlink"  style="display:none;"></a><input type="button" name="submit" value="Generate Excel" class="export" id="excel-button" onclick="tableToExcel('interviewTracker', 'interviewTracker', 'interviewTracker.xls')" /></td></tr>
                                                    </table>
                                                </div> -->
                                                <div class="download-btn">
                                                    <button type="button" id="excel-button" value="Generate Excel" onclick="tableToExcel('interviewTracker', 'interviewTracker', 'interviewTracker.xls')" class="download-button export">
                                                        <div class="docs">
                                                        <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                            stroke="currentColor" height="20" width="20" viewBox="0 0 24 24">
                                                            <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                            <polyline points="14 2 14 8 20 8"></polyline>
                                                            <line y2="13" x2="8" y1="13" x1="16"></line>
                                                            <line y2="17" x2="8" y1="17" x1="16"></line>
                                                            <polyline points="10 9 9 9 8 9"></polyline>
                                                        </svg>
                                                        Excel
                                                        </div>
                                                        <div class="download">
                                                        <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                            stroke="currentColor" height="24" width="24" viewBox="0 0 24 24">
                                                            <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                            <polyline points="7 10 12 15 17 10"></polyline>
                                                            <line y2="3" x2="12" y1="15" x1="12"></line>
                                                        </svg>
                                                        </div>
                                                    </button>
                                                </div>
                                            <?php } ?>
                                        </div>
                                      
                                    </div>
                             </div>
<!--accordion end-->
                           </div>
                           <div class="tab-pane fade" id="b">
                              <div class="content_accordion">
                                    <div class="panel-group" id="gb">
                                    <div class="row">
                                        <div class="col-md-11">
                                            <table class="table table-bordered" id="stats">
                                                <tr>
                                                    <th>Manager</th><th>Submission Target</th><th>Submission</th><th>Rejection</th><th>Client Submission</th><th>Interview</th><th>Placement</th>
                                                </tr>
                                                <tr style="background-color: #FFA500;"><td colspan="11"><?php echo date("F Y", strtotime(date('Y-m'))); ?></td></tr>
                                                <?php foreach($stats as $stat) {
                                                    ?>
                                                    
                                                <tr>
                                                <td><?php if ($usertype != 'RECRUITER') {
                                                            foreach($managers as $manager) { echo $manager->username; }
                                                        } else {
                                                            echo $recruitername;
                                                        }?></td><td><?php echo $targets; ?></td><td><?php echo $stat->submissioncount; ?></td><td><?php echo $stat->rejections; ?></td><td><?php echo $stat->submitted_to_client; ?></td><td><?php echo $stat->interviews; ?><td><?php echo $stat->placements; ?></td>
                                                </tr>
                                                <?php } ?>
                                            </table>
                                                <div id="chart-container" style="width:600px;height:600px">
                                                    <canvas id="mycanvas" ></canvas>
                                                </div>
                                        </div>
                                        <?php if($usertype == 'SUPERADMIN' || $usertype == 'HR' || $usertype == 'MIS') {  ?>
                                            <!-- <div class="col-md-1">    
                                                    <table><tr style="border-bottom:none"><td><a id="dlink"  style="display:none;"></a><input type="button" name="submit" value="Generate Excel" class="export" id="excel-button" onclick="tableToExcel('stats', 'STATS', 'stats.xls')" /></td></tr>
                                                    </table>
                                            </div> -->
                                            <div class="download-btn">
                                                    <button type="button" id="excel-button" value="Generate Excel" onclick="tableToExcel('stats', 'STATS', 'stats.xls')" class="download-button export">
                                                        <div class="docs">
                                                        <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                            stroke="currentColor" height="20" width="20" viewBox="0 0 24 24">
                                                            <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                            <polyline points="14 2 14 8 20 8"></polyline>
                                                            <line y2="13" x2="8" y1="13" x1="16"></line>
                                                            <line y2="17" x2="8" y1="17" x1="16"></line>
                                                            <polyline points="10 9 9 9 8 9"></polyline>
                                                        </svg>
                                                        Excel
                                                        </div>
                                                        <div class="download">
                                                        <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                            stroke="currentColor" height="24" width="24" viewBox="0 0 24 24">
                                                            <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                            <polyline points="7 10 12 15 17 10"></polyline>
                                                            <line y2="3" x2="12" y1="15" x1="12"></line>
                                                        </svg>
                                                        </div>
                                                    </button>
                                                </div>
                                        <?php } ?>

                                    </div>
                                    </div>
                             </div>
<!--accordion end-->
                           </div>
                           <!-- <div class="tab-pane fade" id="c">
                                <div class="content_accordion">
                                    <div class="panel-group" id="gc">
                                    <p>No data Available</p>
                                    </div>
                             </div>
                accordion end
                           </div> -->
                            <div class="tab-pane fade" id="d">
                                <div class="content_accordion">
                                    <div class="panel-group" id="gd">
                                        <div class="row">
                                            <div class="col-md-11">                                       
                                                <table class="table table-bordered" id="comparison">
                                                    <tr style="background-color:#FFA500"><td colspan="3">Comparision Report LLW(<?php echo date('jS', strtotime($comparision['last_last_week_sd'])).'-'. date('jS', strtotime($comparision['last_last_week_ed'])).' '.date('F', strtotime($comparision['last_last_week_sd'])); ?>) Vs LW(<?php echo date('jS', strtotime($comparision['last_week_sd'])).'-'. date('jS', strtotime($comparision['last_week_ed'])).' '.date('F', strtotime($comparision['last_week_sd'])); ?>) Submission</td></tr>
                                                    <tr>
                                                        <th>Manager Name</th><th>LLW SUBMISSIONS</th><th>LW SUBMISSIONS</th>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <?php 
                                                        if ($usertype != 'RECRUITER') {
                                                            foreach($managers as $manager) { echo $manager->username; }
                                                        } else {
                                                            echo $recruitername;
                                                        }
                                                            
                                                        ?>
                                                    </td>
                                                    <?php 
                                                    $llw_average = [];
                                                    $lw_average = [];
                                                    if (sizeof($comparision['last_last_week']) > 0) {
                                                    foreach($comparision['last_last_week'] as $compllw) {
                                                        $llw_average[] = $compllw->submissioncount/5;
                                                        ?>
                                                    
                                                    <td><table class="table table-bordered"><tr class="innerRow" style="background-color:#a52a2a;"><th class="innerHeader">Submission Target</th><th class="innerHeader">Total Subs</th><th class="innerHeader">Average</th><th class="innerHeader">Total Submission %</th></tr>
                                                    <tr><td><?php echo $targets; ?></td><td><?php echo $compllw->submissioncount; ?></td><td><?php echo $compllw->submissioncount/5 ?></td><td><?php echo  number_format(($compllw->submissioncount/$targets)*100, 2, '.', '')?></td></tr>
                                                    </table>
                                                </td>
                                                <?php }} else { echo '<td>No Data Available</td>'; } foreach($comparision['last_week'] as $complw) {
                                                    $lw_average[] = $complw->submissioncount/5;
                                                    ?>
                                                <td><table class="table table-bordered"><tr><th class="innerHeader">Submission Target</th><th class="innerHeader">Total Subs</th><th class="innerHeader">Average</th><th class="innerHeader">Total Submission %</th><th class="innerHeader">DIFF BTW LW,TW AVG</th></tr>
                                                    <tr><td><?php echo $targets; ?></td><td><?php echo $complw->submissioncount; ?></td><td><?php echo $complw->submissioncount/5 ?></td><td><?php echo  number_format(($complw->submissioncount/$targets)*100, 2, '.', '')?></td><td><?php echo $lw_average[0] - $llw_average[0]; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                </table></td>
                                            </div>
                                            <?php if($usertype == 'SUPERADMIN' || $usertype == 'HR' || $usertype == 'MIS') {  ?>
                                                <!-- <div class="col-md-1">    
                                                    <table><tr style="border-bottom:none"><td><a id="dlink"  style="display:none;"></a><input type="button" name="submit" value="Generate Excel" class="export" id="excel-button" onclick="tableToExcel('comparison', 'comparison', 'comparison.xls')" /></td></tr>
                                                    </table>
                                                </div> -->

                                                <div class="download-btn">
                                                    <button type="button" id="excel-button" value="Generate Excel" onclick="tableToExcel('comparison', 'comparison', 'comparison.xls')" class="download-button export">
                                                        <div class="docs">
                                                        <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                            stroke="currentColor" height="20" width="20" viewBox="0 0 24 24">
                                                            <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                            <polyline points="14 2 14 8 20 8"></polyline>
                                                            <line y2="13" x2="8" y1="13" x1="16"></line>
                                                            <line y2="17" x2="8" y1="17" x1="16"></line>
                                                            <polyline points="10 9 9 9 8 9"></polyline>
                                                        </svg>
                                                        Excel
                                                        </div>
                                                        <div class="download">
                                                        <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                            stroke="currentColor" height="24" width="24" viewBox="0 0 24 24">
                                                            <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                            <polyline points="7 10 12 15 17 10"></polyline>
                                                            <line y2="3" x2="12" y1="15" x1="12"></line>
                                                        </svg>
                                                        </div>
                                                    </button>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                             </div>
<!--accordion end-->
                           </div>
                            <div class="tab-pane fade" id="e">
                                <div class="content_accordion">
                                    <div class="panel-group" id="ge">
                                    <div class="row">
                                        <div class="col-md-11">   
                                        <table class="table table-bordered" id="recruiterdata">
                                            <tr>
                                                <th>Recruiter Name</th><th>Submission Target</th><th>Total Submissions</th><th>Submissions %</th><th>Interviews</th><th>Rejections</th><th>Placements</th>
                                            </tr>
                                            <?php foreach($monthyears as $monthyear) {
                                                ?>
                                                <tr style="background-color: #FFA500;"><td colspan="7"><?php echo date("F Y", strtotime($monthyear)); ?></td></tr>
                                            <?php  
                                           if ($usertype != 'RECRUITER') {
                                            foreach($recruitersdata as $key => $recruiterdata) {
                                            //    var_dump($recruiterdata);
                                            $recruiters = $this->recruiter->getRecruitersByManagerID($managerId);
                                                if (is_array($recruiterdata)) {
                                                    foreach ($recruiterdata as $rec_data) {
                                                        $jobdate_yyyymm = date("Y-m", strtotime($rec_data->sdate));
                                                        if ($monthyear == $jobdate_yyyymm) { 
                                                            $recruiters = $this->recruiter->getRecruitersByManagerID($managerId);
                                                            // var_dump($recruite.rs);
                                                            foreach($recruiters as $key1 => $recruiter) {
                                                                if($rec_data->recruiterId == $recruiter->recruiter_id) {
                                                                    $targetResult	= $this->recruiter->getRecruiterTotalTarget($recruiter->recruiter_id, $fromdate, $todate, count($dates)); 
                                                                    $submissionsPercentage = $this->user->calculatePercentage($rec_data->submissioncount, $targetResult['targets']);
                                                                    $recruiter_name = $this->user->get_recruiter_from_id($recruiter->recruiter_id);?>
                                                                <tr>
                                                                    <td><?php echo $recruiter_name; ?></td><td><?php echo $targetResult['targets']; ?></td><td><?php echo $rec_data->submissioncount; ?></td><td><?php echo $submissionsPercentage; ?>%</td><td><?php echo $rec_data->interviews; ?></td><td><?php echo $rec_data->rejections; ?></td><td><?php echo $rec_data->placements; ?></td>
                                                                </tr>
                                                            <?php 
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $jobdate_yyyymm = date("Y-m", strtotime($recruiterdata->sdate)); 
                                                    if ($monthyear == $jobdate_yyyymm) { 
                                                        $recruiters = $this->recruiter->getRecruitersByManagerID($managerId);
                                                        // var_dump($recruite.rs);
                                                        foreach($recruiters as $key1 => $recruiter) {
                                                            if($recruiterdata->recruiterId == $recruiter->recruiter_id) {
                                                                //var_dump($recruiters);exit;
                                                            // $submissionsPercentage = $recruitersdata[$key]->submissioncount;
                                                                $targetResult	= $this->recruiter->getRecruiterTotalTarget($recruiter->recruiter_id, $fromdate, $todate, count($dates)); 
                                                                $submissionsPercentage = $this->user->calculatePercentage($recruiterdata->submissioncount, $targetResult['targets']);
                                                                $recruiter_name = $this->user->get_recruiter_from_id($recruiter->recruiter_id);?>
                                                                <tr>
                                                                    <td><?php echo $recruiter_name; ?></td><td><?php echo $targetResult['targets']; ?></td><td><?php echo $recruiterdata->submissioncount; ?></td><td><?php echo $submissionsPercentage; ?>%</td><td><?php echo $recruiterdata->interviews; ?></td><td><?php echo $recruiterdata->rejections; ?></td><td><?php echo $recruiterdata->placements; ?></td>
                                                                </tr> <?php 
                                                            } else {?>
                                                            <!-- <tr>
                                                                <td><?php //echo $recruiter->recruiter; ?></td><td>0</td><td></td>
                                                            </tr> -->
                                                            <?php
                                                            }
                                                        }
                                                    }
                                               }
                                            }
                                    } else {
                                        foreach($recruitersdata as $key => $recruiterdata) {
                                               
                                            //var_dump($recruiterdata);
                                                 $jobdate_yyyymm = date("Y-m", strtotime($recruiterdata->sdate)); 
                                                 if ($monthyear == $jobdate_yyyymm) { 
                                                                                                       
                                                 //$targetResult	= $this->recruiter->getRecruiterTotalTarget($recruiter->id, $fromdate, $todate, count($dates)); 
                                                 
                                                 $submissionsPercentage = $this->user->calculatePercentage($recruiterdata->submissioncount, $targets); 
                                                 
                                         ?>
                                         <tr>
                                             <td><?php echo $recruitername; ?></td><td><?php echo $targets; ?></td><td><?php echo $recruiterdata->submissioncount; ?></td><td><?php echo $submissionsPercentage; ?>%</td><td><?php echo $recruiterdata->interviews; ?></td><td><?php echo $recruiterdata->rejections; ?></td><td><?php echo $recruiterdata->placements; ?></td>
                                         </tr>
                                         <?php 
                                         
                                     
                                     }}
                                 }
                                    
                                        }
                                            
                                            ?>
                                        </table>
                                    </div>
                                    <?php if($usertype == 'SUPERADMIN' || $usertype == 'HR' || $usertype == 'MIS') {  ?>
                                        <!-- <div class="col-md-1">    
                                            <table><tr style="border-bottom:none"><td><a id="dlink"  style="display:none;"></a><input type="button" name="submit" value="Generate Excel" class="export" id="excel-button" onclick="tableToExcel('recruiterdata', 'RecruiterData', 'RecruiterData.xls')" /></td></tr>
                                            </table>
                                        </div> -->
                                        <div class="download-btn">
                                                    <button type="button" id="excel-button" value="Generate Excel" onclick="tableToExcel('recruiterdata', 'RecruiterData', 'RecruiterData.xls')" class="download-button export">
                                                        <div class="docs">
                                                        <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                            stroke="currentColor" height="20" width="20" viewBox="0 0 24 24">
                                                            <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                            <polyline points="14 2 14 8 20 8"></polyline>
                                                            <line y2="13" x2="8" y1="13" x1="16"></line>
                                                            <line y2="17" x2="8" y1="17" x1="16"></line>
                                                            <polyline points="10 9 9 9 8 9"></polyline>
                                                        </svg>
                                                        Excel
                                                        </div>
                                                        <div class="download">
                                                        <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2"
                                                            stroke="currentColor" height="24" width="24" viewBox="0 0 24 24">
                                                            <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                            <polyline points="7 10 12 15 17 10"></polyline>
                                                            <line y2="3" x2="12" y1="15" x1="12"></line>
                                                        </svg>
                                                        </div>
                                                    </button>
                                                </div>
                                    <?php } ?>
                                    </div>
                                    </div>
                             </div>
<!--accordion end-->
                           </div>
                    
                    </div>
                    
                </div>
                
                <!-- /.row -->
<script src="<?php echo base_url()?>js/Chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        
        var jsonObj = '{"Total_target":"<?php echo $targets ?>","submissions": "<?php echo $stats[0]->submissioncount ?>", "placements":"<?php echo $stats[0]->placements ?>", "submit_to_client":"<?php echo $stats[0]->submitted_to_client ?>", "interviews":"<?php echo $stats[0]->interviews ?>", "rejections":"<?php echo $stats[0]->rejections ?>"}';

        var jsonparse = JSON.parse(jsonObj);
        var jsonkey = [];
        var jsonvalue = [];
        for (var key in jsonparse) {
            jsonkey.push(key);
            jsonvalue.push(jsonparse[key]);
        }

        var chartdata = {
        labels: jsonkey,
        datasets : [
        {
            label: 'July 2022',
            backgroundColor: 'rgb(0,162,237)',
            borderColor: 'rgba(200, 200, 200, 0.75)',
            //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
            //hoverBorderColor: 'rgba(200, 200, 200, 1)',
            data: jsonvalue
        }
        ]
    };
        var ctx = $("#mycanvas");
        var barGraph = new Chart(ctx, {
            type: 'bar',
            data: chartdata
        });

        // var counts={},valuesArray=[];
        // $('#clientTracker tbody tr td:nth-child(1)').each( function(){
        // //add item to array
        // var colValue=$(this).text();
        // console.log(colValue);
        // if($.inArray(colValue,valuesArray)==-1){
        //     counts[colValue]=1;
        //     valuesArray.push(colValue);
            
        // }
        // else{
        //     var oldCount=counts[colValue];
        //     counts[colValue]= ++oldCount;
        // }
        
        // });
        // valuesArray=[];
        // // console.log(valuesArray);
        // $('#clientTracker tbody tr td:nth-child(1)').each( function(){
        //     var currentObj=$(this);
        //     // console.log(currentObj);
        //     if(currentObj.length>0){
        //     $('#clientTracker tbody tr td:nth-child(1)').each( function(){
        //         var colValue=$(this).text();
        //         if($.inArray(colValue,valuesArray)==-1){
        //         var rowspanValue=counts[colValue];
        //         console.log(rowspanValue);
        //         if( currentObj.attr("rowspan")==undefined) {
        //             currentObj.attr("rowspan",rowspanValue);
        //             currentObj.text(colValue);
        //             valuesArray.push(colValue);
        //         }
        //         }
        //     })
        //     }
        // });

    });
    var tableToExcel 	= (function(){
        var uri 	= 'data:application/vnd.ms-excel;base64,', 
        template 	= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>', 
        base64 		= function (s) { return window.btoa(unescape(encodeURIComponent(s))) }, 
        format 		= function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return 		  function (table, name, filename) {
	            if(!table.nodeType) table = document.getElementById(table)
	            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
	            document.getElementById("dlink").href 		= uri + base64(format(template, ctx));
	            document.getElementById("dlink").download 	= filename;
	            document.getElementById("dlink").click();
        }
})()
</script>

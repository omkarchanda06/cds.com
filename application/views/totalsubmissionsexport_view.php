<?php $this->load->view('includes/header'); 
	error_reporting(0);
?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/tcal.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery/tcal.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>js/jquery/jquery-1.4.1.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>js/jquery/jquery.validate.js"></script> 
<script>
$().ready(function() {
	// validate the comment form when it is submitted
	$("#exportForm").validate();
});
function PopupCenter(pageURL, title,w,h) {
var left = (screen.width/2)-(w/2);
var top = (screen.height/2)-(h/2);
var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}

function get_requiter(val){
	$.post("<?php echo base_url();?>index.php/recruitercount/requiterlist",{mid:val},function(result){
			//alert(result);
    	    $("#reqruiterid").html(result);
    	});	
}

var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

</script>

	<!--  start page-heading -->
	<div id="page-heading">
		<h1>Total Submissions
        <?php 
			$session_data = $this->session->userdata('logged_in');
	  		$usertype = $session_data['usertype']; 
			$username = $session_data['username']; 
			//if(($usertype == "ADMIN") || ($username == "Demo")){
		?>
        <form style="float:right; margin: 0px 20px 0px 0px;" id="exportForm" action="<?php echo base_url()?>index.php/totalsubmissionsreport/" onsubmit="return v.exec()" name="registration" method="POST"><table><tr><td>
        
        <select name="managerid" id="managerid" class="inp-form" onchange="get_requiter(this.value);" style="padding:5px; width:150px;"><option value="">Select Manager</option>
		<?php 
			foreach($manager as $mgr){
				$managerid = $mgr->id;
				$managername = $mgr->username;
				echo '<option value="'.$managerid.'">'.$managername.'</option>';
			}
		?></select></td><td>
        
        <select name="reqruiterid" id="reqruiterid" class="inp-form" style="padding:5px; width:150px;">
        <option value="All">Select Recruiter</option>		
		</select></td><td>
        
        <select name="status" class="inp-form" style="padding:5px; width:150px;">
        	<option value="All">Select Status</option>
        	<option value="Submitted to Vendor">Submitted to Vendor</option>
            <option value="Rejected">Rejected</option>
			<option value="Submitted to Client">Submitted to Client</option>
            <option value="Interview">Interview</option>
            <option value="Rejected after Interview">Rejected after Interview</option>
            <option value="Placement">Placement</option>
		</select></td>
		<td><input type="text" name="date1" id="date1" readonly class="inp-form tcal required" style="width:150px;" placeholder="Select a Date"/></td>
		<td><input type="text" name="date2" id="date2" readonly class="inp-form tcal required" style="width:150px;"  placeholder="Select a Date"/></td>
		<td>
		<input type="submit" name="submit" value="submit" class="form-submit" />
		<!--<input type="button" name="submit" value="Generate PDF" style="background-color:#407195; font:13px calibri; padding:5px 10px; color:#ecf2f7; border-radius:5px; border:none; cursor:pointer;" onclick="generatePDF()" />--> 
		<a id="dlink"  style="display:none;"></a>
		<input type="button" name="submit" value="Generate Excel" id="excel-button" class="export" onclick="tableToExcel('product-table', 'Manager Count Table', 'TotalSubmissionReport.xls')" /></td></tr></table></form><?php //} ?></h1>
        
        </h1>
	</div>
	<!-- end page-heading -->

	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="<?php echo base_url()?>images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="<?php echo base_url()?>images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
		
			<!--  start table-content  -->
			<div id="table-content">
			  <!-- start product-table ..................................................................................... -->
				
				<table border="0" width="98%" cellpadding="0" cellspacing="0" id="product-table" style="font:13px Cambria;">
				<tr>
					
					<th class="table-header-repeat line-left minwidth-1" align="center" width="100" bgcolor="#333333" style="color:#fff; height:50px;">Date</th>
					<th class="table-header-repeat line-left" width="200" align="center" width="105" bgcolor="#333333" style="color:#fff; height:50px;">Consultant</th>
					<th class="table-header-repeat line-left" align="center" width="200" bgcolor="#333333" style="color:#fff; height:50px;">For Position</th>
					<th class="table-header-repeat line-left" align="center" bgcolor="#333333" style="color:#fff; height:50px;">Total IT</th>
					<th class="table-header-options line-left" align="center" bgcolor="#333333" style="color:#fff; height:50px;">Loc</th>
					<th class="table-header-repeat line-left" width="200" align="center"  bgcolor="#333333" style="color:#fff; height:50px;">Manager</th>
					<th class="table-header-options line-left" align="center" width="200" bgcolor="#333333" style="color:#fff; height:50px;">Submitted by</th>
					<th class="table-header-options line-left" align="center" width="200" bgcolor="#333333" style="color:#fff; height:50px;">Visa</th>
					<th class="table-header-options line-left" align="center" width="200" bgcolor="#333333" style="color:#fff; height:50px;">Comments</th>
                    <th class="table-header-options line-left" align="center" width="200" bgcolor="#333333" style="color:#fff; height:50px;">Status</th>
				</tr>
				
				
				
				<?php
				$i=1;
				$date='';
				foreach($rows->result() as $res1){
				//print_r($rows->result());exit;
				if($i%2==0){
					$class="#ECECEC";
				}else{
					$class='';
				}
					if($date == ''){
						$date=$res1->sdate; 
					
					}
					
					if($date != $res1->sdate){
						$date=$res1->sdate; 
					echo '<tr><td colspan="8" class="gerua"></td></tr>';
					}
				?>
				<tr bgcolor='<?php echo $class;?>'>
					<td>
					<?php //echo $res1['userid']; ?>
                    <?php echo date("d",strtotime($res1->sdate)); ?>
					<?php echo date("M",strtotime($res1->sdate)); ?>
					<?php echo date("Y",strtotime($res1->sdate)); ?>
					</td>
					<td><?php echo $res1->cname; ?> </td>
					<td><?php echo $res1->psub; ?></a></td>
					<td><?php echo $res1->totalit; ?></td>
					<td><?php echo $res1->st; ?></td>
					<td><?php echo $res1->manager; ?></a></td>
					<td><?php echo $res1->recruiter; ?></a></td>
					<td><?php echo $this->visa->getVisaName($res1->visa); ?></a></td>
					<td>
						<a class="comment-section" data-id="<?=$res1->id?>" href="javascript:void(0);" 
						   onclick="PopupCenter('<?php echo base_url(); ?>index.php/totalsubmissions/comments/<?php echo $res1->id; ?>', 'myPop1',600,600);">Read / Add</a>
						<div class="comment-view-section" id="comment-view-section-<?=$res1->id?>">Loading...</div>
					</td>
                    <td><?php echo $res1->status; ?></a></td>
				</tr>
			
				<?php 
				$i++;
				} ?>
				
				</table>
				<!--  end product-table................................... --> 
			</div>
			<!--  end content-table  -->
				<!--  start paging..................................................... -->

			
			<div class="clear"></div>
		 
		</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<!--<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr> -->
	</table>
<?php $this->load->view('includes/footer'); ?>


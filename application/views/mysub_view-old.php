<?php $this->load->view('includes/header'); ?>
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery/jquery-1.8.3.js"></script> 
<script language="JavaScript" src="<?php echo base_url() ?>js/jquery/jquery.validate.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/tcal.css" />

<link rel="stylesheet" href="<?php echo base_url() ?>css/jquery-ui.css" />

<script src="<?php echo base_url() ?>js/jquery/jquery-ui.js"></script>
<script>

  $(function(){
	var pickerOpts = {
		dateFormat:"yy-mm-dd"
	};	
	$("#sdate").datepicker(pickerOpts);
	$("#mdate").datepicker(pickerOpts);
	$("#interview_date").datepicker(pickerOpts);
	$("#submissionForm").validate();
});
</script>
<div id="page-heading"><h1>Submission Edit Form</h1></div>
<?php
foreach($keys->result() as $result){
	
}
//print_r($sub_attachments);
?>

<form name="registration" id="submissionForm" method="post" onsubmit="return v.exec()" enctype="multipart/form-data">
<input type="hidden" name="sid" value="<?php echo $result->id;?>" />

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"><img src="<?php echo base_url()?>images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="<?php echo base_url()?>images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td >
		<!-- start id-form -->
		<table border="0"  width="100%"  cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
		<th valign="top">Select a date:</th>
		<td class="noheight">
		<?php if($this->session->userdata('logged_in')['id'] != 57) { ?>
		<input type="text" class="inp-form tcal required" value="<?php echo $result->sdate;?>" readonly disabled="disabled"/>
		<input type="hidden" name="sdate" id="sdate" value="<?php echo $result->sdate;?>"/>
		<?php } else { ?>
		<input type="text" class="inp-form tcal required" name="sdate" id="sdate" value="<?php echo $result->sdate;?>"/>	
		<?php }?>
		</td>
		<td></td>
	</tr>
		<tr>
			<th valign="top">Consultant Name :</th>
			<td><input type="text" name='cname' class="inp-form required" value="<?php echo $result->cname;?>" /></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Position Submitted to :</th>
			<td><input type="text" class="search inp-form required" name="psub"  value="<?php echo $result->psub;?>" /></td>
			<td>
			</td>
		</tr>
		<tr>
		<th valign="top">Total IT Exp:</th>
		<td>
			<select class="styledselect_form_1" name="totalit" required>
			<?php
			for($i=0;$i<41;$i++){
				if($i==$result->totalit){
					echo "<option selected value=".$i.">".$i." Years</option>";
				}else{
					echo "<option value=".$i.">".$i." Years</option>";
				}
			}		
			?>	
			</select>

</td>
		<td></td>
		</tr>
		<tr>
		<th valign="top">End Client :</th>
		<td><input type="text" class="inp-form required" name="company"  value="<?php echo $result->company;?>" /></td>
		<td></td>
		</tr> 
		<tr>
			<th valign="top">Location :</th>
			<td>	
			<select  name="location"  class="styledselect_form_1 required">
			<?php
					
			foreach($result1 as $res){
			//print_r('123');exit;
				if($res->lid == $result->locationid){
					echo '<option selected value='.$res->lid.'>'.$res->location.'</option>';
				}else{
					echo '<option value='.$res->lid.'>'.$res->location.'</option>';
				}
			}
			?>

			</select>
		</td>
			<td></td>
		</tr>
		
	<tr>
		<th>Submitted by :</th>
		<td>
			<!-- <select  name="submittedby" class="styledselect_form_1 required"> -->
			<?php
			// $recruiterName = '';
			// foreach($result2 as $res_r) {
			// 	if($result->submittedby == $res_r->id){
			// 		//echo '<option selected value='.$res_r->id.'>'.$res_r->recruiter.'</option>';
			// 		$recruiterName = $res_r->recruiter;
			// 	}
			// 	// else{
			// 	// 	echo '<option value='.$res_r->id.'>'.$res_r->recruiter.'</option>';
			// 	// }
			// }
			?>
		<!-- </select> -->
<input type="text" class="inp-form required" name="search-submittedby" id="search-submittedby" 
	   value="<?=$this->recruiter->getRecruiter($result->submittedby)?>" 
	   <?php if($this->session->userdata('logged_in')['id'] != 57) { ?>
	   disabled="disabled"
	   <?php } ?>
	   />
<input type="hidden" name="submittedby" id="submittedby" value="<?=$result->submittedby?>"/>
</td>

	</tr>
	<!-- <tr>
		<th>Vendor Company :</th>
		<td><input type="text" name="vcompany" class="inp-form required"  value="<?php echo $result->vcompany;?>" /></td>
	</tr>
	<tr>
		<th>Vendor Contact Name :</th>
		<td><input type="text" class="inp-form required" name="vcontact"  value="<?php echo $result->vcontact;?>" /></td>
	</tr>
	<tr>
		<th>Vendor Email :</th>
		<td><input type="text" class="inp-form required email" name="vemail"   value="<?php echo $result->vemail;?>" /></td>
	</tr>
	<tr> -->
		<th>Consultant Rate :</th>
		<td><input type="text" class="inp-form required" name="crate"  value="<?php echo $result->crate;?>" /></td>
	</tr>
	
	<tr>
		<th>Submission Rate :</th>
		 <td><input type="text" name="srate" class="inp-form required"  value="<?php echo $result->srate;?>" /></td>
	</tr>
	<tr>
		<th>Visa :</th>
		<td>
		<select  name="visa" class="styledselect_form_1" required>
			<?php
			foreach($result3 as $res3){
				if($res3->vid==$result->visa){
					echo '<option selected value='.$res3->vid.'>'.$res3->visatype.'</option>';
				}else{
					echo '<option value='.$res3->vid.'>'.$res3->visatype.'</option>';
				}
			}
			?>
			
		</select>
		</td>
	</tr>

	<tr>
		<th>Relocate :</th>
		<td>
		<select  name="relocate" class="styledselect_form_1 required">
		<?php $relocateOptions = array('YES', 'NO'); ?>
    	<option value="">Select</option>
    	<?php foreach($relocateOptions as $relocateOption): ?>
		<option value="<?=$relocateOption?>"
		<?php if($result->relocate == $relocateOption): ?>
		selected="selected"
		<?php endif; ?>
		><?=$relocateOption?></option>
		<?php endforeach; ?>
		</select>
		</td>
	</tr>

	<th>Employer Details :</th>
	<td><textarea type="text" name="employerdetails"  style="width: 308px; height: 70px;" required><?php echo $result->employerdetails;?></textarea></td>

		<tr>
		<th>Status :</th>
		 <td>
			<select name="status" id="status" class="styledselect_form_1 required">
            <?php 
            	$rejectedString = '';
            	foreach ($rejectedArray as $value) {
            			$rejectedString .= '<option value = "'.$value.'">'.$value.'</option>';
            	}

				if($result->status == 'Submitted to Vendor'){
					echo '<option value = "Submitted to Vendor">Submitted to Vendor</option>
						  <option value = "Submitted to Client">Submitted to Client</option>';
					echo $rejectedString;	  
						 	 
				}elseif(in_array($result->status, $rejectedArray)){
					echo '<option value = "'.$result->status.'">'.$result->status.'</option>';

				}elseif($result->status == 'Submitted to Client'){
					echo '<option value = "Submitted to Client">Submitted to Client</option>
						  <option value = "Interview">Interview</option>
						  <option value = "Rejected">Rejected</option>';

				}elseif($result->status == 'Interview'){
					echo '<option value = "Interview">Interview</option>
						  <option value = "Placement">Placement</option>
						  <option value = "Rejected After Interview">Rejected After Interview</option>
						  <option value = "No Feedback">No Feedback</option>
						  <option value = "Onhold">Onhold</option>';

				}elseif($result->status == 'Placement'){
					echo '<option value = "Placement">Placement</option>
						  <option value = "Backout">Backout</option>';

				}elseif($result->status == 'Rejected After Interview'){
					echo '<option value = "Rejected After Interview">Rejected After Interview</option>';
				}elseif($result->status == 'No Feedback'){
					echo '<option value = "No Feedback">No Feedback</option>';
				}elseif($result->status == 'Onhold'){
					echo '<option value = "Onhold">Onhold</option>';
				}elseif($result->status == 'Rejected'){
					echo '<option value = "Rejected">Rejected</option>';
				}
			?>
			<?php /*?><option <?php echo ($result->status == 'Submitted to Vendor') ? "Selected": "";?>  value="Submitted to Vendor">Submitted to Vendor</option>
            <option <?php echo ($result->status == 'Rejected') ? "Selected": "";?>  value="Rejected">Rejected</option>
			<option <?php echo ($result->status == 'Submitted to Client') ? "Selected": "";?> value="Submitted to Client">Submitted to Client</option>
            <option <?php echo ($result->status == 'Interview') ? "Selected": "";?>  value="Rejected">Interview</option>
            <option <?php echo ($result->status == 'Rejected after Interview') ? "Selected": "";?>  value="Rejected">Rejected after Interview</option>
            <option <?php echo ($result->status == 'Placement') ? "Selected": "";?>  value="Rejected">Placement</option><?php */?>
			<?php /*?><option <?php echo ($result->status == 'Interview in Progress') ? "Selected": "";?> value="Interview in Progress">Interview in Progress</option>
			<option <?php echo ($result->status == 'Waiting for Final Confirmation') ? "Selected": "";?> value="Waiting for Final Confirmation">Waiting for Final Confirmation</option>
			<option <?php echo ($result->status == 'Placed Should be starting soon') ? "Selected": "";?> value="Placed Should be starting soon">Placed Should be starting soon</option>
			<option <?php echo ($result->status == 'Blacklist him/her fake candidate') ? "Selected": "";?> value="Blacklist him/her fake candidate">Blacklist him/her fake candidate</option>
			<option <?php echo ($result->status == 'Didnt show up to start the project') ? "Selected": "";?> value="Didnt show up to start the project">Didn't show up to start the project</option>
			<option <?php echo ($result->status == 'Didnt took the interview not reliable') ? "Selected": "";?> value="Didnt took the interview not reliable">Didn't took the interview not reliable</option>
			<option <?php echo ($result->status == 'Dont work with him/her') ? "Selected": "";?> value="Dont work with him/her">Don't work with him/her</option>
			<option <?php echo ($result->status == 'Position Filled') ? "Selected": "";?> value="Position Filled">Position Filled</option>
			<option <?php echo ($result->status == 'Backout') ? "Selected": "";?> value="Backout">Backout</option><?php */?>
			</select>	 
		 </td>
	</tr>
	<tr>


	<tr class="interview-date-section">
		<th valign="top">Interview date:</th>
		<td class="noheight">
		<?php if($result->status == 'Submitted to Client'): ?>
		<input type="text" name="interview_date" id="interview_date" readonly class="inp-form tcal"/>
		<?php else :  ?>
		<input type="text"  name="interview_date" id="interview_date" class="inp-form tcal" 
				value="<?=$result->interview_date?>" />	
		<!-- <input type="text" class="inp-form tcal" value="<?=$result->interview_date?>" disabled="disabled" readonly/>	
		<input type="hidden" name="interview_date" id="interview_date" 
			   class="inp-form tcal" value="<?=$result->interview_date?>"/>	 -->
		<?php endif; ?>
		</td>
		<td></td>
	</tr>

	<!-- <tr>
		<th valign="top">Status updated date:</th>
		<td class="noheight">
		<input type="text" name="mdate" id="mdate" readonly class="inp-form tcal" value="<?php //echo $result->mdate;?>" required />
		</td>
		<td></td>
	</tr> -->
	<th>Resume Upload :</th>
	<td><input type="file" name="userfile" size="20" /></td>
	</tr>
		
	<tr>
	<th>Check the Resume :</th>
	<td><input type="hidden" name="file" value="<?php echo $sub_attachments->resume;?>"/> <a href="<?php echo base_url() ?><?php echo $sub_attachments->resume;?>">View Resume</a></td>
	<tr> 
	<th valign="top">Copy Paste Resume :</th>
	<td><textarea name="copypaste" style="width: 308px; height: 70px;" required><?php echo $sub_attachments->resumecopy;?></textarea></td>
	</tr>
	</tr>
	<tr>
		<th>&nbsp;</th>
		<td valign="top">
			<input type="submit" name="submit" value="submit" class="form-submit" />
			<input type="reset" value="" class="form-reset"  />
		</td>
		<td></td>
	</tr>
	</table>
	<!-- end id-form  -->

	</td>
</tr>
<tr>
<td><img src="<?php echo base_url() ?>images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
 
<div class="clear"></div>
 
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
</form>
<script>
// form fields description structure
// var a_fields = {

// 	'sdate':{'l':'Submission Date','r':true},
// 	'cname':{'l':'Consultant Name','r':true},
// 	'psub':{'l':'Position Submitted to','r': true},
// 	'totalit':{'l':'Total IT Experience','r':true},
// 	'company':{'l':'Company','r': true},
// 	'submittedby':{'l':'Street Address','r':true},
// 	'vcompany':{'l':'Vendor Company','r':true},
// 	'vcontact':{'l':'Vendor Contact','r':true},
// 	'vemail':{'l':'Vendor email','r':true},
// 	'crate':{'l':'Consultant Rate','r':true},
// 	'srate':{'l':'Submitted Rate','r':true},
// 	'employerdetails':{'l':'Employer Details','r':true}
	
	
// },

// o_config = {
// 	'to_disable' : ['Submit', 'Reset'],
// 	'alert' : 1
// }

// // validator constructor call
// var v = new validator('registration', a_fields, o_config);

</script>
<?php if($error !=''){ ?>
<script type="text/javascript">
$(document).ready(function(){
	alert( "<?php echo $error;?>");
});
</script>
<?php } ?>

<?php if($result->status != 'Interview' && $result->status != 'Placement' && $result->status != 'Rejected After Interview'): ?>
<script type="text/javascript">$('.interview-date-section').hide();</script>
<?php endif; ?>

<script type="text/javascript">
$(document).on('change', '#status', function(){
	var txt = $(this).val();
	if(txt == 'Interview' || txt == 'Placement' || txt == 'Rejected After Interview' || txt == 'Backout') {
		$('.interview-date-section').show();
		$('#interview_date').prop('required', 1);
	} else {
		$('.interview-date-section').hide();
		$('#interview_date').prop('required', 0);
	}
});
</script>


<?php $this->load->view('includes/footer'); ?>
	
<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url()?>css/profile.css" />
<link rel="stylesheet" href="<?php echo base_url()?>css/add-edit-user.css" />
    <?php //print_r($usertypes_data); ?>
    <!-- Start of body -->
    <div class="background">
      <div class="title-div">
        <p class="title"> <?php 
        $updatemessage = $this->session->flashdata('updatemessage');
        if(!empty($updatemessage)){
          echo $updatemessage;
        } else {
          echo "Add User Forms";
          } ?>
        </p>
      </div>
      <?php $attributes = array('name' => 'add-user-form', 'id' => 'add-user-form'); echo form_open('', $attributes); ?>
      <div class="input-container">
        <div class="input-div">
          <div class="input-name">User Name :</div>
          <input class="selectpicker" placeholder="User Name" name="uname" required/><span class="cds-forms-fields-error"><?php echo form_error('uname'); ?></span>
        </div>
        <div class="input-div">
          <div class="input-name">PT Number :</div>
          <input class="selectpicker" placeholder="PT Number" name="ptno" style="text-transform:uppercase" required/><span class="cds-forms-fields-error"><?php echo form_error('ptno'); ?></span>
        </div>
        <div class="input-div">
          <div class="input-name">Email :</div>
          <input class="selectpicker" placeholder="Email" name="email" required/><span class="cds-forms-fields-error"><?php echo form_error('email'); ?></span>
        </div>
        <div class="input-div">
          <div class="input-name">Status :</div>
          <select class="selectpicker" placeholder="Status" name="status" required>
              <option value="">Select</option>
              <option value="ACTIVE">Active</option>
							<option value="INACTIVE">Inactive</option>
          </select>
        </div>
        <div class="input-div">
          <div class="input-name">Password :</div>
          <input class="selectpicker" type="password" placeholder="Password" name="pwd" required/><span class="cds-forms-fields-error"><?php echo form_error('pwd'); ?></span>
        </div>
        <div class="input-div">
          <div class="input-name">Confirm Password :</div>
          <input class="selectpicker" type="password" placeholder="Confirm Password" name="cpwd" required/><span class="cds-forms-fields-error"><?php echo form_error('cpwd'); ?></span>
        </div>
        <div class="input-div">
          <div class="input-name">User Category :</div>
          <select class="selectpicker" placeholder="User Category" name="ucat" required>
              <option value="">Select</option>
              <option value="1">Panzer</option>
							<!-- <option value="2">RPO</option>
							<option value="3">Canada</option>																		 -->
							<option value="4">All (Super Admin)</option>
          </select>
        </div>
        <div class="input-div">
          <div class="input-name">User Department :</div>
          <select class="selectpicker" placeholder="User Category" name="udepartment" required>
              <option value="">Select</option>
              <option value="ITSTAFFING">IT Staffing</option>
							<option value="MARKETING">Marketing</option>
							<option value="SOFTWARE">Software</option>																		
							<option value="HR">Human Resource</option>
							<option value="ACCOUNTS">Accounts</option>
							<option value="NETWORK">Network</option>
							<option value="OFFICEADMINISTRATION">Office Administration</option>
          </select>
        </div>
        <div class="input-div">
          <div class="input-name">User Type :</div>
          <select class="selectpicker" name="utype" placeholder="User Type" >
            <option value="">Select</option>
            <?php foreach ($usertypes_data as $usertype) {
              // echo "<option value= $usertype > ".str_replace("'", "", $usertype)."</option>";
              if ($usertype->usertype !='' && $usertype->usertype != '0') {
                echo "<option value= $usertype->usertype> $usertype->usertype</option>";
              }
            } ?>
          </select>
        </div>
      </div>
      <div class="form-btn">
      <input type="hidden" name="adduser" value="adduser">
        <button class="cssbuttons-io-button">
          Submit
          <div class="icon">
            <svg
              height="24"
              width="24"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M0 0h24v24H0z" fill="none"></path>
              <path
                d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z"
                fill="currentColor"
              ></path>
            </svg>
          </div>
        </button>
        <!-- clear form btn -->
        <button class="button">
          <div class="trash">
            <div class="top">
              <div class="paper"></div>
            </div>
            <div class="box"></div>
            <div class="check">
              <svg viewBox="0 0 8 6">
                <polyline points="1 3.4 2.71428571 5 7 1"></polyline>
              </svg>
            </div>
          </div>
          <span>Clear Form</span>
        </button>
        <!-- js for btn -->
        <script>
          document.querySelectorAll(".button").forEach((button) =>
            button.addEventListener("click", (e) => {
              if (!button.classList.contains("delete")) {
                button.classList.add("delete");
                document.getElementById("add-user-form").reset();
                setTimeout(() => button.classList.remove("delete"), 3200);
              }
              e.preventDefault();
            })
          );
        </script>
      </div>
    </div>
  <!-- css for input fields -->
  <link rel="stylesheet" href="assets/js/test_js/1.css" />
  <link rel="stylesheet" href="assets/js/test_js/2.css" />
  <!-- script for input fields -->
  <script src="assets/js/test_js/1.js"></script>
  <script src="assets/js/test_js/2.js"></script>
  <script src="assets/js/test_js/3.js"></script>
  <?php $this->load->view('includes/footer'); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Dashboard</title>
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/index.css">
        <link rel="stylesheet" href="<?php echo base_url()?>css/profile.css" />
        <link rel="stylesheet" href="<?php echo base_url()?>css/add-edit-user.css" />
        <script src="<?php echo base_url()?>assets/js/index.js"></script>
        <script src="<?php echo base_url()?>assets/js/libs/jquery.min.js"></script>
        <script src="<?php echo base_url()?>js/jquery_popup.js"></script>
    </head>
    <body>
        <div class="app-container">
            <div class="app-left">
                <button class="close-menu">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"/><line x1="6" y1="6" x2="18" y2="18"/></svg>
                </button>
                <div class="app-logo">
                    <span><img class="logo" src="<?php echo base_url()?>assets/images/logo.png" alt="Panzer"></span>
                </div>
                 
                <ul class="nav-list">
                    <li class="search-box">
                        <input class="search" type="text" placeholder="&#128269;Search...">
                    </li>
                    <li class="nav-list-item active">
                        <a class="nav-list-link" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-columns"><path d="M12 3h7a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2h-7m0-18H5a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h7m0-18v18"/></svg>
                            Dashboard
                        </a>
                    </li>
                    <li class="nav-list-item">
                        <a class="nav-list-link" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-briefcase"><rect x="2" y="7" width="20" height="14" rx="2" ry="2"/><path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"/></svg>
                            Projects
                        </a>
                    </li>
                    <li class="nav-list-item">
                        <a class="nav-list-link" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"/><polyline points="13 2 13 9 20 9"/></svg>
                            To Do List
                        </a>
                    </li>
                    <li class="nav-list-item">
                        <a class="nav-list-link" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"/><circle cx="9" cy="7" r="4"/><path d="M23 21v-2a4 4 0 0 0-3-3.87"/><path d="M16 3.13a4 4 0 0 1 0 7.75"/></svg>
                            Team
                        </a>
                    </li>

                    <li class="nav-list-item">
                        <a class="nav-list-link" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-pie-chart"><path d="M21.21 15.89A10 10 0 1 1 8 2.83"/><path d="M22 12A10 10 0 0 0 12 2v10z"/></svg>
                            Settings
                        </a>
                    </li>
                </ul>
                <!-- Add progress bars -->

                <?php
            $session_data = $this->session->userdata('logged_in');
            $usertype = $session_data['username'];
            $first_date_of_last_month = date("Y-m-d", mktime(0, 0, 0, date("m")-1, 1));
            $last_date_of_last_month = date("Y-m-d", mktime(0, 0, 0, date("m"), 0));

            // echo "first and last day of Month<br/>";
            $dt = date('Y-m-d');
            $first_date_of_month = date("Y-m-01", strtotime($dt));
            $last_date_of_month = date("Y-m-t", strtotime($dt));
            /*echo 'First day : '. $first_date_of_month.' - Last day : '. $last_date_of_month."<br/>";*/

            $first_date_of_month_timestamp = strtotime($first_date_of_month);
            $first_day_of_month = date('D', $first_date_of_month_timestamp);

            /*echo "<br/><br/>first and last day of last week<br/>";*/
            $last_week_start_dt = date("Y-m-d",strtotime("last sunday",strtotime("-1 week")));
            $last_week_end_dt = date("Y-m-d",strtotime("saturday",strtotime("-1 week")));

            $the_day_of_week = date("w",strtotime($dt)); //sunday is 0

            $first_date_of_week = date("Y-m-d",strtotime( $dt )-60*60*24*($the_day_of_week)+60*60*24*1 );
            $last_date_of_week = date("Y-m-d",strtotime($first_date_of_week)+60*60*24*4 );

            function find_the_first_monday_of_month($date) {
                $time = strtotime($date);
                $year = date('Y', $time);
                $month = date('m', $time);

                for($day = 1; $day <= 31; $day++) {
                    $time = mktime(0, 0, 0, $month, $day, $year);
                    if (date('N', $time) == 1) {
                        return date('Y-m-d', $time);
                    }
                }
            }

            $session_data 	= $this->session->userdata('logged_in');
            //print_r($session_data);
            $usertype 		= $session_data['usertype'];
            $recruiterArray = array();
            $totalDates					= 0;
            $totalTargets				= 0;
            $submission_percent_arr = [];
            $rejections_percent_arr = [];
            $placements_percent_arr = [];
            $nofeedbacks_percent_arr = [];
            $onholds_percent_arr = [];
            $interviews_percent_arr = [];
            ?>

            <div class="bottom-content">
                <!-- Dropdown -->
                <div class="dropdown">
                    <select name="time_period" id="time_period" onchange="get_time_period_options(this)">
                        <option selected="selected" class="time_period_options" value="current_week" first-date="<?php echo $first_date_of_week;?>" last-date="<?php echo $last_date_of_week;?>">Current Week</option>
                        <option class="time_period_options" value="last_week" first-date="<?php echo $last_week_start_dt;?>" last-date="<?php echo $last_week_end_dt;?>">Last Week</option>
                        <option class="time_period_options" value="current_month" first-date="<?php echo $first_date_of_month;?>" last-date="<?php echo $last_date_of_month;?>">Current Month</option>
                        <option class="time_period_options" first-date="<?php echo $first_date_of_last_month;?>" last-date="<?php echo $last_date_of_last_month;?>">Last Month</option>
                    </select>

                </div>
                <?php 
                foreach($dates as $d): $day1=date("D", strtotime($d)); $day2=date("d M", strtotime($d)); $totalDates++;
                endforeach;
                if ($usertype == "SUPERADMIN") {
                    //print_r($Dirs_Assodir_Mgrs);exit;
                    foreach($Dirs_Assodir_Mgrs as $manager):
                        //print_r($manager->id);
                        $recruiters = $this->recruiter->getRecruitersByManagerID($manager->id);
                        foreach($recruiters as $recruiter):
                            //print_r($recruiter->recruiter_id);
                            $recruiterArray[$recruiter->recruiter_id] = $this->recruiter->getRecruiter($recruiter->recruiter_id);
                            $targetResult	= $this->recruiter->getRecruiterTotalTarget($recruiter->recruiter_id, $first_date_of_week, $last_date_of_week, $totalDates); 
                            $targets 		= $targetResult['sumtarget']; 
                            $targets 		= ($targets)? ($targets/5)*$totalDates : 0;
                            $totalTargets = $totalTargets + $targets;
                            /**Recruiter Submissions by Date **/
                            $submissionsOfAllDates = 0; 
                            foreach($dates as $d):
                                if($this->recruiter->getRecruiterAttendance($recruiterArray[$recruiter->recruiter_id], $d) != ''):
                                else:
                                ($subcount[$recruiter->recruiter_id][$d]['submissioncount'] ? $subcount[$recruiter->recruiter_id][$d]['submissioncount'] : 0 );
                                endif; 
                                $submissionsbyDate[$d] 		= $submissionsbyDate[$d] + $subcount[$recruiter->recruiter_id][$d]['submissioncount']; 
                                $submissionsOfAllDates		= $submissionsOfAllDates + $subcount[$recruiter->recruiter_id][$d]['submissioncount'];
                            endforeach;
                            
                            /*onholds*/
                            if(!isset($subcount[$recruiter->recruiter_id]['onholds'])) $subcount[$recruiter->recruiter_id]['onholds'] = 0;
                            array_push($onholds_percent_arr,$subcount[$recruiter->recruiter_id]['onholds']);
                            
                            /*No feedbacks*/
                            if(!isset($subcount[$recruiter->recruiter_id]['nofeedbacks'])) $subcount[$recruiter->recruiter_id]['nofeedbacks'] = 0;
                            array_push($nofeedbacks_percent_arr,$subcount[$recruiter->recruiter_id]['nofeedbacks']);

                            /*Interviews*/
                            if(!isset($subcount[$recruiter->recruiter_id]['interviews'])) $subcount[$recruiter->recruiter_id]['interviews'] = 0;
                            array_push($interviews_percent_arr,$subcount[$recruiter->recruiter_id]['interviews']);
                            
                            /*Rejection*/
                            if(!isset($subcount[$recruiter->recruiter_id]['rejections'])) $subcount[$recruiter->recruiter_id]['rejections'] = 0;
                            array_push($rejections_percent_arr,$subcount[$recruiter->recruiter_id]['rejections']);
                            
                            /*Placement*/
                            if(!isset($subcount[$recruiter->irecruiter_id]['placements'])) $subcount[$recruiter->recruiter_id]['placements'] = 0;
                            array_push($placements_percent_arr,$subcount[$recruiter->recruiter_id]['placements']);
                            
                            /*submissions */
                            //$submissionsPercentage = $this->user->calculatePercentage($submissionsOfAllDates, $targetResult['targets']);
                            array_push($submission_percent_arr,$submissionsOfAllDates);
                        
                        endforeach;
                    endforeach;
                } elseif($usertype == "DIRECTOR"){
                    foreach ($manager_ids as $managerID) {
                        $submissions     = $this->submission->getAllSubmissionsOfRecruitersByManagerID($first_date_of_week, $last_date_of_week, $managerID->manager_id);
                        $subcount     = $this->submission->createSubmissionsInfoArrayForRecruiter($submissions);
                        $recruiters = $this->recruiter->getRecruitersByManagerID($managerID->manager_id);
                        //print_r($subcount);
                        foreach($recruiters as $recruiter): 
                            $recruiterArray[$recruiter->recruiter_id] = $this->recruiter->getRecruiter($recruiter->recruiter_id);
                            $targetResult	= $this->recruiter->getRecruiterTotalTarget($recruiter->recruiter_id, $first_date_of_week, $last_date_of_week, $totalDates); 
                            $targets 		= $targetResult['sumtarget']; 
                            $targets 		= ($targets)? ($targets/5)*$totalDates : 0;
                            $totalTargets = $totalTargets + $targets;
                            /**Recruiter Submissions by Date **/
                            $submissionsOfAllDates = 0;
                            //echo $recruiterArray[$recruiter->recruiter_id]." , ";
                            foreach($dates as $d):
                                //echo $recruiter->recruiter_id." ".$recruiterArray[$recruiter->recruiter_id];
                                if($this->recruiter->getRecruiterAttendance($recruiterArray[$recruiter->recruiter_id], $d) != ''):
                                else:
                                ($subcount[$recruiter->recruiter_id][$d]['submissioncount'] ? $subcount[$recruiter->recruiter_id][$d]['submissioncount'] : 0 );
                                endif; 
                                $submissionsbyDate[$d] 		= $submissionsbyDate[$d] + $subcount[$recruiter->recruiter_id][$d]['submissioncount']; 
                                $submissionsOfAllDates		= $submissionsOfAllDates + $subcount[$recruiter->recruiter_id][$d]['submissioncount'];
                            endforeach;
                            
                            /*onholds*/
                            if(!isset($subcount[$recruiter->recruiter_id]['onholds'])) $subcount[$recruiter->recruiter_id]['onholds'] = 0;
                            array_push($onholds_percent_arr,$subcount[$recruiter->recruiter_id]['onholds']);
                            
                            /*No feedbacks*/
                            if(!isset($subcount[$recruiter->recruiter_id]['nofeedbacks'])) $subcount[$recruiter->recruiter_id]['nofeedbacks'] = 0;
                            array_push($nofeedbacks_percent_arr,$subcount[$recruiter->recruiter_id]['nofeedbacks']);

                            /*Interviews*/
                            if(!isset($subcount[$recruiter->recruiter_id]['interviews'])) $subcount[$recruiter->recruiter_id]['interviews'] = 0;
                            array_push($interviews_percent_arr,$subcount[$recruiter->recruiter_id]['interviews']);
                            
                            /*Rejection*/
                            if(!isset($subcount[$recruiter->recruiter_id]['rejections'])) $subcount[$recruiter->recruiter_id]['rejections'] = 0;
                            array_push($rejections_percent_arr,$subcount[$recruiter->recruiter_id]['rejections']);
                            
                            /*Placement*/
                            if(!isset($subcount[$recruiter->recruiter_id]['placements'])) $subcount[$recruiter->recruiter_id]['placements'] = 0;
                            array_push($placements_percent_arr,$subcount[$recruiter->recruiter_id]['placements']);
                            
                            /*submissions */
                            //$submissionsPercentage = $this->user->calculatePercentage($submissionsOfAllDates, $targetResult['targets']);
                            array_push($submission_percent_arr,$submissionsOfAllDates);
                        
                        endforeach;
                    }
                    //echo $interviews_percent_count = number_format(array_sum($interviews_percent_arr),2);
                } elseif($usertype == "ASSOCIATEDIRECTOR"){
                    $recruiters = $this->recruiter->getRecruitersByManagerID($asscoiate_dirId);
                    // print_r($recruiters);exit;
                    foreach($recruiters as $recruiter): 
                        $recruiterArray[$recruiter->recruiter_id] = $recruiter->recruiter_id;
                        $targetResult	= $this->recruiter->getRecruiterTotalTarget($recruiter->recruiter_id, $first_date_of_week, $last_date_of_week, $totalDates); 
                        $targets 		= $targetResult['sumtarget']; 
                        $targets 		= ($targets)? ($targets/5)*$totalDates : 0;
                        $totalTargets = $totalTargets + $targets;
                        /**Recruiter Submissions by Date **/
                        $submissionsOfAllDates = 0; 
                        foreach($dates as $d):

                        if($this->recruiter->getRecruiterAttendance($recruiterArray[$recruiter->recruiter_id], $d) != ''):
                        else:
                        ($subcount[$recruiter->recruiter_id][$d]['submissioncount'] ? $subcount[$recruiter->recruiter_id][$d]['submissioncount'] : 0 );
                        endif; 
                        $submissionsbyDate[$d] 		= $submissionsbyDate[$d] + $subcount[$recruiter->recruiter_id][$d]['submissioncount']; 
                        $submissionsOfAllDates		= $submissionsOfAllDates + $subcount[$recruiter->recruiter_id][$d]['submissioncount'];
                        endforeach;

                        /*onholds*/
                        if(!isset($subcount[$recruiter->recruiter_id]['onholds'])) $subcount[$recruiter->recruiter_id]['onholds'] = 0;
                        array_push($onholds_percent_arr,$subcount[$recruiter->recruiter_id]['onholds']);

                        /*No feedbacks*/
                        if(!isset($subcount[$recruiter->recruiter_id]['nofeedbacks'])) $subcount[$recruiter->recruiter_id]['nofeedbacks'] = 0;
                        array_push($nofeedbacks_percent_arr,$subcount[$recruiter->recruiter_id]['nofeedbacks']);

                        /*Interviews*/
                        if(!isset($subcount[$recruiter->recruiter_id]['interviews'])) $subcount[$recruiter->recruiter_id]['interviews'] = 0;
                        array_push($interviews_percent_arr,$subcount[$recruiter->recruiter_id]['interviews']);

                        /*Rejection*/
                        if(!isset($subcount[$recruiter->recruiter_id]['rejections'])) $subcount[$recruiter->recruiter_id]['rejections'] = 0;
                        array_push($rejections_percent_arr,$subcount[$recruiter->recruiter_id]['rejections']);

                        /*Placement*/
                        if(!isset($subcount[$recruiter->recruiter_id]['placements'])) $subcount[$recruiter->recruiter_id]['placements'] = 0;
                        array_push($placements_percent_arr,$subcount[$recruiter->recruiter_id]['placements']);

                        /*submissions */
                        //$submissionsPercentage = $this->user->calculatePercentage($submissionsOfAllDates, $targetResult['targets']);
                        array_push($submission_percent_arr,$submissionsOfAllDates);

                    endforeach;
                } elseif($usertype == "MANAGER"){
                    $recruiters = $this->recruiter->getRecruitersByManagerID($managerId);
                    foreach($recruiters as $recruiter): 
                        $recruiterArray[$recruiter->recruiter_id] = $recruiter->recruiter_id;
                        $targetResult	= $this->recruiter->getRecruiterTotalTarget($recruiter->recruiter_id, $first_date_of_week, $last_date_of_week, $totalDates); 
                        $targets 		= $targetResult['sumtarget']; 
                        $targets 		= ($targets)? ($targets/5)*$totalDates : 0;
                        $totalTargets = $totalTargets + $targets;
                        /**Recruiter Submissions by Date **/
                        $submissionsOfAllDates = 0; 
                        foreach($dates as $d):

                        if($this->recruiter->getRecruiterAttendance($recruiterArray[$recruiter->recruiter_id], $d) != ''):
                        else:
                        ($subcount[$recruiter->recruiter_id][$d]['submissioncount'] ? $subcount[$recruiter->recruiter_id][$d]['submissioncount'] : 0 );
                        endif; 
                        $submissionsbyDate[$d] 		= $submissionsbyDate[$d] + $subcount[$recruiter->recruiter_id][$d]['submissioncount']; 
                        $submissionsOfAllDates		= $submissionsOfAllDates + $subcount[$recruiter->recruiter_id][$d]['submissioncount'];
                        endforeach;

                        /*onholds*/
                        if(!isset($subcount[$recruiter->recruiter_id]['onholds'])) $subcount[$recruiter->recruiter_id]['onholds'] = 0;
                        array_push($onholds_percent_arr,$subcount[$recruiter->recruiter_id]['onholds']);

                        /*No feedbacks*/
                        if(!isset($subcount[$recruiter->recruiter_id]['nofeedbacks'])) $subcount[$recruiter->recruiter_id]['nofeedbacks'] = 0;
                        array_push($nofeedbacks_percent_arr,$subcount[$recruiter->recruiter_id]['nofeedbacks']);

                        /*Interviews*/
                        if(!isset($subcount[$recruiter->recruiter_id]['interviews'])) $subcount[$recruiter->recruiter_id]['interviews'] = 0;
                        array_push($interviews_percent_arr,$subcount[$recruiter->recruiter_id]['interviews']);

                        /*Rejection*/
                        if(!isset($subcount[$recruiter->recruiter_id]['rejections'])) $subcount[$recruiter->recruiter_id]['rejections'] = 0;
                        array_push($rejections_percent_arr,$subcount[$recruiter->recruiter_id]['rejections']);

                        /*Placement*/
                        if(!isset($subcount[$recruiter->recruiter_id]['placements'])) $subcount[$recruiter->recruiter_id]['placements'] = 0;
                        array_push($placements_percent_arr,$subcount[$recruiter->recruiter_id]['placements']);

                        /*submissions */
                        //$submissionsPercentage = $this->user->calculatePercentage($submissionsOfAllDates, $targetResult['targets']);
                        array_push($submission_percent_arr,$submissionsOfAllDates);

                    endforeach;
                }else{
                    // echo $usertype;
                    //print_r($subcount_details);exit();
                    foreach($subcount as $subcount_details){
                        $targetResult	= $this->recruiter->getRecruiterTotalTarget($recruiter_id, $first_date_of_week, $last_date_of_week, $totalDates);
                        $targets 		= $targetResult['sumtarget']; 
                        $targets 		= ($targets)? ($targets/5)*$totalDates : 0;
                        $totalTargets = $totalTargets + $targets;
                        $submissionsOfAllDates = 0;
                        $recruiter_name = $session_data['username'];
                        foreach($dates as $d){
                        //echo $subcount_details[$d]['submissioncount'];  
                        if($this->recruiter->getRecruiterAttendance($recruiter_name, $d) != ''):
                        else:
                        ($subcount_details[$d]['submissioncount'] ? $subcount_details[$d]['submissioncount'] : 0 );
                        endif; 
                        $submissionsbyDate[$d] 		= $submissionsbyDate[$d] + $subcount_details[$d]['submissioncount']; 
                        $submissionsOfAllDates		= $submissionsOfAllDates + $subcount_details[$d]['submissioncount'];
                        }
                        /*onholds*/
                        if(!isset($subcount_details['onholds'])) $subcount_details['onholds'] = 0;
                        array_push($onholds_percent_arr,$subcount_details['onholds']);

                        /*No feedbacks*/
                        if(!isset($subcount_details['nofeedbacks'])) $subcount_details['nofeedbacks'] = 0;
                        array_push($nofeedbacks_percent_arr,$subcount_details['nofeedbacks']);

                        /*Interviews*/
                        if(!isset($subcount_details['interviews'])) $subcount_details['interviews'] = 0;
                        array_push($interviews_percent_arr,$subcount_details['interviews']);

                        /*Rejection*/
                        if(!isset($subcount_details['rejections'])) $subcount_details['rejections'] = 0;
                        array_push($rejections_percent_arr,$subcount_details['rejections']);

                        /*Placement*/
                        if(!isset($subcount_details['placements'])) $subcount_details['placements'] = 0;
                        array_push($placements_percent_arr,$subcount_details['placements']);

                        /*submissions */
                        //$submissionsPercentage = $this->user->calculatePercentage($submissionsOfAllDates, $targetResult['targets']);
                        array_push($submission_percent_arr,$submissionsOfAllDates);
                    }
                }
                $total_subm_percentage = array_sum($submission_percent_arr)/$totalTargets*100;
                if (is_nan($total_subm_percentage)) {
                    $total_subm_percentage = number_format(0,2);
                }else{
                    $total_subm_percentage = number_format($total_subm_percentage,2);
                }
                if (is_null(array_sum($rejections_percent_arr)) && is_null(array_sum($placements_percent_arr)) && is_null(array_sum($nofeedbacks_percent_arr)) && is_null(array_sum($onholds_percent_arr)) && is_null(array_sum($interviews_percent_arr))) {
                    $rejections_percent_count = 0;
                    $placement_percent_count = 0;
                    $nofeedbacks_percent_count = 0;
                    $onholds_percent_count = 0;
                    $interviews_percent_count = 0;
                } else {
                    $rejections_percent_count = number_format(array_sum($rejections_percent_arr),2);
                    $placement_percent_count = number_format(array_sum($placements_percent_arr),2);
                    $nofeedbacks_percent_count = number_format(array_sum($nofeedbacks_percent_arr),2);
                    $onholds_percent_count = number_format(array_sum($onholds_percent_arr),2);
                    $interviews_percent_count = number_format(array_sum($interviews_percent_arr),2);
                }

                // $total_subm_percentage = array_sum($submission_percent_arr)/$totalTargets*100;
                // $total_subm_percentage = number_format($total_subm_percentage,2);
                // $rejections_percent_count = number_format(array_sum($rejections_percent_arr),2);
                // $placement_percent_count = number_format(array_sum($placements_percent_arr),2);
                // $nofeedbacks_percent_count = number_format(array_sum($nofeedbacks_percent_arr),2);
                // $onholds_percent_count = number_format(array_sum($onholds_percent_arr),2);
                //print_r($subcount);?>
                <!-- Submissions -->
                <div class="dashboard-status-loader" style="display: none;">
                <img class="preloader-img" src = "<?php echo base_url()?>assets/images/preloader.gif" style="width: 100px; display: table; margin: auto; padding-top: 2.5em">
                </div>
                <div class="status-result">
                    <div class="submissions_div">
                        <div class="progress_title" ><!--<span class="title_color"></span>-->Submissions</div>
                        <progress id="total-sub-progress" value="<?php echo $total_subm_percentage; ?>" max="100"></progress>
                        <span class="pregresspercent" id="total-submission-value"><?php echo $total_subm_percentage ?> %</span>   
                    </div>          
                    <!-- No feedback -->
                    <div class="nofeedback_div">
                        <div class="progress_title">No Feedback</div>
                        <progress id="total-nofeedbacks-progress" value="<?php echo $nofeedbacks_percent_count; ?>" max="100"></progress>
                        <span class="pregresspercent" id="total-nofeedbacks-value"><?php echo $nofeedbacks_percent_count ?> %</span>   
                    </div>
                    <!-- Interviews -->
                    <div class="nofeedback_div">
                        <div class="progress_title">Interviews</div>
                        <progress id="total-interviews-progress" value="<?php echo $interviews_percent_count; ?>" max="100"></progress>
                        <span class="pregresspercent" id="total-interviews-value"><?php echo $interviews_percent_count ?> %</span>   
                    </div>
                    <!-- Rejected -->
                    <div class="rejected_div">
                        <div class="progress_title">Rejected</div>
                        <progress id="total-rejections-progress" value="<?php echo $rejections_percent_count; ?>" max="100"></progress>
                        <span class="pregresspercent" id="total-rejections-value"><?php echo $rejections_percent_count ?> %</span>   
                    </div>
                    <!-- On Hold -->
                    <div class="onhold_div">
                        <div class="progress_title">On Hold</div>
                        <progress id="total-onholds-progress" value="<?php echo $onholds_percent_count; ?>" max="100"></progress>
                        <span class="pregresspercent" id="total-onholds-value"><?php echo $onholds_percent_count ?> %</span>   
                    </div>
                    <!-- Placed -->
                    <div class="placed_div">
                        <div class="progress_title">Placed</div>
                        <progress id="total-placements-progress" value="<?php echo $placement_percent_count; ?>" max="100"></progress>
                        <span class="pregresspercent" id="total-placements-value"><?php echo $placement_percent_count ?> %</span>
                    </div>
                </div>
            </div>
        </div>
            </div>
            <div class="app-main">
                <div class="main-header-line">
                    <!-- <div class="action-buttons">
                        <button class="open-right-area">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-activity"><polyline points="22 12 18 12 15 21 9 3 6 12 2 12"/></svg>
                        </button>
                        <button class="menu-button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"/><line x1="3" y1="6" x2="21" y2="6"/><line x1="3" y1="18" x2="21" y2="18"/></svg>
                        </button>
                    </div> -->
                </div>
                <div class="chart-row three">
                    <div class="chart-container-wrapper">
                        <div class="chart-container">
                            <div class="chart-info-wrapper">
                                <div class="st">
                                    <div id="st">IST:</div>
                                    <div id="IST"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chart-container-wrapper">
                        <div class="chart-container">
                            <div class="chart-info-wrapper">
                                <div class="st">
                                    <div id="st">EST:</div>
                                    <div id="EST"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chart-container-wrapper">
                        <div class="chart-container">
                            <div class="chart-info-wrapper">
                                <div class="st">
                                    <div id="st">CST:</div>
                                    <div id="CST"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chart-container-wrapper">
                        <div class="chart-container">
                            <div class="st">
                                <div id="st">PST:</div>
                                <div id="PST"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="chart-row two">
                    <div class="chart-container-wrapper big">
                        <div class="chart-container">
                            <div class="chart-container-header">
                                <h2 class="gm">Good Morning!</h2>
                                <h2 class="gm-text">Let's Organize your Daily Task.</h2>
                            </div>
                            <div class="main_content">
                                 <a href="<?php echo base_url('index.php/resumedatabase'); ?>" target="_blank">
                                    <div class="content_div">
                                        <div class="content_icon"></div>
                                        <div class="content_text">Resume Database</div>
                                    </div>
                                </a>
                                <a href="employeebd.html" target="_blank">
                                    <div class="content_div">
                                        <div class="content_icon"></div>
                                        <div class="content_text">Employee Database</div>    
                                    </div>
                                </a>
                            </div>
                            <div class="main_content">
                                <?php 
                                $hide_job_posting_users = array("HR", "MIS", "RECRUITER", "ACCOUNTSEXECUTIVE");
                                if (!    in_array($usertype, $hide_job_posting_users)) {?>
                                <a href="<?php echo base_url('index.php/requirements/add_requirement'); ?>" target="_blank">
                                    <div class="content_div">
                                        <div class="content_icon"></div>
                                        <div class="content_text">Job Posting</div>
                                    </div>
                                </a><?php } ?>
                                <a href="<?php echo base_url('index.php/Reports'); ?>" target="_blank">
                                    <div class="content_div">
                                        <div class="content_icon"></div>
                                        <div class="content_text">Reports</div>    
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="app-right">
                <button class="close-right">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"/><line x1="6" y1="6" x2="18" y2="18"/></svg>
                </button>
                <div class="navigation">
                    <div class="user-box">
                        <div class="image-box">
                            <img src="<?php echo base_url()?>assets/images/profile.png" alt="avatar" />
                        </div>
                        <p class="username"><?php echo $session_data['username']; ?></p>
                    </div>
                    <div class="menu-toggle"></div>
                    <ul class="menu">
                        <li>
                            <a href="#">Profile</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>index.php/dashboard/logout">Logout</a>
                        </li>
                    </ul>
                </div>
                <div class="profile-box" style="position: unset;">
                    <div class="profile-photo-wrapper">
                        <img src="<?php echo base_url()?>assets/images/rewards_img_1.jpg" alt="Awards">
                    </div>
                </div>
                <div class="app-right-content">
                    <div class="app-right-section">
                        <div class="notifications-div">
                            <div class="notifications-title">Notifications</div>
                            <div class="notifications-list">
                                <ul class="notifications">
                                    <?php

                                    switch ($usertype) {
                                        case 'SUPERADMIN':
                                            // echo "<b>Director's interviews</b>: <br/>";
                                            $i = 0;
                                            foreach ($Dirs_Assodir_Mgrs as $users) {
                                                $interviews = $this->submission->getInterviewListofManager($first_date_of_week, $last_date_of_week, $users->id);
                                                if (!empty($interviews)) {
                                                    foreach ($interviews as $intv_data) {
                                                        if (date("Y-m-d") == $intv_data->interview_date) {
                                                            $i = 1;
                                                            $time = $intv_data->interview_time;
                                                            $mode_of_interview = $intv_data->mode_of_interview;
                                                            $time_zone = $intv_data->time_zone;
                                                            $candidate_name = $intv_data->cname;
                                                            $username = $this->user->get_manager_from_id($intv_data->userid);
                                                            echo "<li><b>$username's </b>candidate <b>$candidate_name</b> has $mode_of_interview interview today at $time $time_zone.</li>";
                                                        }
                                                    }
                                                }
                                            }
                                            if ($i == 0) {
                                                echo "<li>Don't have any interviews today for any candidate's.</li>";
                                            }
                                            // $j = 0;
                                            // echo "<br/><b>Manager's interviews</b>: <br/>";
                                            // foreach ($managers as $manager) {
                                            //     $interviews = $this->submission->getInterviewListofManager($first_date_of_week, $last_date_of_week, $manager->id);
                                            //     if (!empty($interviews)) {
                                            //         foreach ($interviews as $intv_data) {
                                            //             // echo $intv_data->userid;
                                            //             if (date("Y-m-d") == $intv_data->interview_date) {
                                            //                 $j = 1;
                                            //                 $time = $intv_data->interview_time;
                                            //                 $mode_of_interview = $intv_data->mode_of_interview;
                                            //                 $time_zone = $intv_data->time_zone;
                                            //                 $candidate_name = $intv_data->cname;
                                            //                 // echo $intv_data->userid;
                                            //                 $manager_name = $this->user->get_manager_from_id($intv_data->userid);
                                            //                 echo "<li><b>$manager_name: </b>Candidate <b>$candidate_name</b> has $mode_of_interview interview today at $time $time_zone.</li>";
                                            //             }
                                            //         }
                                            //     }
                                            // }
                                            // if ($j == 0) {
                                            //     echo "<li>Manager's don't have any interviews today</li>";
                                            // }
                                            break;

                                        case 'DIRECTOR':
                                            echo "<b>My interviews</b>: <br/>";
                                            $dir_interviews = $this->submission->getInterviewListofManager($first_date_of_week, $last_date_of_week, $dirID);
                                            if (!empty($dir_interviews)) {
                                                foreach ($dir_interviews as $intv_data) {
                                                    if (date("Y-m-d") == $intv_data->interview_date) {
                                                        $time = $intv_data->interview_time;
                                                        $mode_of_interview = $intv_data->mode_of_interview;
                                                        $time_zone = $intv_data->time_zone;
                                                        $candidate_name = $intv_data->cname;
                                                        $dir_name = $this->user->get_manager_from_id($intv_data->userid);
                                                        echo "<li>Your candidate <b>$candidate_name</b> has $mode_of_interview interview today at $time $time_zone.</li>";

                                                    }else{
                                                        echo "<li><b>You</b> don't have any interviews today. </li>";
                                                    }
                                                }
                                            }else{
                                                echo "<li><b>You</b> don't have any interviews today. </li>";
                                            }

                                            echo "<br/><b>Manager's interviews</b>: <br/>";
                                            $i=0;
                                            foreach ($manager_ids as $mgr_ID) {
                                                //echo $mgr_ID->manager_id."<br/>";
                                                $interviews = $this->submission->getInterviewListofManager($first_date_of_week, $last_date_of_week, $mgr_ID->manager_id);
                                                if (!empty($interviews)) {
                                                    foreach ($interviews as $intv_data) {
                                                        // echo $intv_data->userid;
                                                        if (date("Y-m-d") == $intv_data->interview_date) {
                                                            $i=1;
                                                            $time = $intv_data->interview_time;
                                                            $mode_of_interview = $intv_data->mode_of_interview;
                                                            $time_zone = $intv_data->time_zone;
                                                            $candidate_name = $intv_data->cname;
                                                            // echo $intv_data->userid;
                                                            $manager_name = $this->user->get_manager_from_id($intv_data->userid);
                                                            echo "<li><b>$manager_name's </b>candidate <b>$candidate_name</b> has $mode_of_interview interview today at $time $time_zone.</li>";

                                                        }
                                                    }
                                                }
                                            }
                                            if ($i == 0) {
                                                echo "<li>Don't have any interviews today for your managers.</li>";
                                            }
                                                break;

                                            case 'ASSOCIATEDIRECTOR':
                                                //echo $asscoiate_dirId;
                                                $interviews = $this->submission->getInterviewListofManager($first_date_of_week, $last_date_of_week, $asscoiate_dirId);
                                                if (!empty($interviews)) {
                                                    foreach ($interviews as $intv_data) {
                                                        if (date("Y-m-d") == $intv_data->interview_date) {
                                                            $time = $intv_data->interview_time;
                                                            $mode_of_interview = $intv_data->mode_of_interview;
                                                            $time_zone = $intv_data->time_zone;
                                                            $candidate_name = $intv_data->cname;
                                                            $manager_name = $this->user->get_manager_from_id($intv_data->userid);
                                                            echo "<li>Your candidate <b>$candidate_name</b> has $mode_of_interview interview today at $time $time_zone.</li>";
                                                        }else{
                                                            echo "<li><b>You</b> don't have any interviews today. </li>";
                                                        }
                                                    }
                                                }else{
                                                    echo "<li><b>You</b> don't have any interviews today. </li>";
                                                }
                                                break;
                                            case 'MANAGER':
                                                // echo $managerId; 
                                                $interviews = $this->submission->getInterviewListofManager($first_date_of_week, $last_date_of_week, $managerId);
                                                if (!empty($interviews)) {
                                                    foreach ($interviews as $intv_data) {
                                                        if (date("Y-m-d") == $intv_data->interview_date) {
                                                            $time = $intv_data->interview_time;
                                                            $mode_of_interview = $intv_data->mode_of_interview;
                                                            $time_zone = $intv_data->time_zone;
                                                            $candidate_name = $intv_data->cname;
                                                            $manager_name = $this->user->get_manager_from_id($intv_data->userid);
                                                            echo "<li>Your candidate <b>$candidate_name</b> has $mode_of_interview interview today at $time $time_zone.</li>";
                                                        }else{
                                                            echo "<li><b>You</b> don't have any interviews today. </li>";
                                                        }
                                                    }
                                                }else{
                                                    echo "<li><b>You</b> don't have any interviews today. </li>";
                                                }
                                        
                                        default:
                                            # code...
                                            break;
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <div class="congrats-div">
                            <div class="congrats-title">Congratulations</div>
                            <div class="congrats-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime iste voluptatibus dignissimos iusto aliquam eligendi deserunt.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- script for time zone -->
        <script>
            // IST

            var getIST = function () {
                document.getElementById("IST").innerHTML = new Date().toLocaleString("en-US", {
                    timeZone: 'Asia/Kolkata',
                    timeStyle: 'short',
                    hourCycle: 'h12'
                })
            }
            getIST();
            setInterval(getIST, 1000);

            // EST

            var getEST = function () {
                document.getElementById("EST").innerHTML = new Date().toLocaleString("en-US", {
                    timeZone: 'America/New_York',
                    timeStyle: 'short',
                    hourCycle: 'h12'
                })
            }
            getEST();
            setInterval(getEST, 1000);

            // CST

            var getCST = function () {
                document.getElementById("CST").innerHTML = new Date().toLocaleString("en-US", {
                    timeZone: 'America/Guayaquil',
                    timeStyle: 'short',
                    hourCycle: 'h12'
                })
            }
            getCST();
            setInterval(getCST, 1000);

            // PST

            var getPST = function () {
                document.getElementById("PST").innerHTML = new Date().toLocaleString("en-US", {
                    timeZone: 'America/Los_Angeles',
                    timeStyle: 'short',
                    hourCycle: 'h12'
                })
            }
            getPST();
            setInterval(getPST, 1000);

            /*handling data through ajax*/
            // var from_date, to_date;
            // from_date = "<?php echo $first_date_of_week;?>";
            // to_date = "<?php echo $last_date_of_week;?>";
            
            function get_time_period_options(selectObject) {
                $(".dashboard-status-loader").show();
                $(".status-result").hide();
                var from_date = selectObject.options[selectObject.selectedIndex].getAttribute('first-date');
                var to_date = selectObject.options[selectObject.selectedIndex].getAttribute('last-date');
                //console.log(from_date+" "+to_date);
                $.ajax({ 
                    method: "POST",
                    url: "<?php echo base_url('index.php/dashboard/updateprogressbars') ?>",
                    dataType:"json",
                    data: {from_date:from_date, to_date:to_date}, 
                    success:function(data){
                    //console.log(data);
                    if (data.status == "success") {
                        $(".dashboard-status-loader").hide();
                        var total_sub_percent = data.total_sub_percent;
                        var total_rejections_percent = data.total_rejections_percent;
                        var total_placements_percent = data.total_placements_percent;
                        var total_nofeedbacks_percent = data.total_nofeedbacks_percent;
                        var total_onholds_percent = data.total_onholds_percent;
                        var total_interviews_percent = data.total_interviews_percent;
                        $("#total-sub-progress").val(total_sub_percent.toFixed(2));
                        $("#total-submission-value").html(" "+total_sub_percent.toFixed(2)+" %");
                        $("#total-rejections-progress").val(total_rejections_percent.toFixed(2));
                        $("#total-rejections-value").html(" "+total_rejections_percent.toFixed(2)+" %");
                        $("#total-placements-progress").val(total_placements_percent.toFixed(2));
                        $("#total-placements-value").html(" "+total_placements_percent.toFixed(2)+" %");
                        $("#total-nofeedbacks-progress").val(total_nofeedbacks_percent.toFixed(2));
                        $("#total-nofeedbacks-value").html(" "+total_nofeedbacks_percent.toFixed(2)+" %");
                        $("#total-onholds-progress").val(total_onholds_percent.toFixed(2));
                        $("#total-onholds-value").html(" "+total_onholds_percent.toFixed(2)+" %");
                        $("#total-interviews-progress").val(total_interviews_percent.toFixed(2));
                        $("#total-interviews-value").html(" "+total_interviews_percent.toFixed(2)+" %");
                        $(".status-result").show();
                    }else{
                        console.log("error");
                    } 
                    },
                    error: function(error) {
                    console.log("error data");
                    console.log(error);
                    } 
                });
            }

            /**profile js */
            let menuToggle = document.querySelector('.menu-toggle');
            let navigation = document.querySelector('.navigation');

            menuToggle.onclick = function() {
            navigation.classList.toggle('active');
            }

        </script>

    </body>
</html>

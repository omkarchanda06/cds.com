<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/profile.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>css/add-edit-user.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/resume_db.css" />
<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script>
	function get_requiter(val) {
		$.post("<?php echo base_url(); ?>index.php/recruitercount/requiterlist", {
			mid: val
		}, function(result) {
			$("#reqruiterid").html(result);
		});
	}

	function get_manager(val) {
		$.post("<?php echo base_url(); ?>index.php/managercount/managerlist", {
			Dirid: val
		}, function(result) {
			//alert(result);
			$("#managerid").html(result);
		});
	}

	function generatePDF() {
		var managerid = document.getElementById("managerid").value;
		var reqruiterid = document.getElementById("reqruiterid").value;
		var date1 = document.getElementById("date1").value;
		var date2 = document.getElementById("date2").value;
		if (date1 == "" || date2 == "") {
			alert("Please Select Date");
		} else {
			window.open('<?php echo base_url() ?>index.php/pdfrecruitercount/index/' + date1 + '/' + date2 + '/' + managerid + '/' + reqruiterid, '_blank', true)
		}
	}

	var tableToExcel = (function() {
		var uri = 'data:application/vnd.ms-excel;base64,',
			template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
			base64 = function(s) {
				return window.btoa(unescape(encodeURIComponent(s)))
			},
			format = function(s, c) {
				return s.replace(/{(\w+)}/g, function(m, p) {
					return c[p];
				})
			}
		return function(table, name, filename) {
			if (!table.nodeType) table = document.getElementById(table)
			var ctx = {
				worksheet: name || 'Worksheet',
				table: table.innerHTML
			}

			document.getElementById("dlink").href = uri + base64(format(template, ctx));
			document.getElementById("dlink").download = filename;
			document.getElementById("dlink").click();

		}
	})()
</script>

<div class="reports-div">
	<div class="reports-inputs">
		<div class="container-fluid">
			<h2> Recruiters Count </h2>
			<div class="row">
				<form action="<?php echo base_url() ?>index.php/recruitercount/" onsubmit="return v.exec()" name="registration" method="POST">
					<?php if ($usertype == "SUPERADMIN" || $usertype == "HR" || $usertype == "MIS") { ?>
						<div class="col-md-2">
							<!-- <label for="inputState">Directors</label> -->
							<label>Select Director: </label>
							<select name="directorid" id="directorid" class="inp-form" onchange="get_manager(this.value);" style="padding:5px; width:150px;">
								<?php
								if (!empty($directors)) {
									// echo '<option value="All">Select Director</option>';
									echo '<option value="All">All</option>';
									foreach ($directors as $dir) {
										$directorid = $dir->id;
										$directorname = $dir->username;
										echo '<option value="' . $directorid . '">' . $directorname . '</option>';
									}
								}
								?>
							</select>
						</div>
					<?php } ?>
					<?php if ($usertype == "SUPERADMIN" || $usertype == "DIRECTOR" || $usertype == "HR" || $usertype == "MIS") { ?>
						<div class="col-md-2">
							<label>Select Manager: </label>
							<select name="managerid" id="managerid" class="inp-form" onchange="get_requiter(this.value);" style="padding:5px; width:150px;">
								<!-- <option value="">Select Manager</option> -->
								<option value="All">All</option>
								<?php
								if (!empty($managerslist)) {
									foreach ($managerslist as $mgr) {
										$managerid = $mgr->manager_id;
										$managername = $this->user->get_manager_from_id($mgr->manager_id);
										echo '<option value="' . $managerid . '">' . $managername . '</option>';
									}
								}
								?>
							</select>
						</div>
					<?php } ?>
					<?php if ($usertype == "SUPERADMIN" || $usertype == "DIRECTOR" || $usertype == "HR" || $usertype == "MIS") { ?>
						<div class="col-md-2">
							<label>Select Recruiter: </label>
							<select name="reqruiterid" id="reqruiterid" class="inp-form" style="padding:5px; width:150px;">
								<!-- <option value="">Select Recruiter</option> -->
								<option value="All">All</option>
								<?php
								if (!empty($recruiterlist)) {
									foreach ($recruiterlist as $rcr) {
										// $recruiterid = $rcr->recruiter_id;
										$recruitername = $this->user->get_recruiter_from_id($rcr->recruiter_id);
										echo '<option value="' . $rcr->recruiter_id . '">' . $recruitername . '</option>';
									}
								}
								?>
							</select>
						</div>
					<?php } ?>
					<div class="col-md-2">
						<label for="inputState">From : </label>
						<input type="date" id="date1" name="date1">
					</div>
					<div class="col-md-2">
						<label for="inputState">To : </label>
						<input type="date" id="date2" name="date2">
					</div>
					<div class="col-md-2">
						<input type="submit" name="submit" value="submit" class="form-submit">
						<?php if ($usertype == "SUPERADMIN" || $usertype == "MIS") { ?>
							<input type="button" name="submit" value="Generate PDF" id="excel-button" onclick="generatePDF()" /><a id="dlink" style="display:none;"></a>
							<input type="button" name="submit" value="Generate Excel" id="excel-button" class="export" onclick="tableToExcel('product-table', 'Manager Count Table', 'Recruitercount.xls')" />
						<?php } ?>
					</div>
				</form>
			</div>

			<div style="padding-bottom:450px;">

				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
					<tr>
						<td id="tbl-border-left"></td>
						<td>
							<div id="content-table-inner">
								<div id="table-content">
									<?php if (!$showBlank) : ?>

										<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="cds-table" style="font:12px arial;">
											<thead>
												<th class="table-header-repeat line-left minwidth-1 table-head-imp">Manager Names</th>
												<th class="table-header-repeat line-left minwidth-1 table-head-imp" style="width:85px;">Submissions Target</th>
												<?php
												$totalTargets				= 0;
												$submissionsbyDate			= array();
												$totalSubmissionsOfAllDates = 0;
												$totalDates					= 0;
												$totalAverageSubmissions 	= 0;

												$totalSubmittedToClients 	= 0;
												$totalInterviews 			= 0;
												$totalRejected 				= 0;
												$totalPlacements 			= 0;

												foreach ($dates as $d) : $day1 = date("D", strtotime($d));
													$day2 = date("d M", strtotime($d));
													$totalDates++; ?>
													<th class="table-header-repeat line-left table-head"><?= $day2 ?><br /><?= $day1 ?></th>
												<?php endforeach; ?>

												<th class="table-header-repeat line-left minwidth-1 table-head">Total Subs</th>
												<th class="table-header-repeat line-left minwidth-1 table-head">Average</th>
												<th class="table-header-repeat line-left minwidth-1 table-head">Total Submission %</th>
												<th class="table-header-repeat line-left minwidth-1 table-head">Submitted to client</th>
												<th class="table-header-repeat line-left minwidth-1 table-head">Interviews</th>
												<th class="table-header-repeat line-left minwidth-1 table-head">Rejections / NO updates</th>
												<th class="table-header-repeat line-left minwidth-1 table-head">Placements</th>
												<th class="table-header-repeat line-left minwidth-1 table-head">Quality Percentage %</th>
											</thead>




											<?php
											// For Interview List
											$managerName 	= '';
											$recruiterArray = array();
											?>

											<?php foreach ($managers as $manager) : ?>
												<?php
												// For Interview List
												$managerName = $manager->username;
												?>
												<tr>
													<td colspan="<?= $totalDates + 10 ?>" class="alternate-row" style="background: #407195 !important;">
														<h3 style="margin:0;color: white;"><strong><?= $manager->username ?></strong></h3>
													</td>
												</tr>
												<?php $recruiters = $this->recruiter->getRecruitersByManagerID($manager->id); ?>
												<?php foreach ($recruiters as $recruiter) : ?>
													<tr>

														<?php
														// For Interview List
														$recruiter_name = $this->assignrecruiterstomgr->get_recruitername($recruiter->recruiter_id);
														$recruiterArray[$recruiter->recruiter_id] = $recruiter_name->recruiter;
														?>

														<!-- Recruiter Name -->
														<td><?php echo $recruiter_name->recruiter; ?></td>

														<!-- Recruiter Targets -->
														<?php
														$targetResult	= $this->recruiter->getRecruiterTotalTarget($recruiter->recruiter_id, $fromDate, $toDate, $totalDates);
														$targets 		= $targetResult['sumtarget'];
														$targets 		= ($targets) ? ($targets / 5) * $totalDates : 0;
														?>

														<td align="center"><?= $targets ?></td>
														<?php $totalTargets = $totalTargets + $targets; ?>


														<!-- Recruiter Submissions by Date -->
														<?php
														$submissionsOfAllDates = 0;
														foreach ($dates as $d) :
														?>

															<?php //if($this->recruiter->getRecruiterAttendance($recruiter->recruiter, $d) == 1): 
															if ($this->recruiter->getRecruiterAttendance($recruiter_name, $d) != '') :
															?>
																<td align="center" style="color: white; background:#FF0000;"> <strong>A</strong> </td>
															<?php else : ?>
																<td align="center"><?= ($subcount[$recruiter->recruiter_id][$d]['submissioncount'] ? $subcount[$recruiter->recruiter_id][$d]['submissioncount'] : 0) ?></td>
															<?php endif; ?>
															<?php
															$submissionsbyDate[$d] 		= $submissionsbyDate[$d] + $subcount[$recruiter->recruiter_id][$d]['submissioncount'];
															$submissionsOfAllDates		= $submissionsOfAllDates + $subcount[$recruiter->recruiter_id][$d]['submissioncount'];
															?>
														<?php endforeach; ?>


														<!-- Recruiter Total Submissions-->
														<td align="center"><?= $submissionsOfAllDates ?></td>


														<!-- Recruiter Average Submissions-->
														<?php $averageSubmissions 		= $submissionsOfAllDates / $totalDates; ?>
														<td align="center"><?= round($averageSubmissions, 1) ?></td>

														<!-- Recruiter Submissions Percentage -->
														<?php $submissionsPercentage = $this->user->calculatePercentage($submissionsOfAllDates, $targetResult['targets']); ?>
														<td align="center" <?php if (round($submissionsPercentage) >= 90) : ?> style="background:#FFFF00" <?php endif; ?>><?= round($submissionsPercentage) ?> %</td>


														<!-- Recruiter Submissions Status Area -->

														<!-- Recruiter Submitted to clients -->
														<td align="center">
															<?php
															if (!isset($subcount[$recruiter->id]['submitted_to_client'])) $subcount[$recruiter->id]['submitted_to_client'] = 0;
															echo $subcount[$recruiter->id]['submitted_to_client'];
															?>
														</td>

														<!-- Recruiter Interviews -->
														<td align="center">
															<?php
															// if(!isset($subcount[$recruiter->id]['interviews'])) $subcount[$recruiter->id]['interviews'] = 0;
															// echo $subcount[$recruiter->id]['interviews'];
															$interviewsCount = $this->submission->countInterviewOfRecruiter($fromDate, $toDate, $recruiter->recruiter_id);
															echo $interviewsCount;
															?>
														</td>

														<!-- Recruiter Rejected -->
														<td align="center">
															<?php
															if (!isset($subcount[$recruiter->recruiter_id]['rejections'])) $subcount[$recruiter->recruiter_id]['rejections'] = 0;
															echo $subcount[$recruiter->recruiter_id]['rejections'];
															?>
														</td>

														<!-- Recruiter Placements -->
														<td align="center">
															<?php
															if (!isset($subcount[$recruiter->recruiter_id]['placements'])) $subcount[$recruiter->recruiter_id]['placements'] = 0;
															echo $subcount[$recruiter->recruiter_id]['placements'];
															?>
														</td>

														<!-- Recruiter Quality Percentage -->
														<?php $qualityPercentage  = $this->user->calculatePercentage($subcount[$recruiter->recruiter_id]['submitted_to_client'], $submissionsOfAllDates); ?>
														<td align="center"><?= round($qualityPercentage) ?> %</td>
													</tr>

													<?php
													// Total counting section for all managers
													if ($managerId == 'All') :

														foreach ($dates as $d) :
															if (isset($subcount[$recruiter->recruiter_id][$d]['submissioncount'])) :
																$totalSubmissionsOfAllDates = $totalSubmissionsOfAllDates + $subcount[$recruiter->recruiter_id][$d]['submissioncount'];
															endif;
														endforeach;

														$totalAverageSubmissions 	= $totalAverageSubmissions + $averageSubmissions;


														if (isset($subcount[$recruiter->recruiter_id]['submitted_to_client'])) :
															$totalSubmittedToClients    = $totalSubmittedToClients + $subcount[$recruiter->recruiter_id]['submitted_to_client'];
														endif;

														// if(isset($subcount[$recruiter->id]['interviews'])):	
														// $totalInterviews  			= $totalInterviews + $subcount[$recruiter->id]['interviews'];
														// endif;
														$totalInterviews  			= $totalInterviews + $interviewsCount;

														if (isset($subcount[$recruiter->recruiter_id]['rejections'])) :
															$totalRejected  			= $totalRejected + $subcount[$recruiter->recruiter_id]['rejections'];
														endif;

														if (isset($subcount[$recruiter->recruiter_id]['placements'])) :
															$totalPlacements  			= $totalPlacements + $subcount[$recruiter->recruiter_id]['placements'];
														endif;

													// If one of the manager is selected
													else :

														$totalInterviews  			= $totalInterviews + $interviewsCount;

														if ($managerId != '' && $managerId != 'All') :
															$conditionQuery = ' AND userid=' . $manager->id . ' AND submission_category=' . $setCategoryID;
														//$subResult 		= $this->submission->getSubmissionsStat($conditionQuery, $fromDate, $toDate);

														//if($managerId != '' && $managerId != 'All') {
														//	$graphTitle	 = $subResult['graphTitle'].' ['.$manager->username.']';
														//	$statusArray = $subResult['statusArray'];
														//}
														endif;

													endif;
													?>

												<?php endforeach; ?>
											<?php endforeach; ?>



											<?php if ($managerId == 'All') : ?>
												<thead>
													<th class="table-header-repeat line-left minwidth-1 table-bottom">Total</th>
													<th class="table-header-repeat line-left minwidth-1 table-bottom"><?= $totalTargets ?></th>
													<?php foreach ($dates as $d) : ?>
														<th class="table-header-repeat line-left table-bottom"><?= $submissionsbyDate[$d] ?></th>
													<?php endforeach; ?>
													<th class="table-header-repeat line-left minwidth-1 table-bottom"><?= $totalSubmissionsOfAllDates ?></th>
													<th class="table-header-repeat line-left minwidth-1 table-bottom"><?= round($totalAverageSubmissions, 1) ?></th>

													<?php $totalSubmissionsPercentage = $this->user->calculatePercentage($totalSubmissionsOfAllDates, $totalTargets); ?>
													<th class="table-header-repeat line-left minwidth-1 table-bottom" <?php if (round($totalSubmissionsPercentage) >= 90) : ?> style="background:#FFFF00" <?php endif; ?>><?= round($totalSubmissionsPercentage) ?> %</th>

													<th class="table-header-repeat line-left minwidth-1 table-bottom"><?= $totalSubmittedToClients ?></th>
													<th class="table-header-repeat line-left minwidth-1 table-bottom"><?= $totalInterviews ?></th>
													<th class="table-header-repeat line-left minwidth-1 table-bottom"><?= $totalRejected ?></th>
													<th class="table-header-repeat line-left minwidth-1 table-bottom"><?= $totalPlacements ?></th>
													<?php $totalqualityPercentage = $this->user->calculatePercentage($totalSubmittedToClients, $totalSubmissionsOfAllDates); ?>
													<th class="table-header-repeat line-left minwidth-1 table-bottom"><?= round($totalqualityPercentage) ?> %</th>
												</thead>
											<?php endif; ?>


										</table>


										<!-- <?php if ($managerId != '' && $managerId != 'All') : ?>
											<table class="list-table">

												<thead>
													<th class="table-header-repeat line-left minwidth-1 table-head-imp">S. No</th>
													<th class="table-header-repeat line-left minwidth-1 table-head-imp">S. Date</th>
													<th class="table-header-repeat line-left minwidth-1 table-head-imp">Candidate Name</th>
													<th class="table-header-repeat line-left minwidth-1 table-head-imp">For Position</th>
													<th class="table-header-repeat line-left minwidth-1 table-head-imp">Manager</th>
													<th class="table-header-repeat line-left minwidth-1 table-head-imp">Submitted By</th>
													<th class="table-header-repeat line-left minwidth-1 table-head-imp">Interview Date</th>
													<th class="table-header-repeat line-left minwidth-1 table-head-imp">Status</th>
												</thead>

												<tbody>
													<?php $lists = $this->submission->getInterviewListofManager($fromDate, $toDate, $managerId); ?>
													<?php if (sizeof($lists) > 0) : ?>
														<?php $i = 1;
														foreach ($lists as $key => $list) : ?>
															<tr>
																<th align="center"><?= $i ?></th>
																<th align="center"><?= $list->sdate ?></th>
																<th align="center"><?= $list->cname ?></th>
																<th align="center"><?= $list->psub ?></th>
																<th align="center"><?= $managerName ?></th>
																<th align="center"><?= $recruiterArray[(int)$list->submittedby] ?></th>
																<th align="center"><?= $list->interview_date ?></th>
																<th align="center"><?= $list->status ?></th>
															</tr>
														<?php $i++;
														endforeach; ?>
													<?php else : ?>
														<tr>
															<td colspan="7" align="center">No Rows Found</td>
														</tr>
													<?php endif; ?>
												</tbody>

											</table>
											<br>
											<br>
										<?php endif; ?> -->


									<?php endif; ?>

								</div>




								<!-- <?php if ($managerId != '' && $managerId != 'All') : ?>

									<div id="chartContainer" style="height: 300px; width: 100%;"></div>
									<script src="<?= base_url() ?>js/canvasjs.min.js"></script>
									<script type="text/javascript">
										window.onload = function() {
											var chart = new CanvasJS.Chart("chartContainer", {
												title: {
													text: "<?= $graphTitle ?>"
												},
												exportFileName: "Pie Chart",
												exportEnabled: true,
												animationEnabled: true,
												legend: {
													verticalAlign: "bottom",
													horizontalAlign: "center"
												},
												data: [{
													type: "pie",
													showInLegend: true,
													toolTipContent: "{legendText}: <strong>{y}</strong>",
													indexLabel: "{label} {y}",
													dataPoints: [

														{
															y: <?= $statusArray['Submitted to Client'] ?>,
															legendText: " Submitted to Client",
															label: "Submitted to Client"
														},

														{
															y: <?= $totalInterviews ?>,
															legendText: " Interview",
															label: "Interview"
														},

														{
															y: <?= $statusArray['Rejected'] ?>,
															legendText: " Rejected",
															label: "Rejected"
														},

														{
															y: <?= $statusArray['Placement'] ?>,
															legendText: " Placement",
															label: "Placement"
														}

													]
												}]
											});
											chart.render();
										}
									</script>
								<?php endif; ?> -->

							</div>

			</div>
			<!--  end content-table  -->
			<!--  start paging..................................................... -->

			<!--  end paging................ -->
			<div class="clear"></div>

		</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
		</tr>
		</table>

		<?php $this->load->view('includes/footer'); ?>
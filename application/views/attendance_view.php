<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/profile.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>css/add-edit-user.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>css/resume_db.css" />
<script>
  $(function() {
    var pickerOpts = {
      dateFormat: "yy-mm-dd"
    };
    $(".datepicker").datepicker(pickerOpts);
  });
</script>
<div class="search-resume-div" style="width: fit-content;padding-right: 60px;margin-left: 15%;">
  <div class="search-inputs">
    <div class="search-text">Recruiters Attendance [ <?php if (isset($category)) echo $category; else echo 'All'; ?> ]</div>
  </div>
</div>
<?php if ($this->session->userdata('logged_in')['id'] == 57) : ?>
  <!-- Extra -->
  <!-- <div style="float:right; padding: 0px 17px 10px 0px;">
    <form method="get" action="<?php echo base_url(); ?>index.php/attendance/search">
      <strong>Search Attendance:</strong>
      <input type="text" name="date" class="inp-form datepicker" readonly />
      <input type="submit" class="inp-form" name="submit" value="GO" />
    </form>
  </div> -->
  <!-- Extra -->
<?php endif; ?>
<!-- table -->
<div class="table-div">
  <?php
  //$error='';
  if ($error != '') {
    echo $error;
  }
  ?>
  <div class="table">
    <table id="myTable" style="width: 80% !important;">
      <thead>
        <tr>
          <th class="table-header" style="border-top-left-radius: 20px"> S.NO </th>
          <th class="table-header">Recruiter Name</th>
          <th class="table-header">Manager Name</th>
          <th class="table-header">Attendance</th>
          <th class="table-header" style="border-top-right-radius: 20px">Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1;
        //print_r($row);exit;
        foreach ($row as $res) {
          // $query = $this->db->query("select status from recruiters_attendance where rec='".$res->recruiter."' and date='".date("Y-m-d")."'");
          // $date = date("2013-02-26");
          $date = date("Y-m-d");
          $recruiter_status = $this->recruiter->get_recruiter_attendance($res->recruiter, $date);
          if (!empty($recruiter_status->result())) {
            foreach ($recruiter_status->result() as $row) {
              $status_att  = $row->status;
            }

            if ($status_att == 0) {
              $att = "P";
              $att1 = 1;
            } else {
              $att = "A";
              $att1 = 0;
            }
          } else {
            $att = "P";
            $att1 = 1;
          }
        ?>
          <tr>
            <td class="table-data"><?php echo $i; ?> </td>
            <td class="table-data"><?php echo $res->recruiter; ?></td>
            <td class="table-data"><?php echo $res->manager; ?></td>
            <td class="table-data"><?php echo $att; ?></td>
              <td style="text-align:center;" class="options-width">
                <a href="<?php echo base_url()?>index.php/recruiterattendance/status_attendence/<?php echo $att1;?>/<?php echo $res->recruiter;?>" title="Attendance" class="options-width icon-target" >
                <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" class="iconify iconify--clarity" viewBox="0 0 36 36"><circle cx="17.99" cy="10.36" r="6.81" fill="currentColor" class="clr-i-solid clr-i-solid-path-1"/><path fill="currentColor" d="M12 26.65a2.8 2.8 0 014.85-1.8L20.71 29l6.84-7.63A16.81 16.81 0 0018 18.55 16.13 16.13 0 005.5 24a1 1 0 00-.2.61V30a2 2 0 001.94 2h8.57l-3.07-3.3a2.81 2.81 0 01-.74-2.05z" class="clr-i-solid clr-i-solid-path-2"/><path fill="currentColor" d="M28.76 32a2 2 0 001.94-2v-3.76L25.57 32z" class="clr-i-solid clr-i-solid-path-3"/><path fill="currentColor" d="M33.77 18.62a1 1 0 00-1.42.08l-11.62 13-5.2-5.59a1 1 0 00-1.41-.11 1 1 0 000 1.42l6.68 7.2L33.84 20a1 1 0 00-.07-1.38z" class="clr-i-solid clr-i-solid-path-4"/><path fill="none" d="M0 0h36v36H0z"/></svg>
                </a>
              </td>
          </tr>
        <?php $i++;
        } ?>
      </tbody>
    </table>
    <?php $this->load->view('includes/footer'); ?>
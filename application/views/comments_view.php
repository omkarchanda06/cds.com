<style type="text/css">
#wrapper {
	width:98%;
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	margin:0;
	padding:0;
}
.comments {
	float:left;
	width:99%;
	border:1px solid #cccccc;
	padding:5px 10px 10px 5px;
	margin-bottom:10px;
}
span.user {
	float:left;
	width:20%;
}
span.date {
	float:right;
}
span.comment-content {
	float:left;
	margin:10px 0 0 0;
	line-height:22px;
	width:100%;
}
.leave-comments {
	float:left;
	padding:0 0 0 10px;
	width:100%;
	border:1px solid #cccccc;
	background-color:#eeeeee;
}
.leave-comments h3 {
	width:100%;
	color: #333333;
    font-size: 18px;
    font-weight: bold;
    line-height: 18px;
    margin-bottom: 20px;
}
.leave-comments label {
    float: left;
    font-weight: bold;
    width: 10%;
}	
.leave-comments textarea {
	border: 1px solid #CCCCCC;
    border-radius: 3px 3px 3px 3px;
    float: left;
    height: auto;
    margin: 0;
    padding: 10px 0 0;
    text-indent: 10px;
  
}
.leave-comments textarea:hover {
	box-shadow:0 0 2px 0;
}
.form-submit {
    float: right;
    height: 35px;
    margin: 10px 0 0;
    padding: 0;
    width: 20%;
}
.form-submit #submit {
    background-color: #FF8000;
    border: medium none;
    border-radius: 3px 3px 3px 3px;
    color: #FFFFFF;
    cursor: pointer;
    line-height: 25px;
    margin: 0;
    padding: 3px;
    text-decoration: none;
}
.form-submit #submit:hover {
background-color:#D76B00;
}
</style>
</head>
<body>
<?php
//print_r($this->uri->segment(3));exit;
$key=$this->uri->segment(3);
?>
	<div id="wrapper"><!--wrapper starts-->
	
<?php

foreach($row as $result)
{
//print_r($result);exit;
?>

		<div class="comments"><!--comments starts-->
			<span class="user"><?php echo $result->username;?> said :</span>
			<span class="date">Date : <?php echo $result->ondate;?></span>
			<span class="comment-content"><?php echo $result->comment;?></span>
		</div><!--comments ends-->
	<?php
	
	}?>
		<div class="leave-comments"><!--leave-comments starts-->
		<?php
		if($error != '')
		{
			echo $error;
		}
		?>
<?php 
$session_data = $this->session->userdata('logged_in');
$userId = $session_data['id'];
?>
		<form method="post" action="<?php echo base_url()?>index.php/totalsubmissions/comments/<?php echo $key; ?>">
			<h3>Leave a Reply </h3>
			<table>
				<tr>
					<td valign="top"><p><label>Comment</label></p></td>
					<td><p><textarea name="comment" cols="50" rows="5"></textarea></p><input type="hidden" name="sid" value="<?php echo $key;?>"><input type="hidden" name="uid" value="<?php echo $userId;?>"></td>
				<tr><td colspan="2" align="right"><p class="form-submit"><input type="submit" value="Add Comment" id="submit" name="submit"></p></td></tr>
			</table>	
		</form>			
						
		</div><!--leave-comments ends-->
	</div><!--wrapper ends-->



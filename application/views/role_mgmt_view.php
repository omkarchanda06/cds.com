<?php $this->load->view('includes/header'); ?>

<div id="page-heading"><h1>Role Management</h1></div>

<table width="100%" cellspacing="0" cellpadding="0" border="0" id="content-table">
<tbody>
<tr>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowleft.jpg"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowright.jpg"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	
	
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
	<tr valign="top" height="30">
		<td><span style="color:#ff0000; line-height: 50px;">
			<!-- start id-form -->
			<?php
			//$error='';
			if($error!=''){
				echo $error;	
			}
			?>	</span>	
			<table cellspacing="0" cellpadding="0" border="0" id="id-form" width="100%" >
			<tbody>
				<tr valign="top">
					<td>			
						<strong>Role Name</strong>
					</td>
					<td>			
						<strong>Action</strong>
					</td>
				</tr>
				<tr>
					<td colspan = "2" >			
						<hr>
					</td>
				</tr>
				<?php 
					foreach($rolemgmt as $rmgmt){
				?>
				<tr>
					<td width="80%" height="30">			
						<?php echo $rmgmt->name; ?>
					</td>
					<td width="20%">			
						<a href="<?php echo base_url(); ?>index.php/rolemanagement/edit/<?php echo $rmgmt->id; ?>">Edit</a>
					</td>
				</tr>
				<?php
					}	
				?>		
			
			</tbody>
			</table>
			<!-- end id-form  -->

		</td>
	</tr>
<!--<tr>
<td><img width="695" height="1" alt="blank" src="images/shared/blank.gif"></td>

</tr> -->
	</tbody>
	</table>
	</form>
	
	<div class="clear"></div>
	 
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</tbody>
</table>

<?php $this->load->view('includes/footer'); ?>

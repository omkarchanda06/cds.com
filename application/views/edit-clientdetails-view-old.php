<?php $this->load->view('includes/header'); ?>

<div id="page-heading">
    <h1><?php if(!empty($result)){
        echo $result;
        }else{
            echo "Update Client Details";
        } ?>
    </h1>
</div>

<table width="100%" cellspacing="0" cellpadding="0" border="0" id="content-table">
<tbody>
<tr>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowleft.jpg"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowright.jpg"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
    <?php  if(sizeof($clientdetails) > 0) { foreach ($clientdetails as $detail) { ?>
	<?php echo form_open('client/update'); ?>
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
	<tr valign="top" height="30">
		<td><span style="color:blue; line-height: 23px;"><?php if($message!=''){ echo $message; }?></span>	
			<table cellspacing="0" cellpadding="0" border="0" id="id-form" width="100%" >
			<tbody>
				<tr>
					<th valign="top">Client Name :</th>
					<td><input type="text" class="inp-form"  name="client_name" value="<?php echo $detail->client_name;?>"
					     style="width:500px;" placeholder="Client Name"> 
					</td>
					<!-- <td style="color:red;"><?php echo form_error('job_title') ?></td> -->
				</tr>
                <tr>
					<th valign="top">Client Company :</th>
					<td><input type="text" class="inp-form"  name="client_company" value="<?php echo $detail->client_company;?>"
					     style="width:500px;" placeholder="Client Company"> 
					</td>
				</tr>
                <tr>
					<th valign="top">Client Contact :</th>
					<td><input type="text" class="inp-form"  name="client_contact" value="<?php echo $detail->client_contact;?>"
					     style="width:500px;" placeholder="Client Contact"> 
					</td>
				</tr>
                <tr>
					<th valign="top">Client Email :</th>
					<td><input type="text" class="inp-form"  name="client_email" value="<?php echo $detail->client_email;?>"
					     style="width:500px;" placeholder="Client Email"> 
					</td>
				</tr>
                <tr>
					<th>Comments :</th>
					 <td><textarea class="inp-form" name="comments" placeholder="Comments" style="  width: 500px; min-height: 150px;"><?php echo $detail->comments;?></textarea>
					 <div class="field-error"></div>
					 </td><td></td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td valign="top" style="padding-top: 10px;">
                        <input type="hidden" name="id" value="<?=$detail->id?>">
						<input type="submit" name="updateclientdetails" class="form-submit" value="update client Details"/>
						<input type="reset" class="form-reset" value="">
					</td>
					<td></td>
				</tr>
			
			</tbody>
			</table>
			<!-- end id-form  -->

		</td>
	</tr>
	</tbody>
	</table>
	</form>
    <?php } } ?>
	
	<div class="clear"></div>
	 
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</tbody>
</table>

<?php $this->load->view('includes/footer'); ?>

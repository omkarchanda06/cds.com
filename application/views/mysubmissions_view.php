<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/resume_db.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/tcal.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery/tcal.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/custom.js"></script>
<script>
	function PopupCenter(pageURL, title, w, h) {
		var left = (screen.width / 2) - (w / 2);
		var top = (screen.height / 2) - (h / 2);
		var targetWin = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
	}

	var tableToExcel = (function() {
		var uri = 'data:application/vnd.ms-excel;base64,',
			template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
			base64 = function(s) {
				return window.btoa(unescape(encodeURIComponent(s)))
			},
			format = function(s, c) {
				return s.replace(/{(\w+)}/g, function(m, p) {
					return c[p];
				})
			}
		return function(table, name, filename) {
			if (!table.nodeType) table = document.getElementById(table)
			var ctx = {
				worksheet: name || 'Worksheet',
				table: table.innerHTML
			}
			document.getElementById("dlink").href = uri + base64(format(template, ctx));
			document.getElementById("dlink").download = filename;
			document.getElementById("dlink").click();
		}
	})()
</script>
<!-- default menu -->
<div class="search-resume-div" style="width: fit-content;padding-right: 60px;">
	<div class="search-inputs">
		<div class="search-text">My Submissions</div>
	</div>
</div>
<div class="table-search" style="justify-content:flex-end !important; margin-top:20px !important;">
	<form style="float:right; padding:4px 20px 4px 0px; display: flex;" id="exportForm" action="<?php echo base_url() ?>index.php/mysubmissionsreport/" onsubmit="return v.exec()" name="registration" method="POST">
		<div class="input-div">
			<select name="status" class="inp-form" style="padding:5px; width:150px;">
				<option value="All">Select Status</option>
				<option value="Submitted to Vendor">Submitted to Vendor</option>
				<option value="Rejected">Rejected</option>
				<option value="Submitted to Client">Submitted to Client</option>
				<option value="Interview">Interview</option>
				<option value="Rejected after Interview">Rejected after Interview</option>
				<option value="Placement">Placement</option>
			</select>
		</div>
		<div class="input-div">
			Start
			<input style="margin-left: 5px" type="date" class="form-control ppDate" name="date1" id="date1" placeholder="Start Date" />
		</div>
		<div class="input-div">
			End
			<input style="margin-left: 5px" type="date" class="form-control ppDate" name="date2" id="date2" placeholder="End Date" />
		</div>

		<!-- submit btn -->
		<!-- <div class="animate-btn">
      <div class="search-btn"> -->
		<button type="submit" class="cssbuttons-io-button">
			Submit
			<input type="hidden" name="submit" value="submit" />
			<div class="icon">
				<svg height="24" width="24" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
					<path d="M0 0h24v24H0z" fill="none"></path>
					<path d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z" fill="currentColor"></path>
				</svg>
			</div>
		</button>
		<!-- <a id="dlink"  style="display:none;"></a>
		<input type="button" name="submit" value="Generate Excel" 
		              class="export" id="excel-button" 
		        onclick="tableToExcel('product-table', 'Manager Count Table', 'MySubmission.xls')" /> -->
	</form>
	<!-- </div>
    </div> -->
</div>
<div class="table-div">
	<div class="table">
		<table id="myTable">
			<thead>
				<tr>
					<th class="table-header" style="border-top-left-radius: 20px">Date</th>
					<th class="table-header">Consultant</th>
					<th class="table-header">For Position</th>
					<th class="table-header">Total IT</th>
					<th class="table-header">End Client</th>
					<th class="table-header">Loc</th>
					<!-- <th class="table-header">Manager</th> -->
					<th class="table-header">Submitted by</th>
					<th class="table-header">Consultant Rate</th>
					<th class="table-header">Visa</th>
					<th class="table-header">Submission Rate</th>
					<th class="table-header">Employer Details</th>
					<th class="table-header">Status</th>
					<th class="table-header">Comments</th>
					<th class="table-header" style="border-top-right-radius: 20px">
						Action
					</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$i = 1;
				$date = '';
				foreach ($rows->result() as $res1) {
					//print_r($res1);exit;
					if ($i % 2 == 0) {
						$class = "alternate-row";
					} else {
						$class = "";
					}
					if ($date == '') {
						$date = $res1->sdate;
					}

					if ($date != $res1->sdate) {
						$date = $res1->sdate;
						echo '<tr><td colspan="25" bgcolor="grey"></td></tr>';
					}

				?>
					<tr class="<?php echo $class; ?>">
						<td>
							<?php //echo $res1['userid']; 
							?>
							<?php echo date("M", strtotime($res1->sdate)); ?>
							<?php echo date("d", strtotime($res1->sdate)); ?>


						</td>
						<td align="left"><a href="<?php echo base_url(); ?>/<?php echo $res1->resume; ?>" target="_blank" title="<?php echo $res1->cname ?>"><?php echo substr($res1->cname, 0, 12) ?></a> </td>
						<td><a href="#" title="<?php echo $res1->psub ?>"><?php echo substr($res1->psub, 0, 7) ?></a></td>
						<td><?php echo $res1->totalit; ?></td>
						<td><a href="#" title="<?php echo $res1->company ?>"><?php echo substr($res1->company, 0, 6) ?></a> </td>
						<td><?php echo $res1->st; ?></td>
						<td><a href="#" title="<?php echo $res1->recruiter ?>"><?php echo substr($res1->recruiter, 0, 7) ?></a> </td>
						<!-- Vendor details Comment start<td>					
					<?php
					$session_data = $this->session->userdata('logged_in');

					$userId = $session_data['id'];
					$utype = $session_data['usertype'];
					if ($res1->userid == $userId || $utype == 'DIRECTOR') {
						//echo substr('abcdef', 0, 4); 
						echo '<a href="#" title="' . $res1->vcompany . '" style="color:#000;">' . substr($res1->vcompany, 0, 6) . '</a>';
					} else {
						echo '***';
					}
					?>
					</td>
					<td>					
					<?php
					if ($res1->userid == $userId || $utype == 'DIRECTOR') {
						echo '<a href="#" title="' . $res1->vcontact . '">' . substr($res1->vcontact, 0, 7) . '</a> ';
					} else {
						echo '***';
					}
					?>
					</td>
					<td>					
					<?php
					if ($res1->userid == $userId || $utype == 'DIRECTOR') {
						echo '<a href="#" title="' . $res1->vemail . '">' . substr($res1->vemail, 0, 7) . '</a> ';
					} else {
						echo '***';
					}
					?>
					</td> Vendor details Comment end -->
						<td><?php echo $res1->crate; ?></td>
						<td><?php echo $res1->visatype; ?></td>
						<td>
							<?php
							if ($res1->userid == $userId || $utype == 'DIRECTOR') {
								echo $res1->srate;
							} else {
								echo '***';
							}
							?>
						</td>
						<td onmouseover="document.getElementById('div<?php echo $i; ?>').style.display = 'block';document.getElementById('div-<?php echo $i; ?>').style.display = 'none';" onmouseout="document.getElementById('div<?php echo $i; ?>').style.display = 'none';document.getElementById('div-<?php echo $i; ?>').style.display = 'block';">
							<div id="div-<?php echo $i; ?>" style="display: block;">View</div>
							<div id="div<?php echo $i; ?>" style="display: none;"><?php echo $res1->employerdetails; ?></div>
						</td>


						<td>
							<a class="edit-status" data-id="<?= $res1->id ?>" title="<?php echo $res1->status ?>"><?php echo substr($res1->status, 0, 6) ?></a>
							<div class="status-option-section" id="status-option-section-<?= $res1->id ?>"></div>
						</td>

						<td><a class="comment-section" data-id="<?= $res1->id ?>" href="javascript:void(0);" onclick="PopupCenter('<?php echo base_url(); ?>index.php/totalsubmissions/comments/<?php echo $res1->id; ?>', 'myPop1',600,600);">Read / Add</a>
							<div class="comment-view-section" id="comment-view-section-<?= $res1->id ?>">Loading...</div>
						</td>

						<td><a class="info-tooltip" href="<?php echo base_url(); ?>index.php/totalsubmissions/editmysubmissions/<?php echo $res1->id; ?>" title="EDIT" alt="EDIT"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" class="iconify iconify--material-symbols" width="20" height="20" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
									<path fill="currentColor" d="M5 19h1.4l8.625-8.625l-1.4-1.4L5 17.6ZM19.3 8.925l-4.25-4.2l1.4-1.4q.575-.575 1.413-.575q.837 0 1.412.575l1.4 1.4q.575.575.6 1.388q.025.812-.55 1.387ZM17.85 10.4L7.25 21H3v-4.25l10.6-10.6Zm-3.525-.725l-.7-.7l1.4 1.4Z">
									</path>
								</svg></a>
							<!--<a class="icon-2 info-tooltip" title="DELETE" alt="DELETE" href="<?php echo base_url(); ?>index.php/totalsubmissions/deletemysubmissions/<?php echo $res1->id; ?>" onclick="return confirm('Are you sure you want to delete?')"></a>-->
						</td>

					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<!-- Start of pagination -->
	<?php //if($links){ echo "<ul id='pagination-digg'>".$links."</ul>"; } 
	?>
	<!-- <ul class="pagination:container"> -->
	<?php
	if ($links) {
		echo "<ul class='pagination:container'>" . $links . "</ul>";
	}
	?>
</div>
</div>

<!-- Interview modal popup -->
<div class="interview_modal" id="submission-id-"></div>
<div class="interview_modal_box">
	<a href="#" class="interview_modal_close"></a>
	<div class="interview_inner_modal_box">
		<div id="interview-details-form">
			<h3>Add Interview Details:</h3>
			<div class="interview-date-row">
				<label>Select Date: </label>
				<input type="date" id="interview-date" name="interview-date" required />
			</div>
			<div class="interview-time-row">
				<label>Select Time: </label>
				<input type="time" id="interview-time" name="interview-time" required />
			</div>
			<div class="interview-timezone-row">
				<label>Time Zone: </label>
				<select id="timezone-of-interview" required>
					<option value="EST">EST</option>
					<option value="CST">CST</option>
					<option value="MST">MST</option>
					<option value="PST">PST</option>
				</select>
			</div>
			<div class="interview-mode-row">
				<label>Mode of Interview: </label>
				<select id="mode-of-interview" required>
					<option value="Phone">Phone</option>
					<option value="Video">Video</option>
					<option value="inperson">In - Person</option>
				</select>
			</div><br />
			<!-- <input type="hidden" id="target-id" name="target-id" /> -->
			<button type="button" class="excel-button submit-interview-btn" id="submit-interview-details" name="interview-submit">Submit</button>
			<br />
			<label id="interview-error-message"></label>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= base_url() ?>js/mysubmissions.js"></script>
<?php $this->load->view('includes/footer'); ?>
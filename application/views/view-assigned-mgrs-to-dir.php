<?php $this->load->view('includes/header'); ?>

<div id="page-heading"><h1>View Assigned Managers to Directors</h1></div>


<table border="0" width="98%" cellpadding="0" cellspacing="0" id="product-table" class="target-table">
    <thead>
        <th class="table-header-repeat line-left minwidth-1">Director Names</th>
        <th class="table-header-repeat line-left minwidth-2">Manager Names</th>
        <th class="table-header-repeat line-left minwidth-2">Status</th>
        <th class="table-header-repeat line-left minwidth-2">Edit</th>
    </thead>
    <tbody>
        <?php //print_r($viewall);exit(); ?>
    <?php foreach($viewall as $view) { ?>
        <tr class="assigned-mgr-to-dir-<?php echo $view->id; ?>">
            <td>
            <?php $director_id = $view->director_id; 
            $director_name = $this->assignmanagerstodirector->get_username($director_id);
            echo $director_name->username;
            ?>
            </td>
            <td><?php $manager_id = $view->manager_id; 
            $manager_name = $this->assignmanagerstodirector->get_username($manager_id);
            echo $manager_name->username;
            ?></td>
            <td>
                <?php 
                if ($view->isActive !=0) {
                    echo "Active";
                } else {
                    echo  "<span style='color: red;'>Inactive</span>";
                }
                 
                ?>
            </td>
            <td style="text-align:center;" class="options-width"><a class="edit-job" href="<?php echo base_url() ?>index.php/assignmgrstodir/edit/<?=$director_id?>" id="<?=$director_id?>">Edit</a></td>
        </tr>
    <?php }?>	
    </tbody>			
</table>
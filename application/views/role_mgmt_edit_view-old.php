<?php $this->load->view('includes/header'); ?>

<div id="page-heading"><h1>Role Management</h1></div>

<table width="100%" cellspacing="0" cellpadding="0" border="0" id="content-table">
<tbody>
<tr>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowleft.jpg"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowright.jpg"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	
	
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
	<tr valign="top" height="30">
		<td><span style="color:#ff0000; line-height: 50px;">
			<!-- start id-form -->
			<?php
			//$error='';
			if($error!=''){
				echo $error;	
			}
			?>	</span>	
			<?php echo form_open('rolemanagement/update_role_management'); ?>
			<table cellspacing="0" cellpadding="0" border="0" id="id-form" width="100%" >
			<tbody>
				<tr valign="center" height="20">
					<td>			
						<strong>&nbsp;</strong>
					</td>
					<td  align="right" css="margin-right:10px">	
						<input type="hidden" name="roleID" value="<?php echo $role_id?>">	
						<input type="submit" name="Update" value="Update">
					</td>
				</tr>
				<tr valign="center" height="20">
					<td>			
						<strong>Item</strong>
					</td>
					<td  align="right" css="margin-right:10px">			
						<strong>Select all <input type = "checkbox" name="select-all" id="select-all"></strong>
					</td>
				</tr>
				
				<tr>
					<td colspan = "2" >			
						<hr>
					</td>
				</tr>
				<?php 

					foreach($menu as $rmenu){
						if($rmenu->parent_id == 0){
							$menuName = str_replace(" ", "",strtolower($rmenu->manu_name));
				?>
				<tr style="background-color:#5693f5">
					<td width="80%" height="30" >			
						<?php echo $rmenu->manu_name; ?>
					</td>
					<td width="20%"  align="right">			
						<input type = "checkbox" name = 'menuIDs[]' id='menuID_<?php echo $rmenu->id;?>' value = '<?php echo $rmenu->id;?>' class="<?php echo $menuName ?>">

					</td>
				</tr>
					<?php
							$parentID = $rmenu->id;
							foreach($menu as $rmenu1){
								if($rmenu1->category == 2 && $rmenu1->parent_id == $parentID){
					?>
								<tr style="background-color:#9bbffa">
									<td width="80%" height="30">			
										<?php echo $rmenu1->manu_name; ?>
									</td>
									<td width="20%"  align="right">			
										<input type = "checkbox" name = 'menuIDs[]' id='menuID_<?php echo $rmenu1->id;?>' value = '<?php echo $rmenu1->id;?>' class="<?php echo $menuName ?>_sub">

									</td>
								</tr>
					<?php				
								}
								$categoryID = $rmenu1->parent_id;
								$parentID1 = $rmenu1->id;
								foreach($menu as $rmenu2){
									if($rmenu2->category == 3 && $rmenu2->parent_id == $parentID1 && $parentID == $categoryID){
					?>
								<tr style="background-color:#FFF">
									<td width="80%" height="30">			
										<?php echo $rmenu2->manu_name; ?>
									<td width="20%"  align="right">			
										<input type = "checkbox" name = 'menuIDs[]' id='menuID_<?php echo $rmenu2->id;?>' value = '<?php echo $rmenu2->id;?>' class="<?php echo $menuName ?>_sub">

									</td>
								</tr>
					<?php				
									}
								}
						}
					echo '<tr><td colspan="2" height="10"><hr></td></tr>';	
					}
				}
				?>		
			
			</tbody>
			</table>
			</form>
			<!-- end id-form  -->

		</td>
	</tr>
<!--<tr>
<td><img width="695" height="1" alt="blank" src="images/shared/blank.gif"></td>

</tr> -->
	</tbody>
	</table>
	</form>
	
	<div class="clear"></div>
	 
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</tbody>
</table>
<script language="JavaScript">
$('#select-all').click(function(event) {   
    if(this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;                        
        });
    } else {
        $(':checkbox').each(function() {
            this.checked = false;                       
        });
    }
}); 
<?php 
	foreach($menu as $rmenu){
	if($rmenu->parent_id == 0){
		$menuName = str_replace(" ", "",strtolower($rmenu->manu_name));
?>
$('.<?php echo $menuName;?>').click(function(event) {   
    if(this.checked) {
        // Iterate each checkbox
        $('.<?php echo $menuName;?>_sub:checkbox').each(function() {
            this.checked = true;                        
        });
    } else {
        $('.<?php echo $menuName;?>_sub:checkbox').each(function() {
            this.checked = false;                       
        });
    }
}); 
	<?php }} ?>
 
</script>
<?php $this->load->view('includes/footer'); ?>

<?php //$this->load->view('includes/header'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panzeradmin - Home</title>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/resume_db.css" />
<!--  Jquery core -->
<script src="<?php echo base_url()?>js/jquery/jquery-1.8.3.js" type="text/javascript"></script>

<!-- Jquery UI -->
<script src="<?php echo base_url()?>js/jquery/jquery-ui.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.css" type="text/css" media="screen" title="default"/>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/tcal.css" /> -->
<!-- <script type="text/javascript" src="<?php echo base_url()?>js/jquery/tcal.js"></script>  -->
<script src="<?php echo base_url()?>assets/js/checkbox.js"></script>
<!-- default menu -->
    <div class="default_nav">
      <div class="logo">
        <img src="<?php echo base_url()?>assets/images/logo.png" alt="Panzer" />
      </div>
      <!-- Profile (this should be in all pages) -->
      <div class="navigation">
        <div class="user-box">
          <div class="image-box">
            <img src="<?php echo base_url()?>assets/images/profile.png" alt="avatar" />
          </div>
          <p class="username"><?php 
          echo $_SESSION['logged_in']['username'];
        ?></p>
        </div>
        <div class="menu-toggle"></div>
        <ul class="menu">
          <li>
            <a href="<?php echo base_url()?>index.php/dashboard">Dashboard</a>
          </li>
          <li>
            <a href="#">Profile</a>
          </li>
          <li>
            <a href="<?php echo base_url()?>index.php/resumedatabase/logout">Logout</a>
          </li>
        </ul>
      </div>
      <!-- js for profile animation -->
      <script src="<?php echo base_url()?>assets/js/profile.js"></script>
      
    </div>
<script>
function PopupCenter(pageURL, title,w,h) {
var left = (screen.width/2)-(w/2);
var top = (screen.height/2)-(h/2);
var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
</script>
<script type="text/javascript" src="<?php echo base_url()?>js/libs/base64.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/libs/sprintf.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jspdf.js"></script>
<SCRIPT language="javascript">
$(function(){
 
    // add multiple select / deselect functionality
    $("#selectall").click(function () {
          $('.case').attr('checked', this.checked);
    });
 
    // if all checkbox are selected, check the selectall checkbox
    // and viceversa
    $(".case").click(function(){
 
        if($(".case").length == $(".case:checked").length) {
            $("#selectall").attr("checked", "checked");
        } else {
            $("#selectall").removeAttr("checked");
        }
 
    });
	$('.close_window').click(function(){
		$(".slidingDiv").hide();
		//$('.show_hide').attr('z','open');
	});
	
});
function onchangeS(val){
	var other = new Array;
	if(val == "other"){
		$.each($("input[name='case[]']:checked"), function() {
			other.push($(this).val());
		});
		//alert(other);
		if(other == "" || other == "undefine"){
			alert ("Please select resume first...");
			return false;
		}else{
			$(".slidingDiv").show();
			$(".show_hide").hide();
		}
	}else if(val == "me"){
		$(".slidingDiv").hide();
		
		
		$.each($("input[name='case[]']:checked"), function() {
			other.push($(this).val());
		});
		if(other == ""){
			alert ("Please select resume first...");
		}else{
		$("#mainloading").fadeIn();
		$.post("<?php echo base_url();?>/index.php/mailme",{consultantlist:other},function(result){
			alert(result);
			$("#mainloading").fadeOut('fast');
		});
		}
		
	}
}
function sendmail(){
		
		var other = new Array;
		
		$.each($("input[name='case[]']:checked"), function() {
		  	other.push($(this).val());
		});
		var toemail = $("#toemail").val();
		if(toemail == ""){
			alert ("Please add Email address");
			return false;
		}
		var tosubject = $("#tosubject").val();
		if(tosubject == ""){
			alert ("Please tosubject");
			return false;
		}
		var tomsg = $("#tomsg").val();
		$(".slidingDiv").hide();
		$("#mainloading").fadeIn();
		$.post("<?php echo base_url();?>/index.php/sendmailother",{consultantlist:other, toemail:toemail, tosubject:tosubject, tomsg:tomsg},function(result){
    		alert(result);
			$("#mainloading").fadeOut('fast');
  		});
		$("#toemail").val("");
		$("#tosubject").val("");
		$("#tomsg").val("");
}

function client_update(id,name,email){
	$.post("<?php echo base_url();?>/index.php/emailbody/clientupdate",{id:id, name:name, email:email},function(result){
		window.location="mailto:"+email+"?subject=Any Update - "+name+"&body="+result;
	});
}
function client_interview(id,name,email){
	$.post("<?php echo base_url();?>/index.php/emailbody/clientinterview",{id:id, name:name, email:email},function(result){
		window.location="mailto:"+email+"?subject=Interview Feedback - "+name+"&body="+result;
	});
}
function resume_email(id,name,psub){
	$.post("<?php echo base_url();?>/index.php/emailbody",{id:id, name:name, psub:psub},function(result){
			//alert(result);
			window.location="mailto:?subject=Job Title - "+name+" - "+psub+"&body="+result;
  	});
}
</SCRIPT>
<div class="search-resume-div">
      <div class="search-inputs">
        <div class="search-text">Search Resume</div>
        <form action="<?php echo base_url()?>index.php/search" method="get">
        <div class="container">
          <!-- <form class="form-inline"> -->
          <div class="form-group">
            <input type="text" name="tsearch" id="jobtitle" placeholder="Job Title" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['tsearch'])):'');?>" name="email"
              required
            />
          </div>
          <div class="form-group">
            <input type="text" id="ksearch1" placeholder="Keyword 1" name="ksearch1" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['ksearch1'])):'');?>" required
            />
          </div>
          <div class="form-group">
            <input type="text" id="ksearch2" placeholder="Keyword 2" name="ksearch2" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['ksearch2'])):'');?>" required
            />
          </div>
          <div class="form-group">
            <input type="text" id="ksearch3" placeholder="Keyword 3" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['ksearch3'])):'');?>" name="ksearch3"
            />
          </div>
          <div class="form-group">
            <input type="text" id="ksearch4" placeholder="Keyword 4" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['ksearch4'])):'');?>" name="ksearch4"
            />
          </div>
          <?php 
        $userType = $this->session->userdata('logged_in')['usertype'];
        if($userType == 'SUPERADMIN' || $userType == 'ADMIN' || $userType == 'MANAGER'): ?>

          <div class="form-group">
            <input type="text" id="candid" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['candid'])):'');?>" placeholder="ID" name="candid" />
          </div>
        <?php endif; ?>
        </div>
        <div class="container">
          <div class="form-group">
            <input type="text" id="cname" placeholder="Consultant Name" value="<?=(isset($_REQUEST['cname'])?(htmlentities($_REQUEST['cname'])):'');?>" name="cname"
            />
          </div>
         
          <div class="form-group">
            <select name="datebtw" id="datebtw" required>
              <option value="">Uploaded Duration</option>
              <?php 
                if(isset($_REQUEST['datebtw'])) {
              ?>
              <option value="<?php echo $_REQUEST['datebtw']; ?>" selected><?=(isset($_REQUEST['datebtw'])?(htmlentities($_REQUEST['datebtw'])):'');
            }
              ?>
              <option value="1 Week">1 Week</option>
              <option value="2 Week">2 Week</option>
              <option value="3 Week">3 Week</option>
              <option value="1 Month">1 Month</option>
              <option value="3 Month">3 Month</option>
              <option value="6 Month">6 Month</option>
              <option value="1 Year">1 Year</option>
              <option value="All">All</option>
            </select>
          </div>
          <div class="form-group">
            <div id="list_loc" class="dropdown-check-list" tabindex="100" onclick="myFunction('loc')">
              <span class="anchor">Select Location</span>

              <ul id="items_loc" class="items">
                <!-- <li><input type="checkbox" value="1" />Alabama</li> -->
                <?php foreach($key1->result() as $result) { 

                  echo '<li><input type="checkbox" name="location[]" value="'.$result->lid.'"/>'.$result->location.'</li>';
                }?>
              </ul>
            </div>
          </div>
          <div class="form-group">
            <select id="yrsexp" name="yrsexp">
              <option value="">Years of Exp</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
              <option value="21">21</option>
              <option value="22">22</option>
              <option value="23">23</option>
              <option value="24">24</option>
              <option value="25">25</option>
            </select>
          </div>
          <div class="form-group">
            <div id="list_visa" class="dropdown-check-list" tabindex="100" onclick="myFunction('visa')">
              <span class="anchor" >Select Visa</span>
              <ul id="items_visa" class="items">
                <li><input type="checkbox" />EAD OPT</li>
                <?php foreach($visatypes->result() as $visa) { 
                  echo '<li><input type="checkbox" name="visa[]" value="'.$visa->vid.'"/>'.$visa->visatype.'</li>';
                }?>
              </ul>
            </div>
          </div>

          <div class="form-group">
            <select id="relocate" name="relocate">
              <option value="">Relocate</option>
              <option value="YES">YES</option>
              <option value="NO">NO</option>
            </select>
          </div>

          <!-- </form> -->
        </div>
        <input type="hidden" name="searchtype" value="<?=$searchtype;?>" /> 
        <input type="hidden" name="categorytype" value="<?php if(isset($categorytype)) echo $categorytype;?>" />
        <!-- submit btn -->
        <div class="animate-btn">
          <div class="search-btn">
            <button type="submit" class="cssbuttons-io-button">
              Submit
              <div class="icon">
                <svg
                  height="24"
                  width="24"
                  viewbox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M0 0h24v24H0z" fill="none"></path>
                  <path
                    d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z"
                    fill="currentColor"
                  ></path>
                </svg>
              </div>
            </button>
          </div>
          <!-- <div class="resume-count">Total Resume: 462626</div> -->
        </div>
      </form>
      </div>
    </div>
<div id="mainloading" style="display:none;"><div id="loading2" ><img src="<?php echo base_url();?>images/loading_m.gif" alt="" />Sending  Mail...</div>	</div>
<?php	echo $html; ?>
</div>
			<!--  end content-table  -->
				<!--  start paging..................................................... -->

			<!--  end paging................ -->
			
			<div class="clear"></div>
		 
		</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<!--<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr> -->
	</table>
	
	<div id="main-wrapper">
 <!-- <a href="javascript:void(0);" class="show_hide" z='open'>compose</a>-->
 
</div>
<script type="text/javascript">
$('.delete-submission').click(function(e){
   e.preventDefault();
   var retVal = confirm("Are you sure, you want to delete this submission?");
   if(retVal == true){
      window.location.href=$(this).attr('href');
      return true;
   }else{
      return false;
   }
});
</script>	
<?php $this->load->view('includes/footer'); ?>

<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url()?>css/profile.css" />
<link rel="stylesheet" href="<?php echo base_url()?>css/add-edit-user.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/resume_db.css" />
    <!-- default menu -->
    
    <!-- End of profile -->
    <!-- End of default menu -->
 
    <div class="search-resume-div">
      <div class="search-inputs">
        <div class="search-text">Search Resume</div>
        <form action="<?php echo base_url()?>index.php/search" method="get">
        <div class="container">
          <!-- <form class="form-inline"> -->
          <div class="form-group">
            <input type="text" name="tsearch" id="jobtitle" placeholder="Job Title" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['tsearch'])):'');?>" name="email"
            required />
          </div>
          <div class="form-group">
            <input type="text" id="ksearch1" placeholder="Keyword 1" name="ksearch1" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['ksearch1'])):'');?>"
            required />
          </div>
          <div class="form-group">
            <input type="text" id="ksearch2" placeholder="Keyword 2" name="ksearch2" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['ksearch2'])):'');?>"
            required />
          </div>
          <div class="form-group">
            <input type="text" id="ksearch3" placeholder="Keyword 3" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['ksearch3'])):'');?>" name="ksearch3"
            />
          </div>
          <div class="form-group">
            <input type="text" id="ksearch4" placeholder="Keyword 4" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['ksearch4'])):'');?>" name="ksearch4"
            />
          </div>
          <?php 
        $userType = $this->session->userdata('logged_in')['usertype'];
        if($userType == 'SUPERADMIN' || $userType == 'ADMIN' || $userType == 'MANAGER'): ?>

          <div class="form-group">
            <input type="text" id="candid" value="<?=(isset($_REQUEST['ksearch1'])?(htmlentities($_REQUEST['candid'])):'');?>" placeholder="ID" name="candid" />
          </div>
        <?php endif; ?>
        </div>
        <div class="container">
          <div class="form-group">
            <input type="text" id="cname" placeholder="Consultant Name" value="<?=(isset($_REQUEST['cname'])?(htmlentities($_REQUEST['cname'])):'');?>" name="cname"
            />
          </div>
          <div class="form-group">
            <select name="datebtw" id="datebtw" required>
              <option value="">Uploaded Duration</option>
              <option value="1 Week">1 Week</option>
              <option value="2 Week">2 Week</option>
              <option value="3 Week">3 Week</option>
              <option value="1 Month">1 Month</option>
              <option value="3 Month">3 Month</option>
              <option value="6 Month">6 Month</option>
              <option value="1 Year">1 Year</option>
              <option value="All">All</option>
            </select>
          </div>
          <div class="form-group">
            <div id="list_loc" class="dropdown-check-list" tabindex="100" onclick="myFunction('loc')">
              <span class="anchor">Select Location</span>

              <ul id="items_loc" class="items">
                <!-- <li><input type="checkbox" value="1" />Alabama</li> -->
                <?php foreach($key1->result() as $result) { 

                  echo '<li><input type="checkbox" name="location[]" value="'.$result->lid.'"/>'.$result->location.'</li>';
                }?>
              </ul>
            </div>
          </div>
          <div class="form-group">
            <select id="yrsexp" name="yrsexp">
              <option value="">Years of Exp</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
              <option value="21">21</option>
              <option value="22">22</option>
              <option value="23">23</option>
              <option value="24">24</option>
              <option value="25">25</option>
            </select>
          </div>
          <div class="form-group">
            <div id="list_visa" class="dropdown-check-list" tabindex="100" onclick="myFunction('visa')">
              <span class="anchor" >Select Visa</span>
              <ul id="items_visa" class="items">
                <li><input type="checkbox" />EAD OPT</li>
                <?php foreach($visatypes->result() as $visa) { 
                  echo '<li><input type="checkbox" name="visa[]" value="'.$visa->vid.'"/>'.$visa->visatype.'</li>';
                }?>
              </ul>
            </div>
          </div>

          <div class="form-group">
            <select id="relocate" name="relocate">
              <option value="">Relocate</option>
              <option value="YES">YES</option>
              <option value="NO">NO</option>
            </select>
          </div>

          <!-- </form> -->
        </div>
        <input type="hidden" name="searchtype" value="<?=$searchtype;?>" /> 
        <input type="hidden" name="categorytype" value="<?php if(isset($categorytype)) echo $categorytype;?>" />
        <!-- submit btn -->
        <div class="animate-btn">
          <div class="search-btn">
            <button type="submit" class="cssbuttons-io-button">
              Submit
              <div class="icon">
                <svg
                  height="24"
                  width="24"
                  viewbox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M0 0h24v24H0z" fill="none"></path>
                  <path
                    d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z"
                    fill="currentColor"
                  ></path>
                </svg>
              </div>
            </button>
          </div>
          <!-- <div class="resume-count">Total Resume: 462626</div> -->
        </div>
      </form>
      </div>
    </div>


<script src="<?php echo base_url()?>assets/js/checkbox.js"></script>
<?php //$this->load->view('includes/footer'); ?>
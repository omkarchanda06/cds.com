<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/profile.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>css/add-edit-user.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>css/resume_db.css" />

<div class="search-resume-div" style="width: fit-content;padding-right: 60px;margin-left: 15%;">
  <div class="search-inputs">
    <div class="search-text">All Recruiters</div>
  </div>
</div>
<!-- table -->
<div class="table-div">
  <?php
  //$error='';
  if ($error != '') {
    echo $error;
  }
  ?>
  <div class="table">
    <table id="myTable" style="width: 80% !important;">
      <thead>
        <tr>
          <th class="table-header" style="border-top-left-radius: 20px"> S.NO </th>
          <th class="table-header"> User ID :</th>
          <th class="table-header">Recruiter Name</th>
          <th class="table-header">Manager Name</th>
          <th class="table-header">Targets</th>
          <th class="table-header">Status</th>
          <th class="table-header" style="border-top-right-radius: 20px">Assign</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1;
        foreach ($recruiters as $recruit) { ?>
          <tr class="target-recruiter-<?php echo $recruit->id; ?>">
            <td class="table-data"><?php echo $i; ?> </td>
            <td class="table-data"><?php echo $recruit->id; ?></td>
            <td class="table-data"><?php echo $recruit->recruiter; ?></td>
            <td class="table-data"><?php echo $this->user->get_manager_from_id($recruit->mid); ?></td>
            <td class="table-data" id="target-print-<?php echo $recruit->id; ?>"><?php echo $recruit->targets; ?></td>
            <?php if ($recruit->status == 'INACTIVE') { ?>
              <td style="text-align:center; color:red;" class="rec-status-<?php echo $recruit->id; ?>"><?php echo $recruit->status; ?></td>
            <?php } else { ?>
              <td style="text-align:center;" class="rec-status-<?php echo $recruit->id; ?>"><?php echo $recruit->status; ?></td>
            <?php } ?>
            <td style="text-align:center;" class="options-width">
              <a href="#" id="<?php echo $recruit->id; ?>" class="paulund_modal icon-target info-tooltip">
                <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" class="iconify iconify--clarity" viewBox="0 0 36 36">
                  <circle cx="17.99" cy="10.36" r="6.81" fill="currentColor" class="clr-i-solid clr-i-solid-path-1" />
                  <path fill="currentColor" d="M12 26.65a2.8 2.8 0 014.85-1.8L20.71 29l6.84-7.63A16.81 16.81 0 0018 18.55 16.13 16.13 0 005.5 24a1 1 0 00-.2.61V30a2 2 0 001.94 2h8.57l-3.07-3.3a2.81 2.81 0 01-.74-2.05z" class="clr-i-solid clr-i-solid-path-2" />
                  <path fill="currentColor" d="M28.76 32a2 2 0 001.94-2v-3.76L25.57 32z" class="clr-i-solid clr-i-solid-path-3" />
                  <path fill="currentColor" d="M33.77 18.62a1 1 0 00-1.42.08l-11.62 13-5.2-5.59a1 1 0 00-1.41-.11 1 1 0 000 1.42l6.68 7.2L33.84 20a1 1 0 00-.07-1.38z" class="clr-i-solid clr-i-solid-path-4" />
                  <path fill="none" d="M0 0h36v36H0z" />
                </svg>
              </a>
            </td>
          </tr>
        <?php $i++;
        } ?>
      </tbody>
    </table>
    <!-- <div class="paulund_modal_box">
      <a href="#" class="paulund_modal_close"></a>
      <div class="paulund_inner_modal_box">
        <div id="target-form">
          <h3>Edit Profile of: <span id="recruiter-name" style="color:black;">SomeName</span></h3><br />
          <label>Manager: </label>
          <div id="form-field" class="rec-manager-area">
            <input type="text" id="manager-name" name="manager-name" />
          </div>
          <label>Target: </label>
          <div id="form-field">
            <input type="text" id="target-num" name="target-num" />
          </div>
          <label>Status: </label>
          <div id="form-field" class="rec-status-area">

          </div>

          <input type="hidden" id="target-id" name="target-id" />
          <button type="button" class="excel-button" id="set-target" name="set-target">Update Recruiter</button>
          <br />
          <label id="error-message"></label>
        </div>
      </div>
    </div> -->

    <div class="form_popup_div paulund_modal_box">
        <div class="form_popup">
            <div class="popup_title">
                <h3>Edit Profile of: <span id="recruiter-name">SomeName</span></h3>
            </div>
            <div class="popup_close">
                <a href="#"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                        aria-hidden="true" role="img" class="iconify iconify--ion" width="32" height="32"
                        preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512">
                        <path
                            d="M331.3 308.7L278.6 256l52.7-52.7c6.2-6.2 6.2-16.4 0-22.6-6.2-6.2-16.4-6.2-22.6 0L256 233.4l-52.7-52.7c-6.2-6.2-15.6-7.1-22.6 0-7.1 7.1-6 16.6 0 22.6l52.7 52.7-52.7 52.7c-6.7 6.7-6.4 16.3 0 22.6 6.4 6.4 16.4 6.2 22.6 0l52.7-52.7 52.7 52.7c6.2 6.2 16.4 6.2 22.6 0 6.3-6.2 6.3-16.4 0-22.6z"
                            fill="currentColor"></path>
                        <path
                            d="M256 76c48.1 0 93.3 18.7 127.3 52.7S436 207.9 436 256s-18.7 93.3-52.7 127.3S304.1 436 256 436c-48.1 0-93.3-18.7-127.3-52.7S76 304.1 76 256s18.7-93.3 52.7-127.3S207.9 76 256 76m0-28C141.1 48 48 141.1 48 256s93.1 208 208 208 208-93.1 208-208S370.9 48 256 48z"
                            fill="currentColor"></path>
                    </svg>
                </a>
            </div>
            <!--popup_close-->
            <div class="field_title">Manager:</div>
            <div id="form-field" class="display_field rec-manager-area">
                <div class="input-div">
                  <input type="text" id="manager-name" name="manager-name" />
                </div>
            </div>
            <div class="display_field">
                <div class="field_title">Target:</div>
                <div class="input-div">
                  <input type="text" id="target-num" name="target-num" />
                </div>
            </div>
            <div class="display_field">
                <div class="field_title">Status:</div>
                <div id="form-field" class="rec-status-area input-div">
                    
                </div>
            </div>
            <input type="hidden" id="target-id" name="target-id" />
            <div class="submit_btn" id="set-target" name="set-target">
                <div class="submit_icon"><svg xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img"
                        class="iconify iconify--ic" width="20" height="20" preserveAspectRatio="xMidYMid meet"
                        viewBox="0 0 24 24">
                        <path fill="currentColor"
                            d="m18 7l-1.41-1.41l-6.34 6.34l1.41 1.41L18 7zm4.24-1.41L11.66 16.17L7.48 12l-1.41 1.41L11.66 19l12-12l-1.42-1.41zM.41 13.41L6 19l1.41-1.41L1.83 12L.41 13.41z">
                        </path>
                    </svg>
                </div>
                <div class="submit_text">&nbsp;&nbsp;Update Recruiter</div>
            </div>
            <label id="error-message"></label>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/custom.js"></script>
    <?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('includes/header'); ?>

<div id="page-heading"><h1>Clients Details</h1></div>

<table width="100%" cellspacing="0" cellpadding="0" border="0" id="content-table">
<tbody>
<tr>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowleft.jpg"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th class="sized" rowspan="3"><img width="20" height="300" alt="" src="<?php echo base_url() ?>images/shared/side_shadowright.jpg"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">

				<table border="0" width="98%" cellpadding="0" cellspacing="0" 
				       id="product-table" class="target-table">
				<thead>
					<th class="table-header-repeat line-left minwidth-1">ID</th>
					<th class="table-header-repeat line-left minwidth-2">Name</th>
					<th class="table-header-repeat line-left minwidth-2">Company</th>
					<th class="table-header-repeat line-left minwidth-2">Contact</th>
					<th class="table-header-repeat line-left minwidth-2">Email</th>
					<th class="table-header-repeat line-left minwidth-2">Comments</th>
					<th class="table-header-repeat line-left minwidth-2">Edit</th>
				</thead>
				<tbody>
				<?php foreach($clientdetails as $details) { ?>
				<tr class="jobtitle-<?php echo $details->id; ?>">

					<td style="text-align:center;"><?=$details->id?></td>
					<td><?=$details->client_name?></td>
					<td><?=$details->client_company?></td>
					<td><?=$details->client_contact?></td>
					<td><?=$details->client_email?></td>
					<td><?=$details->comments?></td>
					<td style="text-align:center;" class="options-width"><a href="<?php echo base_url() ?>index.php/client/edit/<?=$details->id?>" id="<?=$details->id?>">Edit</a></td>
					<!-- <td style="text-align:center;" class="options-width"><a class="delete-jobtitle" href="<?php echo base_url() ?>index.php/jobtitle/delete/<?=$details->id?>" id="<?=$details->id?>">Delete</a></td> -->
				</tr>
                <?php  }?>	
                </tbody>			
				</table>
                <div style="padding:0px 0px 420px 0px;"></div>				
				<!--  end product-table................................... --> 
				<!-- </form> -->

	<div class="clear"></div>
	 
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</tbody>
</table>

<script type="text/javascript" src="<?php echo base_url(); ?>js/custom.js"></script>
<?php $this->load->view('includes/footer'); ?>
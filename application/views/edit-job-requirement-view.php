<?php $this->load->view('includes/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>css/profile.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>css/add-edit-user.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>js/test_js/1.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>js/test_js/2.css" />
<!-- Start of body -->
<div class="background">
    <div class="title-div">
        <p class="title">
            <?php
            $result = $this->session->flashdata('result');
            if (!empty($result)) {
                echo $result;
            } else {
                echo "Edit Job Requirement";
            } ?>
        </p>
        <?php if ($message != '') {
            echo $message;
        } ?>
    </div>
    <?php if (sizeof($jobdetails) > 0) {
        $attributes = array('id' => 'job-requirement-form'); echo form_open('requirements/update', $attributes); ?>
        <div class="input-container">
            <div class="input-div">
                <div class="input-name">Select a Date :</div>
                <input class="selectpicker" aria-label="Default select example" name="jdate" id="jdate" type="date" value="<?php echo $jobdetails->date;?>" style="width: 63%" />
            </div>
            <div class="input-div">
                <div class="input-name">Job Title :</div>
                <input type="text" class="selectpicker" name="psub" id="psub" placeholder="Job Title" value="<?php echo $jobdetails->job_title; ?>" required />
                <span id="job-title-pg" style="display: none;"> The Job title is not exists click <a href="<?php echo base_url() ?>index.php/jobtitle/addjobtitle">here</a> to add.</span>
            </div>
            <div class="input-div">
                <div class="input-name">City :</div>
                <input type="text" class="selectpicker" name="city" id="city" placeholder="City" value="<?php echo $jobdetails->city; ?>" required />
            </div>
            <div class="input-div">
                <div class="input-name">Select State :</div>
                <select name="location" class="styledselect_form_1" required>
                    <option value="">Select State</option>
                    <?php foreach ($key1->result() as $result) {
                            if ($jobdetails->location == $result->lid) {
                                echo '<option value=' . $result->lid . ' selected>' . $result->location . '</option>';
                            } else {
                                echo '<option value=' . $result->lid . '>' . $result->location . '</option>';
                            }
                        } ?>
                </select>
            </div>
            <div class="input-div">
                <div class="input-name">Duration :</div>
                <select name="datebtw" class="selectpicker" aria-label="Default select example" data-live-search="true" placeholder="Duration" id="job-duration" required>
                <option <?php if ($jobdetails->duration == "1 Week") {
                            echo 'selected="selected"';
                        } ?> value="1 Week">1 Week</option>
                <option <?php if ($jobdetails->duration == "2 Weeks") {
                            echo 'selected="selected"';
                        } ?> value="2 Weeks">2 Weeks</option>
                <option <?php if ($jobdetails->duration == "3 Weeks") {
                            echo 'selected="selected"';
                        } ?> value="3 Weeks">3 Weeks</option>
                <option <?php if ($jobdetails->duration == "1 Month") {
                            echo 'selected="selected"';
                        } ?> value="1 Month">1 Month</option>
                <option <?php if ($jobdetails->duration == "3 Months") {
                            echo 'selected="selected"';
                        } ?> value="3 Months">3 Months</option>
                <option <?php if ($jobdetails->duration == "6 Months") {
                            echo 'selected="selected"';
                        } ?> value="6 Months">6 Months</option>
                <option <?php if ($jobdetails->duration == "1 Year") {
                            echo 'selected="selected"';
                        } ?> value="1 Year">1 Year</option>
                <option <?php if ($jobdetails->duration == "18 Months") {
                            echo 'selected="selected"';
                        } ?> value="18 Months">18 Months</option>
                <option <?php if ($jobdetails->duration == "2 Years") {
                            echo 'selected="selected"';
                        } ?> value="2 Years">2 Years</option>
                <option <?php if ($jobdetails->duration == "Full Time") {
                            echo 'selected="selected"';
                        } ?> value="Full Time">Full Time</option>
                </select>
            </div>
            <div class="input-div">
            <?php if ($jobdetails->duration != 'Full Time') { ?>
                <div class="input-name" id="rate-label">Rate :</div>
                <input type="text" class="selectpicker" value="<?php echo $jobdetails->bill_rate; ?>" name="jobrate" id="jobrate" placeholder="Rate" required /><span id="job-rate-error" style="color: red;"></span>
                <input type="text" class="selectpicker" name="salaryrate" id="salaryrate" placeholder="Salary" style="display: none;"/>
                <?php } else { ?>
                    <div class="input-name" id="rate-label">Salary :</div>
                    <input type="text" class="selectpicker" value="<?php echo $jobdetails->bill_rate; ?>" name="jobrate" id="jobrate" style="display: none;" placeholder="Rate"/>
                    <input type="text" class="selectpicker" value="<?php echo $jobdetails->bill_rate; ?>" name="salaryrate" id="salaryrate" placeholder="Salary" required /><span id="job-salary-rate-error" style="color: red;"></span>
                <?php } ?>
            </div>
            <div class="input-div">
                <div class="input-name">Client Name :</div>
                <input type="text" class="selectpicker" value="<?php echo $client_name; ?>" name="client_name" id="client_name" placeholder="Client Name" required /><span id="job-pg-add-client" style="display: none;">
                    The client is not exists click <a href="<?php echo base_url() ?>index.php/client/clientform">here</a> to add.</span>
            </div>
        </div>
        <div class="input-div" style="margin: auto; margin-bottom: 20px; width: 70%">
            <div class="input-name">Job Desctiption :</div>
            <textarea class="selectpicker" aria-label="Default select example" placeholder="Job Description" name="comments" style="
            width: 500px;
            height: 150px;
            border: 2px solid #0087c4;
            border-radius: 10px;
          "><?php echo $jobdetails->job_description; ?></textarea>
        </div>
        <div class="form-btn">
        <input type="hidden" name="id" value="<?= $jobdetails->id ?>">
            <button class="cssbuttons-io-button" name="updatejobrequirement">
                Update
                <div class="icon">
                    <svg height="24" width="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 0h24v24H0z" fill="none"></path>
                        <path d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z" fill="currentColor"></path>
                    </svg>
                </div>
            </button>
            <!-- clear form btn -->
            <!-- <button class="button">
                <div class="trash">
                    <div class="top">
                        <div class="paper"></div>
                    </div>
                    <div class="box"></div>
                    <div class="check">
                        <svg viewBox="0 0 8 6">
                            <polyline points="1 3.4 2.71428571 5 7 1"></polyline>
                        </svg>
                    </div>
                </div>
                <span>Clear Form</span>
            </button>-->
            <!-- js for btn -->
            <!--<script>
                document.querySelectorAll(".button").forEach((button) =>
                    button.addEventListener("click", (e) => {
                        if (!button.classList.contains("delete")) {
                            button.classList.add("delete");
                            document.getElementById("job-requirement-form").reset();
                            setTimeout(() => button.classList.remove("delete"), 3200);
                        }
                        e.preventDefault();
                    })
                );
            </script> -->
        </div>
    <?php echo form_close();
    } ?>
</div>
<!-- script for input fields -->
<!-- <script src="<?php echo base_url() ?>js/test_js/1.js"></script> -->
<script src="<?php echo base_url() ?>js/test_js/2.js"></script>
<script src="<?php echo base_url() ?>js/test_js/3.js"></script>

</html>
<?php $this->load->view('includes/footer'); ?>
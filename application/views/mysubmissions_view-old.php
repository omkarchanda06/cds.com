<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/tcal.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery/tcal.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>js/custom.js"></script> 
<script>
function PopupCenter(pageURL, title,w,h) {
var left 		= (screen.width/2)-(w/2);
var top  		= (screen.height/2)-(h/2);
var targetWin 	= window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}

var tableToExcel 	= (function(){
        var uri 	= 'data:application/vnd.ms-excel;base64,', 
        template 	= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>', 
        base64 		= function (s) { return window.btoa(unescape(encodeURIComponent(s))) }, 
        format 		= function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return 		  function (table, name, filename) {
	            if(!table.nodeType) table = document.getElementById(table)
	            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
	            document.getElementById("dlink").href 		= uri + base64(format(template, ctx));
	            document.getElementById("dlink").download 	= filename;
	            document.getElementById("dlink").click();
        }
})()
</script>

	<!--  start page-heading -->
	<div id="page-heading">
		<h1>My Submissions
         <form style="float:right; padding:4px 20px 4px 0px;" 
         id="exportForm" action="<?php echo base_url()?>index.php/mysubmissionsreport/" 
         onsubmit="return v.exec()" name="registration" method="POST">
         <table><tr>
         <td style="width:500px"><div class="search_area">
                	<input type="search" class="light-table-filter" data-table="target-table" placeholder="Search here" />
                	<img class="reset-search-bar" src="<?php echo base_url() ?>images/close-modal.png"/>
                </div><td> 
         <select name="reqruiterid" id="reqruiterid" class="inp-form" style="padding:5px;">
        <option value="All">Select Recruiter</option>
        <?php
        foreach($reqruiterlist as $rlist){
			echo '<option value='.$rlist->id.'>'.$rlist->recruiter.'</option>';
		}
		?>		
		</select></td>
		<td>
		<select name="status" class="inp-form" style="padding:5px; width:150px;">
        	<option value="All">Select Status</option>
        	<option value="Submitted to Vendor">Submitted to Vendor</option>
            <option value="Rejected">Rejected</option>
			<option value="Submitted to Client">Submitted to Client</option>
            <option value="Interview">Interview</option>
            <option value="Rejected after Interview">Rejected after Interview</option>
            <option value="Placement">Placement</option>
		</select>
		</td>
		<td><input type="text" name="date1" id="date1" readonly 
		           class="inp-form tcal required" placeholder="Select a Date"
		           style="width:100px;"/></td>
		<td><input type="text" name="date2" id="date2" readonly 
		           class="inp-form tcal required"  placeholder="Select a Date"
		           style="width:100px;"/></td>
		<td><input type="submit" name="submit" value="submit" class="form-submit" />
		<!--<input type="button" name="submit" value="Generate PDF" style="background-color:#407195; font:13px calibri; padding:5px 10px; color:#ecf2f7; border-radius:5px; border:none; cursor:pointer;" onclick="generatePDF()" />--> 
		<a id="dlink"  style="display:none;"></a>
		<input type="button" name="submit" value="Generate Excel" 
		              class="export" id="excel-button" 
		        onclick="tableToExcel('product-table', 'Manager Count Table', 'MySubmission.xls')" /></td>
		        </tr></table></form>
        
        
        </h1>
	</div>

<!-- end page-heading -->
<?php //$error='';
/*if($error!=''){
echo $error;	
}*/
			?>
			
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="<?php echo base_url() ?>images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="<?php echo base_url() ?>images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
		
			<!--  start table-content  -->
			<div id="table-content">
			  <!-- start product-table ..................................................................................... -->
			  	

				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="target-table" style="font:13px Cambria;">
				<tr>
					<th class="table-header-repeat line-left" align="center" width="65">Date</th>
					<th class="table-header-repeat line-left" align="center" width="105">Consultant</th>
					<th class="table-header-repeat line-left" align="center">For Position</th>
					<th class="table-header-repeat line-left" align="center">Total IT</th>
					<th class="table-header-repeat line-left">End Client</th>
					<th class="table-header-options line-left" align="center">Loc</th>
					<th class="table-header-options line-left">Submitted by</th>
					<!-- <th class="table-header-options line-left">Vendor Company</th>
					<th class="table-header-options line-left">Vendor Contact</th>
					<th class="table-header-options line-left">Vendor Email</th> -->
					<th class="table-header-options line-left">Client Rate</th>
					<th class="table-header-options line-left" align="center" width="65" >Visa</th>
					<th class="table-header-options line-left">Consultant Rate</th>
					<th class="table-header-options line-left">Employer Details</th>
					<th class="table-header-options line-left" align="center" width="65">Status</th>
					<th class="table-header-options line-left" width="105" align="center">Comments</th>
					<th class="table-header-options line-left" width="105" align="center">Actions</th>
				</tr>
				
				<?php
				$i=1;
				$date='';
				foreach($rows->result() as $res1){
				//print_r($res1);exit;
				if($i%2==0){
				$class="alternate-row";
				}else{
				$class="";
				}
					if($date == ''){
						$date=$res1->sdate; 
					
					}
					
					if($date != $res1->sdate){
						$date=$res1->sdate; 
					echo '<tr><td colspan="25" bgcolor="grey"></td></tr>';
					}
				
				?>
				<tr class="<?php echo $class;?>">
					<td>
					<?php //echo $res1['userid']; ?>
					<?php echo date("M",strtotime($res1->sdate)); ?>
					<?php echo date("d",strtotime($res1->sdate)); ?>
					 	 	 	 	 	 	 	 		 	 	 	

					</td>
					<td align="left"><a href="<?php echo base_url(); ?>/<?php echo $res1->resume; ?>" target="_blank" title="<?php echo $res1->cname ?>"><?php echo substr($res1->cname, 0, 12)?></a> </td>
					<td><a href="#" title="<?php echo $res1->psub ?>"><?php echo substr($res1->psub, 0, 7)?></a></td>
					<td><?php echo $res1->totalit; ?></td>
					<td><a href="#" title="<?php echo $res1->company ?>"><?php echo substr($res1->company, 0, 6)?></a> </td>
					<td><?php echo $res1->st; ?></td>
					<td><a href="#" title="<?php echo $res1->recruiter ?>"><?php echo substr($res1->recruiter, 0, 7)?></a> </td>
					<!-- Vendor details Comment start<td>					
					<?php 
					$session_data = $this->session->userdata('logged_in');
	  	
	   				$userId = $session_data['id'];
					$utype = $session_data['usertype'];
					if($res1->userid == $userId || $utype == 'ADMIN'){
						//echo substr('abcdef', 0, 4); 
						echo '<a href="#" title="'.$res1->vcompany.'" style="color:#000;">'.substr($res1->vcompany, 0, 6).'</a>'; 
						}else{
						echo '***';
						}
					?>
					</td>
					<td>					
					<?php 
					if($res1->userid == $userId || $utype == 'ADMIN'){
						echo '<a href="#" title="'.$res1->vcontact.'">'.substr($res1->vcontact, 0, 7).'</a> '; 
						}else{
						echo '***';
						}
					?>
					</td>
					<td>					
					<?php 
					if($res1->userid == $userId || $utype == 'ADMIN'){
						echo '<a href="#" title="'.$res1->vemail.'">'.substr($res1->vemail, 0, 7).'</a> '; 
						}else{
						echo '***';
						}
					?>
					</td> Vendor details Comment end -->
					<td><?php echo $res1->crate; ?></td>
					<td><?php echo $res1->visatype; ?></td>
					<td>
					<?php 
					if($res1->userid == $userId || $utype == 'ADMIN'){
						echo $res1->srate; 
						}else{
						echo '***';
						}
					?>
					</td>
					<td onmouseover="document.getElementById('div<?php echo $i;?>').style.display = 'block';document.getElementById('div-<?php echo $i;?>').style.display = 'none';" onmouseout="document.getElementById('div<?php echo $i;?>').style.display = 'none';document.getElementById('div-<?php echo $i;?>').style.display = 'block';"><div id="div-<?php echo $i;?>" style="display: block;">View</div><div id="div<?php echo $i;?>" style="display: none;"><?php echo $res1->employerdetails; ?></div></td>


					<td>
						<a class="edit-status" data-id="<?=$res1->id?>" title="<?php echo $res1->status ?>"><?php echo substr($res1->status, 0, 6)?></a>
						<div class="status-option-section" id="status-option-section-<?=$res1->id?>"></div>
					</td>

					<td><a class="comment-section" data-id="<?=$res1->id?>" href="javascript:void(0);" onclick="PopupCenter('<?php echo base_url(); ?>index.php/totalsubmissions/comments/<?php echo $res1->id; ?>', 'myPop1',600,600);">Read / Add</a>
						<div class="comment-view-section" id="comment-view-section-<?=$res1->id?>">Loading...</div>
					</td>

					<td><a class="icon-1 info-tooltip" href="<?php echo base_url(); ?>index.php/totalsubmissions/editmysubmissions/<?php echo $res1->id;?>" title="EDIT" alt="EDIT" ></a><!--<a class="icon-2 info-tooltip" title="DELETE" alt="DELETE" href="<?php echo base_url(); ?>index.php/totalsubmissions/deletemysubmissions/<?php echo $res1->id;?>" onclick="return confirm('Are you sure you want to delete?')"></a>--></td>
				
				</tr>
			
				<?php 
				$i++;
				} ?>
				</table>
				<!--  end product-table................................... --> 
				<?php if($links){ echo "<ul id='pagination-digg'>".$links."</ul>"; } ?>
				
			</div>
			
			<!--  end content-table  -->
				<!--  start paging..................................................... -->

			<!--  end paging................ -->
			
			<div class="clear"></div>
		 
		</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<!--<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr> -->
	</table>

<div id="status-options" style="display: none;">
	<select name="status-options" id="status-options">
		<option value="">Select Option</option>
		<option value="Submitted to Client">Submitted to Client</option>
		<option value="Submitted to Vendor">Submitted to Vendor</option>
		<option value="Rejected (Reason: Comm Skills)">Rejected (Reason: Comm Skills)</option>
		<option value="Rejected (Reason: Position on Hold/Closed/Filled)">Rejected (Reason: Position on Hold/Closed/Filled)</option>
		<option value="Rejected (Reason: Technical Skills)">Rejected (Reason: Technical Skills)</option>
		<option value="Rejected (Reason: Rate Concern)">Rejected (Reason: Rate Concern)</option>
		<option value="Rejected (Reason: Location)">Rejected (Reason: Location)</option>
		<option value="Interview">Interview</option>
		<option value="Rejected After Interview">Rejected After Interview</option>
		<option value="Placement">Placement</option>
		<option value="Backout">Backout</option>
	</select>
	<span class="submit-option">SUBMIT</span>
	<span class="close-options">CLOSE</span>
</div>

<!-- Interview modal popup -->
<div class="interview_modal" id="submission-id-"></div>
<div class="interview_modal_box">
	<a href="#" class="interview_modal_close"></a>
	<div class="interview_inner_modal_box">
		<div id="interview-details-form">
			<h3>Add Interview Details:</h3>
			<div class="interview-date-row">
				<label>Select Date: </label>
				<input type="date" id="interview-date" name="interview-date" required/>
			</div>
			<div class="interview-time-row">
				<label>Select Time: </label>
				<input type="time" id="interview-time" name="interview-time" required/>
			</div>
			<div class="interview-timezone-row">
				<label>Time Zone: </label>
				<select id="timezone-of-interview" required>
					<option value="EST">EST</option>
					<option value="CST">CST</option>
					<option value="MST">MST</option>
					<option value="PST">PST</option>
				</select>	
			</div>
			<div class="interview-mode-row">
				<label>Mode of Interview: </label>
				<select id="mode-of-interview" required>
					<option value="Phone">Phone</option>
					<option value="Video">Video</option>
					<option value="inperson">In - Person</option>
				</select>	
			</div><br/>
			<!-- <input type="hidden" id="target-id" name="target-id" /> -->
			<button type="button" class="excel-button submit-interview-btn" id="submit-interview-details" name="interview-submit">Submit</button>
			<br />
			<label id="interview-error-message"></label>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?=base_url()?>js/mysubmissions.js"></script>

<?php $this->load->view('includes/footer'); ?>

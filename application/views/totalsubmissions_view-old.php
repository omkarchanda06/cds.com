<?php 
	$this->load->view('includes/header'); 
	error_reporting(0);
?>
<style type="text/css">
label	{
color: #F00;
line-height: 2px;
font-size: 12px;
position: absolute;
margin: -10px 0px 0px -130px;
}
.comment-view-section {
	margin: -32px 210px 0px 0px;
}

</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/tcal.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery/tcal.js"></script> 
<!--<script type="text/javascript" src="<?php echo base_url()?>js/jquery/jquery-1.4.1.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url()?>js/jquery/jquery.validate.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>js/custom.js"></script> 
<script>
$().ready(function() {
	// validate the comment form when it is submitted
	$("#exportForm").validate();
});
function PopupCenter(pageURL, title, w, h) {
var left 		= (screen.width/2)-(w/2);
var top 		= (screen.height/2)-(h/2);
var targetWin 	= window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}

function get_requiter(val){
	$.post("<?php echo base_url();?>index.php/recruitercount/requiterlist",{mid:val},function(result){
			//alert(result);
    	    $("#reqruiterid").html(result);
    	});	
}
function get_manager(val){
	$.post("<?php echo base_url();?>index.php/managercount/managerlist",{Dirid:val},function(result){
			//alert(result);
    	    $("#managerid").html(result);
    	});	
}

var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })() /*document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();*/
</script>

	<!--  start page-heading -->

	<?php  $session_data = $this->session->userdata('logged_in'); ?>

	<?php if($session_data['ptno'] == 'PT 005'): ?>
	<div style="float: right; padding: 5px 80px 0px 5px; font-size: 15px;">
			Total Resumes: <strong><?php echo $totalResumes; ?></strong>
	</div>
	<?php endif; ?>


	<div id="page-heading">
		<h1>Total Submissions [All]
        <?php
	  		$usertype = $session_data['usertype']; 
			$username = $session_data['username']; ?>
			<form style="float:right; padding: 4px 20px 4px 0px;" id="exportForm" action="<?php echo base_url()?>index.php/totalsubmissionsreport/" onsubmit="return v.exec()" name="registration" method="POST">
				<table><tr><td>
			<?php
			if($usertype == 'SUPERADMIN'){ ?>
				<select name="directorid" id="directorid" class="inp-form" onchange="get_manager(this.value);" style="padding:5px; width:150px;"><option value="">Select Director</option>
				<?php
				if (!empty($directors)) {
					foreach($directors as $dir){
						$directorid = $dir->id;
						$directorname = $dir->username;
						echo '<option value="'.$directorid.'">'.$directorname.'</option>';
					}
				}
				?></select></td><td>
			
				<select name="managerid" id="managerid" class="inp-form" onchange="get_requiter(this.value);" style="padding:5px; width:150px;"><option value="">Select Manager</option>
				<?php
				if (!empty($managerslist)) {
					foreach($managerslist as $mgr){
						$managerid = $mgr->manager_id;
						$managername = $this->user->get_manager_from_id($mgr->manager_id);
						echo '<option value="'.$managerid.'">'.$managername.'</option>';
					}
				}
				?>
				</select></td><td>
				
				<select name="reqruiterid" id="reqruiterid" class="inp-form" style="padding:5px; width:150px;">
				<option value="All">Select Recruiter</option>
				<?php
				if (!empty($recruiterlist)) {
					foreach($recruiterlist as $rcr){
						$recruiterid = $rcr->recruiter_id;
						$recruitername = $this->user->get_recruiter_from_id($rcr->recruiter_id);
						echo '<option value="'.$recruiterid.'">'.$recruitername.'</option>';
					}
				}elseif(!empty($recruiterID)){
					$recruitername = $this->user->get_recruiter_from_id($recruiterID);
					echo '<option value="'.$recruiterID.'" selected = selected>'.$recruitername.'</option>';
				}
				?>
				</select></td><td>
				
				<select name="status" class="inp-form" style="padding:5px; width:150px;">
					<option value="All">Select Status</option>
					<option value="Submitted to Vendor">Submitted to Vendor</option>
					<option value="Rejected">Rejected</option>
					<option value="Submitted to Client">Submitted to Client</option>
					<option value="Interview">Interview</option>
					<option value="Rejected after Interview">Rejected after Interview</option>
					<option value="Placement">Placement</option>
				</select></td>
				<td><input type="text" name="date1" id="date1" readonly 
						class="inp-form tcal required" placeholder="Select a Date"
						style="width:150px;"/></td>
				<td><input type="text" name="date2" id="date2" readonly 
						class="inp-form tcal required"  
						placeholder="Select a Date"
						style="width:150px;"/></td>
				<td><input type="submit" name="submit" value="submit" class="form-submit" />
				<!--<input type="button" name="submit" value="Generate PDF" style="background-color:#407195; font:13px calibri; padding:5px 10px; color:#ecf2f7; border-radius:5px; border:none; cursor:pointer;" onclick="generatePDF()" />--> 
				<a id="dlink"  style="display:none;"></a>
				<input type="button" name="submit" 
					value="Generate Excel" id="excel-button"       
					class="export" onclick="tableToExcel('product-table', 'Manager Count Table', 'TotalSubmission.xls')" />
				</td><?php }?></tr></table>
			</form></h1>
        
        </h1>
	</div>
	<!-- end page-heading -->

	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table" >
	<tr>
		<th rowspan="3" class="sized"><img src="<?php echo base_url()?>images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="<?php echo base_url()?>images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
		
			<!--  start table-content  -->
			<div id="table-content">
			  <!-- start product-table ..................................................................................... -->
				
				<table border="0" width="98%" cellpadding="0" cellspacing="0" id="product-table" class="target-table-total" style="font:13px Cambria;">
				<tr>
					
					<th class="table-header-repeat line-left minwidth-1" align="center" width="65">Date</th>
					<th class="table-header-repeat line-left minwidth-2" align="center" width="105">Consultant</th>
					<th class="table-header-repeat line-left minwidth-3" align="center" width="200">For Position</th>
					<th class="table-header-repeat line-left minwidth-4" align="center">Total IT</th>
					<th class="table-header-repeat line-left minwidth-5">End Client</th>
					<th class="table-header-options line-left minwidth-rashid">Loc</th>
					<th class="table-header-repeat line-left minwidth-5">Manager</th>
					<th class="table-header-options line-left minwidth-6">Submitted by</th>
						<?php 
					if($_SESSION['utype'] == 'DIRECTOR'){
						?>
						<th class="table-header-options line-left minwidth-6">Vendor Company</th>
						<th class="table-header-options line-left minwidth-6">Vendor Contact</th>
						<?php
						}
					?>
					
					<th class="table-header-options line-left minwidth-6">Consultant Rate</th>
					<th class="table-header-options line-left minwidth-7">Visa</th>
					<th class="table-header-options line-left minwidth-7">Relocate</th>
						<?php 
					if($_SESSION['utype'] == 'DIRECTOR'){
						?>
						<th class="table-header-options line-left minwidth-6">Submission Rate</th>
						<?php
						}
					?>
					
					<th class="table-header-options line-left minwidth-6" >Employer Details</th>
					<th class="table-header-options line-left minwidth-6">Status</th>
					<th class="table-header-options line-left minwidth-6">Comments</th>
                    <?php if($session_data['usertype'] == 'SUPERADMIN' || $session_data['id'] == 23){  ?>
					<th class="table-header-options line-left minwidth-6">Action</th>
					<?php } ?>
				</tr>
				
				
				
				<?php
				
				//rint_r($rows->result());exit;
				$i=1;
				$date='';
				$classInactive = "userinactive";
				foreach($rows->result() as $res1){
				//print_r($rows->result());exit;
				if($i%2==0){
				$class="alternate-row";
				
				}else{
				$class='';
				}
					if($date == ''){
						$date=$res1->sdate; 
					
					}
					
					if($date != $res1->sdate){
						$date=$res1->sdate; 
					echo '<tr><td colspan="25" class="gerua"></td></tr>';
					}
				?>
				<tr class='<?php echo $class;?> <?php if ($res1->view == 1) { echo $classInactive; } ?>'>
					<td>
					<?php //echo $res1['userid']; ?>
					<?php echo date("M",strtotime($res1->sdate)); ?>
					<?php echo date("d",strtotime($res1->sdate)); ?>
					 	 	 	 	 	 	 	 		 	 	 	

					</td>
					<td><a href="<?php echo base_url(); ?>/<?php echo $res1->resume; ?>" target="_blank" title="<?php echo $res1->cname ?>"><?php echo substr($res1->cname, 0, 10)?></a> </td>
					<td><a href="#" target="_blank" title="<?php echo $res1->psub ?>"><?php echo substr($res1->psub, 0, 12)?></a></td>
					<td><?php echo $res1->totalit; ?></td>
					<td><?php echo $res1->company; ?></td>
					<td><?php echo $res1->st; ?></td>
					<td><a href="#" target="_blank" title="<?php echo $res1->manager ?>"><?php echo substr($res1->manager, 0, 6)?></a></td>
					<td><a href="#" target="_blank" title="<?php echo $res1->recruiter ?>"><?php echo substr($res1->recruiter, 0, 12)?></a></td>
										
					<?php 
					if($_SESSION["utype"] == "ADMIN"){
						echo "<td>".$res1->vcompany."</td>"; 
						}
				
					if($_SESSION['utype'] == 'ADMIN'){
						echo "<td>".$res1->vcontact."</td>"; 
						}
					?>
					<td><a href="#" title="<?php echo $res1->crate; ?>"><?php echo substr($res1->crate,0,8); ?></a></td>
					<td><?php echo $res1->visatype; ?></td>
					<td><?php echo ($res1->relocate)? $res1->relocate : 'NA'; ?></td>
					<?php 
					if($_SESSION['utype'] == 'ADMIN'){
						echo "<td>".$res1->srate."</td>"; 
						}
					$view = $res1->view;
					if($view == 0){
						$view1 = 1;	
					} else {
						$view1 = 0;		
					}
					?>
					
					<td onmouseover="document.getElementById('div<?php echo $i;?>').style.display = 'block';document.getElementById('div-<?php echo $i;?>').style.display = 'none';" onmouseout="document.getElementById('div<?php echo $i;?>').style.display = 'none';document.getElementById('div-<?php echo $i;?>').style.display = 'block';"><div id="div-<?php echo $i;?>" style="display: block;">View</div><div id="div<?php echo $i;?>" style="display: none;"><?php echo $res1->employerdetails; ?></div></td>


					<td><a href="#" title="<?php echo $res1->status ?>"><?php echo substr($res1->status, 0, 6)?></a></td>

					<td>
						<a class="comment-section" data-id="<?=$res1->id?>" href="javascript:void(0);" 
						   onclick="PopupCenter('<?php echo base_url(); ?>index.php/totalsubmissions/comments/<?php echo $res1->id; ?>', 'myPop1',600,600);">Read / Add</a>
						<div class="comment-view-section" id="comment-view-section-<?=$res1->id?>">Loading...</div>
					</td>


					<?php if($session_data['usertype'] == 'SUPERADMIN' || $session_data['id'] == 23){ ?>
					<td class="options-width">

					<!-- <a title="Status" class="icon-5 info-tooltip inactiveuser"></a> -->

					<a href="<?php echo base_url()?>index.php/totalsubmissions/view/<?php echo $view1;?>/<?php echo $res1->id; ?>" title="Status" class="icon-5 info-tooltip inactiveuser"></a>

					<?php if($session_data['id'] != 23): ?>
					<a href="<?php echo base_url()?>index.php/totalsubmissions/deletesubmission/<?php echo $res1->id; ?>" title="Delete" class="icon-2 info-tooltip delete-submission"></a>
					<?php endif; ?>

					<?php if($session_data['id'] == 57){ ?>
					<a href="<?php echo base_url()?>index.php/totalsubmissions/editmysubmissions/<?php echo $res1->id; ?>" title="Edit" class="icon-1 info-tooltip"></a>

					</td>
					<?php } ?>

					<?php } ?>
				</tr>
			
				<?php 
				$i++;
				} ?>
				
				</table>
				<!--  end product-table................................... --> 
				
				<?php if($links){
					echo "<ul id=\"pagination-digg\">";
					echo $links;
					echo "</ul>";
				} ?>				
			</div>
			<!--  end content-table  -->
				<!--  start paging..................................................... -->

			
			<div class="clear"></div>
		 
		</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<!--<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr> -->
	</table>
<script type="text/javascript">
$('.delete-submission').click(function(e){
   e.preventDefault();
   var retVal = confirm("Are you sure, you want to delete this submission?");
   if(retVal == true){
      window.location.href=$(this).attr('href');
      return true;
   }else{
      return false;
   }
});
$('.inactiveuser').on('click', function() {
	$(this).closest('tr').css("background-color", "red");
    $(this).toggleClass('highlighted');
});
</script>	
<div id="status-options" style="display: none;">
	<select name="status-options" id="status-options">
		<option value="">Select Option</option>
		<option value="Submitted to Client">Submitted to Client</option>
		<option value="Submitted to Vendor">Submitted to Vendor</option>
		<option value="Rejected (Reason: Comm Skills)">Rejected (Reason: Comm Skills)</option>
		<option value="Rejected (Reason: Position on Hold/Closed/Filled)">Rejected (Reason: Position on Hold/Closed/Filled)</option>
		<option value="Rejected (Reason: Technical Skills)">Rejected (Reason: Technical Skills)</option>
		<option value="Rejected (Reason: Rate Concern)">Rejected (Reason: Rate Concern)</option>
		<option value="Rejected (Reason: Location)">Rejected (Reason: Location)</option>
		<option value="Interview">Interview</option>
		<option value="Placement">Placement</option>
		<option value="Backout">Backout</option>
	</select>
	<span class="submit-option">SUBMIT</span>
	<span class="close-options">CLOSE</span>
</div>	
<script type="text/javascript" src="<?=base_url()?>js/mysubmissions.js"></script>
<?php $this->load->view('includes/footer'); ?>


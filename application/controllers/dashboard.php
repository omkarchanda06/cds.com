<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(1);
class Dashboard extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('user', '', TRUE);
        $this->load->model('submission', '', TRUE);
        $this->load->model('manager', '', TRUE);
        $this->load->model('director', '', TRUE);
        $this->load->model('recruiter', '', TRUE);
        $this->load->library('pagination');
        $this->load->helper('url');
        $this->load->library('session');
        session_start();
    }

    function index()
    {
        $session_data = $this->session->userdata('logged_in');
        $usertype = $session_data['usertype'];
        $managerId = $session_data['id'];
        $dt = date('Y-m-d');
        //$dt = date('2022-06-18');
        $the_day_of_week = date("w", strtotime($dt)); //sunday is 0
        $fromDate = date("Y-m-d", strtotime($dt) - 60 * 60 * 24 * ($the_day_of_week) + 60 * 60 * 24 * 1);
        $toDate = date("Y-m-d", strtotime($fromDate) + 60 * 60 * 24 * 4);
        if ($this->session->userdata('logged_in')) {
            if ($usertype == "SUPERADMIN") {
                $submissions     = $this->submission->getAllSubmissionsOfRecruiters($fromDate, $toDate);
                $data['dates']         = $this->submission->getDatesArray($fromDate, $toDate);
                $data['subcount']     = $this->submission->createSubmissionsInfoArrayForRecruiter($submissions);
                $data['Dirs_Assodir_Mgrs'] = $this->director->getDirs_Assodir_Mgrs();
                $data['managers'] = $this->manager->getAllManagers();
                $data['directors'] = $this->director->getAllDirectors();
            } elseif ($usertype == "DIRECTOR") {
                $dirID = $session_data['id'];
                $manager_ids = $this->submission->getManagersByDirector($dirID);
                $data['dates'] = $this->submission->getDatesArray($fromDate, $toDate);
                $data['manager_ids'] = $manager_ids;
                $data['dirID'] = $dirID;
            } elseif ($usertype == "ASSOCIATEDIRECTOR") {
                $asscoiate_dirId      = $session_data['id'];
                $data['asscoiate_dirId'] = $asscoiate_dirId;
                $data['dates']         = $this->submission->getDatesArray($fromDate, $toDate);
                $submissions     = $this->submission->getAllSubmissionsOfRecruitersByManagerID($fromDate, $toDate, $asscoiate_dirId);
                $data['subcount']     = $this->submission->createSubmissionsInfoArrayForRecruiter($submissions);
            } elseif ($usertype == "MANAGER") {
                $managerId      = $session_data['id'];
                $data['managerId'] = $managerId;
                $data['dates']         = $this->submission->getDatesArray($fromDate, $toDate);
                $submissions     = $this->submission->getAllSubmissionsOfRecruitersByManagerID($fromDate, $toDate, $managerId);
                $data['subcount']     = $this->submission->createSubmissionsInfoArrayForRecruiter($submissions);
            } else {
                //$recruiter_name = $session_data['username'];
                $recruiter_id = $session_data['id'];
                //$recruiter_details = $this->recruiter->getRecruiterByName($recruiter_name);
                //$recruiter_id = $recruiter_details[0]->id;
                $submissions     = $this->submission->getAllSubmissionsOfRecruiter($fromDate, $toDate, $recruiter_id);
                $data['dates']         = $this->submission->getDatesArray($fromDate, $toDate);
                $data['subcount']     = $this->submission->createSubmissionsInfoArrayForRecruiter($submissions);
                $data['recruiter_id'] = $recruiter_id;
            }
            //print_r($submissions);
            // $data['dates'] 		= $this->submission->getDatesArray($fromDate, $toDate);	
            // $data['subcount'] 	= $this->submission->createSubmissionsInfoArrayForRecruiter($submissions);
            $this->load->view('dashboard_view', $data);
        } else {
            redirect('/');
        }
    }

    /*getting dates from ajax sending data*/

    function UpdateProgressBars()
    {
        $session_data = $this->session->userdata('logged_in');
        $usertype = $session_data['usertype'];
        //$managerId = $session_data['id'];
        $fromDate = $this->input->post('from_date');
        $toDate = $this->input->post('to_date');
        $recruiterArray = array();
        $totalDates                    = 0;
        $totalTargets                = 0;
        $submission_percent_arr = [];
        $submissionsbyDate            = array();
        $rejections_percent_arr = [];
        $placements_percent_arr = [];
        $nofeedbacks_percent_arr = [];
        $onholds_percent_arr = [];
        $interviews_percent_arr = [];
        $dates         = $this->submission->getAllSubmissions($fromDate, $toDate);
        if ($usertype == "SUPERADMIN") {
            $submissions     = $this->submission->getAllSubmissionsOfRecruiters($fromDate, $toDate);
            $dates = $this->submission->getDatesArray($fromDate, $toDate);
            foreach ($dates as $d) : $day1 = date("D", strtotime($d));
                $day2 = date("d M", strtotime($d));
                $totalDates++;
            endforeach;
            $subcount     = $this->submission->createSubmissionsInfoArrayForRecruiter($submissions);
            $managers = $this->manager->getAllManagers();
            foreach ($managers as $manager) :
                $recruiters = $this->recruiter->getRecruitersByManagerID($manager->id);
                foreach ($recruiters as $recruiter) :
                    $recruiterArray[$recruiter->id] = $recruiter->recruiter;
                    $targetResult    = $this->recruiter->getRecruiterTotalTarget($recruiter->id, $fromDate, $toDate, $totalDates);
                    $targets         = $targetResult['sumtarget'];
                    $targets         = ($targets) ? ($targets / 5) * $totalDates : 0;
                    $totalTargets = $totalTargets + $targets;
                    /**Recruiter Submissions by Date **/
                    $submissionsOfAllDates = 0;
                    foreach ($dates as $d) :
                        if ($this->recruiter->getRecruiterAttendance($recruiter->recruiter, $d) != '') :
                        else : ($subcount[$recruiter->id][$d]['submissioncount'] ? $subcount[$recruiter->id][$d]['submissioncount'] : 0);
                        endif;
                        $submissionsbyDate[$d]         = $submissionsbyDate[$d] + $subcount[$recruiter->id][$d]['submissioncount'];
                        $submissionsOfAllDates        = $submissionsOfAllDates + $subcount[$recruiter->id][$d]['submissioncount'];
                    endforeach;

                    /*onholds*/
                    if (!isset($subcount[$recruiter->id]['onholds'])) $subcount[$recruiter->id]['onholds'] = 0;
                    array_push($onholds_percent_arr, $subcount[$recruiter->id]['onholds']);

                    /*No feedbacks*/
                    if (!isset($subcount[$recruiter->id]['nofeedbacks'])) $subcount[$recruiter->id]['nofeedbacks'] = 0;
                    array_push($nofeedbacks_percent_arr, $subcount[$recruiter->id]['nofeedbacks']);

                    /*Interviews*/
                    if (!isset($subcount[$recruiter->id]['interviews'])) $subcount[$recruiter->id]['interviews'] = 0;
                    array_push($interviews_percent_arr, $subcount[$recruiter->id]['interviews']);

                    /*Rejection*/
                    if (!isset($subcount[$recruiter->id]['rejections'])) $subcount[$recruiter->id]['rejections'] = 0;
                    array_push($rejections_percent_arr, $subcount[$recruiter->id]['rejections']);

                    /*Placement*/
                    if (!isset($subcount[$recruiter->id]['placements'])) $subcount[$recruiter->id]['placements'] = 0;
                    array_push($placements_percent_arr, $subcount[$recruiter->id]['placements']);

                    /*submissions */
                    //$submissionsPercentage = $this->user->calculatePercentage($submissionsOfAllDates, $targetResult['targets']);
                    array_push($submission_percent_arr, $submissionsOfAllDates);

                endforeach;
            endforeach;
        } elseif ($usertype == "DIRECTOR") {
            $dirID = $session_data['id'];
            $manager_list_ids = $this->submission->getManagersByDirector($dirID);
            foreach ($manager_list_ids as $managerId) {
                $submissions     = $this->submission->getAllSubmissionsOfRecruitersByManagerID($fromDate, $toDate, $managerId);
            }
        } elseif ($usertype == "MANAGER") {
            $managerId = $session_data['id'];
            $submissions     = $this->submission->getAllSubmissionsOfRecruitersByManagerID($fromDate, $toDate, $managerId);
            $dates         = $this->submission->getDatesArray($fromDate, $toDate);
            $subcount     = $this->submission->createSubmissionsInfoArrayForRecruiter($submissions);

            foreach ($dates as $d) : $day1 = date("D", strtotime($d));
                $day2 = date("d M", strtotime($d));
                $totalDates++;
            endforeach;

            $recruiters = $this->recruiter->getRecruitersByManagerID($managerId);

            //print_r($recruiters);
            foreach ($recruiters as $recruiter) :
                $recruiterArray[$recruiter->id] = $recruiter->recruiter;
                $targetResult    = $this->recruiter->getRecruiterTotalTarget($recruiter->id, $fromDate, $toDate, $totalDates);
                $targets         = $targetResult['sumtarget'];
                $targets         = ($targets) ? ($targets / 5) * $totalDates : 0;
                $totalTargets = $totalTargets + $targets;
                /**Recruiter Submissions by Date **/
                $submissionsOfAllDates = 0;
                foreach ($dates as $d) :

                    if ($this->recruiter->getRecruiterAttendance($recruiter->recruiter, $d) != '') :
                    else :
                        $subcount[$recruiter->id][$d]['submissioncount'] ? $subcount[$recruiter->id][$d]['submissioncount'] : 0;
                    endif;
                    $submissionsbyDate[$d]         = $submissionsbyDate[$d] + $subcount[$recruiter->id][$d]['submissioncount'];
                    $submissionsOfAllDates        = $submissionsOfAllDates + $subcount[$recruiter->id][$d]['submissioncount'];
                endforeach;

                /*Rejection count*/
                if (!isset($subcount[$recruiter->id]['rejections'])) $subcount[$recruiter->id]['rejections'] = 0;
                array_push($rejections_percent_arr, $subcount[$recruiter->id]['rejections']);

                /*Placement*/
                if (!isset($subcount[$recruiter->id]['placements'])) $subcount[$recruiter->id]['placements'] = 0;
                array_push($placements_percent_arr, $subcount[$recruiter->id]['placements']);

                /*onholds*/
                if (!isset($subcount[$recruiter->id]['onholds'])) $subcount[$recruiter->id]['onholds'] = 0;
                array_push($onholds_percent_arr, $subcount[$recruiter->id]['onholds']);

                /*No feedbacks*/
                if (!isset($subcount[$recruiter->id]['nofeedbacks'])) $subcount[$recruiter->id]['nofeedbacks'] = 0;
                array_push($nofeedbacks_percent_arr, $subcount[$recruiter->id]['nofeedbacks']);

                /*Interviews*/
                if (!isset($subcount[$recruiter->id]['interviews'])) $subcount[$recruiter->id]['interviews'] = 0;
                array_push($interviews_percent_arr, $subcount[$recruiter->id]['interviews']);

                //$submissionsPercentage = $this->user->calculatePercentage($submissionsOfAllDates, $targetResult['targets']);
                array_push($submission_percent_arr, $submissionsOfAllDates);
            endforeach;
        } elseif ($usertype == "TA ASSOCIATE") {
            $recruiter_id = $session_data['id'];
            //$recruiter_name = $session_data['username'];
            //$recruiter_details = $this->recruiter->getRecruiterByName($recruiter_name);
            //$recruiter_id = $recruiter_details[0]->id;
            $submissions     = $this->submission->getAllSubmissionsOfRecruiter($fromDate, $toDate, $recruiter_id);
            $dates         = $this->submission->getDatesArray($fromDate, $toDate);
            $subcount     = $this->submission->createSubmissionsInfoArrayForRecruiter($submissions);
            foreach ($dates as $d) : $day1 = date("D", strtotime($d));
                $day2 = date("d M", strtotime($d));
                $totalDates++;
            endforeach;
            if (!empty($subcount)) {
                foreach ($subcount as $subcount_details) {
                    //print_r($subcount_details);
                    $targetResult    = $this->recruiter->getRecruiterTotalTarget($recruiter_id, $fromDate, $toDate, $totalDates);
                    $targets         = $targetResult['sumtarget'];
                    $targets         = ($targets) ? ($targets / 5) * $totalDates : 0;
                    $totalTargets = $totalTargets + $targets;
                    $submissionsOfAllDates = 0;
                    $recruiter_name = $session_data['username'];
                    foreach ($dates as $d) {
                        //echo $subcount_details[$d]['submissioncount'];  
                        if ($this->recruiter->getRecruiterAttendance($recruiter_name, $d) != '') :
                        else : ($subcount_details[$d]['submissioncount'] ? $subcount_details[$d]['submissioncount'] : 0);
                        endif;
                        $submissionsbyDate[$d]         = $submissionsbyDate[$d] + $subcount_details[$d]['submissioncount'];
                        $submissionsOfAllDates        = $submissionsOfAllDates + $subcount_details[$d]['submissioncount'];
                    }

                    /*onholds*/
                    if (!isset($subcount_details['onholds'])) $subcount_details['onholds'] = 0;
                    array_push($onholds_percent_arr, $subcount_details['onholds']);

                    /*No feedbacks*/
                    if (!isset($subcount_details['nofeedbacks'])) $subcount_details['nofeedbacks'] = 0;
                    array_push($nofeedbacks_percent_arr, $subcount_details['nofeedbacks']);

                    /*Rejection*/
                    if (!isset($subcount_details['rejections'])) $subcount_details['rejections'] = 0;
                    array_push($rejections_percent_arr, $subcount_details['rejections']);

                    /*Placement*/
                    if (!isset($subcount_details['placements'])) $subcount_details['placements'] = 0;
                    array_push($placements_percent_arr, $subcount_details['placements']);

                    /*Interviews*/
                    if (!isset($subcount_details['interviews'])) $subcount_details['interviews'] = 0;
                    array_push($interviews_percent_arr, $subcount_details['interviews']);

                    /*submissions */
                    //$submissionsPercentage = $this->user->calculatePercentage($submissionsOfAllDates, $targetResult['targets']);
                    array_push($submission_percent_arr, $submissionsOfAllDates);
                }
            } else {
                $submission_percent_arr = 0;
                $rejections_percent_arr = 0;
                $placements_percent_arr = 0;
                $nofeedbacks_percent_arr = 0;
                $onholds_percent_arr = 0;
                $interviews_percent_arr = 0;
            }
        } else {
            $submission_percent_arr = 0;
            $rejections_percent_arr = 0;
            $placements_percent_arr = 0;
            $nofeedbacks_percent_arr = 0;
            $onholds_percent_arr = 0;
            $interviews_percent_arr = 0;
        }

        if (is_nan(array_sum($submission_percent_arr) / $totalTargets * 100) && is_null(array_sum($rejections_percent_arr)) && is_null(array_sum($placements_percent_arr)) && is_null(array_sum($nofeedbacks_percent_arr)) && is_null(array_sum($onholds_percent_arr)) && is_null(array_sum($interviews_percent_arr))) {
            $sub_percent_sum = 0;
            $rejections_percent_count = 0;
            $placement_percent_count = 0;
            $nofeedbacks_percent_count = 0;
            $onholds_percent_count = 0;
            $interviews_percent_count = 0;
        } else {
            $sub_percent_sum = array_sum($submission_percent_arr) / $totalTargets * 100;
            $rejections_percent_count = array_sum($rejections_percent_arr);
            $placement_percent_count = array_sum($placements_percent_arr);
            $nofeedbacks_percent_count = array_sum($nofeedbacks_percent_arr);
            $onholds_percent_count = array_sum($onholds_percent_arr);
            $interviews_percent_count = array_sum($interviews_percent_arr);
        }

        $result = array("status" => "success", "total_sub_percent" => $sub_percent_sum, "total_rejections_percent" => $rejections_percent_count, "total_placements_percent" => $placement_percent_count, "total_nofeedbacks_percent" => $nofeedbacks_percent_count, "total_onholds_percent" => $onholds_percent_count, "total_interviews_percent" => $interviews_percent_count);
        echo json_encode($result);
    }

    function logout()
    {
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('/', 'refresh');
    }

    /*change pwd function*/
    function changepwd()
    {
        $data['error'] = '';
        if ($this->session->userdata('logged_in')) {
            $data["menuname"] = 'profile';
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['emailaddress'] = $session_data['emailaddress'];
            $data['ptno'] = $session_data['ptno'];
            $this->load->library('form_validation');
            $this->form_validation->set_rules('username', 'User Name', 'required');
            $this->form_validation->set_rules('emailaddress', 'Email Address', 'required');
            $this->form_validation->set_rules('ptno', 'PT No', 'required');
            //$this->form_validation->set_rules('oldpwd', 'Old Password', 'required');
            //$this->form_validation->set_rules('newpwd', 'New Password', 'required');
            //$this->form_validation->set_rules('cpwd', 'Confirm Password', 'required');

            if ($this->form_validation->run() == FALSE) {
                //Field validation failed.  User redirected to login page
                //$this->load->view('cdslogpage_view');
                //$result = $this->user->change_pwd($data);
                $this->load->view('changepwd_view', $data);
            } else {
                $username = $data['username'];
                //Go to private area
                $emailaddress = $this->input->post('emailaddress');
                $email = explode("@", $emailaddress);

                if ($email[1] == "panzersolutions.com") {
                    $this->db->select('*');
                    $this->db->where('username', $username);
                    $query = $this->db->get('users');

                    foreach ($query->result() as $key) {
                        $id = $key->id;
                        $password = $key->password;
                    }
                    if (md5($this->input->post('oldpwd')) == $password) {
                        $emailaddress = $this->input->post('emailaddress');
                        $ptno = $this->input->post('ptno');
                        $cpassword = md5($this->input->post('cpwd'));
                        $newpassword = md5($this->input->post('newpwd'));
                        if ($newpassword == $cpassword) {
                            $data = array(
                                'password' => $newpassword,
                                'email' => $emailaddress,
                                'ptno' => $ptno
                            );

                            $this->db->where('id', $id);
                            $this->db->update('users', $data);
                            $data['username'] = $username;
                            $data['emailaddress'] = $emailaddress;
                            $data['ptno'] = $ptno;

                            $data['error'] = 'Profile and password changed successfully';

                            $this->session->unset_userdata('logged_in');
                            session_destroy();
                            $this->load->view('updatepassword', $data);
                        } else {

                            $data['error'] = 'Passwords are not matching, Please try again!';
                            $this->load->view('changepwd_view', $data);
                        }
                    } else {
                        $emailaddress = $this->input->post('emailaddress');
                        $ptno = $this->input->post('ptno');
                        $data = array(
                            'email' => $emailaddress,
                            'ptno' => $ptno
                        );

                        $this->db->where('id', $id);
                        $this->db->update('users', $data);
                        $data['username'] = $username;
                        $data['emailaddress'] = $emailaddress;
                        $data['ptno'] = $ptno;

                        $data['error'] = 'Profile update successfully';

                        $this->session->unset_userdata('logged_in');
                        session_destroy();
                        $this->load->view('updatepassword', $data);
                    }
                } else {
                    $ptno = $this->input->post('ptno');
                    $data['username'] = $username;
                    $data['ptno'] = $ptno;
                    $data['error'] = 'ERROR : Please use only @panzersolutions.com email address';
                    $this->load->view('changepwd_view', $data);
                }
                //redirect('totalsubmissions', 'refresh');
            }
        } else {
            //If no session, redirect to ctslogpge page
            redirect('cdslogpage', 'refresh');
        }
    }

    function all_recruiters()
    {
        $session_data = $this->session->userdata('logged_in');
        $usertype = $session_data['usertype'];

        if ($usertype != "ADMIN" && $usertype != "SUPERADMIN") {
            redirect('dashboard/changepwd', 'refresh');
        }
        $data["recruiters"] = $this->user->get_all_recruiters();
        $this->load->view('all_recruiters_view', $data);
    }

    function update_recruiter()
    {

        header('Content-Type: application/json');

        if (isset($_POST['target'])) {
            $managerID = $_POST['managerid'];
            $target = $_POST['target'];
            $status = $_POST['status'];
            //$ptno   = $_POST['ptno'];
            $id = $_POST['id'];
        }

        $getManagerName = $this->user->get_manager_from_id($managerID);


        $data = array(
            'mid'     => $managerID,
            'targets' => $target,
            'status'  => $status,
            //'ptno'    => $ptno
        );
        // print_r($data);exit;
        $update_recruiter_data = $this->user->update_recruiter_data($data, $id);
        if ($update_recruiter_data == "updated") {
            echo json_encode(array(
                'status' => 'success',
                'managername' => $getManagerName,
                'targets' => $target,
                'recstatus' => $status,
                //'ptno'    => $ptno,
                'id' => $id
            ));
        }
    }

    function recruiters()
    {
        if ($this->session->userdata('logged_in')) {
            $data['menuname'] = 'recruiters';
            $data['category'] = 'My Recruiters';
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['error'] = '';
            $data['row'] = $this->user->recruiters_data();
            $this->load->view('recruiters_view', $data);
        } else {
            //If no session, redirect to ctslogpge page
            redirect('cdslogpage', 'refresh');
        }
    }
}

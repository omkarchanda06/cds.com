<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class resumedatabase extends CI_Controller {

  function __construct()
  {
    parent::__construct();
	$this->load->helper('url');
	$this->load->library('session');
  $this->load->model('user','',TRUE);
  }

  function index()
  {
  	if($this->session->userdata('logged_in')){
      $session_data = $this->session->userdata('logged_in');
      
      $data['key1']=$this->user->get_addsub_users_location(1);
      $data['visatypes']=$this->user->get_addsub_users_visa(1);
      $data['searchtype'] = $this->uri->segment(1);
      $data['category'] = 'All';
     // var_dump($session_data['category']);die();
  		$this->load->view('resumedatabase/index', $data);
  	}
    //$this->load->helper('form');
    //$this->load->view('cdslogpage_view');
  }

  function logout()
  {
    $this->session->unset_userdata('logged_in');
    session_destroy();
    $this->load->view('cdslogpage_view');
  }

}

?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(1);
class Client extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('user','',TRUE);
        $this->load->model('clientdetails','',TRUE);
        $this->load->library('pagination');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('session');
        session_start();
    }

    function index() {
        $remove_access_users = array("HR", "MIS", "RECRUITER", "ACCOUNTSEXECUTIVE");
        $session_data = $this->session->userdata('logged_in');
        $usertype = $session_data['usertype'];
        if($this->session->userdata('logged_in') && !in_array($usertype, $remove_access_users)) {
        // $session_data         = $this->session->userdata('logged_in');
        // $usertype = $session_data['usertype']; 
        $data['id']     = $session_data['id'];
        if ($usertype == "SUPERADMIN") {
            $data['clientdetails'] 	= $this->clientdetails->getAllClientsDetails();
        } else {
            $data['clientdetails'] 	= $this->clientdetails->getAllClientsDetailsByID($data['id']);
        }
        $this->load->view('clients_view', $data);
        }else{
        redirect('cdslogpage', 'refresh');
        }
    }

    function clientform() {
        $remove_access_users = array("HR", "MIS", "RECRUITER", "ACCOUNTSEXECUTIVE");
        $session_data = $this->session->userdata('logged_in');
        $usertype = $session_data['usertype'];
        if($this->session->userdata('logged_in') && !in_array($usertype, $remove_access_users)) {
            // $session_data         = $this->session->userdata('logged_in');
            $data['username']     = $session_data['username'];
            $this->load->view('add-client-view', $data);
        } else {
            redirect('cdslogpage', 'refresh');
        }
    }

    function addclient(){
        $this->form_validation->set_rules('client_name','Client Name','trim|required');
        $this->form_validation->set_rules('client_company','Client Company','trim|required');
        $this->form_validation->set_rules('client_contact','Client Contact','trim|required');
        $this->form_validation->set_rules('client_email','Client Email','trim|required');
        $this->form_validation->set_rules('client_status','Status','required');
        $this->form_validation->set_rules('comments','Comments','trim|required');

        if($this->form_validation->run() == FALSE){
            $data['message']	  = 'Please check for the errors';
            $session_data         = $this->session->userdata('logged_in');
	        $data['username']     = $session_data['username'];
		    $this->load->view('add-client-view', $data);
        }else{
            $session_data = $this->session->userdata('logged_in');
            $user_id = $session_data['id'];
            $client_name = trim($this->input->post('client_name'));
            $client_company = trim($this->input->post('client_company'));
            $client_contact = trim($this->input->post('client_contact'));
            $client_email = $this->input->post('client_email');
            $client_comments = trim($this->input->post('comments'));
            $client_status = trim($this->input->post('client_status'));

            $data = array(
				'user_id' => $user_id, 
				'client_name' => $client_name,
                'client_company' => $client_company,
                'client_contact' => $client_contact,
                'client_email' => $client_email,
                'client_status' => $client_status,
                'comments' => $client_comments
			);
            $data['result'] = $this->clientdetails->insert_client_details($data);
            $this->session->set_flashdata('updatemessage', $data['result']);
            redirect('client/clientform');
            //$this->load->view('add-client-view', $result);
        }
    }

    function edit($id) {
        $remove_access_users = array("HR", "MIS", "RECRUITER", "ACCOUNTSEXECUTIVE");
        $session_data = $this->session->userdata('logged_in');
        $usertype = $session_data['usertype'];
        if($this->session->userdata('logged_in') && !in_array($usertype, $remove_access_users)) {
            $session_data         = $this->session->userdata('logged_in');
            $data['clientdetails']  	= $this->clientdetails->getClientDetailsByID($id);
            //$data['result']  	= "Client details updated successfully";
            $this->load->view('edit-clientdetails-view', $data);
        } else {
            redirect('cdslogpage', 'refresh');
        } 
   }

   function update() {

        $this->form_validation->set_rules('client_name','Client Name','trim|required');
        $this->form_validation->set_rules('client_company','Client Company','trim|required');
        $this->form_validation->set_rules('client_contact','Client Contact','trim|required');
        $this->form_validation->set_rules('client_email','Client Email','trim|required');
        $this->form_validation->set_rules('client_status','Status','required');
        $this->form_validation->set_rules('comments','Comments','trim|required');
        $id = $this->input->post('id');
        if($this->form_validation->run() == FALSE) {	
            $data['message']	  = 'Please check for the errors';
            $session_data         = $this->session->userdata('logged_in');
            $data['clientdetails']  	= $this->clientdetails->getClientDetailsByID($id);
            $data['menuname']     = 'jobtitle';
            $this->load->view('edit-clientdetails-view', $data);
        } else {
            $client_name = trim($this->input->post('client_name'));
            $client_company = trim($this->input->post('client_company'));
            $client_contact = trim($this->input->post('client_contact'));
            $client_email = $this->input->post('client_email');
            $client_status = trim($this->input->post('client_status'));
            $client_comments = trim($this->input->post('comments'));

            $data = array(
				'client_name' => $client_name,
                'client_company' => $client_company,
                'client_contact' => $client_contact,
                'client_email' => $client_email,
                'client_status' => $client_status,
                'comments' => $client_comments
			);
            $data['updatemessage'] = $this->clientdetails->updateclientdetails($id, $data);
            //$this->load->view('add-client-view', $result);
            $this->session->set_flashdata('updatemessage', $data['updatemessage']);
            redirect('client/clientform');
        }
    }
}
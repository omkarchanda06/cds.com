<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(1);
class totalsubmissions extends CI_Controller {

  function __construct()
  {
    parent::__construct();
	$this->load->model('user','',TRUE);
	$this->load->model('submission','',TRUE);
	$this->load->library('pagination');
	$this->load->helper('url');
	$this->load->library('session');
	$this->load->model('recruiter','',TRUE);
	$this->load->library('form_validation');
	session_start();
  }

 
	function index() {
		//print_r('hi');exit;
		if($this->session->userdata('logged_in')) {
		
			$session_data = $this->session->userdata('logged_in');

			//Data by Category
			$usertype = $session_data['usertype']; 

			if($usertype == "SUPERADMIN"){
				$data['key1']=$this->user->addsub_users_location();
				$data['visatypes']=$this->user->addsub_users_visa();
			} else {
				$data['key1']=$this->user->get_addsub_users_location($session_data['category']);
				$data['visatypes']=$this->user->get_addsub_users_visa($session_data['category']);
			}
			//End of Data by Category

			//$data['key1']=$this->user->addsub_users_location();
			$data['roles'] = $this->session->userdata('roles');

			$data['category'] = 'All';
			
			$config = array();
			$config["base_url"] = base_url() . "index.php/totalsubmissions/index/";		

			// Submission count based on the USER Category
			$usertype = $session_data['usertype']; 
			if($usertype == "SUPERADMIN") {
				$coutsub = $this->user->submissions_count();
			} else {
				$userCategory = $session_data['category'];
				$data['category'] = $this->user->getCategoryName($userCategory);	
				$coutsub = $this->user->get_submissions_count($userCategory);
			}

			foreach ($coutsub as $cs){
				$cs = $cs->countsub;
			}
			$config["total_rows"] = $cs;
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			
			// Next Link
			$config['next_link'] = '&raquo;';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			
			// Previous Link
			$config['prev_link'] = '&laquo;';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			// Current Link
			$config['cur_tag_open'] = '<li class="active">';
			$config['cur_tag_close'] = '</li>';
			
			// Digit Link
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			
			$config["per_page"] = 30;
			$config["uri_segment"] = 3;
			
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			
			// All submission based on the USER Category
			$usertype = $session_data['usertype'];
			$results = $this->user->submissions_result($config["per_page"], $page);
			//print_r($results); die;
			$data["links"] = $this->pagination->create_links();
			$data['rows'] = $results;
			$data['username'] = $session_data['username'];
			$data['searchtype'] = $this->uri->segment(1);
			//$data['rows'] = $this->user->submissions_result();
			// print_r($_SESSION);exit;


			// All submission based on the USER Category
			
			if($usertype == "SUPERADMIN") {
				$data["directors"] = $this->user->get_directors();
			} elseif($usertype == "DIRECTOR") {
				$userCategory = $session_data['category'];
				$directorId = $session_data['id']; 
				$data["managerslist"] = $this->user->get_managers($directorId);
			} elseif($usertype == "MANAGER"){
				$managerId = $session_data['id'];
				$data["recruiterlist"] = $this->user->get_reqruiter($managerId);
			}else{
				$data['recruiterID'] = $session_data['id'];
			}

			//$data['userid'] = $session_data['id'];

			$data["menuname"] = 'submissions';
			$data['totalResumes'] = $this->user->totalsubmissions();
			$this->load->view('totalsubmissions_view', $data);	
		
		}
		else
		{
		//If no session, redirect to login page
		redirect('cdslogpage', 'refresh');
		}
		//$this->load->view('totalsubmissions_view');
	}

  
	function total_panzer_submissions() {
		if($this->session->userdata('logged_in')) {

		$session_data = $this->session->userdata('logged_in');
		$usertype = $session_data['usertype']; 

		if($usertype != "SUPERADMIN") {
			redirect('dashboard/changepwd', 'refresh');
		}
		
		$data['category'] = 'Panzer';
		$data['categorytype'] = 1;
		$session_data = $this->session->userdata('logged_in');
		// $data['key1']=$this->user->addsub_users_location();
		$data['key1']=$this->user->get_addsub_users_location(1);
		$data['roles'] = $this->session->userdata('roles');

		$data['visatypes']=$this->user->get_addsub_users_visa(1);
			
			$config = array();
			$config["base_url"] = base_url() . "index.php/totalsubmissions/index/";
			$coutsub = $this->user->get_submissions_count(1);
			foreach ($coutsub as $cs){
				$cs = $cs->countsub;
			}
			$config["total_rows"] = $cs;
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			
			// Next Link
			$config['next_link'] = '&raquo;';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			
			// Previous Link
			$config['prev_link'] = '&laquo;';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			// Current Link
			$config['cur_tag_open'] = '<li class="active">';
			$config['cur_tag_close'] = '</li>';
			
			// Digit Link
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			
			$config["per_page"] = 30;
			$config["uri_segment"] = 3;
			
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$results = $this->user->get_submissions_result($config["per_page"], $page, 1);
			//print_r($results); die;
			$data["links"] = $this->pagination->create_links();
			$data['rows'] = $results;
		
			$data['username'] = $session_data['username'];
			$data['searchtype'] = $this->uri->segment(1);
			//$data['rows'] = $this->user->submissions_result();
			// print_r($_SESSION);exit;
			$data["manager"] = $this->user->get_manager_category(1);
			$data['userid'] = $session_data['id'];

			$data["menuname"] = 'submissions';
			$data['totalResumes'] = $this->user->totalsubmissions();
			$this->load->view('totalsubmissions_view', $data);	
		
		}
		else {
			//If no session, redirect to login page
			redirect('cdslogpage', 'refresh');
		}
			//$this->load->view('totalsubmissions_view');
	}



    function total_rpo_submissions()
  {
  	//print_r('hi');exit;
    if($this->session->userdata('logged_in'))
    {

	  $session_data = $this->session->userdata('logged_in');
	  $usertype = $session_data['usertype']; 


	  if($usertype != "SUPERADMIN") {
         redirect('cdsdashboard/changepwd', 'refresh');
       }

       
	  $data['category'] = 'RPO';
	  $data['categorytype'] = 2;
	  $session_data = $this->session->userdata('logged_in');
	  // $data['key1']=$this->user->addsub_users_location();
	  $data['key1']=$this->user->get_addsub_users_location(2);
	  $data['roles'] = $this->session->userdata('roles');

	  $data['visatypes']=$this->user->get_addsub_users_visa(2);
		
		$config = array();
		$config["base_url"] = base_url() . "index.php/totalsubmissions/index/";
		$coutsub = $this->user->get_submissions_count(2);
		foreach ($coutsub as $cs){
			$cs = $cs->countsub;
		}
		$config["total_rows"] = $cs;
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		// Next Link
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		// Previous Link
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		// Current Link
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		// Digit Link
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config["per_page"] = 30;
        $config["uri_segment"] = 3;
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $results = $this->user->get_submissions_result($config["per_page"], $page, 2);
		//print_r($results); die;
		$data["links"] = $this->pagination->create_links();
		$data['rows'] = $results;
	  
      	$data['username'] = $session_data['username'];
		$data['searchtype'] = $this->uri->segment(1);
      	//$data['rows'] = $this->user->submissions_result();
	 	// print_r($_SESSION);exit;
		$data["manager"] = $this->user->get_manager_category(2);
		$data['userid'] = $session_data['id'];

		$data["menuname"] = 'submissions';
		$data['totalResumes'] = $this->user->totalsubmissions();
	  	$this->load->view('totalsubmissions_view', $data);	
	
    }
    else
    {
      //If no session, redirect to login page
      redirect('cdslogpage', 'refresh');
	}
	//$this->load->view('totalsubmissions_view');
  }



      function total_canada_submissions() {
  	//print_r('hi');exit;
    if($this->session->userdata('logged_in'))
    {

	  $session_data = $this->session->userdata('logged_in');
	  $usertype = $session_data['usertype']; 

	  if($usertype != "SUPERADMIN") {
         redirect('cdsdashboard/changepwd', 'refresh');
       } 

	  $data['category'] = 'Canada';
	  $data['categorytype'] = 3;
	  $session_data = $this->session->userdata('logged_in');
	  // $data['key1']=$this->user->addsub_users_location();
	  $data['key1']=$this->user->get_addsub_users_location(3);
	  $data['roles'] = $this->session->userdata('roles');

	  $data['visatypes']=$this->user->get_addsub_users_visa(3);
		
		$config = array();
		$config["base_url"] = base_url() . "index.php/totalsubmissions/index/";
		$coutsub = $this->user->get_submissions_count(3);
		foreach ($coutsub as $cs){
			$cs = $cs->countsub;
		}
		$config["total_rows"] = $cs;
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		// Next Link
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		// Previous Link
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		// Current Link
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		// Digit Link
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config["per_page"] = 30;
        $config["uri_segment"] = 3;
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $results = $this->user->get_submissions_result($config["per_page"], $page, 3);
		//print_r($results); die;
		$data["links"] = $this->pagination->create_links();
		$data['rows'] = $results;
	  
      	$data['username'] = $session_data['username'];
		$data['searchtype'] = $this->uri->segment(1);
      	//$data['rows'] = $this->user->submissions_result();
	 	// print_r($_SESSION);exit;
		$data["manager"] = $this->user->get_manager_category(3);
		$data['userid'] = $session_data['id'];

		$data["menuname"] = 'submissions';
		$data['totalResumes'] = $this->user->totalsubmissions();
	  	$this->load->view('totalsubmissions_view', $data);	
	
    }
    else
    {
      //If no session, redirect to login page
      redirect('cdslogpage', 'refresh');
	}
	//$this->load->view('totalsubmissions_view');
  }




  function view(){
	$view = $this->uri->segment(3); 
	$submisionid = $this->uri->segment(4); 
	//echo $submisionid; exit;
	$updateview = $this->user->updateview($view, $submisionid);
	redirect('totalsubmissions', 'refresh');
  }

  
  function usumission(){
	 if($this->session->userdata('logged_in'))
    {
	  
	  $session_data = $this->session->userdata('logged_in');
	  $usertype = $session_data['usertype']; 

	  if($usertype != "SUPERADMIN"){
	  redirect('cdsdashboard/changepwd', 'refresh');
	  }  

	  $data['visatypes']=$this->user->addsub_users_visa();

	  $session_data = $this->session->userdata('logged_in');
	  $data['key1']=$this->user->addsub_users_location();
	  $data['roles'] = $this->session->userdata('roles');
		
		$config = array();
		$config["base_url"] = base_url() . "index.php/totalsubmissions/usumission/index/";
		$coutsub = $this->user->usubmissions_count();
		foreach ($coutsub as $cs){
			$cs = $cs->countsub;
		}
		$config["total_rows"] = $cs;
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		// Next Link
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		// Previous Link
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		// Current Link
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		// Digit Link
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config["per_page"] = 30;
        $config["uri_segment"] = 3;
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $results = $this->user->usubmissions_result($config["per_page"], $page);
		//print_r($results); die;
		$data["links"] = $this->pagination->create_links();
		$data['rows'] = $results;
	  
      	$data['username'] = $session_data['username'];
		$data['searchtype'] = $this->uri->segment(1);
      	//$data['rows'] = $this->user->submissions_result();
	 	// print_r($_SESSION);exit;
		$data["manager"] = $this->user->get_manager();
		$data['userid'] = $session_data['id'];
		$data["menuname"] = 'submissions';
		$data['totalResumes'] = $this->user->totalsubmissions();
	  	$this->load->view('utotalsubmissions_view', $data);	
	
    }
    else
    {
      //If no session, redirect to login page
      redirect('cdslogpage', 'refresh');
	} 
  }

  

  function panzer_usumission(){
	 if($this->session->userdata('logged_in'))
    {

	  $session_data = $this->session->userdata('logged_in');
	  $usertype = $session_data['usertype']; 


	  if($usertype != "SUPERADMIN") {
         redirect('cdsdashboard/changepwd', 'refresh');
       }  


      $data['visatypes']=$this->user->get_addsub_users_visa(1);

	  $data['category'] = 'Panzer';
	  $session_data = $this->session->userdata('logged_in');
	  // $data['key1']=$this->user->addsub_users_location();
	  $data['key1']=$this->user->get_addsub_users_location(1);
	  $data['roles'] = $this->session->userdata('roles');
		
		$config = array();
		$config["base_url"] = base_url() . "index.php/totalsubmissions/panzer_usumission/index/";
		$coutsub = $this->user->get_usubmissions_count(1);
		foreach ($coutsub as $cs){
			$cs = $cs->countsub;
		}
		$config["total_rows"] = $cs;
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		// Next Link
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		// Previous Link
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		// Current Link
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		// Digit Link
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config["per_page"] 	= 30;
        $config["uri_segment"] 	= 4;
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $results = $this->user->get_usubmissions_result($config["per_page"], $page, 1);
		//print_r($results); die;
		$data["links"] = $this->pagination->create_links();
		$data['rows'] = $results;
	  
      	$data['username'] = $session_data['username'];
		$data['searchtype'] = $this->uri->segment(1);
      	//$data['rows'] = $this->user->submissions_result();
	 	// print_r($_SESSION);exit;
		$data["manager"] = $this->user->get_manager();
		$data['userid'] = $session_data['id'];
		$data["menuname"] = 'submissions';
		$data['totalResumes'] = $this->user->totalsubmissions();
	  	$this->load->view('utotalsubmissions_view', $data);	
	
    }
    else
    {
      //If no session, redirect to login page
      redirect('cdslogpage', 'refresh');
	} 
  }



  function rpo_usumission(){
	 if($this->session->userdata('logged_in'))
    {

	  $session_data = $this->session->userdata('logged_in');
	  $usertype = $session_data['usertype']; 


	  if($usertype != "SUPERADMIN") {
         redirect('cdsdashboard/changepwd', 'refresh');
       }   

	  $data['visatypes']=$this->user->get_addsub_users_visa(2);

	  $data['category'] = 'RPO';
	  $session_data = $this->session->userdata('logged_in');
	  // $data['key1']=$this->user->addsub_users_location();
	  $data['key1']=$this->user->get_addsub_users_location(2);
	  $data['roles'] = $this->session->userdata('roles');
		
		$config = array();
		$config["base_url"] = base_url() . "index.php/totalsubmissions/rpo_usumission/index/";
		$coutsub = $this->user->get_usubmissions_count(2);
		foreach ($coutsub as $cs){
			$cs = $cs->countsub;
		}
		$config["total_rows"] = $cs;
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		// Next Link
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		// Previous Link
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		// Current Link
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		// Digit Link
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config["per_page"] = 30;
        $config["uri_segment"] = 4;
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $results = $this->user->get_usubmissions_result($config["per_page"], $page, 2);
		//print_r($results); die;
		$data["links"] = $this->pagination->create_links();
		$data['rows'] = $results;
	  
      	$data['username'] = $session_data['username'];
		$data['searchtype'] = $this->uri->segment(1);
      	//$data['rows'] = $this->user->submissions_result();
	 	// print_r($_SESSION);exit;
		$data["manager"] = $this->user->get_manager();
		$data['userid'] = $session_data['id'];
		$data["menuname"] = 'submissions';
		$data['totalResumes'] = $this->user->totalsubmissions();
	  	$this->load->view('utotalsubmissions_view', $data);	
	
    }
    else
    {
      //If no session, redirect to login page
      redirect('cdslogpage', 'refresh');
	} 
  }


   
   function canada_usumission(){

	 if($this->session->userdata('logged_in'))
    {

	  $session_data = $this->session->userdata('logged_in');
	  $usertype = $session_data['usertype']; 


	if($usertype != "SUPERADMIN") {
         redirect('cdsdashboard/changepwd', 'refresh');
       }  

	  $data['visatypes']=$this->user->get_addsub_users_visa(3);

	  $data['category'] = 'Canada';
	  $session_data = $this->session->userdata('logged_in');
	  // $data['key1']=$this->user->addsub_users_location();
	  $data['key1']=$this->user->get_addsub_users_location(3);
	  $data['roles'] = $this->session->userdata('roles');
		
		$config = array();
		$config["base_url"] = base_url() . "index.php/totalsubmissions/canada_usumission/index/";
		$coutsub = $this->user->get_usubmissions_count(3);
		foreach ($coutsub as $cs){
			$cs = $cs->countsub;
		}
		$config["total_rows"] = $cs;
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		// Next Link
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		// Previous Link
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		// Current Link
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		// Digit Link
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config["per_page"] = 30;
        $config["uri_segment"] = 4;
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $results = $this->user->get_usubmissions_result($config["per_page"], $page, 3);
		//print_r($results); die;
		$data["links"] = $this->pagination->create_links();
		$data['rows'] = $results;
	  
      	$data['username'] = $session_data['username'];
		$data['searchtype'] = $this->uri->segment(1);
      	//$data['rows'] = $this->user->submissions_result();
	 	// print_r($_SESSION);exit;
		$data["manager"] = $this->user->get_manager();
		$data['userid'] = $session_data['id'];
		$data["menuname"] = 'submissions';
		$data['totalResumes'] = $this->user->totalsubmissions();
	  	$this->load->view('utotalsubmissions_view', $data);	
	
    }
    else
    {
      //If no session, redirect to login page
      redirect('cdslogpage', 'refresh');
	} 
  }







	function mysubmissions() {
			
		if($this->session->userdata('logged_in')) {
			// $data['menuname'] = 'submissions';
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];	

			//Data by Category
			$usertype = $session_data['usertype']; 
			if($usertype == "SUPERADMIN"){
				$data['visatypes']=$this->user->addsub_users_visa();
				$data['key1']=$this->user->addsub_users_location();
			}  else {
				$data['visatypes']=$this->user->get_addsub_users_visa($session_data['category']);
				$data['key1']=$this->user->get_addsub_users_location($session_data['category']);
			}
			//End of Data by Category

			$config = array();
			$config["base_url"] = base_url() . "index.php/totalsubmissions/mysubmissions/index/";
			$coutsub = $this->user->mysubmissions_count();
			foreach ($coutsub as $cs){
				$cs = $cs->countsub;
			}
			$config["total_rows"] = $cs;
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			
			// Next Link
			$config['next_link'] = '&raquo;';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			
			// Previous Link
			$config['prev_link'] = '&laquo;';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			// Current Link
			$config['cur_tag_open'] = '<li class="active">';
			$config['cur_tag_close'] = '</li>';
			
			// Digit Link
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			
			$config["per_page"] = 30;
			$config["uri_segment"] = 4;
			
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			$results = $this->user->mysubmissions_result($config["per_page"], $page);
			$data["links"] = $this->pagination->create_links();
			$data['rows'] = $results;
			$data['searchtype'] = $this->uri->segment(2);
			$managerId = $session_data['id'];
			// $data["reqruiterlist"] = $this->user->get_reqruiter($managerId);
			$data["menuname"] = 'submissions';
			$this->load->view('mysubmissions_view',$data);
		}
		else{
			redirect('cdslogpage', 'refresh');
		}		
	}

 function deletemysubmissions($id)
 {
 	//print_r($id);exit;
	//$data['key1']=$this->user->addsub_users_location();
	$data['error']='';
	$data['records']=$this->user->delete_mysubs($id);
	$data['error']='Deleted Succesfully';
	$session_data = $this->session->userdata('logged_in');
	$data['username'] = $session_data['username'];	

	//Data by Category
    $usertype = $session_data['usertype']; 
    if($usertype == "SUPERADMIN"){
    $data['key1']=$this->user->addsub_users_location();
    $data['visatypes']=$this->user->addsub_users_visa();
    }  else {
    $data['key1']=$this->user->get_addsub_users_location($session_data['category']);
    $data['visatypes']=$this->user->get_addsub_users_visa($session_data['category']);
    }
    //End of Data by Category

	$config = array();
	$config["base_url"] = base_url() . "index.php/totalsubmissions/mysubmissions/index/";
	$coutsub = $this->user->mysubmissions_count();
	foreach ($coutsub as $cs){
		$cs = $cs->countsub;
	}
	$config["total_rows"] = $cs;
	$config['first_link'] = 'First';
	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';
	
	$config['last_link'] = 'Last';
	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';
	
	// Next Link
	$config['next_link'] = '&raquo;';
	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';
	
	// Previous Link
	$config['prev_link'] = '&laquo;';
	$config['prev_tag_open'] = '<li>';
	$config['prev_tag_close'] = '</li>';
	
	// Current Link
	$config['cur_tag_open'] = '<li class="active">';
	$config['cur_tag_close'] = '</li>';
	
	// Digit Link
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	
	$config["per_page"] = 30;
	$config["uri_segment"] = 4;
	
	$this->pagination->initialize($config);
	$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
	$results = $this->user->mysubmissions_result($config["per_page"], $page);
	$data["links"] = $this->pagination->create_links();
	$data['rows'] = $results;
	$data['searchtype'] = $this->uri->segment(2);
	$data["menuname"] = 'submissions';
	$this->load->view('mysubmissions_view',$data);
 }	
 
 function editmysubmissions($id) {
	$data['error']='';
  	if($this->session->userdata('logged_in')) {
      $data["menuname"] = 'submissions';
	  $session_data = $this->session->userdata('logged_in');
      $data['username'] = $session_data['username'];
	  //$data['searchtype'] = $this->uri->segment(2);
	  //$data['key1']=$this->user->addsub_users_location();


	  	$rejectedArray = array(
								'Rejected (Reason: Comm Skills)',
								'Rejected (Reason: Position on Hold/Closed/Filled)',
								'Rejected (Reason: Technical Skills)',
								'Rejected (Reason: Rate Concern)',
								'Rejected (Reason: Location)'
							  );

	  	$data['rejectedArray'] = $rejectedArray;

	    //Data by Category
	    $usertype = $session_data['usertype']; 
	    if($usertype == "SUPERADMIN"){
	    	$data['key1']=$this->user->addsub_users_location();
	    	$data['visatypes']=$this->user->addsub_users_visa();
	    }  else {
	    	$data['key1']=$this->user->get_addsub_users_location($session_data['category']);
	    	$data['visatypes']=$this->user->get_addsub_users_visa($session_data['category']);
	    }
	    //End of Data by Category
	    
	  
	  if($this->input->post('submit')== 'submit'){
	  	//print_r($_POST);exit;
		
	  	$config['upload_path'] 		= 'resumes/';
		$config['allowed_types'] 	= '*';
		$config['max_size']			= '2048';
		$config['max_width']  		= '1024';
		$config['max_height']  		= '768';
		
		$this->load->library('upload', $config);
		if($_FILES["userfile"]["name"] != ""){
			if(!$this->upload->do_upload()){
				
				$error = array('error' => $this->upload->display_errors());
				
				$data['error']= 'The filetype you are attempting to upload is not allowed.';
				$data['keys']=$this->user->mysub_val($id);
				$data['sub_attachments'] = $this->user->my_sub_attach($id);
				// $data['result1']=$this->user->mysub_val_location();	
				// $data['result2']=$this->user->mysub_val_recruiters();	
				// $data['result3']=$this->user->mysub_val_visa();

				//Data by Category
			    $usertype = $session_data['usertype']; 
			    if($usertype == "SUPERADMIN"){
					$data['result1']=$this->user->mysub_val_location();
					$data['result2']=$this->user->mysub_val_recruiters();
					$data['result3']=$this->user->mysub_val_visa();
			    }  else {
					$data['result1']=$this->user->get_mysub_val_location($session_data['category']);
					$data['result2']=$this->user->get_mysub_val_recruiters($session_data['category']);
					$data['result3']=$this->user->get_mysub_val_visa($session_data['category']);
			    }
			    //End of Data by Category


				$data['searchtype'] = "mysubmissions";
				$this->load->view('mysub_view', $data);
			} else {
				$a = $this->upload->data();
				$filename =  $a['file_name'];
				$data = array('upload_data' => $this->upload->data());
				$data['error']=$this->user->edit_submissions_model($filename);
				$data['keys']=$this->user->mysub_val($id);
				$data['sub_attachments'] = $this->user->my_sub_attach($id);
				// $data['result1']=$this->user->mysub_val_location();	
				// $data['result2']=$this->user->mysub_val_recruiters();	
				// $data['result3']=$this->user->mysub_val_visa();

				//Data by Category
			    $usertype = $session_data['usertype']; 
			    if($usertype == "SUPERADMIN"){
			    $data['result1']=$this->user->mysub_val_location();
			    $data['result2']=$this->user->mysub_val_recruiters();
			    $data['result3']=$this->user->mysub_val_visa();
			    }  else {
			    $data['result1']=$this->user->get_mysub_val_location($session_data['category']);
			    $data['result2']=$this->user->get_mysub_val_recruiters($session_data['category']);
			    $data['result3']=$this->user->get_mysub_val_visa($session_data['category']);
			    }
			    //End of Data by Category

				$data['searchtype'] = "mysubmissions";
				$this->load->view('mysub_view', $data);
			}
		}else{
			$filename = $this->input->post('file');
			$filename = explode("resumes/", $filename);
			$data['error']=$this->user->edit_submissions_model($filename[1]);
			$data['keys']=$this->user->mysub_val($id);
			$data['sub_attachments'] = $this->user->my_sub_attach($id);
			// $data['result1']=$this->user->mysub_val_location();	
			// $data['result2']=$this->user->mysub_val_recruiters();	
			// $data['result3']=$this->user->mysub_val_visa();
			//Data by Category
		    $usertype = $session_data['usertype']; 
		    if($usertype == "SUPERADMIN"){
				$data['result1']=$this->user->mysub_val_location();
				$data['result2']=$this->user->mysub_val_recruiters();
				$data['result3']=$this->user->mysub_val_visa();
		    }  else {
				$data['result1']=$this->user->get_mysub_val_location($session_data['category']);
				$data['result2']=$this->user->get_mysub_val_recruiters($session_data['category']);
				$data['result3']=$this->user->get_mysub_val_visa($session_data['category']);
		    }
		    //End of Data by Category
			$data['searchtype'] = "mysubmissions";
			$this->load->view('mysub_view', $data);
		}
	  //$data['keys']=$this->user->users_update($id);
	  
	  //$data['error']='User Updated Successfully';
	 // $this->load->view('mysub_view', $data);
	  } else{
	   $data['keys']=$this->user->mysub_val($id);
	   $data['sub_attachments'] = $this->user->my_sub_attach($id);	
	   // $data['result1']=$this->user->mysub_val_location();	
	   // $data['result2']=$this->user->mysub_val_recruiters();	
	   // $data['result3']=$this->user->mysub_val_visa();
	   //Data by Category
	    $usertype = $session_data['usertype']; 
	    if($usertype == "SUPERADMIN"){
			$data['result1']=$this->user->mysub_val_location();
			$data['result2']=$this->user->mysub_val_recruiters();
			$data['result3']=$this->user->mysub_val_visa();
	    } else {
			$data['result1']=$this->user->get_mysub_val_location($session_data['category']);
			$data['result2']=$this->user->get_mysub_val_recruiters($session_data['category']);
			$data['result3']=$this->user->get_mysub_val_visa($session_data['category']);
	    }
	    //End of Data by Category	
	   $data['searchtype'] = "mysubmissions";
	   $this->load->view('mysub_view', $data);
	  }
	 
	  //$data['keys1']=$this->user->users_edit_model($id);
	  //print_r($data);exit;
	  	
	
    } else {
      //If no session, redirect to ctslogpge page
	  $data['searchtype'] = "mysubmissions";
      redirect('cdslogpage', 'refresh');
	}
	
 }
 
	function comments($id) {
		//print_r($id);
		$_SESSION['id']=$id;
		//print_r($_SESSION);exit;
		if($this->session->userdata('logged_in')) {
			$data["menuname"] = 'submissions';
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];	
			//$_SESSION['id']=$id;
			$data['error']='';
			if($this->input->post('submit')== 'Add Comment') {
				//print_r($_POST);exit;
				
				$data['error']=$this->user->commments_insert();
				$data['row']= $this->user->comments_data();
				$this->load->view('comments_view',$data);	
			} else{
				$data['row']= $this->user->comments_data();
				$this->load->view('comments_view',$data);	
			}
		} else{
			redirect('cdslogpage', 'refresh');
		}
	}
	
  function edit($id)
  {
		if($this->session->userdata('logged_in'))
	    {
	    $data["menuname"] = 'submissions';	
		$session_data = $this->session->userdata('logged_in');
		$data['username'] = $session_data['username'];	
		$data['error']='';
		if($this->uri->segment(4) == 'ok')
		{
			
		$data['records']=$this->user->update_data($id);
		$data['records1']=$this->user->update_attachments($id);
		$data['error']=$this->uri->segment(4);	
		$this->load->view('update_view',$data);	
		}
		else{
		$data['records']=$this->user->update_data($id);
		$data['records1']=$this->user->update_attachments($id);
		//print_r($data);exit;
		$this->load->view('update_view',$data);	
		}
		}
		else{
			redirect('cdslogpage', 'refresh');
		}
		
	}
	
  function pic_edit($id,$sid)
  {
  		$session_data = $this->session->userdata('logged_in');
		$data['username'] = $session_data['username'];
		if($this->uri->segment(5) == 'dok')
		{//print_r($sid);exit;
		$data["menuname"] = 'submissions';
		$data['records']=$this->user->update_data($id);
		$data['records1']=$this->user->update_attachments1($id);
		$data['error']=$this->uri->segment(5);	
		$this->load->view('update_view',$data);	
		}
	}

  function update_rec($id)
  {
		if($this->session->userdata('logged_in'))
	    {
			$date=$this->input->post('datepicker');
			$date1=$this->input->post('deadline');
			$data=$this->user->update_record($id);
		}
		else{
			redirect('cdslogpage', 'refresh');
		}
	
	}
  function insert_job()
  {
		if($this->session->userdata('logged_in'))
	    {
		$date=$this->input->post('datepicker');
		$date1=$this->input->post('deadline');
		$converdate=explode('/',$date);
		$datepciker=$converdate[2].'-'.$converdate[1].'-'.$converdate[0];
		$converdate1=explode('/',$date1);
		$deadline=$converdate1[2].'-'.$converdate1[1].'-'.$converdate1[0];	
		$this->user->insert_data($datepciker,$deadline);
		redirect('home/index');
		}
		else{
			redirect('cdslogpage', 'refresh');
		}	
		
		
	}
	
  function delete($id,$sid)
  {
		$data['records']=$this->user->delete_data($id,$sid);
		redirect('home/index');
	}
  function delete_pic($id,$sid,$filename)
  {
		
		$this->db->delete('attachments', array('id' => $sid));
		$filestring = base_url().'uploads/'.$filename;
		$filestring = APPPATH.'../uploads/'.$filename;
		unlink ($filestring);
		redirect('home/pic_edit/'.$id.'/'.$sid.'/dok');
	}
	
  function doUpload1($id)
  {
	
	$config['upload_path'] = 'uploads/';
	$config['allowed_types'] = '*';
	$config['max_size'] = '1000';
	$config['max_width'] = '1920';
	$config['max_height'] = '1280';

	$this->load->library('upload', $config);
	if(!$this->upload->do_upload())
	echo $this->upload->display_errors();
	else {
	$fInfo = $this->upload->data();
	$data=$this->user->insert_attach($id);
	
	}
	}
		
  function logout()
  {
    $this->session->unset_userdata('logged_in');
    session_destroy();
	//print_r($_SESSION);exit;
    redirect('cdsdashboard', 'refresh');
  }



  function deletesubmission($submissionID) {

  	$session_data = $this->session->userdata('logged_in');
  	if($session_data['usertype'] == 'SUPERADMIN' || $session_data['id'] == 23){
  		$this->user->delete_mysubs($submissionID);		
  	} 
  		redirect('totalsubmissions', 'refresh');	
  	

  }



  function update_submission_status() {

  	header('Content-Type: application/json');

  	if($this->session->userdata('logged_in')){

  		$id 	=	$this->input->post('id');
		$status	=	$this->input->post('status');

		if($status == "Interview"){
			$interview_modal = true;
		}else{
			$interview_modal = false;
		}
		
		$this->user->updateSubmissionsStatus($id, $status);		
		
		echo json_encode(array(
  				'status' 	=> 	'success',
				'interview_modal' => $interview_modal,
  				'message'	=>	'status updated successfully'
  				));
		exit();	
  	}


	  	echo json_encode(array(
	  				'status' 	=> 	'failure',
	  				'message'	=>	'bad request'
	  					));

  }


  function getsubmissioncomments() {

  	header('Content-Type: application/json');

  	if($this->session->userdata('logged_in')){

  		$id			 			= $this->input->post('id');
		$data['getcomments'] 	= $this->user->commentsBySubmissionsID($id);
		$html 		 			= $this->load->view('comments',$data, true);

		echo json_encode(array(
  				'status' 	=> 	'success',
  				'message'	=>	'status updated successfully',
  				'html'		=> 	$html
  				));
		exit();	
  	}
	  	echo json_encode(array(
	  				'status' 	=> 	'failure',
	  				'message'	=>	'bad request'
	  					));

  }
  function searchResumeResults() {
  	
  }
  /**get interview details */

//   function get_interview_details(){
// 	header('Content-Type: application/json');
	
// 	if(isset($_POST['submission_id'])) {
// 		$interview_date = $_POST['interview_date'];
// 		$interview_time = $_POST['interview_time'];  
// 		$interview_mode = $_POST['interview_mode'];
// 		$submission_id = $_POST['submission_id'];
// 	}


//     $data = array(
// 				'interview_date' => $interview_date,
// 				'interview_time'  => $interview_time,
// 				'mode_of_interview'    => $interview_mode
// 				);    
	
// 	$details_inserted = $this->user->insert_interview_details($submission_id, $data);

// 	if ($details_inserted == "interview details added successfully") {
// 		echo json_encode(array(
// 			'status' => 'success',
// 			'message' => 'Details saved successfully'
// 			));
// 	} else {
// 		echo json_encode(array(
// 			'status' => 'failed',
// 			'message' => 'Details not saved'
// 			));
// 	}
//   }

  /**get interview details */

  function get_interview_details(){
	header('Content-Type: application/json');
	
	if(isset($_POST['submission_id'])) {
		$interview_date = $_POST['interview_date'];
		$interview_time = $_POST['interview_time'];  
		$interview_mode = $_POST['interview_mode'];
		$time_zone = $_POST['time_zone'];
		$submission_id = $_POST['submission_id'];
	}


    $data = array(
				'interview_date' => $interview_date,
				'interview_time'  => $interview_time,
				'mode_of_interview'    => $interview_mode,
				'time_zone'    => $time_zone
				);    
	
	$details_inserted = $this->user->insert_interview_details($submission_id, $data);

	if ($details_inserted == "interview details added successfully") {
		echo json_encode(array(
			'status' => 'success',
			'message' => 'Details saved successfully'
			));
	} else {
		echo json_encode(array(
			'status' => 'failed',
			'message' => 'Details not saved'
			));
	}
  }


}

?>
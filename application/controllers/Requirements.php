<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(1);
class Requirements extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('user','',TRUE);
        $this->load->model('clientdetails','',TRUE);
        $this->load->model('jobrequirements','',TRUE);
        $this->load->library('pagination');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('session');
        session_start();
    }

    function index() {
        $remove_access_users = array("HR", "MIS", "RECRUITER", "ACCOUNTSEXECUTIVE");
        $session_data = $this->session->userdata('logged_in');
        $usertype = $session_data['usertype'];
        if($this->session->userdata('logged_in') && !in_array($usertype, $remove_access_users)) {
            // $session_data         = $this->session->userdata('logged_in');
            $data['username']     = $session_data['username'];
            $user_id = $session_data['id'];
            $data['jobrequirementsdetails'] 	= $this->jobrequirements->JobRequirementDetailsByUserID($user_id);
            $state_names = array();
            foreach ($data['jobrequirementsdetails'] as $value) {
                $state_id = $value->location;
                $state_name = $this->user->getLocationByID($state_id);
                array_push($state_names, (object)[
                    'statename' => $state_name
                ]);
            }
            $data['states'] = $state_names;
            $this->load->view('view-job-requirements', $data);
        }else{
            redirect('cdslogpage', 'refresh');
        }
    }

    function add_requirement(){
        $remove_access_users = array("HR", "MIS", "RECRUITER", "ACCOUNTSEXECUTIVE");
        $session_data = $this->session->userdata('logged_in');
        $usertype = $session_data['usertype'];
        if($this->session->userdata('logged_in') && !in_array($usertype, $remove_access_users)) {
            // $session_data         = $this->session->userdata('logged_in');
            $data['username']     = $session_data['username'];
            $data['key1']=$this->user->addsub_users_location();
            $this->load->view('add-job-requirement-view',$data);
        }else{
            redirect('cdslogpage', 'refresh');
        }
    }

    function save_requirement_data(){
        if($this->session->userdata('logged_in')) {
            $session_data         = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['key1']=$this->user->addsub_users_location();
            $this->load->library('form_validation');
            $this->form_validation->set_rules('jdate', 'Date', 'required');
            $this->form_validation->set_rules('psub', 'Job Title', 'required');
            $this->form_validation->set_rules('city', 'City', 'required');
            $this->form_validation->set_rules('location', 'State', 'required');
            $this->form_validation->set_rules('datebtw', 'Duration', 'required');
            $this->form_validation->set_rules('rate', 'Rate', '');
            $this->form_validation->set_rules('salaryrate', 'Salary', '');
            $this->form_validation->set_rules('client_name', 'Client Name', 'required');

            if($this->form_validation->run() == FALSE) {
                $data['message'] = "All the fields are required";
                $this->load->view('add-job-requirement-view', $data);
            }
            else {
                $session_data = $this->session->userdata('logged_in');
                $user_id = $session_data['id'];
                $job_date = trim($this->input->post('jdate'));
                $job_title = trim($this->input->post('psub'));
                $job_city = trim($this->input->post('city'));
                $job_location = trim($this->input->post('location'));
                $job_duration = trim($this->input->post('datebtw'));
                $job_rate_per_hr = trim($this->input->post('jobrate'));
                $job_salary = trim($this->input->post('salaryrate'));
                $client_name = trim($this->input->post('client_name'));
                $job_description = trim($this->input->post('comments'));
                if (!empty($job_rate_per_hr)) {
                    $bill_rate = $job_rate_per_hr;
                } else {
                    $bill_rate = $job_salary;
                }
                $permalink = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $job_title)));
                $client_details = $this->clientdetails->getClientDetailsByUserID($user_id);
                $client_company = "";
                $user_email = $this->user->getEmailByManagerID($user_id);
                foreach($client_details as $client_detail){
                    if ($client_detail->client_name == $client_name) {
                        $client_company = $client_detail->client_company;
                    }
                }
                $insert_data = array(
                    'job_title' => $job_title,
                    'permalink' => $permalink,
                    'city' => $job_city,
                    'location' => $job_location,
                    'duration' => $job_duration,
                    'bill_rate' => $bill_rate,
                    'client_company' => $client_company,
                    'job_description' => $job_description,
                    'user_id' => $user_id,
                    'manager_email' => $user_email,
                    'date' => $job_date
                );
                //print_r($insert_data);exit();
                $data['message'] = $this->jobrequirements->insert_job_details($insert_data);
                //$this->load->view('add-job-requirement-view', $data);
                $this->session->set_flashdata('result', $data['message']);
                redirect('requirements/add_requirement');
            }
        }else{
            redirect('cdslogpage', 'refresh');
        }
    }

    function submit($id) {
        if($this->session->userdata('logged_in')) {
            $session_data         = $this->session->userdata('logged_in');
            $this->addsubmissionsView($id);
            //$data['result']  	= "Client details updated successfully";
           // $this->load->view('edit-addsubmissions-view', $data);
        } else {
            redirect('cdslogpage', 'refresh');
        } 
   }
   function addsubmissionsView($id){
    //echo $id;exit;
      $data['error']='';
  
      if($this->session->userdata('logged_in')){
  
       $data["menuname"] = 'submissions'; 
       $session_data = $this->session->userdata('logged_in');
       $data['username'] = $session_data['username'];
       $data['searchtype'] = 'totalsubmissions';
  
      if($this->input->post('submit')=='submit'){
    
      $config['upload_path'] = 'resumes/';
      $config['allowed_types'] = 'doc|pdf|docx';
      $config['max_size'] = '2048';
      $config['max_width']  = '1024';
      $config['max_height']  = '768';
  
      $this->load->library('upload', $config);
  
      if(!$this->upload->do_upload()){
        
        $data['clientdetails']  	= $this->jobrequirements->JobRequirementDetailsByID($id);
        
        
        $error = array('error' => $this->upload->display_errors());
        $data['error']= 'The filetype you are attempting to upload is not allowed.';
        $data['key1']=$this->user->addsub_users_location();
        $data['key2']=$this->user->addsub_users_recruiters();
        $data['key3']=$this->user->addsub_users_visa();
        $data['job_id'] = $id;
        
        $this->load->view('add_submission_view', $data);
  
      } else {
        
        $a = $this->upload->data();
        $filename =  $a['file_name'];
        $data = array('upload_data' => $this->upload->data());
        $data['error']= $this->user->submissions_insert($filename);
  
  
        if($_FILES['uploadvisa']['name'] != '') {
           $this->visa->uploadVisaCopy($_POST['psub'], $_POST['cname'], $_FILES['uploadvisa']);
        } 
      
          //Data by Category
          $usertype = $session_data['usertype']; 
  
        if($usertype == "SUPERADMIN" && $session_data['category'] == 4){
  
        $data['key1'] = $this->user->addsub_users_location();
        $data['key2'] = $this->user->addsub_users_recruiters();
        $data['key3'] = $this->user->addsub_users_visa();
  
        }  else  {
  
        $userCategory   = $session_data['category'];  
        $data['key1']   = $this->user->get_addsub_users_location($userCategory);
        $data['key2']   = $this->user->get_addsub_users_recruiters($userCategory);
        $data['key3']   = $this->user->get_addsub_users_visa($userCategory); 
  
        }
        
        $data['clientdetails']  	= $this->jobrequirements->JobRequirementDetailsByID($id);
       
        //End of Data by Category
        $this->load->view('add_submission_view', $data);
      }
  
    } else {
  
      //Data by Category
      $usertype = $session_data['usertype']; 
      if($usertype == "SUPERADMIN"){
      $data['key1']=$this->user->addsub_users_location();
      $data['key2']=$this->user->addsub_users_recruiters();
      $data['key3']=$this->user->addsub_users_visa();
      }  else {
      $userCategory = $session_data['category'];  
      $data['key1']=$this->user->get_addsub_users_location($userCategory);
      $data['key2']=$this->user->get_addsub_users_recruiters($userCategory);
      $data['key3']=$this->user->get_addsub_users_visa($userCategory); 
      }
      $data['clientdetails']  	= $this->jobrequirements->JobRequirementDetailsByID($id);
      $data['job_id'] = $id;
      //End of Data by Category
      $this->load->view('add_submission_view',$data);  
    }
  
    } else {
        //If no session, redirect to ctslogpge page
        redirect('cdslogpage', 'refresh');
    }
      
    }

    function edit($id){
        $remove_access_users = array("HR", "MIS", "RECRUITER", "ACCOUNTSEXECUTIVE");
        $session_data = $this->session->userdata('logged_in');
        $usertype = $session_data['usertype'];
        if($this->session->userdata('logged_in') && !in_array($usertype, $remove_access_users)) {
            $session_data         = $this->session->userdata('logged_in');
            $data['username']     = $session_data['username'];
            $data['key1']=$this->user->addsub_users_location();
            $job_details = $this->jobrequirements->JobRequirementDetailsByID($id);
            $data['jobdetails'] = $job_details;
            $client_company = $job_details->client_company;
            $get_client_name = $this->clientdetails->getClientNameByCompanyname($client_company);
            $client_name = $get_client_name[0]->client_name;
            $data['client_name'] = $client_name;
            $this->load->view('edit-job-requirement-view', $data); 
        }else{
            redirect('cdslogpage', 'refresh');
        } 
    }

    function update(){
        if($this->session->userdata('logged_in')) {
            $session_data         = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['key1']=$this->user->addsub_users_location();
            $this->load->library('form_validation');
            $this->form_validation->set_rules('jdate', 'Date', 'required');
            $this->form_validation->set_rules('psub', 'Job Title', 'required');
            $this->form_validation->set_rules('city', 'City', 'required');
            $this->form_validation->set_rules('location', 'State', 'required');
            $this->form_validation->set_rules('datebtw', 'Duration', 'required');
            $this->form_validation->set_rules('rate', 'Rate', '');
            $this->form_validation->set_rules('salaryrate', 'Salary', '');
            $this->form_validation->set_rules('client_name', 'Client Name', 'required');
            $id = $this->input->post('id');
            if($this->form_validation->run() == FALSE) {
                $data['message'] = "All the fields are required";
                $this->load->view('edit-job-requirement-view', $data);
            }
            else {
                $user_id = $session_data['id'];
                $job_date = trim($this->input->post('jdate'));
                $job_title = trim($this->input->post('psub'));
                $job_city = trim($this->input->post('city'));
                $job_location = trim($this->input->post('location'));
                $job_duration = trim($this->input->post('datebtw'));
                $job_rate_per_hr = trim($this->input->post('jobrate'));
                $job_salary = trim($this->input->post('salaryrate'));
                $client_name = trim($this->input->post('client_name'));
                $job_description = trim($this->input->post('comments'));
                if (!empty($job_rate_per_hr)) {
                    $bill_rate = $job_rate_per_hr;
                } else {
                    $bill_rate = $job_salary;
                }
                $permalink = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $job_title)));
                $client_details = $this->clientdetails->getClientDetailsByUserID($user_id);
                $client_company = "";
                $user_email = $this->user->getEmailByManagerID($user_id);
                foreach($client_details as $client_detail){
                    if ($client_detail->client_name == $client_name) {
                        $client_company = $client_detail->client_company;
                    }
                }
                $update_data = array(
                    'job_title' => $job_title,
                    'permalink' => $permalink,
                    'city' => $job_city,
                    'location' => $job_location,
                    'duration' => $job_duration,
                    'bill_rate' => $bill_rate,
                    'client_company' => $client_company,
                    'job_description' => $job_description,
                    'user_id' => $user_id,
                    'manager_email' => $user_email,
                    'date' => $job_date
                );
                $data['message'] = $this->jobrequirements->update_job_details($id, $update_data);
                $this->session->set_flashdata('result', $data['message']);
                redirect('requirements/add_requirement');
            }
        }else{
            redirect('cdslogpage', 'refresh');
        }
    }
}
/**/
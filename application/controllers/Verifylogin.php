<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class Verifylogin extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->model('user','',TRUE);
	  $this->load->library('session');
	  $this->load->helper('url');
  }

  function index() {
    //This method will hecho "okk";exit;ave the credentials validation
    
    $this->load->library('form_validation');
	  $this->load->helper('security');
    $this->form_validation->set_rules('ptno', 'ptno', 'trim|required|xss_clean');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
    if($this->form_validation->run() == FALSE) {
      $this->load->view('cdslogpage_view');
    }
    else {
      redirect('dashboard', 'refresh');
    }
    
  }
  
  function check_database($password) {
    //Field validation succeeded.  Validate against database
    $pt_no = $this->input->post('ptno');
   //print_r($_POST);exit;
   //echo $pt_no." ".$password;
    //query the database
    $result = $this->user->login($pt_no, $password);
    
    if($result) {
      $sess_array = array();
      foreach($result as $row) {
        $sess_array = array(
          'id' => $row->id,
          'username' => $row->username,
          'usertype' => $row->usertype,
          'emailaddress' => $row->email,
          'ptno' => $row->ptno,
          'category' => $row->user_category
        );
        $userType = $row->user_category; 
        $this->session->set_userdata('logged_in', $sess_array);
		  //print_r($this->session->userdata); die('ABCD');
      }
      $menus = $this->user->getMenuAsUserType($userType);
		  $this->session->set_userdata('menudata', $menus);
      return TRUE;
    }
    else {
      $this->form_validation->set_message('check_database', 'Invalid username or password');
      return false;
    }
  }
}
?>
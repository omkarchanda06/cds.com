<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class submissions extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->helper('url');
    $this->load->library('session');
    $this->load->model('user','',TRUE);
    $this->load->model('visa');
    $this->load->library('form_validation');
  }

  function index(){
  //print_r($_POST);exit;
    $data['error']='';

    if($this->session->userdata('logged_in')){

     $data["menuname"] = 'submissions'; 
     $session_data = $this->session->userdata('logged_in');
     $data['username'] = $session_data['username'];
     $data['searchtype'] = 'totalsubmissions';

     //var_dump($session_data['category']); die();
    if($this->input->post('submit')=='submit'){  
  
    $config['upload_path'] = 'resumes/';
    $config['allowed_types'] = 'doc|pdf|docx';
    $config['max_size'] = '2048';
    $config['max_width']  = '1024';
    $config['max_height']  = '768';

    $this->load->library('upload', $config);
    
    if(!$this->upload->do_upload()){

      $error = array('error' => $this->upload->display_errors());
      $data['error']= 'The filetype you are attempting to upload is not allowed.';
      $data['key1']=$this->user->addsub_users_location();
      $data['key2']=$this->user->addsub_users_recruiters();
      $data['key3']=$this->user->addsub_users_visa();
      $this->load->view('add_submission_view', $data);

    } else {
      // echo $session_data['usertype'];

      $a = $this->upload->data();
      $filename =  $a['file_name'];
      $data = array('upload_data' => $this->upload->data());
      $data['error']= $this->user->submissions_insert($filename);

      if($_FILES['uploadvisa']['name'] != '') {
         $this->visa->uploadVisaCopy($_POST['psub'], $_POST['cname'], $_FILES['uploadvisa']);
      } 

        //Data by Category
        $usertype = $session_data['usertype']; 

      if($usertype == "SUPERADMIN" && $session_data['category'] == 4){

      $data['key1'] = $this->user->addsub_users_location();
      $data['key2'] = $this->user->addsub_users_recruiters();
      $data['key3'] = $this->user->addsub_users_visa();

      }  else  {

      $userCategory   = $session_data['category'];  
      $data['key1']   = $this->user->get_addsub_users_location($userCategory);
      $data['key2']   = $this->user->get_addsub_users_recruiters($userCategory);
      $data['key3']   = $this->user->get_addsub_users_visa($userCategory); 

      }
      //End of Data by Category

      $this->load->view('add_submission_view', $data);
    }

  } else {

    //Data by Category
    $usertype = $session_data['usertype']; 
    if($usertype == "SUPERADMIN"){
    $data['key1']=$this->user->addsub_users_location();
    $data['key2']=$this->user->addsub_users_recruiters();
    $data['key3']=$this->user->addsub_users_visa();
    }  else {
    $userCategory = $session_data['category'];  
    $data['key1']=$this->user->get_addsub_users_location($userCategory);
    $data['key2']=$this->user->get_addsub_users_recruiters($userCategory);
    $data['key3']=$this->user->get_addsub_users_visa($userCategory); 
    }
    //End of Data by Category

  
    $this->load->view('add_submission_view',$data);  
  }

  } else {
      //If no session, redirect to ctslogpge page
      redirect('cdslogpage', 'refresh');
  }
    
  }
  function add_submissions(){
  //print_r($_POST);exit;
    $data['error']='';

    if($this->session->userdata('logged_in')){

      $data["menuname"] = 'submissions'; 
      $session_data = $this->session->userdata('logged_in');
      $data['username'] = $session_data['username'];
      $data['searchtype'] = 'totalsubmissions';

      if($this->input->post('submit')=='submit'){
    
        $config['upload_path'] = 'resumes/';
        $config['allowed_types'] = 'doc|pdf|docx';
        $config['max_size'] = '2048';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload()){

          $error = array('error' => $this->upload->display_errors());
          $data['error']= 'The filetype you are attempting to upload is not allowed.';
          $data['key1']=$this->user->addsub_users_location();
          $data['key2']=$this->user->addsub_users_recruiters();
          $data['key3']=$this->user->addsub_users_visa();
          //echo "upload";exit();
          $this->load->view('add_submission_view', $data);

          } else {
        
          $a = $this->upload->data();
          $filename =  $a['file_name'];
          $data = array('upload_data' => $this->upload->data());
          $data['error']= $this->user->submissions_insert($filename);
          //echo "upload else";exit();

          if($_FILES['uploadvisa']['name'] != '') {
            $this->visa->uploadVisaCopy($_POST['psub'], $_POST['cname'], $_FILES['uploadvisa']);
          } 
        
            //Data by Category
            $usertype = $session_data['usertype']; 

          if($usertype == "SUPERADMIN" && $session_data['category'] == 4){

          $data['key1'] = $this->user->addsub_users_location();
          $data['key2'] = $this->user->addsub_users_recruiters();
          $data['key3'] = $this->user->addsub_users_visa();

          }  else  {

          $userCategory   = $session_data['category'];  
          $data['key1']   = $this->user->get_addsub_users_location($userCategory);
          $data['key2']   = $this->user->get_addsub_users_recruiters($userCategory);
          $data['key3']   = $this->user->get_addsub_users_visa($userCategory); 

          }
          //End of Data by Category
          $this->load->view('add_submission_view', $data);
        }

      } else {

        //Data by Category
        $usertype = $session_data['usertype']; 
        if($usertype == "SUPERADMIN"){
        $data['key1']=$this->user->addsub_users_location();
        $data['key2']=$this->user->addsub_users_recruiters();
        $data['key3']=$this->user->addsub_users_visa();
        }  else {
        $userCategory = $session_data['category'];  
        $data['key1']=$this->user->get_addsub_users_location($userCategory);
        $data['key2']=$this->user->get_addsub_users_recruiters($userCategory);
        $data['key3']=$this->user->get_addsub_users_visa($userCategory); 
        }
        //echo "else part";exit();
        //End of Data by Category
        $this->load->view('add_submission_view',$data);  
      }

    } else {
      //If no session, redirect to ctslogpge page
      redirect('cdslogpage', 'refresh');
    }
  }

  function logout()
  {
    $this->session->unset_userdata('logged_in');
    session_destroy();
    $this->load->view('cdslogpage_view');
  }

}

?>

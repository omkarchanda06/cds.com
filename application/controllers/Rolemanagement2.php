<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(0);
class Rolemanagement extends CI_Controller {

  function __construct(){
		parent::__construct();
		$this->load->model('user','',TRUE);
		//$this->load->library('pagination');
		$this->load->model('role');
		//$this->load->model('visa');
		$this->load->helper('url');
		$this->load->library('session');
		session_start();
  }

  function index()
  {
  	//print_r('hi');exit;
    if($this->session->userdata('logged_in'))
    {
		$session_data = $this->session->userdata('logged_in');
		$data['username'] = $session_data['username'];
		$data['key1']=$this->user->addsub_users_location();
		$data['rolemgmt'] = $this->role->get_role();
		
		$this->load->view('role_mgmt_view', $data);
	}else{
		//If no session, redirect to ctslogpge page
		redirect('cdslogpage', 'refresh');
	}
  }
  
  function edit($roleId)
  {
    if($this->session->userdata('logged_in'))
    {
		$session_data = $this->session->userdata('logged_in');
		$data['username'] = $session_data['username'];
		$data['menu'] = $this->role->get_menu();
		$data['selectedmenus'] = $this->role->get_selected_menu($roleId);
		$data['roleId'] = $roleId;
		//echo '<pre>'; print_r($data); echo '</pre>';
		$this->load->view('role_mgmt_edit_view', $data);
	}else{
		//If no session, redirect to ctslogpge page
		redirect('cdslogpage', 'refresh');
	}
  }
  function roleMapping() {
  	$roleId = $this->input->post("roleId");
  	$menuId = $this->input->post("menuId");

  	$data = array(
        'role_id'=>$roleId,
        'manu_id'=>$menuId
    );
    $this->db->insert('map_role_menu',$data);
    echo "Inserted";
  }
}

?>
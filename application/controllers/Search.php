<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(0);
class Search extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('user','',TRUE);
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->library('session');
		session_start();
	}
		function index(){
		$session_data = $this->session->userdata('logged_in');
		
		$username = $session_data['username'];
		$searchtype	= $this->input->get("searchtype");
		$searchtype = explode("/",$searchtype);

		// $location	= $this->input->get("location");
		$location	  = implode(', ',$this->input->get("location"));
		$tsearch      = $this->input->get("tsearch");
		$ksearch1     = $this->input->get("ksearch1");
		$ksearch2     = $this->input->get("ksearch2");
		$ksearch3     = $this->input->get("ksearch3");
		$ksearch4     = $this->input->get("ksearch4");
		$cname    	  = $this->input->get("cname");
		$yrsexp    	  = $this->input->get("yrsexp");
		// $visa    	= $this->input->get("visa");
		$visa    	  = implode(', ',$this->input->get("visa"));
		$relocate     = $this->input->get("relocate");
		$categorytype = $this->input->get("categorytype");
		$datebtw      = $this->input->get("datebtw");
		        
		$locationString = implode('&location[]=', $this->input->get("location"));
		$visaString = implode('&visa[]=', $this->input->get("visa"));

		$candid       = $this->input->get("candid");
		$candid       = '';
		if($session_data['usertype'] == 'SUPERADMIN' || $session_data['usertype'] == 'ADMIN' || $session_data['usertype'] == 'MANAGER') {
			$candid = $this->input->get("candid");
		}
        
        $printCategory = ['All'];
		if($categorytype != '' || ($session_data['usertype'] != "ADMIN" && $session_data['usertype'] != "SUPERADMIN")) {
			if($categorytype == '') { $categorytype = $session_data['category']; }
        $printCategory = "[ ".$this->user->getCategoryName($categorytype)." ]";
		}
		
		$date = date("Y-m-d");
		if($datebtw == '1 Week'){
			$date1 = date('Y-m-j', strtotime('-5 Weekday'));
		}else if($datebtw == '2 Week'){
			$date1 = date('Y-m-j', strtotime('-10 Weekday'));
		}else if($datebtw == '3 Week'){
			$date1 = date('Y-m-j', strtotime('-15 Weekday'));
		}else if($datebtw == '1 Month'){
			$date1 = date('Y-m-j', strtotime('-22 Weekday'));
		}else if($datebtw == '3 Month'){
			$date1 = date('Y-m-j', strtotime('-66 Weekday'));
		}else if($datebtw == '6 Month'){
			$date1 = date('Y-m-j', strtotime('-132 Weekday'));
		}else if($datebtw == '1 Year'){
			$date1 = date('Y-m-j', strtotime('-264 Weekday'));
		}else{
			$date1 = "";
		}
		
        
	    //Data by Category
	    $usertype = $session_data['usertype']; 
	    if($usertype == "ADMIN" || $usertype == "SUPERADMIN"){
	    if($categorytype == '')	{
	    $data['key1']=$this->user->addsub_users_location();
	    $data['visatypes']=$this->user->addsub_users_visa();
	    } else {
        $data['key1']=$this->user->get_addsub_users_location($categorytype);
	    $data['visatypes']=$this->user->get_addsub_users_visa($categorytype);
    	}
	    }  else {
	    $data['key1']=$this->user->get_addsub_users_location($session_data['category']);
	    $data['visatypes']=$this->user->get_addsub_users_visa($session_data['category']);
	    }
	    //End of Data by Category

		$data['searchtype']    = $searchtype[0];
		
		if($searchtype[0] == "totalsubmissions" || $searchtype[0] == "search" || $searchtype[0] == "resumedatabase") {
		
		//Data by Category
	    if($usertype == "ADMIN" || $usertype == "SUPERADMIN"){
	    if($categorytype == '')	{
	    $coutsub = $this->user->gettotsubmissions_count($searchtype[0], $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $visa, $relocate);
	    } else {
        $coutsub = $this->user->gettotsubmissions_count_category($searchtype[0], $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $visa, $relocate, $categorytype);
    	}	
	    }  else {
	    $coutsub = $this->user->gettotsubmissions_count_category($searchtype[0], $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $visa, $relocate, $session_data['category']);
	    }
	    //End of Data by Category


			foreach ($coutsub as $cs){
				$cs = $cs->coutsub;
			}
			$config = array();
		
			//Data by Category
		    if($usertype == "ADMIN" || $usertype == "SUPERADMIN"){
		    if($categorytype == '')	{
		     $config["base_url"] = base_url()."index.php/search?tsearch=".$tsearch."&ksearch1=".$ksearch1."&ksearch2=".$ksearch2."&ksearch3=".$ksearch3."&ksearch4=".$ksearch4."&cname=".$cname."&datebtw=".$datebtw."&location[]=".$locationString."&yrsexp=".$yrsexp."&visa[]=".$visaString."&searchtype=".$searchtype[0];
		    } else {
	        $config["base_url"] = base_url()."index.php/search?tsearch=".$tsearch."&ksearch1=".$ksearch1."&ksearch2=".$ksearch2."&ksearch3=".$ksearch3."&ksearch4=".$ksearch4."&cname=".$cname."&datebtw=".$datebtw."&location[]=".$locationString."&yrsexp=".$yrsexp."&visa[]=".$visaString."&searchtype=".$searchtype[0]."&category=".$categorytype;
	    	}
		    }  else {
		    $config["base_url"] = base_url()."index.php/search?tsearch=".$tsearch."&ksearch1=".$ksearch1."&ksearch2=".$ksearch2."&ksearch3=".$ksearch3."&ksearch4=".$ksearch4."&cname=".$cname."&datebtw=".$datebtw."&location[]=".$locationString."&yrsexp=".$yrsexp."&visa[]=".$visaString."&searchtype=".$searchtype[0]."&category=".$session_data['category'];
		    }
		    //End of Data by Category

			$config["total_rows"] = $cs;
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			
			// Next Link
			$config['next_link'] = '&raquo;';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			
			// Previous Link
			$config['prev_link'] = '&laquo;';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			// Current Link
			$config['cur_tag_open'] = '<li class="active">';
			$config['cur_tag_close'] = '</li>';
			
			// Digit Link
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['page_query_string'] = TRUE;
			$config["per_page"] = 30;
			$config["uri_segment"] = 3;
			
			$this->pagination->initialize($config);
			
			$get_vars = $this->input->get();
    		
		     $config["uri_segment"] = 3;
			 $page = ($this->input->get("per_page")) ? $this->input->get("per_page") : 0;
			//print_r($results); die;
			$links = $this->pagination->create_links();

			//extra code
			if($candid != '') {
			$submissions = $this->user->getSubmissionByID($candid);	
			} else {

			//Data by Category
		    if($usertype == "ADMIN" || $usertype == "SUPERADMIN"){
			    if($categorytype == '')	{
			    $submissions = $this->user->getmsubmissiontoall($searchtype[0], $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $config["per_page"], $page, $visa, $relocate);
			    
			    } else {
		        $submissions = $this->user->getmsubmissiontoall_category($searchtype[0], $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $config["per_page"], $page, $visa, $relocate, $categorytype);
		    	}	
		    }  else {
		    $submissions = $this->user->getmsubmissiontoall_category($searchtype[0], $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $config["per_page"], $page, $visa, $relocate, $session_data['category']);
		    	}

			} //extra code

		    //End of Data by Category
			
			$data["html"] = '';
			$data["html"] = '<div id="page-heading" style="font-size: 10px;
    margin-top: 20px;"><h1>Total Submissions Search '.$printCategory.'</h1></div>';
			$data["html"].= '<table border="0" width="100%" cellpadding="0" cellspacing="0" id="myTable tbl_exporttable_to_xls">
							<tr>
								<td id="tbl-border-left"></td>
								<td>
								<div id="content-table-inner" style="width:auto;overfow:hidden;">
									<div id="table-content">
										<table border="0" cellpadding="0" cellspacing="0" id="myTable tbl_exporttable_to_xls" style="font:13px Cambria;">';
										if($username != "Demo"){
										
										$data["html"].= '<tr><td colspan="100" align="right" style="font-size: 15px;text-align:right"><a href="javascript:onchangeS(\'me\')">Mail Me</a> &nbsp; <a href="javascript:onchangeS(\'other\')">Mail Others</a>&nbsp;</td></tr>';	
										} 
										$data["html"].= '<tr>';
										if($username != "Demo"){
											$data["html"].='
											<th class="table-header table-header-repeat line-left minwidth-1" width="35" style="border-top-left-radius: 20px"><input type="checkbox" id="selectall"/></th>';
											}
											$data["html"].='	
											<th class="table-header table-header-repeat line-left minwidth-1" >Date</th>
											<th class="table-header table-header-repeat line-left minwidth-2" >Consultant</th>
											<th class="table-header table-header-repeat line-left minwidth-1" >For Position</th>
											<th class="table-header table-header-repeat line-left minwidth-1">Total IT</th>
											<th class="table-header table-header-repeat line-left minwidth-5">End Client</th>
											<th class="table-header table-header-options line-left minwidth-1">Location</th>
											<th class="table-header table-header-options line-left minwidth-1">Submitted by</th>';
											if($this->session->userdata('logged_in','usertype')== "ADMIN"){
											$data["html"].= '<th class="table-header table-header-options line-left minwidth-6">Vendor Company</th>
                        					<th class="table-header table-header-options line-left minwidth-6">Vendor Contact</th>';
											}
											$data["html"].= '<th class="table-header table-header-options line-left minwidth-16a">Consultant Rate</th>
                        					<th class="table-header table-header-options line-left minwidth-7" width="95">Visa</th>
                        					<th class="table-header table-header-options line-left minwidth-7" width="95">Relocate</th>';
											if($this->session->userdata('logged_in','usertype')== "ADMIN"){
											$data["html"].= '<th class="table-header table-header-options line-left minwidth-6">Submission Rate</th>';
											}																					
											$data["html"].= '<th class="table-header table-header-options line-left minwidth-6">Employer Details</th>
											<th class="table-header table-header-options line-left minwidth-6">Status</th>
											<th class="table-header table-header-options line-left minwidth-6">Comments</th>';	
											if($username != "Demo"){
											$data["html"].='<th class="table-header table-header-options line-left minwidth-16">Mail</th>';
											}
											if($session_data['usertype'] == 'SUPERADMIN' || $session_data['id'] == 23){
												$data["html"].='<th class="table-header table-header-options line-left minwidth-6">Action</th>';
											}
										$data["html"].=' </tr>';
										 
										$i = 1;
										foreach($submissions as $subres)
										{										
											if($i%2==0){
												$class="alternate-row";
											}else{
												$class="";
											}
											$view = $subres->view; 
											if($view == 0){
												$view1 =1;
											}else{
												$view1 =0;
											}
											$data["html"].= '<tr class="'.$class.'">';
											if($username != "Demo"){ 
											$data["html"].= '<td align="center"><input type="checkbox" class="case" id="case_'.$i.'" name="case[]" value="'.$subres->id.'"/></td>';
											}
											$data["html"].= '<td class="'.$class.'">'.date("M d",strtotime($subres->sdate)).'</td>';
											$data["html"].= '<td class="'.$class.'"><a href="'.base_url().'/'.$subres->resume.'" title="'.$subres->cname.'">'.substr($subres->cname, 0, 8).'</a></td>';
											$data["html"].= '<td class="'.$class.'"><a href="#" title="'.$subres->psub.'">'.substr($subres->psub, 0, 8).'</td>';	
											$data["html"].= '<td class="'.$class.'">'.$subres->totalit.'</td>';
											$data["html"].= '<td class="'.$class.'">'.$subres->company.'</td>';
											$data["html"].= '<td class="'.$class.'">'.$subres->st.'</td>';
											$data["html"].= '<td class="'.$class.'"><a href="#" target="_blank" title="'.$subres->recruiter.'">'.substr($subres->recruiter, 0, 10).'</a></td>';
											
											if($this->session->userdata('logged_in','usertype')== "ADMIN"){
											$data["html"].= '<td class="'.$class.'">'.$subres->vcompany.'</td>';
											$data["html"].= '<td class="'.$class.'">'.$subres->vcontact.'</td>';
											}
											$data["html"].= '<td class="'.$class.'"><a href="#" title="'.$subres->crate.'">'.substr($subres->crate, 0, 10).'</a></td>';
											$data["html"].= '<td class="'.$class.'">'.$subres->visatype.'</td>';
											$data["html"].= '<td class="'.$class.'">'.$subres->relocate.'</td>';
											if($this->session->userdata('logged_in','usertype')== "ADMIN"){
											$data["html"].= '<td class="'.$class.'">'.$subres->srate.'</td>';
											}
											
											$data["html"].= '<td onmouseover="document.getElementById(\'div'.$i.'\').style.display = \'block\';document.getElementById(\'div-'.$i.'\').style.display = \'none\';" onmouseout="document.getElementById(\'div'.$i.'\').style.display = \'none\';document.getElementById(\'div-'.$i.'\').style.display = \'block\';"><div id="div-'.$i.'" style="display: block;">View</div><div id="div'.$i.'" style="display: none;">'.$subres->employerdetails.'</div></td>';
											
											
											$data["html"].= '<td class="'.$class.'"><a href="#"  title="'.$subres->status.'">'.substr($subres->status, 0, 10).'</a></td>';	
											$data["html"].= '<td class="'.$class.'"><a href="javascript:void(0);" onclick="PopupCenter(\'totalsubmissions/comments/'.$subres->id.'\', \'myPop1\',600,600);">Read / Add</a></td>';
											if($username != "Demo"){ 
											$data["html"].= '<td><a href="javascript:client_update(\''.$subres->id.'\',\''.$subres->cname.'\',\''.$subres->vemail.'\')">Client Update</a><br><a href="javascript:client_interview(\''.$subres->id.'\',\''.$subres->cname.'\',\''.$subres->vemail.'\');">Client Interview</a><br><a href="javascript:resume_email(\''.$subres->id.'\',\''.$subres->cname.'\',\''.$subres->psub.'\')">Resume Marketing Email</a></td>';}	
											if($session_data['usertype'] == 'SUPERADMIN' || $session_data['id'] == 23){
											$data["html"].= '<td class="options-width" style="padding:0"><a href="'.base_url().'index.php/search/view/'.$view1.'/'. $subres->id.'" title="Status" class="icon-5 info-tooltip"></a>';
											if($session_data['id'] != 23) {
											$data["html"].= '<a href="'.base_url().'index.php/totalsubmissions/deletesubmission/'. $subres->id.'" title="Delete" class="icon-2 info-tooltip delete-submission"></a>';	
											}

											// extra
											if($session_data['id'] == 2 || $session_data['id'] == 57){
											$data["html"].= '<a href="'.base_url().'index.php/totalsubmissions/editmysubmissions/'. $subres->id.'" title="Edit" class="icon-1 info-tooltip"></a>';	
											}
											$data['html'] .= '</td>';
											// extra

											}
											$data["html"].= '</tr>';
											
											$i++;
										}
										
								$data["html"].= '</table>';
								if($links && $candid == ''){
									$data["html"].= "<ul id=\"pagination-digg\">";
									$data["html"].= $links;
									$data["html"].= "</ul>";
								}
		}else{
			$config = array();
			
			//Data by Category
		    if($usertype == "ADMIN" || $usertype == "SUPERADMIN"){
		    if($categorytype == '')	{
		    $config["base_url"] = base_url() . "index.php/search?tsearch=".$tsearch."&ksearch1=".$ksearch1."&ksearch2=".$ksearch2."&ksearch3=".$ksearch3."&ksearch4=".$ksearch4."&cname=".$cname."&datebtw=".$datebtw."&location[]=".$locationString."&yrsexp=".$yrsexp."&searchtype=".$searchtype[0]."";
		    $coutsub = $this->user->gettotsubmissions_count($searchtype[0], $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $visa, $relocate);
		    } else {
	        $config["base_url"] = base_url() . "index.php/search?tsearch=".$tsearch."&ksearch1=".$ksearch1."&ksearch2=".$ksearch2."&ksearch3=".$ksearch3."&ksearch4=".$ksearch4."&cname=".$cname."&datebtw=".$datebtw."&location[]=".$locationString."&yrsexp=".$yrsexp."&searchtype=".$searchtype[0]."&category=".$categorytype;	
		    $coutsub = $this->user->gettotsubmissions_count_category($searchtype[0], $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $visa, $relocate, $categorytype);
	    	}	
		    }  else {
		    $config["base_url"] = base_url() . "index.php/search?tsearch=".$tsearch."&ksearch1=".$ksearch1."&ksearch2=".$ksearch2."&ksearch3=".$ksearch3."&ksearch4=".$ksearch4."&cname=".$cname."&datebtw=".$datebtw."&location[]=".$locationString."&yrsexp=".$yrsexp."&searchtype=".$searchtype[0]."&category=".$session_data['category'];	
		    $coutsub = $this->user->gettotsubmissions_count_category($searchtype[0], $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $visa, $relocate, $session_data['category']);
		    }
		    //End of Data by Category

			foreach ($coutsub as $cs){
				$cs = $cs->coutsub;
			}
			$config["total_rows"] = $cs;
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			
			// Next Link
			$config['next_link'] = '&raquo;';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			
			// Previous Link
			$config['prev_link'] = '&laquo;';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			// Current Link
			$config['cur_tag_open'] = '<li class="active">';
			$config['cur_tag_close'] = '</li>';
			
			// Digit Link
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			
			$config["per_page"] = 30;
			$config["uri_segment"] = 3;
			
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		   
			//print_r($results); die;
			$links = $this->pagination->create_links();
			
			//Data by Category
		    if($usertype == "ADMIN" || $usertype == "SUPERADMIN" || $usertype == "MANAGER"){
		    if($categorytype == '')	{
		    $submissions = $this->user->getmsubmissiontoall($searchtype, $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $config["per_page"], $page, $visa, $relocate);
		    } else {

	        $submissions = $this->user->getmsubmissiontoall_category($searchtype, $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $config["per_page"], $page, $visa, $relocate, $categorytype);
	    	}	
		    }  else {

		    $submissions = $this->user->getmsubmissiontoall_category($searchtype, $tsearch, $ksearch1, $ksearch2, $ksearch3, $ksearch4, $cname, $yrsexp, $date1, $date, $location, $config["per_page"], $page, $visa, $relocate, $session_data['category']);
		    }
		    //End of Data by Category
		    echo "<link rel='stylesheet' href='<?php echo base_url()?>assets/css/resume_db.css' />";
			$data["html"] = '';
			$data["html"] = '<div id="page-heading" style="width:500px;font-size:9px;margin-top:35px"><h1>My Submissions Search</h1></div>';
			$data["html"].= '<table class="table table-bordered" border="0" width="100%" cellpadding="0" cellspacing="0" id="myTable tbl_exporttable_to_xls" style="font:12px Cambria;">';
					if($username != "Demo"){
					
					$data["html"].= '<tr><td colspan="100" align="right" style="font-size: 14px;text-align:right;"><a href="javascript:onchangeS(\'me\')">Mail Me</a> &nbsp; <a href="javascript:onchangeS(\'other\')">Mail Others</a>&nbsp;</td></tr>';	
					} 
					$data["html"].= '<tr>';
					if($username != "Demo"){
						$data["html"].= '<th class="table-header table-header-repeat line-left minwidth-1" width="35"><input type="checkbox" id="selectall"/></th>';
					}
						$data["html"].= '<th class="table-header table-header-repeat line-left" align="center" width="65">Date</th>
						<th class="table-header table-header-repeat line-left" align="center" width="85">Consultant</th>
						<th class="table-header table-header-repeat line-left" align="center" width="105">For Position</th>
						<th class="table-header table-header-repeat line-left" align="center">Total IT</th>
						<th class="table-header table-header-repeat line-left" width="85">End Client</th>
						<th class="table-header table-header-options line-left" align="center">Loc</th>
						<th class="table-header table-header-options line-left">Submitted by</th>
						<th class="table-header table-header-options line-left" width="85">Vendor Company</th>
						<th class="table-header table-header-options line-left">Vendor Contact</th>
						<th class="table-header table-header-options line-left">Vendor Email</th>
						<th class="table-header table-header-options line-left">Consultant Rate</th>
						<th class="table-header table-header-options line-left" align="center" width="65" >Visa</th>
						<th class="table-header-options line-left" align="center" width="65" >Relocate</th>
						<th class="table-header table-header-options line-left">Submission Rate</th>
						<th class="table-header table-header-options line-left">Employer Details</th>
						<th class="table-header table-header-options line-left" align="center" width="75">Status</th>
						<th class="table-header table-header-options line-left" width="105" align="center">Comments</th>
						<th class="table-header table-header-options line-left" width="105" align="center">Actions</th>
						<th class="table-header table-header-options line-left minwidth-16">Mail</th>
					</tr>';
					$i = 1;
					foreach($submissions as $subres){
						if($i%2==0){
							$class="alternate-row";
						}else{
							$class="";
						}
						$data["html"].= '<tr class="'.$class.'">';
												 
						$data["html"].= '<td align="center"><input type="checkbox" class="case" id="case_'.$i.'" name="case[]" value="'.$subres->id.'"/></td>'; 
						$data["html"].= '<td class="'.$class.'">'.date("M d",strtotime($subres->sdate)).'</td>';
						$data["html"].= '<td class="'.$class.'"><a href="'.base_url().'/'.$subres->resume.'" title="'.$subres->cname.'">'.substr($subres->cname, 0, 8).'</a></td>';
						$data["html"].= '<td class="'.$class.'"><a href="#" title="'.$subres->psub.'">'.substr($subres->psub, 0, 9).'</a></td>';	
						$data["html"].= '<td class="'.$class.'">'.$subres->totalit.'</td>';
						$data["html"].= '<td class="'.$class.'">'.$subres->company.'</td>';
						$data["html"].= '<td class="'.$class.'">'.$subres->st.'</td>';
						$data["html"].= '<td class="'.$class.'"><a href="#" target="_blank" title="'.$subres->recruiter.'">'.substr($subres->recruiter, 0, 5).'</a></td>';
						
						$data["html"].= '<td class="'.$class.'"><a href="#" title="'.$subres->vcompany.'">'.substr($subres->vcompany, 0, 4).'</a></td>';
						$data["html"].= '<td class="'.$class.'"><a href="#" title="'.$subres->vcontact.'">'.substr($subres->vcontact, 0, 4).'</a></td>';
						$data["html"].= '<td class="'.$class.'"><a href="#" title="'.$subres->vemail.'">'.substr($subres->vemail, 0, 4).'</a></td>';
						$data["html"].= '<td class="'.$class.'"><a href="#" title="'.$subres->crate.'">'.substr($subres->crate, 0, 7).'</a></td>';
						$data["html"].= '<td class="'.$class.'">'.$subres->visatype.'</td>';
						$data["html"].= '<td class="'.$class.'">'.$subres->relocate.'</td>';
						
						$data["html"].= '<td class="'.$class.'"><a href="#" title="'.$subres->srate.'">'.substr($subres->srate, 0, 7).'</td>';
												
						$data["html"].= '<td onmouseover="document.getElementById(\'div'.$i.'\').style.display = \'block\';document.getElementById(\'div-'.$i.'\').style.display = \'none\';" onmouseout="document.getElementById(\'div'.$i.'\').style.display = \'none\';document.getElementById(\'div-'.$i.'\').style.display = \'block\';"><div id="div-'.$i.'" style="display: block;">View</div><div id="div'.$i.'" style="display: none;">'.$subres->employerdetails.'</div></td>';
						
						$data["html"].= '<td class="'.$class.'"><a href="#"  title="'.$subres->status.'">'.substr($subres->status, 0, 10).'</a></td>';	
						$data["html"].= '<td class="'.$class.'"><a href="javascript:void(0);" onclick="PopupCenter(\'totalsubmissions/comments/'.$subres->id.'\', \'myPop1\',600,600);">Read / Add</a></td>';
						$data["html"].='<td><a class="icon-1 info-tooltip" href="'.base_url().'index.php/totalsubmissions/editmysubmissions/'.$subres->submissionsId.'" title="EDIT" alt="EDIT" ></a></td>';
						
						if($subres->vemail == ""){
							$vemail = "";
						}else{
							$vemail = $subres->vemail;
						}
						$data["html"].= '<td style="width:430px"><a href="javascript:client_update(\''.$subres->id.'\',\''.$subres->cname.'\',\''.$vemail.'\')">Client Update</a><br><a href="javascript:client_interview(\''.$subres->id.'\',\''.$subres->cname.'\',\''.$vemail.'\');">Client Interview</a><br><a href="javascript:resume_email(\''.$subres->id.'\',\''.$subres->cname.'\',\''.$subres->psub.'\')">Resume Marketing Email</a></td>';																												
						$data["html"].= '</tr>';
						$i++;
					}
					 
			$data["html"].= '</table>';
			if($links && $candid == ''){
				$data["html"].= "<ul id=\"pagination-digg\">";
				$data["html"].= $links;
				$data["html"].= "</ul>";
			}
		}
		
		$this->load->view('resumedatabase/search_results',$data);
  				
	}
	
	function view(){
		$view = $this->uri->segment(3); 
		$submisionid = $this->uri->segment(4); 
		//print $_SERVER["HTTP_REFERER"];
		$updateview = $this->user->updateview($view, $submisionid);
		redirect($_SERVER["HTTP_REFERER"], 'refresh');	
	}
}

?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(0);
class Rolemanagement extends CI_Controller {

  function __construct(){
		parent::__construct();
		$this->load->model('user','',TRUE);
		//$this->load->library('pagination');
		$this->load->model('role');
		//$this->load->model('visa');
		$this->load->helper('url');
		$this->load->library('session');
		session_start();
  }

  function index()
  {
  	//print_r('hi');exit;
    if($this->session->userdata('logged_in'))
    {
		$session_data = $this->session->userdata('logged_in');
		$data['username'] = $session_data['username'];
		$data['key1']=$this->user->addsub_users_location();
		$data['rolemgmt'] = $this->role->get_role();
		
		$this->load->view('role_mgmt_view', $data);
	}else{
		//If no session, redirect to ctslogpge page
		redirect('cdslogpage', 'refresh');
	}
  }
  
  function edit()
  {
  	//print_r('hi');exit;
    if($this->session->userdata('logged_in'))
    {
		$this->load->library('form_validation');
		$session_data = $this->session->userdata('logged_in');
		$data['username'] = $session_data['username'];
		$data['menu'] = $this->role->get_menu();
		$data['role_id'] = $this->uri->segment(3);
		//echo '<pre>'; print_r($data); echo '</pre>';
		$this->load->view('role_mgmt_edit_view', $data);
	}else{
		//If no session, redirect to ctslogpge page
		redirect('cdslogpage', 'refresh');
	}
  }
  
	function update_role_management(){
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			$data['key1']=$this->user->addsub_users_location();
			$roleID = $this->input->post('roleID');
			$menuIDs = $this->input->post('menuIDs');
			$user_category = $this->input->post('user_category');
			
			$result = $this->role->updateRoleManagement($roleID, $menuIDs,$user_category);
			//echo '<pre>'; print_r($menuIDs); echo '</pre>'; die();
			$data['rolemgmt'] = $this->role->get_role();
			
			$this->load->view('role_mgmt_view', $data);
		}else{
			//If no session, redirect to ctslogpge page
			redirect('cdslogpage', 'refresh');
		}
	}
}

?>
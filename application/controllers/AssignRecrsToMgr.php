<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(0);
class AssignRecrsToMgr extends CI_Controller {

  function __construct() {
    parent::__construct();
	$this->load->model('assignrecruiterstomgr','',TRUE);
    $this->load->model('assignmanagerstodirector','',TRUE);
	$this->load->library('pagination');
	$this->load->helper('url');
	$this->load->library('session');
    $this->load->helper('form');
	session_start();
  }

    function index(){
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $usertype = $session_data['usertype'];
            if ($usertype == "SUPERADMIN" || $usertype == "DIRECTOR" || $usertype == "ASSOCIATE DIRECTOR") {
                $recruiters_list = $this->assignrecruiterstomgr->get_all_recruiters();
                $managers_list = $this->assignmanagerstodirector->get_all_managers();
                $data['recruiterlist'] = $recruiters_list;
                $data['managerslist'] = $managers_list;
                $this->load->view('assign-recrs-to-mgr-view', $data);
            } else {
                redirect('dashboard/changepwd', 'refresh');
            }
            
        }else {
            redirect('dashboard', 'refresh');
        }
    }

    function get_selected_recrs_mgr_ids(){
        $recruiters_ids = $this->input->post('recruiterlist');
        $manager_id = $this->input->post('managerslist');

        foreach ($recruiters_ids as $recruiters_id) {
            $insert_data = array(
                'recruiter_id' => $recruiters_id,
                'manager_id'  => $manager_id,
                'isActive'    => 1
            );
            $data['message'] = $this->assignrecruiterstomgr->insert_assigned_mgrs_to_dir($insert_data);
        }
        $this->session->set_flashdata('message', $data['message']);
        redirect('assignrecrstomgr', 'refresh');
    }

    function view(){
        $session_data = $this->session->userdata('logged_in');
        $usertype = $session_data['usertype'];
        $user_id = $session_data['id'];
        if($this->session->userdata('logged_in')){
            if ($usertype == "SUPERADMIN") {
                $view_all = $this->assignrecruiterstomgr->view_all();
                $data['viewall'] = $view_all;
                $this->load->view('view-assigned-recrs-to-mgr', $data);
            }else{
                $view_all = $this->assignrecruiterstomgr->get_all_recruitersBymgrID($user_id);
                $data['viewall'] = $view_all;
                $this->load->view('view-assigned-recrs-to-mgr', $data);
            }
        }else {
            redirect('dashboard', 'refresh');
        }
    }

    function edit($id){
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $usertype = $session_data['usertype'];
            if ($usertype == "SUPERADMIN") {
                $recruiters_list = $this->assignrecruiterstomgr->get_all_recruiters();
                $managers_list = $this->assignmanagerstodirector->get_all_managers();
                $data['recruiterslist'] = $recruiters_list;
                $data['managerslist'] = $managers_list;
                $data['managerID'] = $id;
                $this->load->view('edit-assigned-recrs-to-mgr-view', $data);
            } else{
                $recruiters_list = $this->assignrecruiterstomgr->get_all_recruiters();
                $managers_list = $this->assignmanagerstodirector->get_all_managers();
                $data['recruiterslist'] = $recruiters_list;
                $data['managerslist'] = $managers_list;
                $data['managerID'] = $id;
                $this->load->view('edit-assigned-recrs-to-mgr-view', $data);
            }
        } else {
            redirect('dashboard', 'refresh');
        }
    }

    function update(){
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $usertype = $session_data['usertype'];
            if ($usertype == "SUPERADMIN") {
                $manager_id = $this->input->post('managerid');
                $recruiter_ids = $this->input->post('recruiterslist');
                $recruiter_list_byMgr_id = $this->assignrecruiterstomgr->get_all_recruitersBymgrID($manager_id);
                foreach ($recruiter_list_byMgr_id as $recr_id) {
                    if ($recr_id->isActive != 0) {
                        $recruiter_list_db[] = $recr_id->recruiter_id;
                    }
                }
                $clean1 = array_diff($recruiter_list_db, $recruiter_ids); 
                $clean2 = array_diff($recruiter_ids, $recruiter_list_db);
                $final_output = array_merge($clean1, $clean2);

                if (!empty($final_output)) {
                    foreach ($final_output as $recr_id) {
                        $recrID_status = $this->assignrecruiterstomgr->find_mgrData_by_dirid($recr_id, $manager_id);
                        if (!empty($recrID_status)) {
                            if ($recrID_status->isActive == 0) {
                                $active_status = 1;
                                $data['message'] = $this->assignrecruiterstomgr->update_isActive($recr_id, $manager_id, $active_status);
                            } else {
                                $active_status = 0;
                                $data['message'] = $this->assignrecruiterstomgr->update_isActive($recr_id, $manager_id, $active_status);
                            }
                        } else {
                            $insert_data = array(
                                'recruiter_id' => $recr_id,
                                'manager_id'  => $manager_id,
                                'isActive'    => 1
                            );
                            $data['message'] = $this->assignrecruiterstomgr->insert_assigned_mgrs_to_dir($insert_data);
                        }
                    }
                    $this->session->set_flashdata('message', $data['message']);
                    redirect('assignrecrstomgr', 'refresh');
                }else{
                    echo "recruiters status are same";
                }
            }
        }else {
            redirect('dashboard', 'refresh');
        }
    }
}
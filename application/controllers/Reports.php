<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class Reports extends CI_Controller {

  function __construct()
  {
    parent::__construct();
	$this->load->helper('url');
	$this->load->library('session');
  $this->load->model('user','',TRUE);
  $this->load->model('visa');
  $this->load->model('reportsmodel');
  $this->load->model('manager');
  $this->load->model('submission');
  $this->load->model('recruiter');
  $this->load->model('assignmanagerstodirector');
  $this->load->model('clientdetails');
  }

  function index() {
  
    if($this->session->userdata('logged_in')) {
      $session_data = $this->session->userdata('logged_in');
      $usertype = $session_data['usertype']; 
      if($usertype == "SUPERADMIN") {
				$data["directors"] = $this->user->get_directors();
			} elseif($usertype == "DIRECTOR") {
				$directorId = $session_data['id'];
        $data["managerslist"] = $this->assignmanagerstodirector->get_all_managersByDirectorID($directorId);
				// $data["managerslist"] = $this->user->get_managers($directorId);
			} elseif($usertype == "MANAGER"){
				$managerId = $session_data['id'];
				$data["recruiterlist"] = $this->user->get_reqruiter($managerId);
			}else{
				$data['recruiterID'] = $session_data['id'];
			}
      // $data["manager"] = $this->user->get_manager();
      // $data['directors'] = $this->assignmanagerstodirector->get_all_directors();
      //$data['managerslist'] = $this->assignmanagerstodirector->get_all_managers();
      // $data["recruiterlist"] = $this->user->get_reqruiter($managerId);
      $data['usertype'] = $session_data['usertype'];
      $data['managerslist'] = $this->assignmanagerstodirector->get_all_managersByDirectorID($session_data['id']);
      // $data['recruiterlist'] = $this->user->get_reqruiter($session_data['id']);
      $this->load->view('reportspage_view', $data);
    } 
        
  }

  function getreports() {

    if($this->session->userdata('logged_in')) {
      $session_data = $this->session->userdata('logged_in');
      $directorId = $this->input->post('directorId');
      $mngrid = $this->input->post('mngrid');
      $recid = $this->input->post('recid');
      $fromdate = $this->input->post('fromdate');
      $todate = $this->input->post('todate');
      $totalDates 		= $this->submission->getDatesArray($fromdate, $todate);
      //$status = $this->input->post('status');

      $start    = new DateTime($fromdate);
      $start->modify('first day of this month');
      $end      = new DateTime($todate);
      $end->modify('first day of next month');
      $interval = DateInterval::createFromDateString('1 month');
      $period   = new DatePeriod($start, $interval, $end);
      $monthyear = [];
      foreach ($period as $dt) {
          $monthyear[] = $dt->format("Y-m");
      }
      krsort($monthyear);
      
      if ($session_data['usertype'] == "SUPERADMIN" || $session_data['usertype'] == "DIRECTOR" || $session_data['usertype'] == "MIS" || $session_data['usertype'] == "HR") {
        if ($directorId != 'All') {
          
          $data['clientTrackers'] = $this->reportsmodel->submissions_client_tracker_report($mngrid, $recid, $fromdate, $todate);  
          $data['interviewTrackers'] = $this->reportsmodel->submissions_interview_tracker_report($mngrid, $recid, $fromdate, $todate);
          $data['stats'] = $this->reportsmodel->submissions_stats_report($mngrid, $recid, $fromdate, $todate);
          $data['comparision'] = $this->reportsmodel->submissions_comparision_report($mngrid, $recid, $fromdate, $todate);
          $data['recruitersdata'] = $this->reportsmodel->submissions_recruiterdata_report($mngrid, $recid, $fromdate, $todate);  
          $data['monthyears'] = $monthyear;
          $totalDates 		= $this->submission->getDatesArray($fromdate, $todate);
          $targetResult	= $this->manager->getManagerTotalTarget($mngrid, $fromdate, $todate, count($totalDates)); 
          $targets 		= $targetResult['sumtarget'];
          $data['targets'] = $targets;
          if ($mngrid == 'All') {
            $data['managers'] = $this->manager->getManagerByID($mngrid);
          } else {
            $data['managers'] = $this->manager->getManagerByID($mngrid);
          }
          if ($mngrid == 'All') {
            $data['managerId'] = $mngrid;
          } else {
            $data['managerId'] = $mngrid;
          }
        } else {
          // If Director ALL
          
          $data['allclientTrackers'] = $this->reportsmodel->all_submissions_client_tracker_report($fromdate, $todate);
          $data['allinterviewTrackers'] = $this->reportsmodel->all_submissions_interview_tracker_report($fromdate, $todate);
          $data['allstats'] = $this->reportsmodel->all_submissions_stats_tracker_report($fromdate, $todate);
          $data['allrecruitersdata'] = $this->reportsmodel->all_submissions_recruiterdata_report($fromdate, $todate); 
          $data['monthyears'] = $monthyear;
          $data['fromdate'] = $fromdate;
          $data['todate'] = $todate;
          
          $data['usertype'] = $session_data['usertype'];
          if ($fromdate != "" && $todate != "") {
            $data['fromdate'] = $fromdate;
            $data['todate'] = $todate;
            $data['dates'] 		= $this->submission->getDatesArray($fromdate, $todate);	
          } else {
            $data['fromdate'] = date('Y-m-01');
            $data['todate'] = date('Y-m-d');
            $data['dates'] 		= $this->submission->getDatesArray($fromdate, $todate);	
          }
          

          $this->load->view('alldirectorsreports_view', $data);
        }
        

      } elseif ($session_data['usertype'] == "MANAGER") {
        $managerId = $session_data['id'];
        $data['clientTrackers'] = $this->reportsmodel->submissions_client_tracker_report($managerId, $recid, $fromdate, $todate);  
        $data['interviewTrackers'] = $this->reportsmodel->submissions_interview_tracker_report($managerId, $recid, $fromdate, $todate);
        $data['stats'] = $this->reportsmodel->submissions_stats_report($managerId, $recid, $fromdate, $todate);
        $data['comparision'] = $this->reportsmodel->submissions_comparision_report($managerId, $recid, $fromdate, $todate);
        $data['recruitersdata'] = $this->reportsmodel->submissions_recruiterdata_report($managerId, $recid, $fromdate, $todate);
        $data['monthyears'] = $monthyear;
        $totalDates 		= $this->submission->getDatesArray($fromdate, $todate);
        $targetResult	= $this->manager->getManagerTotalTarget($managerId, $fromdate, $todate, count($totalDates)); 
        $targets 		= $targetResult['sumtarget'];
        $data['targets'] = $targets;
        $data['recid'] = $recid;
        if($recid == "All"){
          $getRecruiters = $this->user->get_reqruiter($managerId);
          foreach ($getRecruiters as $recruiters) {
            $recruiter_ids [] = $recruiters->recruiter_id;
          }
          $totalDates 		= $this->submission->getDatesArray($fromdate, $todate);
          $targetResult	= $this->recruiter->getRecruiterArrayTotalTarget($recruiter_ids, $fromdate, $todate, count($totalDates));
          $targets 		= $targetResult['sumtarget'];
          $data['targets'] = $targets;
        }else{
          $targetResult	= $this->recruiter->getRecruiterArrayTotalTarget($recid, $fromdate, $todate, count($totalDates));
          $targets 		= $targetResult['sumtarget'];
          $data['targets'] = $targets;
        }
        if ($mngrid == 'All') {
          $data['managers'] = $this->manager->getManagerByID($managerId);
        } else {
          $data['managers'] = $this->manager->getManagerByID($managerId);
        }
        if ($mngrid == 'All') {
          $data['managerId'] = $managerId;
        } else {
          $data['managerId'] = $managerId;
        }
      } else {
        // if Recruiter
        $username = $session_data['username'];
        $recid = $this->reportsmodel->getRecId($username);

        $data['clientTrackers'] = $this->reportsmodel->rsubmissions_client_tracker_report($recid, $fromdate, $todate);  
        $data['interviewTrackers'] = $this->reportsmodel->rsubmissions_interview_tracker_report($recid, $fromdate, $todate);
        $data['stats'] = $this->reportsmodel->rsubmissions_stats_report($recid, $fromdate, $todate);
        $data['comparision'] = $this->reportsmodel->rsubmissions_comparision_report($recid, $fromdate, $todate);
        $data['recruitersdata'] = $this->reportsmodel->rsubmissions_recruiterdata_report($recid, $fromdate, $todate);  
        $data['monthyears'] = $monthyear;
        
        $totalDates 		= $this->submission->getDatesArray($fromdate, $todate);
        // var_dump($totalDates);
        $targetResult	= $this->manager->getRecruiterTotalTarget($session_data['username'], $fromdate, $todate, count($totalDates)); 
        $targets 		= $targetResult['sumtarget'];
        $data['targets'] = $targets;
        $data['recruitername'] = $session_data['username'];
      }
            
      $data['fromdate'] = $fromdate;
      $data['todate'] = $todate;
      $data['dates'] 		= $this->submission->getDatesArray($fromdate, $todate);	
      $data['usertype'] = $session_data['usertype'];
      //var_dump($data['interviewTrackers']);exit;
      //count($data['dates']);
      //var_dump($data['comparision']['last_week'], $data['comparision']['last_last_week']); exit;
      //var_dump($data['recruitersdata']);exit;
      if ($directorId != 'All' && $session_data['usertype'] != "RECRUITER") {
        $this->load->view('reportsajaxpage_view', $data);
      }else {
        $this->load->view('reportsajaxpage_recr_view', $data);
      }

    }


  }

  function logout()
  {
    $this->session->unset_userdata('logged_in');
    session_destroy();
    $this->load->view('cdslogpage_view');
  }

}

?>

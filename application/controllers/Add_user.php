<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(1);
class Add_User extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('user','',TRUE);
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('form_validation');
        session_start();
    }
      

    function index() {
  	    $data['error']='';
  	    if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $usertype = $session_data['usertype']; 
            if($usertype != "SUPERADMIN"){
                redirect('dashboard/changepwd', 'refresh');
            }  
            
            //$data['menuname']	= 'users';
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['usertypes_data'] = $this->user->get_usertype_data();
            $this->load->library('form_validation');
            $this->form_validation->set_rules('uname', 'Username', 'required');
            $this->form_validation->set_rules('ptno', 'Ptno', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('pwd', 'Password', 'required');
            $this->form_validation->set_rules('cpwd', 'Confirm Password', 'required');
            
            if($this->form_validation->run() == FALSE) {
                $this->load->view('add_user_view', $data);
            }
            else {
                //print_r($_POST);exit;
                $data['error'] = $this->user->add_user_insert();
                //$this->load->view('add_user_view', $data);
                $this->session->set_flashdata('updatemessage', $data['error']);
                redirect('add_user');
            }
        }
    else {
      //If no session, redirect to ctslogpge page
      redirect('cdslogpage', 'refresh');
	}
  }

    function users_list() {
        $data['error']='';
        if($this->session->userdata('logged_in')) {
        
        $session_data = $this->session->userdata('logged_in');
        $usertype = $session_data['usertype']; 
        if($usertype != "SUPERADMIN"){
            redirect('dashboard/changepwd', 'refresh');
        }   

        $data['menuname']	= 'users';
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        $data['keys']=$this->user->users_list_model();
        $this->load->view('users_view', $data);	
        }
        else {
        //If no session, redirect to ctslogpge page
            redirect('cdslogpage', 'refresh');
        }
    }

    function users_edit($id) {
  	    $data['error']='';
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['menuname'] = 'users';
            $data['usertypes_data'] = $this->user->get_usertype_data();
            if($this->input->post('updateuser')== 'updateuser'){
                $data['error']=$this->user->users_edit_model($id);
                $data['keys']=$this->user->users_update($id);
                //$this->load->view('users_data_view', $data);
                $this->session->set_flashdata('updatemessage', $data['error']);
                redirect('add_user');
            }
            else{
                $data['keys']=$this->user->users_update($id);	
                $this->load->view('users_data_view', $data);
            }
        } else {
        //If no session, redirect to ctslogpge page
        redirect('cdslogpage', 'refresh');
        }
    }

    function inactive_users_list() {
  	    $data['error']='';
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $usertype = $session_data['usertype']; 
            if($usertype != "SUPERADMIN"){
                redirect('dashboard/changepwd', 'refresh');
            }
            $data['menuname']	= 'users';	
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['keys']=$this->user->inactive_users_list_model();
            //print_r($data);exit;
            $this->load->view('inactive_users_view', $data);
        } else {
            //If no session, redirect to ctslogpge page
            redirect('cdslogpage', 'refresh');
        }
    }

    function panzer_inactive_userslist() {
  	    $data['error']='';
        if($this->session->userdata('logged_in')) {

            $session_data = $this->session->userdata('logged_in');
            $usertype = $session_data['usertype']; 
            if($usertype != "SUPERADMIN") {
            redirect('dashboard/changepwd', 'refresh');
            }

            $data['menuname']	= 'users';	
            $data['category']	= 'Panzer';
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['keys']=$this->user->get_inactive_list(1);
            //print_r($data);exit;
            $this->load->view('inactive_users_view', $data);
        } else {
            //If no session, redirect to ctslogpge page
            redirect('cdslogpage', 'refresh');
        }
    }

    function rpo_inactive_userslist() {
  	    $data['error']='';
  	    if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $usertype = $session_data['usertype']; 
            if($usertype != "SUPERADMIN") {
            redirect('dashboard/changepwd', 'refresh');
            }
            $data['menuname']	= 'users';	
            $data['category']	= 'RPO';
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['keys']=$this->user->get_inactive_list(2);
            //print_r($data);exit;
            $this->load->view('inactive_users_view', $data);
        } else {
            //If no session, redirect to ctslogpge page
            redirect('cdslogpage', 'refresh');
        }
    }

    function canada_inactive_userslist() {
  	    $data['error']='';
  	    if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $usertype = $session_data['usertype']; 
            if($usertype != "SUPERADMIN") {
            redirect('dashboard/changepwd', 'refresh');
            }
            
            $data['menuname']	= 'users';	
            $data['category']	= 'Canada';
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['keys']=$this->user->get_inactive_list(3);
            //print_r($data);exit;
            $this->load->view('inactive_users_view', $data);
        } else {
            //If no session, redirect to ctslogpge page
            redirect('cdslogpage', 'refresh');
	    }
    }
}
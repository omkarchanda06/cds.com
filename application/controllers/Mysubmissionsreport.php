<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(0);
class Mysubmissionsreport extends CI_Controller {

  function __construct()
  {
    parent::__construct();
	$this->load->model('user','',TRUE);
	$this->load->library('pagination');
	$this->load->helper('url');
	$this->load->library('session');
	session_start();
  }

  function index()
  {
  	//print_r('hi');exit;
    if($this->session->userdata('logged_in')){
	   $session_data = $this->session->userdata('logged_in');
	  	$data['roles'] = $this->session->userdata('roles');
		
		$managerId = $session_data['id'];
		$data["reqruiterlist"] = $this->user->get_reqruiter($managerId);
		$reqruiterid = $this->input->post('reqruiterid');
		$date1 = $this->input->post('date1');
		$date2 = $this->input->post('date2');
		$status = $this->input->post('status');
		if($date1 == ""){
			$date1 = date('Y-m-j', strtotime('-1 Weekday'));
		}
		if($date2 == ""){
			$date2 = date('Y-m-j', strtotime('+3 Weekday'));
		}
		//echo $date1." ".$date2." ".$status." ".$managerId;exit;
		$results = $this->user->submissions_export_result($managerId, $reqruiterid, $date1, $date2, $status);
		
		$data['rows'] 		= $results;
	  	$data['username'] 	= $session_data['username'];
		
		$data["manager"] = $this->user->get_manager();
		$this->load->view('mysubmissionsexport_view', $data);	
	}
    else
    {
      //If no session, redirect to login page
      redirect('cdslogpage', 'refresh');
	}
	//$this->load->view('totalsubmissions_view');
  }
  
  function mysubmissions()
  {
  		
		if($this->session->userdata('logged_in'))
	    {
		$session_data = $this->session->userdata('logged_in');
		$data['username'] = $session_data['username'];	
		$config = array();
		$config["base_url"] = base_url() . "index.php/totalsubmissions/mysubmissions/index/";
		$coutsub = $this->user->mysubmissions_count();
		foreach ($coutsub as $cs){
			$cs = $cs->countsub;
		}
		$config["total_rows"] = $cs;
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		// Next Link
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		// Previous Link
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		// Current Link
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		// Digit Link
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config["per_page"] = 30;
        $config["uri_segment"] = 4;
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $results = $this->user->mysubmissions_result($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();
		$data['rows'] = $results;
		$data['searchtype'] = $this->uri->segment(2);
		$data['key1']=$this->user->addsub_users_location();
		$this->load->view('mysubmissions_view',$data);
		}
		else{
			redirect('cdslogpage', 'refresh');
		}		
	}

 function deletemysubmissions($id)
 {
 	//print_r($id);exit;
	$data['key1']=$this->user->addsub_users_location();
	$data['error']='';
	$data['records']=$this->user->delete_mysubs($id);
	$data['error']='Deleted Succesfully';
	$session_data = $this->session->userdata('logged_in');
	$data['username'] = $session_data['username'];	
	$config = array();
	$config["base_url"] = base_url() . "index.php/totalsubmissions/mysubmissions/index/";
	$coutsub = $this->user->mysubmissions_count();
	foreach ($coutsub as $cs){
		$cs = $cs->countsub;
	}
	$config["total_rows"] = $cs;
	$config['first_link'] = 'First';
	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';
	
	$config['last_link'] = 'Last';
	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';
	
	// Next Link
	$config['next_link'] = '&raquo;';
	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';
	
	// Previous Link
	$config['prev_link'] = '&laquo;';
	$config['prev_tag_open'] = '<li>';
	$config['prev_tag_close'] = '</li>';
	
	// Current Link
	$config['cur_tag_open'] = '<li class="active">';
	$config['cur_tag_close'] = '</li>';
	
	// Digit Link
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	
	$config["per_page"] = 30;
	$config["uri_segment"] = 4;
	
	$this->pagination->initialize($config);
	$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
	$results = $this->user->mysubmissions_result($config["per_page"], $page);
	$data["links"] = $this->pagination->create_links();
	$data['rows'] = $results;
	$data['searchtype'] = $this->uri->segment(2);
	$this->load->view('mysubmissions_view',$data);
	$this->load->view('mysubmissions_view', $data);
 }	
 
 function editmysubmissions($id)
 {
 	
	$data['error']='';
  	if($this->session->userdata('logged_in'))
    {
	  $session_data = $this->session->userdata('logged_in');
      $data['username'] = $session_data['username'];
	  //$data['searchtype'] = $this->uri->segment(2);
	  $data['key1']=$this->user->addsub_users_location();
	  
	  if($this->input->post('submit')== 'submit'){
	  //print_r($_POST);exit;
	 
	  	$config['upload_path'] = 'resumes/';
		$config['allowed_types'] = '*';
		$config['max_size']	= '2048';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		
		$this->load->library('upload', $config);
		if($_FILES["userfile"]["name"] != ""){
			if (!$this->upload->do_upload())
			{
				$error = array('error' => $this->upload->display_errors());
				
				$data['error']= 'The filetype you are attempting to upload is not allowed.';
				$data['keys']=$this->user->mysub_val($id);
				$data['result1']=$this->user->mysub_val_location();	
				$data['result2']=$this->user->mysub_val_recruiters();	
				$data['result3']=$this->user->mysub_val_visa();
				$data['searchtype'] = "mysubmissions";
				$this->load->view('mysub_view', $data);
			}
			else
			{
				$a = $this->upload->data();
				$filename =  $a['file_name'];
				$data = array('upload_data' => $this->upload->data());
				$data['error']=$this->user->edit_submissions_model($filename);
				$data['keys']=$this->user->mysub_val($id);
				$data['result1']=$this->user->mysub_val_location();	
				$data['result2']=$this->user->mysub_val_recruiters();	
				$data['result3']=$this->user->mysub_val_visa();
				$data['searchtype'] = "mysubmissions";
				$this->load->view('mysub_view', $data);
			}
		}else{
			$filename = $this->input->post('file');
			$filename = explode("resumes/", $filename);
			$data['error']=$this->user->edit_submissions_model($filename[1]);
			$data['keys']=$this->user->mysub_val($id);
			$data['result1']=$this->user->mysub_val_location();	
			$data['result2']=$this->user->mysub_val_recruiters();	
			$data['result3']=$this->user->mysub_val_visa();
			$data['searchtype'] = "mysubmissions";
			$this->load->view('mysub_view', $data);
		}
	  //$data['keys']=$this->user->users_update($id);
	  
	  //$data['error']='User Updated Successfully';
	 // $this->load->view('mysub_view', $data);
	  }
	  else{
	   
	   $data['keys']=$this->user->mysub_val($id);	
	   $data['result1']=$this->user->mysub_val_location();	
	   $data['result2']=$this->user->mysub_val_recruiters();	
	   $data['result3']=$this->user->mysub_val_visa();	
	   $data['searchtype'] = "mysubmissions";
	   $this->load->view('mysub_view', $data);
	  }
	 
	  //$data['keys1']=$this->user->users_edit_model($id);
	  //print_r($data);exit;
	  	
	
    }
    else
    {
      //If no session, redirect to ctslogpge page
	  $data['searchtype'] = "mysubmissions";
      redirect('cdslogpage', 'refresh');
	}
	
 }
 
  function comments($id)
  {
  	//print_r($id);
	$_SESSION['id']=$id;
	//print_r($_SESSION);exit;
	if($this->session->userdata('logged_in'))
	{
	$session_data = $this->session->userdata('logged_in');
	$data['username'] = $session_data['username'];	
	//$_SESSION['id']=$id;
	$data['error']='';
	if($this->input->post('submit')== 'Add Comment')
	{
	//print_r($_POST);exit;
	
	$data['error']=$this->user->commments_insert();
	$data['row']= $this->user->comments_data();
	$this->load->view('comments_view',$data);	
	}
	else{
	$data['row']= $this->user->comments_data();
	$this->load->view('comments_view',$data);	
	}
	}
	else{
	redirect('cdslogpage', 'refresh');
   }
  }
	
  function edit($id)
  {
		if($this->session->userdata('logged_in'))
	    {
		$session_data = $this->session->userdata('logged_in');
		$data['username'] = $session_data['username'];	
		$data['error']='';
		if($this->uri->segment(4) == 'ok')
		{
			
		$data['records']=$this->user->update_data($id);
		$data['records1']=$this->user->update_attachments($id);
		$data['error']=$this->uri->segment(4);	
		$this->load->view('update_view',$data);	
		}
		else{
		$data['records']=$this->user->update_data($id);
		$data['records1']=$this->user->update_attachments($id);
		//print_r($data);exit;
		$this->load->view('update_view',$data);	
		}
		}
		else{
			redirect('cdslogpage', 'refresh');
		}
		
	}
	
  function pic_edit($id,$sid)
  {
  		$session_data = $this->session->userdata('logged_in');
		$data['username'] = $session_data['username'];
		if($this->uri->segment(5) == 'dok')
		{//print_r($sid);exit;
		$data['records']=$this->user->update_data($id);
		$data['records1']=$this->user->update_attachments1($id);
		$data['error']=$this->uri->segment(5);	
		$this->load->view('update_view',$data);	
		}
	}

  function update_rec($id)
  {
		if($this->session->userdata('logged_in'))
	    {
			$date=$this->input->post('datepicker');
			$date1=$this->input->post('deadline');
			$data=$this->user->update_record($id);
		}
		else{
			redirect('cdslogpage', 'refresh');
		}
	
	}
  function insert_job()
  {
		if($this->session->userdata('logged_in'))
	    {
		$date=$this->input->post('datepicker');
		$date1=$this->input->post('deadline');
		$converdate=explode('/',$date);
		$datepciker=$converdate[2].'-'.$converdate[1].'-'.$converdate[0];
		$converdate1=explode('/',$date1);
		$deadline=$converdate1[2].'-'.$converdate1[1].'-'.$converdate1[0];	
		$this->user->insert_data($datepciker,$deadline);
		redirect('home/index');
		}
		else{
			redirect('cdslogpage', 'refresh');
		}	
		
		
	}
	
  function delete($id,$sid)
  {
		$data['records']=$this->user->delete_data($id,$sid);
		redirect('home/index');
	}
  function delete_pic($id,$sid,$filename)
  {
		
		$this->db->delete('attachments', array('id' => $sid));
		$filestring = base_url().'uploads/'.$filename;
		$filestring = APPPATH.'../uploads/'.$filename;
		unlink ($filestring);
		redirect('home/pic_edit/'.$id.'/'.$sid.'/dok');
	}

	
  function doUpload1($id){
	
	$config['upload_path'] = 'uploads/';
	$config['allowed_types'] = '*';
	$config['max_size'] = '1000';
	$config['max_width'] = '1920';
	$config['max_height'] = '1280';

	$this->load->library('upload', $config);
		if(!$this->upload->do_upload())
		echo $this->upload->display_errors();
		else {
		$fInfo = $this->upload->data();
		$data=$this->user->insert_attach($id);
		
		}
	}


	function positions() {

	  header('Content-Type: application/json');
	  $search = '';
	  if(isset($_GET['term'])) {
	  $search = trim($_GET['term']); 
	  }

	  $suggestions   = array();

	        if($search != '') {

	        $submissions = $this->user->getJobTitlesBySearchLimit($search, 10);  
	        
	        if(sizeof($submissions) > 0) {
	  foreach ($submissions as $key => $submission) {
	            $suggestions[] = trim($submission->title);
	        }

	        $suggestions = array_unique($suggestions);

	  echo json_encode($suggestions);
	         exit();

	   }

	  }


	  echo json_encode($suggestions);

  }

  /*fetching client names for requirement page*/

  	function getClientsNames() {
		$session_data         = $this->session->userdata('logged_in');
        $user_id = $session_data['id'];
		header('Content-Type: application/json');
		$search = '';
		if(isset($_GET['term'])) {
			$search = trim($_GET['term']); 
		}
		$suggestions   = array();
			
		if($search != '') {
			$submissions = $this->user->getClientNamesBySearchLimit($search, $user_id, 10);  
			
			if(sizeof($submissions) > 0) {
				foreach ($submissions as $key => $submission) {
					$suggestions[] = trim($submission->client_name);
				}
				$suggestions = array_unique($suggestions);

				echo json_encode($suggestions);
				exit();
			}
		}
		echo json_encode($suggestions);
	}

	/*fetching client names with job title*/

	function getClientWithJobTitle() {
		header('Content-Type: application/json');
		$search = '';
		if(isset($_GET['term'])) {
			$search = trim($_GET['term']); 
		}
		$suggestions   = array();
			
		if($search != '') {
			$submissions = $this->user->getClientWithjobTitle($search, 10);  
			
			if(sizeof($submissions) > 0) {
				foreach ($submissions as $key => $submission) {
					$suggestions[] = trim($submission->job_title).', '.trim($submission->client_company);
				}
				$suggestions = array_unique($suggestions);

				echo json_encode($suggestions);
				exit();
			}
		}
		echo json_encode($suggestions);
	}

	/*fetching cities names for requirement page*/

	function getCitiesnames() {
		header('Content-Type: application/json');
		$search = '';
		if(isset($_GET['term'])) {
			$search = trim($_GET['term']); 
		}
		$suggestions   = array();
			
		if($search != '') {
			$submissions = $this->user->getCitiesNamesBySearchLimit($search, 10);
			if(sizeof($submissions) > 0) {
				foreach ($submissions as $key => $submission) {
					$suggestions[] = trim($submission->city);
				}
				$suggestions = array_unique($suggestions);

				echo json_encode($suggestions);
				exit();
			}
		}
		echo json_encode($suggestions);
	}

	/*set selected state based on city field*/

	function getSelectedCity(){
        $selected_city = $this->input->post('selected_city');
		$find_state_code = $this->user->getStateCode($selected_city);
		$state_code = $find_state_code[0]->state_code;
		$get_state_code_id = $this->user->set_location($state_code);
		$state_code_id = $get_state_code_id[0]->lid;
		echo json_encode(array("success"=>"success", "state_code_id"=>$state_code_id));
	}

	/*showing adding job rates dropdown*/
	function ShowingJobRates(){
		header('Content-Type: application/json');
		$search = '';
		if(isset($_GET['term'])) {
			$search = trim($_GET['term']); 
		}
		//echo $search;die();
		$suggestions   = array();
		if($search != '' && $search >=10 && $search <= 150 && strpos($search, 'on') === false) {
			$submissions = array($search." - on CTC", $search." - on 1099", $search." - on W2");
			if(sizeof($submissions) > 0) {
				foreach ($submissions as $submission) {
					$suggestions[] = trim($submission);
				}
				$suggestions = array_unique($suggestions);

				echo json_encode($suggestions);
				exit();
			}
		}else{
			$split_search = str_split($search);
			$search = $split_search[0]."".$split_search[1];
			$submissions = array($search." - on CTC", $search." - on 1099", $search." - on W2");
			if(sizeof($submissions) > 0 && $search >=10 && $search <= 150) {
				foreach ($submissions as $submission) {
					$suggestions[] = trim($submission);
				}
				$suggestions = array_unique($suggestions);

				echo json_encode($suggestions);
				exit();
			}
		}
		echo json_encode($suggestions);
	}

	/*showing adding job salary rates dropdown*/
	function ShowingSalary(){
		header('Content-Type: application/json');
		$search = '';
		if(isset($_GET['term'])) {
			$search = trim($_GET['term']); 
		}
		//echo $search;die();
		$suggestions   = array();
		if($search != '' && $search >=1 && $search <= 300 && strpos($search, 'k') === false) {
			$submissions = array($search."k");
			if(sizeof($submissions) > 0) {
				foreach ($submissions as $submission) {
					$suggestions[] = trim($submission);
				}
				$suggestions = array_unique($suggestions);

				echo json_encode($suggestions);
				exit();
			}
		}else{
			// $split_search = str_split($search);
			// $search = $split_search[0]."".$split_search[1];
			// echo $search;exit();
			// $submissions = array($search."k");
			// if(sizeof($submissions) > 0 && $search >=1 && $search <= 300) {
			// 	foreach ($submissions as $submission) {
			// 		$suggestions[] = trim($submission);
			// 	}
			// 	$suggestions = array_unique($suggestions);

			// 	echo json_encode($suggestions);
			// 	exit();
			// }
		}
		echo json_encode($suggestions);
	}

  function sugggestrecruiter() {

  		header('Content-Type: application/json');
		$search = '';
		if(isset($_GET['term'])) {
		$search = trim($_GET['term']);	
		}

		$suggestions   = array();

        if($search != '') {

        $recruiters = $this->user->getRecruiterBySearchLimit($search, 10);		
        
        $i = 0;
        if(sizeof($recruiters) > 0) {
		foreach ($recruiters as $key => $recruiter) {
            $suggestions[$i]['label'] = trim($recruiter->recruiter);
            $suggestions[$i]['index'] = $recruiter->id;
         $i++;   
        }

        //$suggestions = array_unique($suggestions);

		echo json_encode($suggestions);
         exit();

			}

		}


		echo json_encode($suggestions);

  }


   function logout() {
    $this->session->unset_userdata('logged_in');
    session_destroy();
	//print_r($_SESSION);exit;
    redirect('cdsdashboard', 'refresh');
  }


}



?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class cdslogpage extends CI_Controller {

  function __construct()
  {
    parent::__construct();
	$this->load->helper('url');
	$this->load->library('session');
  }

  function index()
  {
	//  print_r($this->session->userdata); die();
  	if($this->session->userdata('logged_in')){
  		redirect('dashboard/changepwd');
  	}
    $this->load->helper('form');
    $this->load->view('cdslogpage_view');
  }

}

?>
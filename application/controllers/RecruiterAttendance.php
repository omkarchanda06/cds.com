<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(1);
class RecruiterAttendance extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('user', '', TRUE);
        $this->load->model('manager', '', TRUE);
        $this->load->model('director', '', TRUE);
        $this->load->model('recruiter', '', TRUE);
        $this->load->helper('url');
        $this->load->library('session');
        session_start();
    }

    function index()
    {
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $usertype = $session_data['usertype'];

            if ($usertype != "SUPERADMIN") {
                redirect('dashboard/changepwd', 'refresh');
            }
            // $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['row'] = $this->user->attendence_data_all();
            $this->load->view('attendance_view', $data);
        } else {
            //If no session, redirect to ctslogpge page
            redirect('cdslogpage', 'refresh');
        }
    }

    function status_attendence()
    {
        //print_r($rname);exit;
        if ($this->session->userdata('logged_in')) {

            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $status = $this->uri->segment(3);
            $rname = $this->uri->segment(4);
            $rname = str_replace("%20", " ", $rname);
            $update = $this->user->status_attendence_model($status, $rname);
            $data['row'] = $this->user->rec_attendence_data();

            if ($session_data['usertype'] == "SUPERADMIN") {
                // redirect('cdsdashboard/all_recruiters_attendance');
                redirect('recruiterattendance');
            }

            if ($session_data['usertype'] == "ADMIN") {
                // $getCategory = $this->user->getCategoryName($session_data['category']);
                // redirect('cdsdashboard/'.strtolower($getCategory).'_attendance');
                redirect('recruiterattendance/recruiters_attendance');
            }

            redirect('recruiterattendance/recruiters_attendance');
        } else {
            //If no session, redirect to ctslogpge page
            redirect('cdslogpage', 'refresh');
        }
    }

    function recruiters_attendance()
    {
        if ($this->session->userdata('logged_in')) {
            $data['category'] = 'My Recruiters';
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['row'] = $this->user->rec_attendence_data_val();

            $this->load->view('attendance_view', $data);
        } else {
            //If no session, redirect to ctslogpge page
            redirect('cdslogpage', 'refresh');
        }
    }
}


/** cds-db-user-pwd: %PD5pzbkH&}X  username: c270751_cds, dbname: c270751_cds_new */
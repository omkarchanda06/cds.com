<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(1);
class Managercount extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('user','',TRUE);
		$this->load->model('submission','',TRUE);
		$this->load->model('manager','',TRUE);
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->library('session');
		session_start();
	}


	function index(){
  		
		$session_data 	= $this->session->userdata('logged_in');
     	$usertype 		= $session_data['usertype'];
     	$directorId         = $this->input->post('directorid');
		$managerId 			= $this->input->post('managerid');

		if($managerId == "" && $session_data['usertype'] == 'SUPERADMIN'){
			$managerId = "All";
		} else {
			$managerId = $session_data['id'];
		}

		$data['usertype'] = $session_data['usertype'];

		
		$fromDate = $this->input->post('date1');
		if($fromDate == ""){
			$fromDate = date('Y-m-j', strtotime('-1 Weekday'));
		}

		$toDate = $this->input->post('date2');
    	if($toDate == ""){
			$toDate = date('Y-m-j', strtotime('+3 Weekday'));
		}


		$usertype = $session_data['usertype'];

        if($usertype == "SUPERADMIN") {
			$submissions 	= $this->submission->getAllSubmissions($fromDate, $toDate);
        } else {

			if($managerId == '' || $managerId == 'All')	
				$submissions 	= $this->submission->getAllSubmissionsOfCategory($fromDate, $toDate, $session_data['category']);
			else
				$submissions 	= $this->submission->getAllSubmissionsOfManager($fromDate, $toDate, $managerId);	
        }
		

		$data['dates'] 		= $this->submission->getDatesArray($fromDate, $toDate);	
		$data['subcount'] 	= $this->submission->createSubmissionsInfoArrayForManager($submissions);	
		

		$usertype = $session_data['usertype'];

        if($usertype == "SUPERADMIN") {
        $data['managers'] = $this->manager->getAllManagers();

        } else {
        
        if($managerId == '' || $managerId == 'All')	
		$data['managers'] = $this->manager->getManagersByCategory($session_data['category']);
		else
		$data['managers'] = $this->manager->getManagerByID($managerId);

        }

        if($managerId == '' || $managerId == 'All')
        $data['managersOptions'] 	= $this->manager->getAllManagers();
    	else
    	$data['managersOptions'] 	= $this->manager->getManagersByCategory($session_data['category']);

        $data['managerId'] 			= $managerId;
        $data['fromDate'] 			= $fromDate;
        $data['toDate'] 			= $toDate;
        $data['setCategoryID'] 		= $session_data['category'];
		$data['menuname'] 			= 'submissionscount';								
		$this->load->view('managercount_view',$data);

	}






	function category($categoryName = 'panzer'){
  		
		$session_data 		= $this->session->userdata('logged_in');
     	$usertype 			= $session_data['usertype'];
     	$data['category'] 	= strtoupper($categoryName);
		$managerId 			= $this->input->post('managerid');

		if($managerId == ""){
			$managerId =	"All";
		}
		if($usertype == "MANAGER"){
			$managerId = 	$session_data['id'];			
		}


		$fromDate = $this->input->post('date1');
		if($fromDate == ""){
			$fromDate = date('Y-m-j', strtotime('-1 Weekday'));
		}

		$toDate = $this->input->post('date2');
    	if($toDate == ""){
			$toDate = date('Y-m-j', strtotime('+3 Weekday'));
		}

		    $setCategoryID = 1;
		if($categoryName == "rpo") {
			$setCategoryID = 2;
		} else if($categoryName == "canada") {
			$setCategoryID = 3;
		}

		if($managerId == '' || $managerId == 'All')	
		$submissions 	= $this->submission->getAllSubmissionsOfCategory($fromDate, $toDate, $setCategoryID);
		else
		$submissions 	= $this->submission->getAllSubmissionsOfManager($fromDate, $toDate, $managerId);	

		
		$data['dates'] 		= $this->submission->getDatesArray($fromDate, $toDate);	
		$data['subcount'] 	= $this->submission->createSubmissionsInfoArrayForManager($submissions);


		
        if($managerId == '' || $managerId == 'All')	
		$data['managers'] = $this->manager->getManagersByCategory($setCategoryID);
		else
		$data['managers'] = $this->manager->getManagerByID($managerId);


		if($managerId == '' || $managerId == 'All')
        $data['managersOptions'] 	= $this->manager->getAllManagers();
    	else
    	$data['managersOptions'] 	= $this->manager->getManagersByCategory($setCategoryID);

		$data['managerId'] 			= $managerId;
		$data['fromDate'] 			= $fromDate;
        $data['toDate'] 			= $toDate;
		$data['setCategoryID'] 		= $setCategoryID;
		$data['menuname'] 			= 'submissionscount';								
		$this->load->view('managercount_view',$data);

	}
}
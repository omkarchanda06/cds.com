<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(E_ALL & ~E_NOTICE);
class Pdfrecruitercount extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('user','',TRUE);
		$this->load->library('pagination');
		$this->load->helper('url');
	$this->load->library('session');
		session_start();
	}
	
	function index(){
  		
		$session_data = $this->session->userdata('logged_in');
     	
		$usertype = $session_data['usertype'];
		
		$managerId=$this->input->post('managerid');
		if($managerId == ""){
			$managerId ="All";
		}
		$reqruiterId=$this->input->post('reqruiterid');
		if($reqruiterId == ""){
			$reqruiterId ="All";
		}
		if($usertype == "MANAGER"){
			$managerId = $session_data['id'];
			$data["reqruiterlist"] = $this->user->get_reqruiter($managerId);
		}
		
		$date=$this->uri->segment(3);
		if($date == ""){
			$date = date('Y-m-j', strtotime('-1 Weekday'));
		}
		$date1=$this->uri->segment(4);
    	if($date1 == ""){
			$date1 = date('Y-m-j', strtotime('+3 Weekday'));
		}
		$manager = $this->user->getmanager();
		$recruiter = $this->user->getrecruiter();
		$submissions = $this->user->getrsubmission($date, $date1);
		$subcount = array();
		$mgrrcr = array();
		foreach($submissions as $sub){
			$subcount[$sub->recruiterId][$sub->sdate] = $sub->submissioncount;
		}
		//print_r($subcount);
		$dates=array();
		$diff= round((strtotime($date1)-strtotime($date))/(60*60*24))+1;
		for ($i = 0; $i < $diff; $i++) {
			$d = date("Y-m-d", (strtotime($date) + ($i * 86400)));
			$t=strtotime($d); 
			$day = date('w', $t);
			
			if($day == 0 || $day == 6){		
			
			}else{
			$dates[] = $d;
			}
			
		}
		
		$totArray = array();
		
		$html = '';
		$html.= '<br><br> <table border="0" width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<th bgcolor="#FF9900" align="center" bgcolor="#FF9900" align="center">Recruiter Names</th>
											<th bgcolor="#FF9900" align="center">Submissions Target</th>';
											  
											foreach($dates as $d){
												$day1=date("D", strtotime($d));
												$day2=date("d M", strtotime($d));
												$html.='<th bgcolor="#FF9900" align="center">'.$day2.'<br/>'.$day1.'</th>';
											}
										$html.='
												<th bgcolor="#FF9900" align="center">Total Subs</th>
												<th bgcolor="#FF9900" align="center">Average</th>
												<th bgcolor="#FF9900" align="center">Achieved % </th>
												
										</tr>';
										
										foreach($manager as $mgr){
										//	echo $managerId;
											$class="alternate-row";
											
											if($managerId == "All"){
												$html.= '<tr><td colspan="300"><h3 style="margin:0;"><strong><hr></strong></h3></td></tr>';
												$html.='<tr><td colspan="300" class="'.$class.'"><h3 style="margin:0;"><strong>'.$mgr->managerName.'</strong></h3></td></tr>';
											}elseif($managerId == $mgr->managerId){
												$html.= '<tr><td colspan="300"><h3 style="margin:0;"><strong><hr></strong></h3></td></tr>';
												$html.= '<tr><td colspan="300" class="'.$class.'"><h3 style="margin:0;"><strong>'.$mgr->managerName.'</strong></h3></td></tr>';	
											}
											//$html.= '<tr><td colspan="300"><h3 style="margin:0;"><strong>'.$mgr->managerName.'</strong></h3></td></tr>';
											
											$i = 0;
											$cdate =  count($dates);
											foreach($recruiter as $rec){
												/*$i++;	
												if($i%2==0){
													$class="alternate-row";
												}else{
													$class="";
												}*/
												if($managerId == "All"){
													if($rec->managerId == $mgr->managerId){
														$rsubmissionTargets = $rec->rsubmissionTargets;
														$rtarget = $rsubmissionTargets/5;
														$rtarget = $rtarget*$cdate;
														$html.= '<tr>';
														$html.= '<td>'.$rec->recruiterName.'</td>';
														$html.= '<td align="center">'.$rtarget.'</td>';
														
														if(isset($totArray[0]))
														$totArray[0] = $totArray[0] + $rec->rsubmissionTargets;
														else
														$totArray[0] = $rec->rsubmissionTargets;
														
														$submTot = 0;
														$j = 1;
														$cq = 0;
														foreach($dates as $d){
														
															if(isset($subcount[$rec->recruiterId][$d]))
															$subm = ($subcount[$rec->recruiterId][$d]);
															else
															$subm = 0;
															$query = mysql_query("select id from recruiters_attendance where rec='".$rec->recruiterName."' and date = '".$d."' and status = 1");
															$cquery = mysql_num_rows($query);
															if($cquery == 1){
																$html.='<td align="center"> <strong>A</strong> </td>';
																$cq1 = 1;
															}else{
																$html.='<td align="center">'.$subm.'</td>';
																$cq1 =0;
															}
															$submTot = $submTot + $subm;
															$cq = $cq + $cq1;
															if(isset($totArray[$j]))
															$totArray[$j] = $totArray[$j] + $subm;
															else						
															$totArray[$j] = $subm;
															
															$j++;
														}
														
														if(isset($totArray[$j]))
														$totArray[$j] = $totArray[$j] + $submTot;
														else
														$totArray[$j] = $submTot;
														$countdates = count($dates);
														$ctdates = $countdates - $cq;
														if($ctdates == 0 || $submTot == 0){
															$avg = 0;
														}else{
															$avg = $submTot/$ctdates;	
														}
														
														if($rtarget > 0 && $submTot != 0){
															$rectarget = ($rtarget/$countdates)*$ctdates;
															$percentage = ($submTot/$rectarget) * 100;
														}else{
															$percentage =0;
														}
														$per = round($percentage, 0);
														
														$html.= '<td align="center">'.$submTot.'</td>';
														$html.= '<td align="center">'.round($avg, 2).'</td>';
														if($per >= '90'){
															$html.= '<td class="'.$class.'" style="background:#FFFF00" align="center">'.$per.'%</td>';
														}else{
															$html.= '<td class="'.$class.'" align="center">'.$per.'%</td>';
														}
														$html.= '</tr>';
													}
												}elseif($managerId == $mgr->managerId){
													if($rec->managerId == $mgr->managerId){
														$rsubmissionTargets = $rec->rsubmissionTargets;
														$rtarget = $rsubmissionTargets/5;
														$rtarget = $rtarget*$cdate;
														if($reqruiterId == "All"){
															$html.= '<tr>';
															$html.= '<td>'.$rec->recruiterName.'</td>';
															$html.= '<td align="center">'.$rtarget.'</td>';
															
															if(isset($totArray[0]))
															$totArray[0] = $totArray[0] + $rec->rsubmissionTargets;
															else
															$totArray[0] = $rec->rsubmissionTargets;
															
															$submTot = 0;
															$j = 1;
															$cq = 0;
															foreach($dates as $d){
															
																if(isset($subcount[$rec->recruiterId][$d]))
																$subm = ($subcount[$rec->recruiterId][$d]);
																else
																$subm = 0;
																$query = mysql_query("select id from recruiters_attendance where rec='".$rec->recruiterName."' and date = '".$d."' and status = 1");
																$cquery = mysql_num_rows($query);
																if($cquery == 1){
																	$html.='<td align="center"> <strong>A</strong> </td>';
																	$cq1 = 1;
																}else{
																	$html.='<td align="center">'.$subm.'</td>';
																	$cq1 =0;
																}
																$submTot = $submTot + $subm;
																$cq = $cq + $cq1;
																if(isset($totArray[$j]))
																$totArray[$j] = $totArray[$j] + $subm;
																else						
																$totArray[$j] = $subm;
																
																$j++;
															}
															
															if(isset($totArray[$j]))
															$totArray[$j] = $totArray[$j] + $submTot;
															else
															$totArray[$j] = $submTot;
															$countdates = count($dates);
															$ctdates = $countdates - $cq;
															if($ctdates == 0 || $submTot == 0){
																$avg = 0;
															}else{
																$avg = $submTot/$ctdates;	
															}
															
															if($rtarget > 0 && $submTot != 0){
																$rectarget = ($rtarget/$countdates)*$ctdates;
																$percentage = ($submTot/$rectarget) * 100;
															}else{
																$percentage =0;
															}
															$per = round($percentage, 0);
															
															$html.= '<td align="center">'.$submTot.'</td>';
															$html.= '<td align="center">'.round($avg, 2).'</td>';
															if($per >= '90'){
																$html.= '<td class="'.$class.'" style="background:#FFFF00" align="center">'.$per.'%</td>';
															}else{
																$html.= '<td class="'.$class.'" align="center">'.$per.'%</td>';
															}
															$html.= '</tr>';	
														}elseif($reqruiterId == $rec->recruiterId){
														
															$rsubmissionTargets = $rec->rsubmissionTargets;
															$rtarget = $rsubmissionTargets/5;
															$rtarget = $rtarget*$cdate;
															$html.= '<tr>';
															$html.= '<td>'.$rec->recruiterName.'</td>';
															$html.= '<td align="center">'.$rtarget.'</td>';
															
															if(isset($totArray[0]))
															$totArray[0] = $totArray[0] + $rec->rsubmissionTargets;
															else
															$totArray[0] = $rec->rsubmissionTargets;
															
															$submTot = 0;
															$j = 1;
															$cq = 0;
															foreach($dates as $d){
															
																if(isset($subcount[$rec->recruiterId][$d]))
																$subm = ($subcount[$rec->recruiterId][$d]);
																else
																$subm = 0;
																$query = mysql_query("select id from recruiters_attendance where rec='".$rec->recruiterName."' and date = '".$d."' and status = 1");
																$cquery = mysql_num_rows($query);
																if($cquery == 1){
																	$html.='<td align="center"> <strong>A</strong> </td>';
																	$cq1 = 1;
																}else{
																	$html.='<td align="center">'.$subm.'</td>';
																	$cq1 =0;
																}
																$submTot = $submTot + $subm;
																$cq = $cq + $cq1;
																if(isset($totArray[$j]))
																$totArray[$j] = $totArray[$j] + $subm;
																else						
																$totArray[$j] = $subm;
																
																$j++;
															}
															
															if(isset($totArray[$j]))
															$totArray[$j] = $totArray[$j] + $submTot;
															else
															$totArray[$j] = $submTot;
															$countdates = count($dates);
															$ctdates = $countdates - $cq;
															if($ctdates == 0 || $submTot == 0){
																$avg = 0;
															}else{
																$avg = $submTot/$ctdates;	
															}
															
															if($rtarget > 0 && $submTot != 0){
																$rectarget = ($rtarget/$countdates)*$ctdates;
																$percentage = ($submTot/$rectarget) * 100;
															}else{
																$percentage =0;
															}
															$per = round($percentage, 0);
															
															$html.= '<td align="center">'.$submTot.'</td>';
															$html.= '<td align="center">'.round($avg, 2).'</td>';
															if($per >= '90'){
																$html.= '<td class="'.$class.'" style="background:#FFFF00" align="center">'.$per.'%</td>';
															}else{
																$html.= '<td class="'.$class.'" align="center">'.$per.'%</td>';
															}
															$html.= '</tr>';
														}
													}
												}
											}
											
										}
										$html.= '<tr>';
										$totalsub = ($totArray[0]/5)*$cdate;
										
										$html.='
												<th class="table-header-repeat line-left minwidth-1" style="background-color:#666; color:#fff; border:#000 1px solid;" align="center">Total</th>
												<th class="table-header-repeat line-left minwidth-1" style="background-color:#666; color:#fff; border:#000 1px solid;" align="center">'.$totalsub.'</th>';
											$k = 1;
											foreach($dates as $d){												
												$html.='<th class="table-header-repeat line-left" style="background-color:#666; color:#fff; border:#000 1px solid;"align="center">'.$totArray[$k].'</th>';
												$k++;
											}
										if($totArray[$k] > 0){
											$totAvg = $totArray[$k]/count($dates);
										}else{
											$totAvg = 0;
										}
										
										if($totAvg > 0){
											$totPercentage = ($totAvg/$totalsub) * 100;
										}else{
											$totPercentage =0;
										}
										
										$totPer = round($totPercentage, 0);
										$totPer = $totPer*$cdate;			
										$html.='
										        <th class="table-header-repeat line-left minwidth-1" style="background-color:#666; color:#fff; border:#000 1px solid;" align="center">'.$totArray[$k].'</th>
										        <th class="table-header-repeat line-left minwidth-1" style="background-color:#666; color:#fff; border:#000 1px solid;" align="center">'.round($totAvg, 2).'</th>
												<th class="table-header-repeat line-left minwidth-1" style="background-color:#666; color:#fff; border:#000 1px solid;" align="center">'.$totPer.' % </th>				
										</tr>';
										$html.= '</table>';
		
						
		$this->load->library('Pdf');

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetDrawColor(255,255,255);

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
		
		// set header and footer fonts
		//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		
		// set default monospaced font
		//$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->SetFont('times', 'B', 10, '', 'false');
		
		$pdf->AddPage();
		$pdf->SetDrawColor(255,0,0);
		$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
		
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
			
			
			
		$pdf->Output('reqruiter.pdf', 'I');
		
	}
}

?>
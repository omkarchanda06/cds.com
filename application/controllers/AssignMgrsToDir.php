<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(0);
class AssignMgrsToDir extends CI_Controller {

  function __construct() {
    parent::__construct();
	$this->load->model('assignmanagerstodirector','',TRUE);
	$this->load->library('pagination');
	$this->load->helper('url');
	$this->load->library('session');
    $this->load->helper('form');
	session_start();
  }

  function index(){
    if($this->session->userdata('logged_in')){
        $session_data = $this->session->userdata('logged_in');
	  	//print_r($session_data);
        $usertype = $session_data['usertype'];
        if ($usertype == "SUPERADMIN") {
            $directors_list = $this->assignmanagerstodirector->get_all_directors();
            $managers_list = $this->assignmanagerstodirector->get_all_managers();
            //print_r($directors_list);
            $data['directorslist'] = $directors_list;
            $data['managerslist'] = $managers_list;
            $this->load->view('assign-mgr-to-director-view', $data);
        } else {
            redirect('dashboard', 'refresh');
        }
        
    } else {
        redirect('dashboard', 'refresh');
    }
  }

  function get_selected_mgrs_dir_ids(){
    $director_id = $this->input->post('directorslist');
    $manager_ids = $this->input->post('managerslist');

    foreach ($manager_ids as $manager_id) {
        $insert_data = array(
            'director_id' => $director_id,
            'manager_id'  => $manager_id,
            'isActive'    => 1
        );
        $data['message'] = $this->assignmanagerstodirector->insert_assigned_mgrs_to_dir($insert_data);
    }
    $this->session->set_flashdata('message', $data['message']);
    redirect('assignmgrstodir', 'refresh');
  }

    function view(){
        $session_data = $this->session->userdata('logged_in');
        $usertype = $session_data['usertype'];
        $user_id = $session_data['id'];
        if($this->session->userdata('logged_in')){
            if ($usertype == "SUPERADMIN") {
                $view_all = $this->assignmanagerstodirector->view_all();
                $data['viewall'] = $view_all;
                $this->load->view('view-assigned-mgrs-to-dir', $data);
            }else{
                $view_all = $this->assignmanagerstodirector->get_all_managersByDirectorID($user_id);
                $data['viewall'] = $view_all;
                $this->load->view('view-assigned-mgrs-to-dir', $data);
            }
        }else {
            redirect('dashboard', 'refresh');
        }
    }

    function edit($id){
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $usertype = $session_data['usertype'];
            if ($usertype == "SUPERADMIN") {
                $directors_list = $this->assignmanagerstodirector->get_all_directors();
                $managers_list = $this->assignmanagerstodirector->get_all_managers();
                $data['directorslist'] = $directors_list;
                $data['managerslist'] = $managers_list;
                $data['directorID'] = $id;
                $this->load->view('edit-assigned-mgrs-to-dir-view', $data);
            } else {
                $directors_list = $this->assignmanagerstodirector->get_all_directors();
                $managers_list = $this->assignmanagerstodirector->get_all_managers();
                $data['directorslist'] = $directors_list;
                $data['managerslist'] = $managers_list;
                $data['directorID'] = $id;
                $this->load->view('edit-assigned-mgrs-to-dir-view', $data);
            }
        } else {
            redirect('dashboard/changepwd', 'refresh');
        }
    }

    function update(){
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $usertype = $session_data['usertype'];
            if ($usertype == "SUPERADMIN") {
                $director_id = $this->input->post('directorid');
                $manager_ids = $this->input->post('managerslist');
                //print_r($manager_ids);echo "<br/>";
                $manager_list_byDir_id = $this->assignmanagerstodirector->get_all_managersByDirectorID($director_id);
                foreach ($manager_list_byDir_id as $value) {
                    if ($value->isActive != 0) {
                        $manager_list_db[] = $value->manager_id;
                    }
                }
                //print_r($manager_list_db);echo "<br/>";
                $clean1 = array_diff($manager_list_db, $manager_ids); 
                $clean2 = array_diff($manager_ids, $manager_list_db);
                $final_output = array_merge($clean1, $clean2);
                //print_r($final_output);exit();

                if (!empty($final_output)) {
                    foreach ($final_output as $mgr_id) {
                        $mgrID_status = $this->assignmanagerstodirector->find_mgrData_by_dirid($mgr_id, $director_id);
                        if (!empty($mgrID_status)) {
                            //print_r($mgrID_status);
                            if ($mgrID_status->isActive == 0) {
                                $active_status = 1;
                                $data['message'] = $this->assignmanagerstodirector->update_isActive($mgr_id, $director_id, $active_status);
                            } else {
                                $active_status = 0;
                                $data['message'] = $this->assignmanagerstodirector->update_isActive($mgr_id, $director_id, $active_status);
                            }
                        } else {
                            $insert_data = array(
                                'director_id' => $director_id,
                                'manager_id'  => $mgr_id,
                                'isActive'    => 1
                            );
                            $data['message'] = $this->assignmanagerstodirector->insert_assigned_mgrs_to_dir($insert_data);
                        }
                    }
                    $this->session->set_flashdata('message', $data['message']);
                    redirect('assignmgrstodir', 'refresh');
                }else{
                    echo "managers status are same";
                }
            }
        }else {
            redirect('dashboard', 'refresh');
        }
    }
}
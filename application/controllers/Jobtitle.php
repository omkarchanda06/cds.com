<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(1);
class JobTitle extends CI_Controller {

  function __construct() {
    parent::__construct();
	$this->load->model('user','',TRUE);

	//$this->user->filterSuperAdmin();
	
	$this->load->library('pagination');
	$this->load->library('form_validation');
	$this->load->helper('url');
	$this->load->library('session');
	session_start();
  }

  function index() {
    if($this->session->userdata('logged_in')) {
	  $session_data         = $this->session->userdata('logged_in');
      $data['username']     = $session_data['username'];
	//$data['requirements'] = $this->user->getAllRequirements();
	  $data['jobtitles'] 	= $this->user->getAllJobTitles($session_data['id']);
	  $data['menuname']     = 'jobtitle';
	  $this->load->view('jobtitles_view', $data);
	  }else{
      redirect('cdslogpage', 'refresh');
	}
  }

  function addjobtitle() {
      if($this->session->userdata('logged_in')) {
	  $session_data         = $this->session->userdata('logged_in');
      $data['username']     = $session_data['username'];
	  $data['menuname']     = 'jobtitle';
	  $this->load->view('add-jobtitle-view', $data);
	  } else {
      redirect('cdslogpage', 'refresh');
	}
  }

  function add() {

         $this->form_validation->set_rules('job_title','Job Title','trim|required');


         if($this->form_validation->run() == FALSE) {	
         	$data['message']	  = 'Please check for the errors';
            $session_data         = $this->session->userdata('logged_in');
	        $data['username']     = $session_data['username'];
		    $data['menuname']     = 'jobtitle';
		    $this->load->view('add-jobtitle-view', $data);
		 } else {
		 $session_data            = $this->session->userdata('logged_in');

         
         $job_title = trim($this->input->post('job_title'));
         $job_title = preg_replace('/\s+/', ' ',$job_title);

		 $row['title']        = $job_title;
		 $result 			  = $this->user->getJobTitleByTitle($job_title);	


		 if(sizeof($result) > 0) {
		 $data['message']	   	= 'Job Title already exist';	
		 } else  {
		 $data['message']	   = 'Job Title Added Successfully';
		 $this->user->insertJobTitle($row);
		 }


	 	 $session_data         = $this->session->userdata('logged_in');
         $data['username']     = $session_data['username'];
	     $data['menuname']     = 'jobtitle';
	     $this->load->view('add-jobtitle-view', $data);
	     }
	 }

	 function edit($id) {

  	 if($this->session->userdata('logged_in')) {
	  $session_data         = $this->session->userdata('logged_in');
      $data['username']     = $session_data['username'];
      $data['jobtitle']  	= $this->user->getJobTitleByID($id);
	  $data['menuname']     = 'jobtitle';
	  $this->load->view('edit-jobtitle-view', $data);
	  } else {
      redirect('cdslogpage', 'refresh');
	}

  }


  function update() {

         $this->form_validation->set_rules('job_title','Job Title','trim|required');
         
         $id = $this->input->post('id');

         if($this->form_validation->run() == FALSE) {	
         	$data['message']	  = 'Please check for the errors';
            $session_data         = $this->session->userdata('logged_in');
	        $data['username']     = $session_data['username'];
	        $data['jobtitle']     = $this->user->getJobTitleByID($id);
		    $data['menuname']     = 'jobtitle';
	        $this->load->view('edit-jobtitle-view', $data);
		 } else {

		 $session_data          = $this->session->userdata('logged_in');

		 $job_title 			= trim($this->input->post('job_title'));
         $job_title 			= preg_replace('/\s+/', ' ',$job_title);
         $row['title']        	= $job_title;


		 $result 				= $this->user->getJobTitleByTitle($job_title);	


		 if(sizeof($result) > 0) {
		 $data['message']	   	= 'Job Title already exist';	
		 } else  {
		 $data['message']	   	= 'Job Title Updated Successfully';
		 $this->user->updateJobTitle($id, $row);
		 }

		 
	 	 $session_data         	= $this->session->userdata('logged_in');
         $data['username']     	= $session_data['username'];
         $data['jobtitle']  	= $this->user->getJobTitleByID($id);
	     $data['menuname']     	= 'jobtitle';
	     $this->load->view('edit-jobtitle-view', $data);
	     }
	 }


	 function delete($id) {
	 	$this->user->deleteJobTitle($id);
	 	redirect('jobtitle');
	 }

}  
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
error_reporting(1);
class Recruitercount extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('user', '', TRUE);
		$this->load->model('submission', '', TRUE);
		$this->load->model('manager', '', TRUE);
		$this->load->model('recruiter', '', TRUE);
		$this->load->model('assignrecruiterstomgr', '', TRUE);
		$this->load->model('assignmanagerstodirector', '', TRUE);
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->library('session');
		session_start();
	}


	function index()
	{

		$session_data 		= $this->session->userdata('logged_in');
		$usertype 			= $session_data['usertype'];
		$directorId         = $this->input->post('directorid');
		$managerId 			= $this->input->post('managerid');
		$recid = $this->input->post('reqruiterid');

		$data['showBlank']	= false;

		if($managerId == "" && $session_data['usertype'] == 'SUPERADMIN'){
			$managerId 			= "All";
			$data['showBlank'] 	= true;
		} 
		// else {
		// 	$managerId = $session_data['id'];
		// }

		$data['usertype'] = $session_data['usertype'];

		$fromDate = $this->input->post('date1');
		if ($fromDate == "") {
			$fromDate = date('Y-m-j', strtotime('-1 Weekday'));
		}

		$toDate = $this->input->post('date2');
		if ($toDate == "") {
			$toDate = date('Y-m-j', strtotime('+3 Weekday'));
		}

		$usertype = $session_data['usertype'];

		if ($usertype == "SUPERADMIN") {
			if ($directorId == "All" && $managerId == "All" && $recid == "All") {
				$all_dirs = $this->user->get_directors();
				foreach ($all_dirs as $dir) {
					$managerslist = $this->assignmanagerstodirector->get_all_managersByDirectorID($dir->id);
					foreach ($managerslist as $manager) {
						$manager_details[] = $this->manager->getManagerByID($manager->manager_id);
						$submissions     = $this->submission->getAllSubmissionsOfRecruitersByManagerID($fromDate, $toDate, $manager->manager_id);
					}
					$merge_mgrs = array();
					foreach ($manager_details as $manager) {
						$merge_mgrs = array_merge($merge_mgrs , array_values($manager)) ;
					}
				}
				$data["managers"] = $merge_mgrs;
			}else{
				$managerslist = $this->assignmanagerstodirector->get_all_managersByDirectorID($directorId);
				foreach ($managerslist as $manager) {
					if ($managerId == $manager->manager_id) {
						$manager_details[] = $this->manager->getManagerByID($manager->manager_id);
						$submissions     = $this->submission->getAllSubmissionsOfRecruitersByManagerID($fromDate, $toDate, $manager->manager_id);
					}
				}
				$merge_mgrs = array();
				foreach ($manager_details as $manager) {
					$merge_mgrs = array_merge($merge_mgrs , array_values($manager)) ;
				}
				$data["managers"] = $merge_mgrs;
			}
			// $all_dirs = $this->user->get_directors();
			// $all_mgrs = $this->manager->getAllManagers();
			// $submissions 	= $this->submission->getAllSubmissionsOfRecruiters($fromDate, $toDate);
		} elseif ($usertype == "DIRECTOR" && !empty($managerId)){
			if ($managerId == "All") {
				$directorId = $session_data['id'];
				$managerslist = $this->assignmanagerstodirector->get_all_managersByDirectorID($directorId);
				foreach ($managerslist as $manager) {
					$manager_details[] = $this->manager->getManagerByID($manager->manager_id);
					$submissions     = $this->submission->getAllSubmissionsOfRecruitersByManagerID($fromDate, $toDate, $manager->manager_id);
				}
				$merge_mgrs = array();
				foreach ($manager_details as $manager) {
					$merge_mgrs = array_merge($merge_mgrs , array_values($manager)) ;
				}
				$data["managers"] = $merge_mgrs;
			}else{
				$submissions     = $this->submission->getAllSubmissionsOfRecruitersByManagerID($fromDate, $toDate, $managerId);
				$data['managers'] = $this->manager->getManagerByID($managerId);
			}
		}elseif ($usertype == "MANAGER") {
			$managerId      = $session_data['id'];
			$data['managerId'] = $managerId;
			$submissions     = $this->submission->getAllSubmissionsOfRecruitersByManagerID($fromDate, $toDate, $managerId);
		} else {
			$recruiter_id = $session_data['id'];
			//$recruiter_details = $this->recruiter->getRecruiterByName($recruiter_name);
			//$recruiter_id = $recruiter_details[0]->id;
			$submissions     = $this->submission->getAllSubmissionsOfRecruiter($fromDate, $toDate, $recruiter_id);
			// if ($managerId == '' || $managerId == 'All')
			// 	$submissions 	= $this->submission->getAllSubmissionsOfCategoryOfRecruiters($fromDate, $toDate, $session_data['category']);
			// else
			// 	$submissions 	= $this->submission->getAllSubmissionsOfRecruitersByManagerID($fromDate, $toDate, $managerId);
		}
		$data['dates'] 		= $this->submission->getDatesArray($fromDate, $toDate);
		$data['subcount'] 	= $this->submission->createSubmissionsInfoArrayForRecruiter($submissions);


		// Submission count based on the USER Category
		$usertype = $session_data['usertype'];

		if ($usertype == "SUPERADMIN") {
			$data["directors"] = $this->user->get_directors();
		} elseif ($usertype == "DIRECTOR") {
			$directorId = $session_data['id'];
			$data["managerslist"] = $this->assignmanagerstodirector->get_all_managersByDirectorID($directorId);
			
		} elseif ($usertype == "MANAGER") {
			$managerId = $session_data['id'];
			// $data["recruiterlist"] = $this->user->get_reqruiter($managerId);
			if ($managerId == '' || $managerId == 'All') {
				$data['managers'] = $this->manager->getManagersByCategory($session_data['category']);
			} else {
				$data['managers'] = $this->manager->getManagerByID($managerId);
			}
		} else {
			$data['recruiterID'] = $session_data['id'];
		}

		// if ($managerId == '' || $managerId == 'All')
		// 	$data['managersOptions'] 	= $this->manager->getAllManagers();
		// else
		// 	$data['managersOptions'] 	= $this->manager->getManagersByCategory($session_data['category']);


		$data['managerId'] 			= $managerId;
		$data['fromDate'] 			= $fromDate;
		$data['toDate'] 			= $toDate;
		$data['setCategoryID'] 		= $session_data['category'];
		// print_r($data);
		$this->load->view('recruitercount_view', $data);
	}




	function category($categoryName = 'panzer')
	{

		$session_data 		= $this->session->userdata('logged_in');
		$usertype 			= $session_data['usertype'];
		$data['category'] 	= strtoupper($categoryName);
		$managerId 			= $this->input->post('managerid');

		$data['showBlank']	= false;

		if ($managerId == "") {
			$managerId 			= "All";
			$data['showBlank'] 	= true;
		}
		if ($usertype == "MANAGER") {
			$managerId = 	$session_data['id'];
		}


		$fromDate = $this->input->post('date1');
		if ($fromDate == "") {
			$fromDate = date('Y-m-j', strtotime('-1 Weekday'));
		}

		$toDate = $this->input->post('date2');
		if ($toDate == "") {
			$toDate = date('Y-m-j', strtotime('+3 Weekday'));
		}

		$setCategoryID = 1;
		if ($categoryName == "rpo") {
			$setCategoryID = 2;
		} else if ($categoryName == "canada") {
			$setCategoryID = 3;
		}

		if ($managerId == '' || $managerId == 'All')
			$submissions 	= $this->submission->getAllSubmissionsOfCategoryOfRecruiters($fromDate, $toDate, $setCategoryID);
		else
			$submissions 	= $this->submission->getAllSubmissionsOfRecruitersByManagerID($fromDate, $toDate, $managerId);


		$data['dates'] 		= $this->submission->getDatesArray($fromDate, $toDate);
		$data['subcount'] 	= $this->submission->createSubmissionsInfoArrayForRecruiter($submissions);;


		if ($managerId == '' || $managerId == 'All')
			$data['managers'] = $this->manager->getManagersByCategory($setCategoryID);
		else
			$data['managers'] = $this->manager->getManagerByID($managerId);


		if ($managerId == '' || $managerId == 'All')
			$data['managersOptions'] 	= $this->manager->getAllManagers();
		else
			$data['managersOptions'] 	= $this->manager->getManagersByCategory($setCategoryID);

		$data['setCategoryID'] 		= $setCategoryID;
		$data['managerId'] 			= $managerId;
		$data['fromDate'] 			= $fromDate;
		$data['toDate'] 			= $toDate;
		$data['menuname'] 			= 'submissionscount';
		$this->load->view('recruitercount_view', $data);
	}

	// updated by Rahamth for new
	// function requiterlist(){
	// 	$managerId 		= $this->input->post('mid');
	// 	$reqruiterlist 	= $this->user->get_reqruiter($managerId);
	// 	$html = "";
	// 	$html 		   .= '<option value="All">--All--</option>';
	// 	foreach($reqruiterlist as $rlist){
	// 		$reqruiterId 	= $rlist->recruiter_id;
	// 		$reqruiterName 	= $this->user->get_recruiter_from_id($rlist->recruiter_id);
	// 		$html          .='<option value="'.$reqruiterId.'">'.$reqruiterName.'</option>';
	// 	}
	// 	echo $html;
	// }

	// old logic
	function requiterlist()
	{
		$managerId 		= $this->input->post('mid');
		$reqruiterlist 	= $this->user->get_reqruiter($managerId);
		// print_r($reqruiterlist);exit;
		$html = "";
		$html 		   .= '<option value="All">--All--</option>';
		foreach ($reqruiterlist as $rlist) {
			$reqruiterId 	= $rlist->recruiter_id;
			$reqruiterName 	= $this->user->get_recruiter_from_id($rlist->recruiter_id);
			$html          .= '<option value="' . $reqruiterId . '">' . $reqruiterName . '</option>';
		}
		echo $html;
	}
}

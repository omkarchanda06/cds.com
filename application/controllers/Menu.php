<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class menu extends CI_Controller {

  function __construct()
  {
    parent::__construct();
	$this->load->helper('url');
	$this->load->library('session');
  $this->load->model('user','',TRUE);
  $this->load->model('menumodel');
  }

  function index(){
  //print_r($_POST);exit;
    $data['error']='';

    if($this->session->userdata('logged_in')){

      $getMenu['menu_items'] = $this->menumodel->getMenu();  

     $this->load->view('menu_view', $getMenu);
  }
}

  function logout()
  {
    $this->session->unset_userdata('logged_in');
    session_destroy();
    $this->load->view('cdslogpage_view');
  }

}

?>

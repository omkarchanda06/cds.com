const $ = document.querySelector.bind(document);

function TabNavigation() {
  const html = {
    links: [...$('.tab-links').children],
    contents: [...$('.tab-content').children],
    openTab: $('[data-default]'),
  }
  
  function hideAllTabContent(){
    html.contents.forEach(section => {
      section.style.display = "none";
    });
  }
  
  function removeAllActiveClas(){
    html.links.forEach(tab => {
      tab.className = tab.className.replace(' active', '');
    })
  }
  
  function showCurrentTab(id){    
    const tabcontent = $('#'+ id);
    tabcontent.style.display = 'block';
  }
  
  function selecttab(event) {
    hideAllTabContent();
    removeAllActiveClas();
    
    const target = event.currentTarget;
    showCurrentTab(target.dataset.id);
    
    target.className += ' active';
  }
  
  function listenForChange(){
    html.links.forEach(tab => {
      tab.addEventListener('click', selecttab);
    });
  }
  
  function init(){
    hideAllTabContent();
    listenForChange();
    
    html.openTab.click();
  }
  
  return {
    init
  }
}

window.addEventListener('load', () => {
  const tabNavigation = TabNavigation();
  tabNavigation.init();
})
// For time zone
// IST

var getIST = function () {
  document.getElementById("IST").innerHTML = new Date().toLocaleString(
    "en-US",
    {
      timeZone: "Asia/Kolkata",
      timeStyle: "short",
      hourCycle: "h12",
    }
  );
};
getIST();
setInterval(getIST, 1000);

// EST

var getEST = function () {
  document.getElementById("EST").innerHTML = new Date().toLocaleString(
    "en-US",
    {
      timeZone: "America/New_York",
      timeStyle: "short",
      hourCycle: "h12",
    }
  );
};
getEST();
setInterval(getEST, 1000);

// CST

var getCST = function () {
  document.getElementById("CST").innerHTML = new Date().toLocaleString(
    "en-US",
    {
      timeZone: "America/Guayaquil",
      timeStyle: "short",
      hourCycle: "h12",
    }
  );
};
getCST();
setInterval(getCST, 1000);

// PST

var getPST = function () {
  document.getElementById("PST").innerHTML = new Date().toLocaleString(
    "en-US",
    {
      timeZone: "America/Los_Angeles",
      timeStyle: "short",
      hourCycle: "h12",
    }
  );
};
getPST();
setInterval(getPST, 1000);


// script for progress bar
function progress() {
  const progressBar = document.querySelector("progress");
  const iter = (value = 0) => {
    if (value <= 80) {
      progressBar.setAttribute("value", value);
      setTimeout(() => iter(value + 1), 10);
    }
  };
  iter();
}
progress();
